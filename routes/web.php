<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/error/404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
Route::get('/error/500',['as'=>'500','uses'=>'ErrorHandlerController@errorCode500']);
Route::get('sportmonk/api/fetch/{token}', 'Admin\SportMonkFetcherController@getDataFromAPI');

Route::get('sportmonk/api/fetch_all', 'Admin\SportMonkFetcherController@getDataFromAPIAll');

Route::get('football-index/mb-player', 'Admin\MediaBuzzPlayerController@getMbPlayerData');

Route::get('football-index/mb-player-selected-date', 'Admin\MediaBuzzPlayerController@getMbPlayerDataSelectedDate');

Route::get('football-index/pb-player', 'PerformanceBuzzPlayerController@getPbPlayerData');

Route::get('get-pb-data', 'PerformanceBuzzPlayerController@getPBResponse');
Route::get('get-csv', 'PerformanceBuzzPlayerController@getCsvData');

Route::get('index', 'PlayerDataController@index');

Route::get('viewplayer', ['as' => 'viewplayer', 'uses' => 'PlayerDataController@viewplayer']);
Route::get('viewplayer/upgrade', ['as' => 'viewplayer.upgrade', 'uses' => 'PlayerDataController@upgrade']);

Auth::routes();

Route::group(['middleware' => 'admin_guest', 'prefix' => 'backoffice-fis', 'namespace' => 'Admin'], function() {

    Route::get('login', 'LoginController@showLoginForm');
    Route::post('login', 'LoginController@login');

    //Password reset routes
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm');
    Route::post('password/reset', 'ResetPasswordController@reset');
});

//   TEIR 1 ROUTES
Route::get('page/{slug}', ['as' => 'page', 'uses' => 'Admin\PagesController@show']);
##STORE CASHBACK EMAILS
// Route::post('cashback-offer', ['as' => 'cashback-offer-save.store', 'uses' => 'Admin\CashbackEmailController@store']);
//   TEIR 1 ROUTES

Route::get('contact-us', ['as' => 'getcontact', 'uses' => 'HomeController@getContact']);
Route::post('contact-us', ['as' => 'postcontact', 'uses' => 'HomeController@postContact']);
##ARTICLE ROUTES
    Route::get('articles', ['as' => 'articles', 'uses' => 'ArticlesController@index']);

    Route::get('all-articles', ['as' => 'all.articles', 'uses' => 'ArticlesController@loadAllArticles']);

    Route::get('articles/category/{slug}', ['as' => 'articles.category', 'uses' => 'ArticlesController@articlesByCategory']);

    Route::get('article/{slug}', ['as' => 'article.detail', 'uses' => 'ArticlesController@articleDetail']);
    Route::get('media-buzz/team', ['as' => 'media.buzz.team', 'uses' => 'Admin\MediaBuzzTeamController@mediaBuzzTeamIndex']);
Route::group(['middleware' => 'auth'], function() {

    

    ##MEDIABUZZ 
    Route::get('media-buzz/team', ['as' => 'media.buzz.team', 'uses' => 'Admin\MediaBuzzTeamController@mediaBuzzTeamIndex']);

    Route::get('media-buzz/team/data', ['as' => 'media.buzz.team.data', 'uses' => 'Admin\MediaBuzzTeamController@mediaBuzzTeamData']);

    Route::get('media-buzz/player', ['as' => 'media.buzz.player', 'uses' => 'Admin\MediaBuzzPlayerController@mediaBuzzPLayerIndex']);

    Route::get('media-buzz/player/data', ['as' => 'media.buzz.player.data', 'uses' => 'Admin\MediaBuzzPlayerController@mediaBuzzPLayerData']);

    Route::get('pb-planner/{date}', ['as' => 'pb.planner', 'uses' => 'Admin\FixturesController@getPbPlannerData']);
    Route::get('pb-planner-upgrade', ['as' => 'pb.planner.upgrade', 'uses' => 'Admin\FixturesController@getPbPlannerDataUpgrade']);

    Route::get('get_team_players', ['as' => 'get_team_players', 'uses' => 'Admin\FixturesController@getTeamPlayers']);

    Route::get('get_fixtures_list', ['as' => 'get_fixtures_list', 'uses' => 'Admin\FixturesController@getPbPlannerFixtures']);

    Route::get('myaccount', ['as' => 'myaccount', 'uses' => 'Admin\UserController@editAcc']);


    Route::post('myaccount', ['as' => 'myaccount', 'uses' => 'Admin\UserController@updateAcc']);

    ## YOUTUBE ROUTE
    Route::get('youtube', ['as' => 'youtube', 'uses' => 'Admin\PagesController@loadAllYoutubeVideos']);

    Route::get('database', ['as' => 'database', 'uses' => 'DatabaseController@index']);
    Route::get('database/upgrade', ['as' => 'database.upgrade', 'uses' => 'DatabaseController@getUpgrade']);
    Route::get('database/combined-data', ['as' => 'database.combined.data', 'uses' => 'DatabaseController@getCombinedData']);
    Route::get('database/media-data', ['as' => 'database.media.data', 'uses' => 'DatabaseController@getMediaBuzzData']);
    Route::get('database/performance-data', ['as' => 'database.performance.data', 'uses' => 'DatabaseController@getPerformanceBuzzData']);
    Route::get('database/team-data', ['as' => 'database.team.data', 'uses' => 'DatabaseController@getCombinedTeamData']);

    Route::get('performance-buzz-statistics', ['as' => 'performance.buzz.statistics', 'uses' => 'PerformanceBuzzStatisticsController@index']);
    Route::get('performance-buzz-statistics/upgrade', ['as' => 'performance.buzz.statistics.upgrade', 'uses' => 'PerformanceBuzzStatisticsController@getUpgrade']);

    Route::get('performance-buzz-stats-history', ['as' => 'performance.buzz.statshistory', 'uses' => 'PerformanceBuzzStatshistoryController@index']);
    Route::get('performance-buzz-stats-history/upgrade', ['as' => 'performance.buzz.statshistory.upgrade', 'uses' => 'PerformanceBuzzStatshistoryController@getUpgrade']);

    Route::get('performance-buzz/player', ['as' => 'performance.buzz.player', 'uses' => 'PerformanceBuzzPlayerController@index']);

    Route::get('performance-buzz/team', ['as' => 'performance.buzz.team', 'uses' => 'PerformanceBuzzPlayerController@pbTeamHistory']);

    Route::get('performance-buzz/stats', ['as' => 'performance.buzz.stats', 'uses' => 'PerformanceBuzzPlayerController@pbStatsHistory']);

    // TIER 3 UPGRADE ROUTES - STRIPE
    Route::get('upgrade-account', array('as' => 'stripe-payment-form', 'uses' => 'Admin\UserController@payWithStripe',));

    Route::post('upgrade-account', array('as' => 'upgrade-account', 'uses' => 'Admin\UserController@postPaymentWithStripe',));

    Route::get('updatepaymentmethod', array('as' => 'updatepaymentmethod', 'uses' => 'Admin\UserController@updatePayWithStripe',));

    Route::post('updatepaymentmethod', array('as' => 'updatepaymentmethod', 'uses' => 'Admin\UserController@postUpdatePaymentWithStripe',));

    Route::get('getfinalplayerfixturedata', ['as' => 'getfinalplayerfixturedata', 'uses' => 'PlayerDataController@getfinalplayerfixturedata']);

    Route::get('getfixturemonthforyear', ['as' => 'getfixturemonthforyear', 'uses' => 'PlayerDataController@getfixturemonthforyear']);

    Route::get('getfixtureoppositeteam', ['as' => 'getfixtureoppositeteam', 'uses' => 'PlayerDataController@getfixtureoppositeteam']);

    Route::get('getfinalplayerfixturescore', ['as' => 'getfinalplayerfixturescore', 'uses' => 'PlayerDataController@getfinalplayerfixturescore']);

    Route::get('performance-buzz/updatepositions', ['as' => 'performance.buzz.updatepositions', 'uses' => 'PerformanceBuzzPlayerController@updatepositions']);

    ## RESET PASSWORD ROUTES FOR USER
    Route::get('reset-password', ['as' => 'reset-password', 'uses' => 'Admin\UserController@resetPassword']);

    Route::post('reset-password', ['as' => 'reset-password', 'uses' => 'Admin\UserController@updatePassword']);

    ## cancel subcription
    Route::get('cancel-subcription', array('as' => 'cancel-subcription', 'uses' => 'Admin\UserController@cancelsubscriptionforalluser'));
    Route::get('cancel-premium-subcription', array('as' => 'cancel-premium-subcription', 'uses' => 'Admin\UserController@getCancelSubscription'));
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');
Route::get('activate-user/{token}', ['as' => 'activate-user', 'uses' => 'Auth\RegisterController@activateUserAccount']);
Route::get('sportmonk/players/resync', ['as' => 'sportmonk.fetch.api.players.resync', 'uses' => 'Admin\SportMonkFetcherController@getSportMonkApiUpdatePlayers']);
//Only logged in admin can access or send requests to these pages
Route::group(['middleware' => 'admin_auth', 'prefix' => 'backoffice-fis', 'namespace' => 'Admin'], function() {

    ##Logout
    Route::post('logout', 'LoginController@logout');

    ##Dashboard
    Route::get('/', 'DashboardController@index');

    ##Media Buzz Player History
    Route::resource('media-buzz-winner-player', 'MediaBuzzPlayerController', ['names' => [
            'index' => 'media-buzz-winner-player.index',
            'create' => 'media-buzz-winner-player.create',
            'store' => 'media-buzz-winner-player.store',
            'show' => 'media-buzz-winner-player.show',
            'edit' => 'media-buzz-winner-player.edit',
            'update' => 'media-buzz-winner-player.update',
            'destroy' => 'media-buzz-winner-player.destroy',
    ]]);

    ##Performance Buzz Player History
    Route::resource('performance-buzz-winner-player', 'PerformanceBuzzPlayerAdminController', ['names' => [
            'index' => 'performance-buzz-winner-player.index',
            'create' => 'performance-buzz-winner-player.create',
            'store' => 'performance-buzz-winner-player.store',
            'show' => 'performance-buzz-winner-player.show',
            'edit' => 'performance-buzz-winner-player.edit',
            'update' => 'performance-buzz-winner-player.update',
            'destroy' => 'performance-buzz-winner-player.destroy',
    ]]);

    ##Performance Buzz Team History
    Route::resource('performance-buzz-winner-team', 'PerformanceBuzzTeamAdminController', ['names' => [
            'index' => 'performance-buzz-winner-team.index',
            'create' => 'performance-buzz-winner-team.create',
            'store' => 'performance-buzz-winner-team.store',
            'show' => 'performance-buzz-winner-team.show',
            'edit' => 'performance-buzz-winner-team.edit',
            'update' => 'performance-buzz-winner-team.update',
            'destroy' => 'performance-buzz-winner-team.destroy',
    ]]);

    ##CMS Pages
    Route::resource('pages', 'PagesController', ['names' => [
            'index' => 'pages.index',
            'create' => 'pages.create',
            'store' => 'pages.store',
            'show' => 'pages.show',
            'edit' => 'pages.edit',
            'update' => 'pages.update',
            'destroy' => 'pages.destroy',
    ]]);

    ##Spreadsheets Pages
    Route::resource('spreadsheets', 'SpreadsheetsController', ['names' => [
            'index' => 'spreadsheets.index',
            'create' => 'spreadsheets.create',
            'store' => 'spreadsheets.store',
            'show' => 'spreadsheets.show',
            'edit' => 'spreadsheets.edit',
            'update' => 'spreadsheets.update',
            'destroy' => 'spreadsheets.destroy',
    ]]);

    ##Media Buzz Team History
    Route::resource('media-buzz-winner-team', 'MediaBuzzTeamController', ['names' => [
            'index' => 'media-buzz-winner-team.index',
            'create' => 'media-buzz-winner-team.create',
            'store' => 'media-buzz-winner-team.store',
            'show' => 'media-buzz-winner-team.show',
            'edit' => 'media-buzz-winner-team.edit',
            'update' => 'media-buzz-winner-team.update',
            'destroy' => 'media-buzz-winner-team.destroy',
    ]]);

    ## Manage Article Categories routes
    Route::resource('article-category', 'ArticleCategoryController', ['names' => [
            'index' => 'article-category.index',
            'create' => 'article-category.create',
            'store' => 'article-category.store',
            'edit' => 'article-category.edit',
            'update' => 'article-category.update',
            'destroy' => 'article-category.destroy',
    ]]);
    ## Manage Article routes
    Route::resource('articles', 'ArticlesController', ['names' => [
            'index' => 'articles.index',
            'create' => 'articles.create',
            'store' => 'articles.store',
            'edit' => 'articles.edit',
            'update' => 'articles.update',
            'destroy' => 'articles.destroy',
    ]]);

    ##Cashback Emails
    Route::resource('cashback-offer', 'CashbackEmailController', ['names' => [
            'index' => 'cashback-offer.index',
            'create' => 'cashback-offer.create',
            'store' => 'cashback-offer.store',
            'show' => 'cashback-offer.show',
            'edit' => 'cashback-offer.edit',
            'update' => 'cashback-offer.update',
            'destroy' => 'cashback-offer.destroy',
    ]]);

    ##Performance Buzz Full History
    Route::resource('performance-buzz', 'PerformanceBuzzController', ['names' => [
            'index' => 'performance-buzz.index',
            'create' => 'performance-buzz.create',
            'store' => 'performance-buzz.store',
            'show' => 'performance-buzz.show',
            'edit' => 'performance-buzz.edit',
            'update' => 'performance-buzz.update',
            'destroy' => 'performance-buzz.destroy',
    ]]);

    ##contcatus Emails
    Route::resource('contactus-emails', 'ContactUsController', ['names' => [
            'index' => 'contactus-emails.index',
            'destroy' => 'contactus-emails.destroy',
    ]]);

    Route::get('users', ['as' => 'users', 'uses' => 'UserController@index']);

    Route::delete('users/delete/{id}', ['as' => 'users.delete', 'uses' => 'UserController@deleteUser']);

    Route::get('users/edit/{id}', ['as' => 'users.edit', 'uses' => 'UserController@editUser']);

    Route::get('users/add', ['as' => 'users.add', 'uses' => 'UserController@addUser']);
    Route::post('users/store', ['as' => 'users.store', 'uses' => 'UserController@store']);

    Route::post('users/update/{id}', ['as' => 'users.update', 'uses' => 'UserController@updateUser']);

    Route::post('articles/import-articles', ['as' => 'articles.import_articles', 'uses' => 'ArticlesController@importArticles']);

    ## SEASONS & FIXTURES ROUTES
    Route::get('seasons', ['as' => 'seasons', 'uses' => 'SeasonsController@index']);

    Route::get('seasons/data', ['as' => 'seasons.data', 'uses' => 'SeasonsController@seasonsData']);

    Route::get('fixtures', ['as' => 'fixtures', 'uses' => 'FixturesController@index']);

    Route::get('fixtures/data', ['as' => 'fixtures.data', 'uses' => 'FixturesController@fixturesData']);

    Route::get('fixture/details/{id}', ['as' => 'fixture.details', 'uses' => 'FixturesController@fixturesDetails']);

    Route::get('fixture/edit/{id}', ['as' => 'fixture.edit', 'uses' => 'FixturesController@editFixture']);

    Route::post('fixture/update/{id}', ['as' => 'fixture.update', 'uses' => 'FixturesController@updateFixture']);

    Route::post('get_league_season', ['as' => 'get_league_season', 'uses' => 'FixturesController@getLeagueSeasons']);

    //PLAYERS ROUTES
    Route::get('players', ['as' => 'players', 'uses' => 'FixturesController@getPlayers']);

    Route::get('player/edit/{id}', ['as' => 'players.edit', 'uses' => 'FixturesController@editPlayer']);
    Route::post('player/update/{id}', ['as' => 'players.update', 'uses' => 'FixturesController@updatePlayer']);

    //TEAM ROUTES
    Route::get('teams', ['as' => 'teams', 'uses' => 'FixturesController@getTeams']);
    Route::get('teams/data', ['as' => 'teams.data', 'uses' => 'FixturesController@getTeamsData']);
    Route::get('team/edit/{id}', ['as' => 'teams.edit', 'uses' => 'FixturesController@editTeam']);
    Route::post('team/update/{id}', ['as' => 'teams.update', 'uses' => 'FixturesController@updateTeam']);


    Route::get('teams/{id}/players', ['as' => 'teams.players', 'uses' => 'PlayerController@index']);

    Route::get('teams/{id}/players/get', ['as' => 'teams.players.get', 'uses' => 'PlayerController@getPlayers']);

    Route::get('teams/{id}/players/create', ['as' => 'teams.players.create', 'uses' => 'PlayerController@create']);
    Route::post('teams/{id}/players/store', ['as' => 'teams.players.store', 'uses' => 'PlayerController@store']);

    Route::get('teams/{id}/players/edit/{playerid}', ['as' => 'teams.players.edit', 'uses' => 'PlayerController@edit']);
    Route::put('teams/{id}/players/edit/{playerid}', ['as' => 'teams.players.update', 'uses' => 'PlayerController@update']);

    // SPORTMONK API DATA 
    Route::get('sportmonk/fetch-data', ['as' => 'sportmonk.fetch.data', 'uses' => 'SportMonkFetcherController@index']);
    Route::post('sportmonk/fetch-data', ['as' => 'sportmonk.fetch.api.data', 'uses' => 'SportMonkFetcherController@getSportMonkApiData']);


    Route::get('setseasonstandingstatus', ['as' => 'setseasonstandingstatus', 'uses' => 'SeasonsController@setseasonstandingstatus']);
        Route::get('setstartdateseason', ['as' => 'setstartdateseason', 'uses' => 'SeasonsController@setstartdateseason']);
    Route::get('setenddateseason', ['as' => 'setenddateseason', 'uses' => 'SeasonsController@setenddateseason']);
    Route::get('setstartdatefixture', ['as' => 'setstartdatefixture', 'uses' => 'FixturesController@setstartdatefixture']);
    Route::get('setenddatefixture', ['as' => 'setenddatefixture', 'uses' => 'FixturesController@setenddatefixture']);


    //Day and Dividends
    Route::get('day_and_dividends_list', ['as' => 'day_and_dividends_list', 'uses' => 'SeasonsController@day_and_dividends_list']);
    Route::get('day_and_dividends_details/{id}', ['as' => 'day_and_dividends_details', 'uses' => 'SeasonsController@day_and_dividends_details']);
    Route::post('day_and_dividends_post/{id}', ['as' => 'day_and_dividends_post', 'uses' => 'SeasonsController@day_and_dividends_post']);
    Route::get('day_and_dividends/data', ['as' => 'day_and_dividends.data', 'uses' => 'SeasonsController@dayanddividendsData']);
    Route::get('calcalldividends', ['as' => 'calcalldividends', 'uses' => 'SeasonsController@calcalldividends']);
    Route::get('calcallteamsdividends', ['as' => 'calcallteamsdividends', 'uses' => 'SeasonsController@calcallteamsdividends']);
    Route::get('update-positions', ['as' => 'update-positions', 'uses' => 'SeasonsController@updatePbPositions']);
    Route::get('updatePlLeague', ['as' => 'updatePlLeague', 'uses' => 'SeasonsController@updatePlLeague']);
    Route::get('updateTmLeague', ['as' => 'updateTmLeague', 'uses' => 'SeasonsController@updateTmLeague']);
    

    //Upload Performance Buzz Data
    Route::get('performance_buzz_files_list', ['as' => 'performance_buzz_files_list', 'uses' => 'PerformanceBuzzPlayerAdminController@performance_buzz_files_list']);
    Route::get('performance_buzz_files_new', ['as' => 'performance_buzz_files_new', 'uses' => 'PerformanceBuzzPlayerAdminController@performance_buzz_files_new']);
    Route::post('performance_buzz_files_post', ['as' => 'performance_buzz_files_post', 'uses' => 'PerformanceBuzzPlayerAdminController@performance_buzz_files_post']);
    Route::get('performance_buzz_files_del', ['as' => 'performance_buzz_files_del', 'uses' => 'PerformanceBuzzPlayerAdminController@performance_buzz_files_del']);

    Route::get('performance_buzz_data_delete/{id}', ['as' => 'performance_buzz_data_delete', 'uses' => 'PerformanceBuzzController@performance_buzz_data_delete']);
    
    ## MANAGE SKYPE PAYMENT RATES ROUTE
    Route::get('settings', ['as' => 'settings', 'uses' => 'UserController@viewSettings']);
    Route::post('settings', ['as' => 'settings', 'uses' => 'UserController@updateSettings']);
    
    ## RESET PASSWORD ROUTES FOR ADMIN
    Route::get('change-password', ['as' => 'change-password', 'uses' => 'UserController@resetPasswordAdmin']);

    Route::post('change-password', ['as' => 'change-password', 'uses' => 'UserController@updatePasswordAdmin']);

    Route::get('/banner/', 'BannerController@index')->name('banner.index');
    Route::get('/banner/create', 'BannerController@create')->name('banner.create');
    Route::post('/banner/store/', 'BannerController@store')->name('banner.store');
    Route::get('/banner/edit/{id}', 'BannerController@edit')->name('banner.edit');
    Route::post('/banner/update', 'BannerController@update')->name('banner.update');
    Route::post('/banner/delete/{id}', 'BannerController@destroy')->name('banner.destroy');

    Route::get('/advertisement/', 'AdvertisementController@index')->name('advertisement.index');
    Route::get('/advertisement/create/{type}', 'AdvertisementController@create')->name('advertisement.create');
    Route::post('/advertisement/store/', 'AdvertisementController@store')->name('advertisement.store');
    Route::get('/advertisement/edit/{id}', 'AdvertisementController@edit')->name('advertisement.edit');
    Route::post('/advertisement/update', 'AdvertisementController@update')->name('advertisement.update');
    Route::post('/advertisement/delete/{id}', 'AdvertisementController@destroy')->name('advertisement.destroy');

});
Route::get('/sitemap.xml', 'Admin\PagesController@sitemap');

Route::get('/spreadsheets/{file_path}', 'HomeController@downloadSpreadsheet');