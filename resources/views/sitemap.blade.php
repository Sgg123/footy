<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> 
<url>
    <loc>https://www.footyindexscout.co.uk/</loc>
</url>
@foreach($data['page'] as $d)
<url>
    <loc>https://www.footyindexscout.co.uk/page/{{$d->slug}}</loc>
</url>
@endforeach
@foreach($data['article'] as $d)
<url>
    <loc>https://www.footyindexscout.co.uk/article/{{$d->slug}}</loc>
</url>
@endforeach
<url>
    <loc>https://www.footyindexscout.co.uk/media-buzz/player</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/media-buzz/team</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/performance-buzz/player</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/performance-buzz/team</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/performance-buzz-statistics</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/performance-buzz-stats-history</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/database</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/articles</loc>
</url>
</urlset>
