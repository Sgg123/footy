<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content=""/>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @yield('no_follow')

        <title>{{ config('app.name', 'FOOTY INDEX SCOUT') }} @yield('title')</title>
        <!-- StyleSheets -->
        <link rel="icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon" />
        <link rel="shortcut icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon" />
        <link rel="stylesheet" href="{{ asset('assets/front/css/bootstrap/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/front/css/main.css') }}"> 
        <link rel="stylesheet" href="{{ asset('assets/front/css/icomoon.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/front/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/front/css/transition.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/front/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/front/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/front/css/color.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/front/css/responsive.css') }}">
        <!-- FontsOnline -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800|Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <!-- JavaScripts -->
        <script src="{{ asset('assets/front/js/vendor/modernizr.js') }}"></script>
        <!-- Stripe JS -->
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

        <script>
            Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");
            window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
    'APP_URL' => url('/'),
]);
?>
        </script>      

        @guest
        <style>
            @media (max-width: 991px) {
                .header.style-3 .nav-list{ display: none;}
            }
        </style>
        @else
        <style>
            @media (max-width: 991px) {
                .nav-list{ display: none;}
                .header.style-3 .nav-list{ display: none;}
            }
        </style>
        @endguest 
        @yield('meta_desc')
        @yield('page_css')
    </head>
    <body>

        <!-- Wrapper -->
        <div class="wrap push">

            <!-- Header -->
            @include('front.common.header')
            <!-- Header -->
            @if(Route::getCurrentRoute()->uri() == '/')
            @include('front.common.home-slider')
            @endif
            <!-- Slider Holder -->

            <!-- Slider Holder -->

            <!-- Main Content -->
            <main class="main-content">
                @yield('content')
                @php
                    $leftBenners=Helper::benner('left');
                    $rightBenners=Helper::benner('right');
                @endphp
                @if(count($leftBenners) > 0)
                    <div class="leftbanner left-right-banner">
                        @foreach($leftBenners as $key=> $b)
                            <div class="bannerimg">
                                <a href="{{$b->link}}">
                                    <img @if($b->image != '' && file_exists(public_path('uploads/banner/'.$b->image)))
                                        src="{{ asset('uploads/banner/'.$b->image) }}"
                                        @else
                                        src="{{ asset('assets/images/no-image.jpeg') }}"
                                        @endif alt="" class="img-responsive">
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
                @if(count($rightBenners) > 0)
                    <div class="rightbanner left-right-banner">
                    <!-- <div class=""> -->
                        @foreach($rightBenners as $key=> $b)
                            <div class="bannerimg">
                                <a href="{{$b->link}}">
                                    <img @if($b->image != '' && file_exists(public_path('uploads/banner/'.$b->image)))
                                        src="{{ asset('uploads/banner/'.$b->image) }}"
                                        @else
                                        src="{{ asset('assets/images/no-image.jpeg') }}"
                                        @endif alt="" class="img-responsive">
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
            </main>
            <!-- Main Content -->

            <!-- Footer -->
            @include('front.common.footer')
            <!-- Footer -->

        </div>
        <!-- Wrapper -->

        <!-- Responsive Menu -->
        @guest
        @include('front.common.responsive-nav')
        @else
        @include('front.common.responsive-nav')
        @endguest
        <!-- Responsive Menu -->

        <!-- Java Script -->
        <script src="{{ asset('assets/front/js/vendor/jquery.js') }}"></script>        
        <script src="{{ asset('assets/front/js/vendor/bootstrap.min.js') }}"></script>  
        <script src="{{ asset('assets/front/js/bigslide.js') }}"></script>      
        <script src="{{ asset('assets/front/js/slick.js') }}"></script> 
        <!-- <script src="{{ asset('assets/front/js/waterwheelCarousel.js') }}"></script> -->
        <!-- <script src="{{ asset('assets/front/js/contact-form.js') }}"></script>  
        <script src="{{ asset('assets/front/js/countTo.js') }}"></script>        -->
        <script src="{{ asset('assets/front/js/datepicker.js') }}"></script>        
        <!-- <script src="{{ asset('assets/front/js/rating-star.js') }}"></script> -->                           
        <!-- <script src="{{ asset('assets/front/js/range-slider.js') }}"></script>              
        <script src="{{ asset('assets/front/js/spinner.js') }}"></script>            -->
        <script src="{{ asset('assets/front/js/parallax.js') }}"></script>               <!-- 
        <script src="{{ asset('assets/front/js/countdown.js') }}"></script>  -->
        <!-- <script src="{{ asset('assets/front/js/appear.js') }}"></script>                
        <script src="{{ asset('assets/front/js/prettyPhoto.js') }}"></script>   -->         

        <script src="{{ asset('assets/front/js/wow-min.js') }}"></script>                       
        <script src="{{ asset('assets/front/js/main.js') }}"></script> 

        <script>
            (function(i, s, o, g, r, a, m){i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function(){
            (i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date(); a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-92395841-1', 'auto');
            ga('send', 'pageview');</script>

        <!-- TradeDoubler site verification 2942702 -->

        <!-- Facebook Pixel Code -->
        <script>
            !function(f, b, e, v, n, t, s){if (f.fbq)return; n = f.fbq = function(){n.callMethod?
                    n.callMethod.apply(n, arguments):n.queue.push(arguments)}; if (!f._fbq)f._fbq = n;
            n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)}(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1066561756810184'); // Insert your pixel ID here.
            fbq('track', 'PageView');</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1066561756810184&ev=PageView&noscript=1"
                       /></noscript>
        <!-- DO NOT MODIFY -->
        <!-- End Facebook Pixel Code -->

        @yield('page_js')
        @yield('sidebar_js')
        <script type="text/javascript">
            $(document).ready(function() {
            $(document).on("contextmenu", function(){
            return false;
            });
            });</script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(document).bind('cut copy', function (e) {
                    e.preventDefault();
                });

                var totalwidth = $(window).width();
                var containerwidth = $('.main-content .theme-padding .container').width();
                if(containerwidth == null)
                {
                    containerwidth = $('.main-content .theme-padding-bottom .container').width();
                }
                var remainingwidth = totalwidth - containerwidth;
                var bannerwidth = remainingwidth / 2;
                $('.left-right-banner').width(bannerwidth - 10);
            });
        </script>

        @include('front.common.authentication-script')
    </body>
</html>