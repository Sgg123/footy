@extends('layouts.app')

@section('page_css')
@endsection

@section('content')
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>{{ @$page->title }}</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Pages</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('uploads/pages/'.@$page->image_path) }}">
</div>


<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                @include('front.pages.sidebar')
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-8">

                    <div class="team-detail-content theme-padding-bottom">
                        @if ($message = Session::get('success'))
                        <div class="custom-alerts alert alert-success fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            {!! $message !!}
                        </div>
                        <?php Session::forget('success'); ?>
                        @endif
                        @if ($message = Session::get('error'))
                        <div class="custom-alerts alert alert-danger fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            {!! $message !!}
                        </div>
                        <?php Session::forget('error'); ?>
                        @endif
                        @if(\Auth::user()->isPremium())
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Payment Method</div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="POST" id="payment-form" role="form" action="{!! route('updatepaymentmethod') !!}" >
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('card_no') ? ' has-error' : '' }}">
                                        <label for="card_no" class="col-md-4 control-label">Card No</label>
                                        <div class="col-md-6">
                                            <input id="card_no" type="text" class="form-control" name="card_no" value="{{ old('card_no') }}" autofocus>
                                            @if ($errors->has('card_no'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('card_no') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('ccExpiryMonth') ? ' has-error' : '' }}">
                                        <label for="ccExpiryMonth" class="col-md-4 control-label">Expiry Month</label>
                                        <div class="col-md-6">
                                            <input id="ccExpiryMonth" type="text" class="form-control" name="ccExpiryMonth" value="{{ old('ccExpiryMonth') }}" autofocus>
                                            @if ($errors->has('ccExpiryMonth'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ccExpiryMonth') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('ccExpiryYear') ? ' has-error' : '' }}">
                                        <label for="ccExpiryYear" class="col-md-4 control-label">Expiry Year</label>
                                        <div class="col-md-6">
                                            <input id="ccExpiryYear" type="text" class="form-control" name="ccExpiryYear" value="{{ old('ccExpiryYear') }}" autofocus>
                                            @if ($errors->has('ccExpiryYear'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ccExpiryYear') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('cvvNumber') ? ' has-error' : '' }}">
                                        <label for="cvvNumber" class="col-md-4 control-label">CVV No.</label>
                                        <div class="col-md-6">
                                            <input id="cvvNumber" type="text" class="form-control" name="cvvNumber" value="{{ old('cvvNumber') }}" autofocus>
                                            @if ($errors->has('cvvNumber'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('cvvNumber') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>                                    

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary red-btn">
                                                Update Payment Method
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> 
                        @endif
                    </div>
                </div>
                <!-- Match Result Contenet -->
            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
@endsection

@section('page_js')

@endsection