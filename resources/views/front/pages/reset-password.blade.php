@extends('layouts.app')

@section('page_css')
@endsection

@section('content')
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>{{ @$page->title }}</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Pages</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('uploads/pages/'.@$page->image_path) }}">
</div>


<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                @include('front.pages.sidebar')
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-8">

                    <div class="team-detail-content theme-padding-bottom">
                        <div class="panel panel-default">
                            @if ($message = Session::get('success'))
                            <div class="custom-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                {!! $message !!}
                            </div>
                            <?php Session::forget('success'); ?>
                            @endif
                            @if ($message = Session::get('error'))
                            <div class="custom-alerts alert alert-danger fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                {!! $message !!}
                            </div>
                            <?php Session::forget('error'); ?>
                            @endif                            
                            <div class="panel-heading">Reset Password</div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="POST" id="payment-form" role="form" action="{!! route('reset-password') !!}" >
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Old Password</label>
                                        <div class="col-md-6">
                                            <input id="old_password" type="password" class="form-control" name="old_password" autofocus>
                                            @if ($errors->has('old_password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('old_password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">New Password</label>
                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password" >
                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                    </div>                               

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary red-btn">
                                                Update Password
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> 
                    </div>
                </div>
                <!-- Match Result Contenet -->
            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
@endsection

@section('page_js')

@endsection
