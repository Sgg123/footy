@extends('layouts.app')
@section('title', '- Contact Us')
@section('meta_desc')
<meta name="keywords" content="{{ @$page->meta_keywords }}" />
<meta name="description" content="{{ @$page->meta_description }}" />
@endsection

@section('page_css')
@endsection

@section('content')

<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>{{ @$page->title }}</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Pages</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('uploads/pages/'.@$page->image_path) }}">
</div>

<!-- Main Content -->
<main class="main-content"> 
 @include ('alerts.alert')
    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
   
                <h1>{{ @$page->title }}</h1>
                {!! @$page->content !!}


<div class="container">
<div class="panel panel-primary">
  <div class="panel-body"> 
    <form method="POST" action="{{route('postcontact') }}" />
            {{ csrf_field() }}
               <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        <input class="form-control" type="text" name="name" placeholder="name">
                        {!! $errors->first('name', '<p class="alert alert-danger">:message</p>') !!}
                    </div>
                </div>
                 <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email:</strong>
                        <input class="form-control" type="text" name="email" placeholder="Email">
                        {!! $errors->first('email', '<p class="alert alert-danger">:message</p>') !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Message:</strong>
                        <textarea class="form-control" name="message" placeholder="Message" style="height:100px;"> 
                        </textarea>
                        {!! $errors->first('message', '<p class="alert alert-danger">:message</p>') !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <input class="btn btn-success" type="submit" name="save" id="success-btn">
                </div>
      </form>
    </div>
</div>
</div>
            
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
@endsection

@section('page_js')
<!--<script src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<script src="{{ asset('assets/front/js/gmap3.min.js') }}"></script>
<script src="{{ asset('assets/front/js/contact-form.js') }}"></script>
@endsection

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>