<script type="text/javascript">
    @if(Auth::user())
    @if(\Auth::user()->isPremium())
    $(document).on("click", ".view-player", function(){
        $id = $(this).attr("data-id");
        $teamid = $(this).attr("data-teamid");
        $("#player_details_popup").modal();
        $.ajax({
            url: "{{ route('viewplayer') }}?id=" + $id,
            success: function (response) {
                console.log(response);
                $("#playerdata").html(response);
                $('#id2').hide();
            }
        });
    });
    @else
    $(document).on("click", ".view-player", function(){
        window.location = "{{ route('viewplayer.upgrade') }}";
    });
    @endif
    @endif
    $(document).on("click", "#idlast", function(){
        $('#id1').show();
        $(this).addClass('pb_top');
        $('#idtop').removeClass('pb_top');
        $('#id2').hide();
    });
    $(document).on("click", "#idtop", function(){
        $('#id2').show();
        $(this).addClass('pb_top');
        $('#idlast').removeClass('pb_top');
        $('#id1').hide();
    });
    $(document).ready(function(){  
        $(document).on('click', '.show-filter', function(){
            $("#div1").fadeToggle("slow");  
        });
    });
    function getfixturemonthforyear(year){
        $.ajax({
            url: "{{ route('getfixturemonthforyear') }}?player_id=" + $id + "&year=" + year,
            success: function (response) {
                var data = JSON.parse(response);
                var html = '<option>Select</option>';
                // console.log(response);
                var arrayLength = data.length;
                for (var i = 0; i < arrayLength; i++) {
                    html+= '<option value="'+data[i]+'">'+getMonthName(data[i]-1)+'</option>';
                }
                $('#season_month').html(html);
            }
        });
    }
    function getfinalplayerfixturedata(year){
        $.ajax({
            url: "{{ route('getfinalplayerfixturedata') }}?player_id=" + $id + "&teamid=" + $teamid + "&year=" + year,
            success: function (response) {
                $('#player_fixture_score').html(response);
            }
        });
    }
    function getfixtureoppositeteam(month){
        var year = $('#season_year').val();
        $.ajax({
            url: "{{ route('getfixtureoppositeteam') }}?player_id="+$id+"&teamid=" + $teamid + "&year=" + year + "&month="+month,
            success: function (response) {
                var data = JSON.parse(response);
                var html = '<option>Select</option>';
                var arrayLength = data.length;
                for (var i = 0; i < arrayLength; i++) {
                    html+= '<option value="'+data[i].starting_at+'" data-id="'+data[i].teamid+'" data-date="'+data[i].starting_at+'">'+data[i].teamname+' ('+data[i].played_at+')</option>';
                }
                $('#season_fixture').html(html);
            }
        });
    }
    function getfinalplayerfixturescore(date){
        var year = $('#season_year1').val(), month = $('#season_month').val();
        $.ajax({
            url: "{{ route('getfinalplayerfixturescore') }}?player_id="+$id+"&date="+date,
            success: function (response) {
                $('#player_fixture_score').html(response.score).css('background', response.color);
                $('#player_fixture_score').css('color', response.fontcolor);
            }
        });
    }
    getMonthName = function (v) {
        var n = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return n[v]
    }
</script>