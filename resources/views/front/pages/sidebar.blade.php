<div class="col-lg-3 col-sm-3 col-xs-12 using-ex-d">

    <!-- Aside Widget -->
    <div class="aside-widget media-widget">
        @php 
            $data = Helper::getRandomPlayerData(); 
            $playerId = @$data['player_details']->id;
            $team_ids = @$data['player_details']->team_id;
        @endphp

       

        <div class="media-section">
         <div class="col-lg-7 col-xs-7 left-img">
                <img class="players-img view-player" data-id="{{ $playerId }}" data-teamid="{{ $team_ids }}"
                     @if(@$data['player_details']['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$data['player_details']['local_image_path'])))
                     src="{{ asset('uploads/player-images/'. @$data['player_details']['local_image_path']) }}"
                     @else
                     src ="{{ asset('assets/images/no-image.jpeg')}}" 
                     @endif
                     alt="">

            </div>
            @php
            $colorCode = "";
            $title ="";
            @endphp
            @if(@$data['player_details']['football_index_position'] == "Attacker" || @$data['player_details']['football_index_position'] == "Forward")

            @php
            $colorCode = "#C9007B";
            $title ="FWD";
            @endphp

            @elseif(@$data['player_details']['football_index_position'] == "Defender")

            @php
            $colorCode = "#00A185";
            $title ="DEF";
            @endphp

            @elseif(@$data['player_details']['football_index_position'] == "Midfielder")

            @php
            $colorCode = "#0070D1";
            $title ="MID";
            @endphp

            @elseif(@$data['player_details']['football_index_position'] == "Goalkeeper")

            @php
            $colorCode = "#540D6E";
            $title ="GK";
            @endphp

            @endif
            <div class="col-lg-5 col-xs-5 right-img">
                <img class="teams-img" 
                     @if(@$data['player_details']['is_local_image'] == 1 && file_exists(public_path('uploads/team-images/'.@$data['player_details']['team_local_logo'])))
                     src="{{ asset('uploads/team-images/'. @$data['player_details']['team_local_logo']) }}"
                     @else
                     src ="{{ asset('assets/images/no-image.jpeg')}}" 
                     @endif
                     alt="">
                     <span style="background:{{$colorCode}}">{{$title}} </span>  
            </div>          
       
        </div>

        <div class="player-title col-sm-12">
            <h2>{{@$data['player_details']['football_index_common_name']}}</h2>            
        </div>
        <div class="use-sep-v col-sm-12">
            @php
            $colorCode = "";
            $fontcolor ="";
            @endphp
            @if(@$data['player_details']->avgscore <= 30)
            @php
            $colorCode = "#FF0000";
            $fontcolor = "#FFFFFF";
            @endphp
            @elseif(@$data['player_details']->avgscore >= 31 && @$data['player_details']->avgscore <= 70)
            @php
            $colorCode = "#FF5733";
                $fontcolor = "#FFFFFF";
            @endphp
            @elseif(@$data['player_details']->avgscore >= 71 && @$data['player_details']->avgscore <= 110)
            @php
            $colorCode = "#eacd0e";
            $fontcolor = "#000";
            @endphp
            @elseif(@$data['player_details']->avgscore >= 111 && @$data['player_details']->avgscore <= 150)
            @php
            $colorCode = "#FFFF00";
            $fontcolor = "#000  ";
            @endphp
            @elseif(@$data['player_details']->avgscore >= 151 && @$data['player_details']->avgscore <= 180)
            @php
            $colorCode = "#aad77d";
            $fontcolor = "#FFFFFF";
            @endphp
            @elseif(@$data['player_details']->avgscore >= 180)
            @php
            $colorCode = "#39a02b";
            $fontcolor = "#FFFFFF";
            @endphp
            @endif
            <div class="col-lg-3 col-xs-3 player-no" style="color:{{ $fontcolor }}">
                <span style="background:{{$colorCode}}">{{ number_format($data['player_details']->avgscore ,0.5) }}</span>   
            </div>  
            <div class="col-lg-4 col-xs-4 player-rank">        
                <span>Rank {{ $data['playerdetail']}}<br>{{ $data['playersector'] }}</span>
            </div>
            <div class="col-lg-4 col-xs-4 player-overrank">            
                <span>Rank {{ $data['overallrankdetail'] }}<br>Overall</span> 
            </div>           
        </div>
        <div class="detail-of-player">
        @if(@$data['fixture_details'] != "")
            <span class="main-text">Next Fixture:<br></span>
            <span class="main-text">{{@$data['fixture_details']['oppTeamName']}}<br></span>
            @php
            $date1 = date_create(date('Y-m-d'));
            $date2 = date_create(date('Y-m-d',strtotime(@$data['fixture_details']['starting_at'])));

            $diff = date_diff($date1,$date2);
            @endphp
            <span class="main-text">{{date('M d',strtotime(@$data['fixture_details']['starting_at']))}}</span> ( 
            @if(@$diff->format("%a") == 0)
            Today            
            @else
            {{ $diff->format("%a") == 1 ? $diff->format("%a")  .' Day' : $diff->format("%a")  .' Days' }}
            @endif
            )
          @else
          <span class="main-text">Next Fixture:No Data<br></span>
        @endif
         </div>
    </div>
    <!-- Aside Widget -->

    <!-- Last Match -->
    @if(!empty(@$data['standings']))
    <div class="col-lg-12 col-md-12 col-sm-7 col-xs-6 r-full-width standings-table">
        <h3 class="m-b-set-v"><span>{{@$data['league_name']}}</span></h3>
        <div class="last-matches styel-1">

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Team</th>
                        <th>PL</th>
                        <th>GD</th>
                        <th>P</th>
                        <!--<th>D</th>-->
                    </tr>
                </thead>
                <tbody>
                    @if(!empty(@$data['standings']))

                    @for ($i = 0; $i <= 9; $i++)
                    @if(@$data['standings'][$i] != null)
                    <tr>
                        <td>{{@$data['standings'][$i]->position}}</td>
                        <td>{{@$data['standings'][$i]->team_name}}</td>
                        <td>{{@$data['standings'][$i]->overall->games_played}}</td>
                        <td>{{@$data['standings'][$i]->total->goal_difference}}</td>
                        <td>{{@$data['standings'][$i]->points}}</td>                    
                        <!--<td>{{@$standing->overall->draw}}</td>-->                    
                    </tr> 
                    @endif
                    @endfor

                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @endif
    <!-- Last Match -->


</div>
@section('sidebar_js')
@include('front.pages.popup')
@include('front.pages.player-popup-script')
@endsection