@extends('layouts.app')

@section('meta_desc')
<meta name="keywords" content="{{ @$page->meta_keywords }}" />
<meta name="description" content="{{ @$page->meta_description }}" />
@endsection

@section('page_css')
@endsection

@section('content')
<meta name="keywords" content="{{ @$page->meta_keywords }}" />
<meta name="description" content="{{ @$page->meta_description }}" />
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>{{ @$page->title }}</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Pages</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('uploads/pages/'.@$page->image_path) }}">
</div>


<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                @include('front.pages.sidebar')
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-12 sidebarres">

                    <div class="team-detail-content theme-padding-bottom">
                        <h1>{{ @$page->title }}</h1>
                        {!! @$page->content !!}
                    </div>
                    <div>
                        @include('front.pages.cashback-form')
                        {!! @$page->terms_conditions !!}
                    </div>
                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
@endsection

@section('page_js')
<script type="text/javascript" src="{{ asset('assets/front/js/embed.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery.validate.js')}}"></script> 
<script type="text/javascript">

    $("#cashback-form").validate({
        errorPlacement: function(error, element) {
                if (element.attr("name") == "terms") {
                    error.appendTo("#errorToShow");
                } else {
                    error.insertAfter(element);
                }
            },
        rules: {
            email: {
                required: true,
                email: true
            },
            source: {
            required: true,
        },
            terms: {required: true, },
        },
        messages: {
            email: {required: "Please provide email",
                email: "Please provide valid email"},
            source: "Please provide source",
            terms: "Please check the terms checkbox",
        },
        submitHandler: function (form) {
            var paypalEmail = $("#email").val();
            var source = $("#source").val();
            var referenceCode = $("#reference_code").val();
            $formData = new FormData();
            $formData.append('email', paypalEmail);
            $formData.append('source', source);
            $formData.append('reference_code', referenceCode);
            $formData.append('_token', window.Laravel.csrfToken);
            $URL = "{{ route('cashback-offer-save.store') }}";
            $.ajax({
                url: $URL,
                headers: {
                    'X-CSRF-TOKEN': window.Laravel.csrfToken
                },
                data: $formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                window.location.replace("http://clkuk.tradedoubler.com/click?p(277814)a(3050467)g(23716704)epi("+ paypalEmail +")");
                }
            });
        }
    });
</script>
@endsection
