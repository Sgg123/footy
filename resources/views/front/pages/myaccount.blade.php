@extends('layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>{{ @$page->title }}</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Pages</li>
        </ul>
    </div>
</div>

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('uploads/pages/'.@$page->image_path) }}">
</div>

<main class="main-content"> 
  <div class="theme-padding white-bg">
    <div class="container">
      <div class="row">
      @include('front.pages.sidebar')
      <div class="col-lg-9 col-sm-8">
        <div class="team-detail-content theme-padding-bottom">
          <div class="panel panel-default">
            <div class="panel-heading">My Account</div>
            <div class="panel-body">
              <form class="form-horizontal" method="POST" action="{{ route('myaccount') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">First Name</label>

                  <div class="col-md-6">
                      <input id="firstname" type="text" class="form-control" name="firstname" value="{{@$data->firstname}}" required autofocus>

                      @if ($errors->has('firstname'))
                      <span class="help-block">
                          <strong>{{ $errors->first('firstname') }}</strong>
                      </span>
                      @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">Last Name</label>

                  <div class="col-md-6">
                    <input id="lastname" type="text" class="form-control" name="lastname" value="{{@$data->lastname}}" required autofocus>

                    @if ($errors->has('lastname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lastname') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                  <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{@$data->email}}" required>

                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">Phone</label>

                  <div class="col-md-6">
                    <input id="phone" type="text" class="form-control" name="phone" value="{{@$data->phone}}" required>

                    @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                          Update
                      </button>
                  </div>
                </div>

              </form>           
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</main>
@endsection

@section('page_js')

@endsection
