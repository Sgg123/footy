@extends('layouts.app')
@section('no_follow')
<meta name="robots" content="noindex, nofollow">
@endsection
@section('content')


<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Performance Buzz Statistics</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Performance Buzz Statistics</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg') }}">
</div>

<!-- Main Content -->
<main class="main-content">

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                @include('front.pages.sidebar')
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-12 sidebarres">

                    <div class="row">
                        <div class="col-md-12 player-set-m">
                            <form action="" id="buzz_statistics_form">
                                <div class="form-group col-md-3">

                                    <input type="text" class="form-control" name="keyword" placeholder="Player name" value="{{ @Request::get('keyword') }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <!-- <input type="text" class="form-control" name="min_games" placeholder="Minimum number of games" value="{{ @$minGame }}"> -->
                                    <select class="form-control fg-py-drop" name="min_games">
                                        <option value="select">Minimum games</option>
                                        <option value="1" @if(@Request::get('min_games') == 1) selected @endif>1</option>
                                        <option value="2" @if(@Request::get('min_games') == 2) selected @endif>2</option>
                                        <option value="3" @if(@Request::get('min_games') == 3) selected @endif>3</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 py-drop">
                                    <select class="form-control fg-py-drop" name="last" onchange="return reset_dates()">
                                        <option value=''>Select Period</option>
                                        <option value="30" @if(@Request::get('last') == 30) selected @endif>Last 30 days</option>
                                        <option value="100" @if(@Request::get('last') == 100) selected @endif>Last 100 days</option>
                                        <option value="20152016" @if(@Request::get('last') == 20152016) selected @endif>2015/2016</option>
                                        <option value="20162017" @if(@Request::get('last') == 20162017) selected @endif>2016/2017</option>
                                        <option value="20172018" @if(@Request::get('last') == 20172018) selected @endif>2017/2018</option>
                                        <option value="20182019" @if(@Request::get('last') == 20182019) selected @endif>2018/2019</option>
                                        <option value="20192020" @if(@Request::get('last') == 20192020) selected @endif>2019/2020</option>
                                        <option value="20202021" @if(@Request::get('last') == 20202021) selected @endif>2020/2021</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 py-drop">
                                    <select class="form-control fg-py-drop" name="position">
                                        <option value="">Select Position</option>
                                        <option value="Forward" @if(@Request::get('position') == 'Forward') selected @endif>Forward</option>
                                        <option value="Midfielder" @if(@Request::get('position') == 'Midfielder') selected @endif>Midfielder</option>
                                        <option value="Defender" @if(@Request::get('position') == 'Defender') selected @endif>Defender</option>
                                        <option value="Goalkeeper" @if(@Request::get('position') == 'Goalkeeper') selected @endif>Goalkeeper</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="from" id="start_date" placeholder="From" value="{{ @$from }}" readonly>
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" id="end_date" name="to" placeholder="To" value="{{ @$to }}" readonly>
                                </div>
                                <div class="form-group col-md-4 py-drop">
                                    <select class="form-control fg-py-drop" name="league_id">
                                        <option value="">Select League</option>
                                        @foreach(@$leagues as $league)
                                        <option value="{{ $league->id }}" @if(@Request::get('league_id') == $league->id) selected @endif >{{ $league->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div style="display: none;">
                                    <select name="sort" id="buzz_statistics_sort">
                                        <option>select</option>
                                        <option>form_guide</option>
                                        <option>last_score</option>
                                        <option>lineups</option>
                                        <option>top_player_wins</option>
                                        <option>positional_wins</option>
                                        <option>player_name</option>
                                    </select>
                                    <select name="order" id="buzz_statistics_order">
                                        <option>select</option>
                                        <option>DESC</option>
                                        <option>ASC</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2 pull-right">
                                    <button type="submit" class="btn red-btn">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Piont Table -->
                    <div class="macth-fixture">

                        <div class="last-matches styel-3">

                            <!-- Table Style 3 -->
                            <div class="table-responsive padding-top-70">



                                <!-- <img src="{{ asset('assets/front/images/Performance-Stats-Table-Main-View-Top-10.jpg') }}" class="performance-statistics-img">      -->
                                <div class="ad-table table-responsive">
                                    <table class="us-table-v">
                                        <tr class="player-hd-b">
                                            <th class="r-set r-set-2">R</th>
                                            <th class="r-set"><a href="javascript:sort_tab('player_name')" style="color: #ffffff;">Player</a></th>
                                            <th><a href="javascript:sort_tab('form_guide')" style="color: #ffffff;">Form<br>Guide</a></th>
                                            <th><a href="javascript:sort_tab('last_score')" style="color: #ffffff;">last<br>Score</a></th>
                                            <th><a href="javascript:sort_tab('lineups')" style="color: #ffffff;">Games<br>Played</a></th>
                                            <th><a href="javascript:sort_tab('top_player_wins')" style="color: #ffffff;">Top Player<br>Wins</a></th>
                                            <th><a href="javascript:sort_tab('positional_wins')" style="color: #ffffff;">Positional<br>Wins</a></th>
                                            <th>Next Game <br>Versus</th>
                                            <th>Top 5 <br>Average</th>
                                            <th>Playing<br>Status</th>
                                        </tr>
                                        @php
                                        if(@Request::get('position') != ""){
                                        $ranks = \DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id as pid, sc.score FROM(SELECT sector ,player_id, avg(score) AS score FROM `performance_buzz` where sector="' . @Request::get('position') . '" GROUP BY player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                                        }else{
                                        $ranks = \DB::select('SELECT @rank := (@rank+1) AS rank, sc.player_id as pid, sc.score FROM(SELECT player_id, avg(score) AS score FROM `performance_buzz` GROUP BY player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                                        }

                                        @endphp
                                        @foreach(@$performanceBuzzStates as $states)

                                        @php
                                        $fixtureDetail = Helper::getUpcomingFixtureForTeam($states->team_id);

                                        $matchMonth = '';
                                        $matchDay = '';
                                        $teamImage = '';
                                        $teamType = '';
                                        $teamName = '';
                                        if(!empty($fixtureDetail)) {
                                        $teamType = $fixtureDetail['teamType'];
                                        $matchMonth = date('M', strtotime($fixtureDetail['starting_at']));
                                        $matchDay = date('d', strtotime($fixtureDetail['starting_at']));
                                        $teamName = $fixtureDetail['team']->name;
                                        $datediff = $fixtureDetail['datediff'];
                                        if($datediff==0){
                                        $team_title = $teamName.", today will be next game";
                                        }
                                        else{
                                        $team_title = $teamName.", ".$datediff." Day(s) till the next game";
                                        }
                                        if($fixtureDetail['team']->is_local_image == 1) {
                                        $teamImage = asset('uploads/team-images/'.$fixtureDetail['team']->local_image_path);
                                        }
                                        else {

                                        $teamImage = $fixtureDetail['team']->logo_path;
                                        }

                                        }

                                        $color = Helper::getColor($states->form_guide);
                                        @endphp

                                        <tr @if($loop->iteration % 2 == 0)  style="background:#e8e8e8;" @else style="background:#a4d6e494;" @endif >
                                             <td class="pd-set-one"><span>
                                                    @php
                                                    $playerRank = '';
                                                    foreach ($ranks as $value) {
                                                    if ($value->pid == @$states->player_id) {
                                                    $playerRank = $value->rank;
                                                    }
                                                    }
                                                    @endphp
                                                    {{@$playerRank}}
                                                </span></td>
                                            <td class="mb-player-details view-player" data-id="{{ @$states->player_id }}" data-teamid="{{ @$states->team_id }}">
                                                <div class="b-player-b">
                                                    <div class="img-player-v">
                                                        @if($states->is_local_image == 1)

                                                        <img src="{{ asset('uploads/player-images/'.@$states->local_image_path) }}" class="home-mb-team-img">

                                                        @else
                                                        <img src="{{ $states->image_path }}" class="home-mb-team-img">
                                                        @endif

                                                    </div>
                                                    <div class="player-name-c">
                                                        <h2>
                                                            @if($states->football_index_common_name != '')
                                                            {{ $states->football_index_common_name }}
                                                            @else
                                                            {{ $states->player_name }}

                                                            @endif
                                                        </h2><p>{{ $states->team }}
                                                            <span class="fd-pl">{{ $states->football_index_position }}</div>
                                                    </p>
                                                </div>
                                                </div>
                                            </td>
                                            <td class="pd-set-one">
                                                <span class="bg-using-py" style="background: {{ $color['colorCode'] }}; color: {{ $color['fontcolor'] }}" title="Average score from last 5 fixtures">
                                                    {{ $states->form_guide }}
                                                </span>
                                            </td>
                                            <td class="pd-set-one">
                                                <span>{{ $states->last_score }}</span>
                                            </td>
                                            <td class="pd-set-one">
                                                <span>{{ $states->lineups }}</span>
                                            </td>
                                            <td class="pd-set-one">
                                                <span>{{ @$states->top_player_wins }}</span>
                                            </td>
                                            <td class="pd-set-one">
                                                <span>{{ @$states->positional_wins }}</span>
                                            </td>
                                            <td class="three-set-b">
                                                @if(!empty($fixtureDetail))
                                                <div class="f-set-b"><img src="{{ @$teamImage }}" title="{{ $team_title }}">
                                                </div>
                                                <div class="hv-set">
                                                    <span>{{ @$teamType }}</span>
                                                </div>
                                                <div class="s-set-b">{{ $matchMonth }}<br>{{ $matchDay }}</div>
                                                <!--                                                    <div class="t-set-b">
                                                                                                       <div class="fdr-1">FDR<span>1</span>
                                                                                                       </div>
                                                                                                    </div>-->
                                                @endif
                                            </td>
                                            <td class="pd-set-one">
                                                <span>{{ @$states->getTop5Avg() }}</span>
                                            </td>
                                            <td class="last-l-pr">

                                                <div class="latest-l-img define-new-s">
                                                    @if($states->injured == "true")
                                                    <img src="{{ asset('assets/front/images/plus.png') }}">
                                                    @else
                                                    <img src="{{ asset('assets/front/images/minus.png') }}">
                                                    @endif
                                                </div>
                                            </td>

                                        </tr>

                                        @endforeach
                                    </table>
                                    <div class="mb-player-pagination">
                                        {{ @$performanceBuzzStates->appends(Request::capture()->except('page'))->links() }}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Piont Table -->

                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->

@endsection

@section("page_js")
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
                                        $(function () {
                                            $("#start_date").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                //maxDate: 0,
                                                onClose: function (selectedDate) {
                                                    $("#end_date").datepicker("option", "minDate", selectedDate);
                                                }
                                            });
                                            $("#end_date").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                //maxDate: 0,
                                                onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
                                                }
                                            });
                                        });
                                        function sort_tab(value) {
                                            var url_string = window.location.href; //window.location.href
                                            var url = new URL(url_string);
                                            var sort = url.searchParams.get("sort");
                                            var order = url.searchParams.get("order");
                                            if (sort == value) {
                                                if (order == 'DESC') {
                                                    order = 'ASC';
                                                } else {
                                                    order = 'DESC';
                                                }
                                            } else {
                                                order = 'DESC';
                                            }
                                            $('form#buzz_statistics_form')[0].reset();
                                            $('#buzz_statistics_sort').val(value);
                                            $('#buzz_statistics_order').val(order);
                                            $('form#buzz_statistics_form').submit();
                                        }
                                        function reset_dates() {
                                            $('#start_date').val('');
                                            $('#end_date').val('');
                                        }
</script>
@endsection
