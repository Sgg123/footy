
@extends('layouts.app')
@section('no_follow')
<meta name="robots" content="noindex, nofollow">
@endsection
@section('content')


<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Performance Buzz Winner Player History</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>>Performance Buzz Winner Player History</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg') }}">
</div>

<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">
                @if(\Auth::user()->isPremium())
                <!-- Search Form -->
                <div class="col-lg-9 col-sm-12">
                    <form name="performancebuzz_search_form" action="">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="player_name" placeholder="Player name" value="{{ @Request::get('player_name') }}">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="from" id="start_date" placeholder="From" value="{{ @Request::get('from') }}" readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" id="end_date" name="to" placeholder="To" value="{{ @Request::get('to') }}" readonly>
                        </div>
                        <div class="form-group col-md-3 pull-right search-new-add">
                            <button type="submit" class="btn red-btn">Search</button>
                            <a href="{{route('performance.buzz.player')}}" class="btn red-btn">Reset</a>
                        </div>
                    </form>
                </div>
                <!-- Search Form -->
                     @endif
                <!-- Match Result Contenet --> 
                <div class="col-lg-12 col-sm-12 p-d-v-set">

                    <!-- Piont Table -->
                    <div class="macth-fixture">

                        <div class="last-matches styel-3">
                            <!-- Table Style 3 -->
                            <div class="table-responsive padding-top-70">
                                <!--<img src="{{ asset('assets/front/images/PB-History-Table.jpg') }}" class="mb-player-history-img">-->
                                <table class="table table-bordered table-striped table-dark-header advance-v">
                                    <thead class="player_heading player_heading">
                                        <tr>
                                            <th class="date-v-d new-p-set">DATE</th>
                                            <th class="date-v-d table-th-set pg-width-set">PB TOP<br>PLAYER</th>
                                            <th class="date-v-d new-p-set" style="font-size: 11px !important;">SCORE</th>
                                            <th class="top-for pg-width-set">PB TOP<br>FORWARD</th>
                                            <th class="top-for new-p-set-2">
                                            <span class="sc-set">SCORE</span></th>
                                            <th class="mide-v pg-width-set">PB TOP<br>MIDFIELDER</th>
                                            <th class="mide-v sec-d-using"><span class="sc-set">SCORE</span></th>
                                            <th class="defender-v pg-width-set">PB TOP<br>DEFENDER</th>
                                            <th class="defender-v sec-d-using"><span class="sc-set">SCORE</span></th>
                                            <th class="defender-goal pg-width-set">PB TOP<br>GOALKEEPER</th>
                                            <th class="defender-goal sec-d-using"><span class="sc-set">SCORE</span></th>
                                        </tr>
                                    </thead>
                                    <tbody class="player_alldetails d-view-set">
                                        @if(!empty($PBWinners))
                                        @foreach($PBWinners as $history)                                        
                                        <tr>
                                            <td>{{date('d M',strtotime(@$history['date']))}}<br/>{{date('Y',strtotime(@$history['date']))}}</td>

                                            <!--TOP PLAYER DETAILS-->
                                            <td class="view-player" data-id="{{ @$history['topPlayerPlayerId'] }}" data-teamid="{{ @$history['topPlayerTeamId'] }}">
                                                <div class="col-md-4 mb-player-image">
                                                    @if(@$history['topPlayerLocalImagePath'] != "" || @$history['topPlayerImagePath'] != "")
                                                    <img 
                                                        @if(@$history['topPlayerIsLocalImage'] == 1 && @$history['topPlayerLocalImagePath'] != "")
                                                        src="{{ asset('uploads/player-images/'.@$history['topPlayerLocalImagePath'] ) }}"
                                                        @else
                                                        src="{{ @$history['topPlayerImagePath'] }}"
                                                        @endif
                                                        >
                                                        @endif
                                                </div>
                                                <div class="player_heading_title">
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            {{ @$history['topPlayerFCommonName'] != "" ? @$history['topPlayerFCommonName'] : @$history['topPlayerName'] }}
                                                        </div>
                                                        <div class="mb-player-team-name">{{ @$history['topPlayerTeamName'] }}</div>
                                                    </div> 
                                                </div> 
                                            </td>

                                            <td class="mb-player-score">{{ @$history['topPlayerScore'] }}</td>

                                            <!--TOP FORWARD DETAILS-->
                                            <td class="view-player" data-id="{{ @$history['forwardPlayerPlayerId'] }}" data-teamid="{{ @$history['forwardPlayerTeamId'] }}">
                                                <div class="col-md-4 mb-player-image">
                                                    @if(@$history['forwardLocalImagePath'] != "" || @$history['forwardImagePath'] != "")
                                                    <img 
                                                        @if(@$history['forwardIsLocalImage'] == 1 && @$history['forwardLocalImagePath'] != "")
                                                        src="{{ asset('uploads/player-images/'.@$history['forwardLocalImagePath'] ) }}"
                                                        @else
                                                        src="{{ @$history['forwardImagePath'] }}"
                                                        @endif
                                                        >
                                                        @endif
                                                </div>
                                                <div class="player_heading_title">
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            {{ @$history['forwardFCommonName'] != "" ? @$history['forwardFCommonName'] : @$history['forwardName'] }}
                                                        </div>
                                                        <div class="mb-player-team-name">{{ @$history['forwardTeamName'] }}</div>
                                                    </div> 
                                                </div> 
                                            </td>

                                            <td class="mb-player-score">{{ @$history['forwardScore'] }}</td>

                                            <!--TOP MIDFIELDER DETAILS-->
                                            <td class="view-player" data-id="{{ @$history['midfielderPlayerPlayerId'] }}" data-teamid="{{ @$history['midfielderPlayerTeamId'] }}">
                                                <div class="col-md-4 mb-player-image">
                                                    @if(@$history['midfielderLocalImagePath'] != "" || @$history['midfielderImagePath'] != "")
                                                    <img 
                                                        @if(@$history['midfielderIsLocalImage'] == 1 && @$history['midfielderLocalImagePath'] != "")
                                                        src="{{ asset('uploads/player-images/'.@$history['midfielderLocalImagePath'] ) }}"
                                                        @else
                                                        src="{{ @$history['midfielderImagePath'] }}"
                                                        @endif
                                                        >
                                                        @endif
                                                </div>
                                                <div class="player_heading_title">
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            {{ @$history['midfielderFCommonName'] != "" ? @$history['midfielderFCommonName'] : @$history['midfielderName'] }}
                                                        </div>
                                                        <div class="mb-player-team-name">{{ @$history['midfielderTeamName'] }}</div>
                                                    </div> 
                                                </div> 
                                            </td>

                                            <td class="mb-player-score">{{ @$history['midfielderScore'] }}</td>

                                            <!--TOP DEFENDER DETAILS-->
                                            <td class="view-player" data-id="{{ @$history['defenderPlayerPlayerId'] }}" data-teamid="{{ @$history['defenderPlayerTeamId'] }}">
                                                <div class="col-md-4 mb-player-image">
                                                    @if(@$history['defenderLocalImagePath'] != "" || @$history['defenderImagePath'] != "")
                                                    <img 
                                                        @if(@$history['defenderIsLocalImage'] == 1 && @$history['defenderLocalImagePath'] != "")
                                                        src="{{ asset('uploads/player-images/'.@$history['defenderLocalImagePath'] ) }}"
                                                        @else
                                                        src="{{ @$history['defenderImagePath'] }}"
                                                        @endif
                                                        >
                                                        @endif
                                                </div>
                                                <div class="player_heading_title">
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            {{ @$history['defenderFCommonName'] != "" ? @$history['defenderFCommonName'] : @$history['defenderName'] }}
                                                        </div>
                                                        <div class="mb-player-team-name">{{ @$history['defenderTeamName'] }}</div>
                                                    </div> 
                                                </div> 
                                            </td>

                                            <td class="mb-player-score">{{ @$history['defenderScore'] }}</td>

                                            <td class="view-player" data-id="{{ @$history['goalkeeperPlayerPlayerId'] }}" data-teamid="{{ @$history['goalkeeperPlayerTeamId'] }}">
                                                    <div class="col-md-4 mb-player-image">
                                                        @if(@$history['goalkeeperLocalImagePath'] != "" || @$history['goalkeeperImagePath'] != "")
                                                        <img 
                                                            @if(@$history['goalkeeperIsLocalImage'] == 1 && @$history['goalkeeperLocalImagePath'] != "")
                                                            src="{{ asset('uploads/player-images/'.@$history['goalkeeperLocalImagePath'] ) }}"
                                                            @else
                                                            src="{{ @$history['goalkeeperImagePath'] }}"
                                                            @endif
                                                            >
                                                            @endif
                                                    </div>
                                                    <div class="player_heading_title">
                                                        <div class="col-md-8 remove_player_space">
                                                            <div class="mb-player-name">
                                                                {{ @$history['goalkeeperFCommonName'] != "" ? @$history['goalkeeperFCommonName'] : @$history['goalkeeperName'] }}
                                                            </div>
                                                            <div class="mb-player-team-name">{{ @$history['goalkeeperTeamName'] }}</div>
                                                        </div> 
                                                    </div> 
                                                </td>


                                                <td class="mb-player-score">{{ @$history['goalkeeperScore'] }}</td>
                                        </tr>  
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="7">No data found</td>                                            
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                                <div class="col-xs-12">
                                    <div class="mb-player-pagination">
                                     
                @if (@Request::get('player_name') != '' || @Request::get('from') != '' || @Request::get('to') != '')
                {!! $links->appends(['player_name' => @Request::get('player_name'), 'to' => @Request::get('to'),'from' => @Request::get('from')])->links() !!}
                @else
                {!! $links->links() !!}
                @endif

                                    </div>                               
                                </div>
                                @if(!\Auth::user()->isPremium())
                                <div class="col-xs-12 text-center">
                                <br>
                                <table class="MsoTableLightShadingAccent1" style="border-collapse: collapse; border: medium none;display: inline-block;" cellspacing="0" cellpadding="0" border="1" align="center">
                                    <tbody>
                                        <tr style="mso-yfti-irow:12;height:45.5pt">
                                          <td style="width:155.95pt;border:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                                          background:#fff;mso-background-themecolor:accent1;mso-background-themetint:
                                          63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="208">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                          margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                          mso-yfti-cnfc:68" align="center"><b><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
                                          color:#365F91;mso-themecolor:accent1;mso-themeshade:191">100% Full Media and Performance Buzz Winners History</span></b></p>
                                          </td>
                                          <td style="width:111.7pt;border-left:none;
                                          border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                          mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                          mso-border-alt:solid windowtext .5pt;background:#fff;mso-background-themecolor:
                                          accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="149">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                          margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                          mso-yfti-cnfc:64" align="center"><b style="mso-bidi-font-weight:normal"><span style="font-size:20.0pt;mso-bidi-font-size:11.0pt;color:#365F91;mso-themecolor:
                                          accent1;mso-themeshade:191">PREMIUM</span></b></p>
                                          </td>
                                          <td style="width:200.3pt;border-left:none;
                                          border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                          mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                          mso-border-alt:solid windowtext .5pt;background:#fff;mso-background-themecolor:
                                          accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="267">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;margin-bottom:.0001pt;
                                          mso-add-space:auto;line-height:normal;mso-yfti-cnfc:64"><span style="color:#365F91;mso-themecolor:accent1;mso-themeshade:191">View the full and complete winners of both Media and Performance Buzz.</span></p>
                                          </td>
                                         </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="text-center">
                                    <a href="{{url('/upgrade-account')}}" class="text-uppercase btn btn-primary">Click here to upgrade</a>
                                </div><br>
                        <div class="text-center" style="font-weight: bold;font-size: 13pt;">
                         PREMIUM MEMBERSHIP ONLY £5 A MONTH. THE FIRST 30 DAYS ARE FREE!
                        </div>
                                <br>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Piont Table -->

                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
@endsection
@include('front.pages.popup')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@section('page_js')
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>
@include('front.pages.player-popup-script')
@endsection