@extends('layouts.app')
@section('no_follow')
<meta name="robots" content="noindex, nofollow">
@endsection
@section('content')
<?php $data = json_decode(json_encode($data), True); ?>
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Performance Buzz Scores History</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Performance Buzz Scores History</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg') }}">
</div>

<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">
                <!-- Aside -->
                @include('front.pages.sidebar')
                <!-- Aside -->
                @if(\Auth::user()->isPremium())
                <!-- Search Form -->
              <!--   <div class="col-lg-9 col-sm-12 sidebarres">
                    <form name="performancebuzz_search_form" action="">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="team_name" placeholder="Team name" value="{{ @Request::get('team_name') }}">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="from" id="start_date" placeholder="From" value="{{ @Request::get('from') }}" readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" id="end_date" name="to" placeholder="To" value="{{ @Request::get('to') }}" readonly>
                        </div>
                        <div class="form-group col-md-3 pull-right search-new-add">
                            <button type="submit" class="btn red-btn">Search</button>
                            <a href="{{route('performance.buzz.team')}}" class="btn red-btn">Reset</a>
                        </div>
                    </form>
                </div> -->
                <!-- Search Form -->
                @endif

                <!-- Match Result Contenet --> 
                <div class="col-lg-9 col-sm-12 sidebarres">
            
                    <!-- Piont Table -->
                    <div class="macth-fixture">
                        <div class="last-matches styel-3">
                            <!-- Table Style 3 -->
                            <div class="table-responsive padding-top-70">
                                <div class="col-md-12" style="padding-left: 0; padding-right: 0;">
                                    <div class="col-md-5 col-left">
                                      <div class="dt_slc"><form method="get" action="{{route('performance.buzz.statshistory')}}" id="dateform"> <input type="text" class="form-control" name="from" id="select_date" placeholder="Select Date" value="" readonly=""></form> </div>
                                      <div class="mbt">
                                        <div class="ttl_mbt">Media Buzz Top 3</div>
                                        <?php $i = 0;
                                        if(isset($data['mdrank']['allover'])){
                                      foreach($data['mdrank']['allover'] as $mdrank){ 
                                      $i++; ?>
                                        <div class="pl_row">
                                          <div class="pl_col pl_img"><img  @if(@$mdrank['local_image'] == 0)
      src="{{ @$mdrank['image'] }}"
      @elseif(@$mdrank['local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$mdrank['local_image_path'])))
      src="{{ asset('uploads/player-images/'.@$mdrank['local_image_path']) }}"
      @else
      src ="{{ asset('assets/images/no-image.jpeg')}}" 
      @endif></div>
                                          <div class="pl_col pl_name"><?php echo $mdrank['name']; ?></div>
                                          <div class="pl_col pl_scr"><?php echo round($mdrank['score']); ?></div>
                                          <div class="clearfix"></div>
                                        </div>
                                      <?php   if($i > 2){
                                          break;
                                        }
                                        } 
                                        } ?>
                                       
                                      </div>

                                      <div class="search_ply"> <input type="text" class="form-control" onkeyup="myFunction()" name="search" id="search_player" placeholder="Seach Player" value=""> </div>
                                      <div class="search_player">
                                        <table class="search_player" id="player_table" style="width: 100%">
                                            <thead class="search-head">
                                              <th class="sp_col player_num">R</th>
                                              <th class="sp_col player_name">Player</th>
                                              <th class="sp_col players_scr">Score</th>
                                            </thead>
                                            <tbody >
                                            <?php if(isset($data['pbrank']['allover'])){
                                              foreach($data['pbrank']['allover'] as $player){ ?>
                                              <tr class="search-row"><td class="sp_col player_num"><?php echo $player['rank']; ?></td>
                                              <td class="sp_col player_name"><?php echo $player['name']; ?></td>
                                              <td class="sp_col players_scr" ><?php echo round($player['score']); ?></td></tr>
                                            <?php } } else { ?>
                                                <tr class="search-row"><td colspan="3" style="background-color: #F3F3F3;overflow-y: hidden;"><?php echo "No Data available in table."; ?></td>
                                            <?php  }?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>
                                    <div class="col-md-7 col_right">
                                        <?php
                                            if(count($day_and_dividends) > 0){
                                                $position = ($day_and_dividends->top_positional_performance_player)*100; 
                                                $goalkeeper = ($day_and_dividends->goalkeeper)*100; 
                                                $topPlayer = ($day_and_dividends->top_performance_player)*100;
                                                if($day_and_dividends->type_of_day == 1){
                                                    $colorCode= '#CF5A0A';
                                                }elseif($day_and_dividends->type_of_day == 2){
                                                    $colorCode = '#8C909C';
                                                }elseif($day_and_dividends->type_of_day == 3){
                                                    $colorCode = '#DDAB27';
                                                }elseif($day_and_dividends->type_of_day == 4){
                                                    $colorCode = '#439F05';
                                                }elseif($day_and_dividends->type_of_day == 5){
                                                    $colorCode = '#19ADF3';
                                                }else{
                                                  $colorCode = "#439F05";
                                                }
                                            }else{
                                                if ($data['upcomingMatches'][0]['totalmatch']>= 1 && $data['upcomingMatches'][0]['totalmatch'] <= 4){
                                                    $position = "2p"; 
                                                    $topPlayer = "2p";
                                                    $colorCode = "#CF5A0A";
                                                }elseif ($data['upcomingMatches'][0]['totalmatch'] >= 5 && $data['upcomingMatches'][0]['totalmatch'] <= 14){
                                                    $position = "4p"; 
                                                    $topPlayer = "4p";
                                                    $colorCode = "#8C909C";
                                                }elseif ($data['upcomingMatches'][0]['totalmatch'] >= 15){
                                                    $position = "8p"; 
                                                    $topPlayer = "8p";
                                                    $colorCode = "#DDAB27";
                                                }else{
                                                    $position = "0p"; 
                                                    $topPlayer = "0p"; 
                                                    $colorCode = "#439F05";                                     
                                                }
                                            }
                                       ?>
                                      <div class="cr_row1 text-center" style="background-color: <?php echo $colorCode; ?>">
                                        <div class="cr_row1_ttl">

                                        <?php   $today = date("j F Y",strtotime($data['upcomingMatches'][0]['date'])); 
                                          echo $today." ".strtoupper($data['upcomingMatches'][0]['title']);
                                         ?>
                                        </div>                     
                                      </div>
                                      <div class="cr_row2">
                                        <div class="perfrm-col dbl_fixtures"><?php echo($data['upcomingMatches'][0]['totalmatch']); ?> Fixtures</div>
                                        <div class="perfrm-col dbl_top_plyr">Top Player <span><?php echo($topPlayer); ?></span></div>
                                        <div class="perfrm-col dbl_positions">Positions <span><?php echo($position); ?></span></div>
                                        <div class="perfrm-col dbl_positions">GK <span><?php echo($goalkeeper); ?></span></div>
                                      </div>
          <?php if(strtoupper($data['upcomingMatches'][0]['title']) == strtoupper("Media Day") || strtoupper($data['upcomingMatches'][0]['title']) == strtoupper("Media Madness Top 5")): ?>

                                      <div class="cr_row3">
                                        <div class="perfrm-col lblue"><?php echo($data['upcomingMatches'][0]['defender']); ?> 1st</div>
                                        <div class="perfrm-col orange"><?php echo($data['upcomingMatches'][0]['midfielder']); ?> 2nd</div>
                                        <div class="perfrm-col dpink"><?php echo($data['upcomingMatches'][0]['attacker']); ?> 3rd</div>
                                        <div class="perfrm-col gk"><?php echo($data['upcomingMatches'][0]['goalkeeper']); ?> GK</div>
                                        <div class="clearfix"></div>
                                      </div>
          <?php else: ?>
          <div class="cr_row3">
                                        <div class="perfrm-col lblue"><?php echo($data['upcomingMatches'][0]['defender']); ?> def</div>
                                        <div class="perfrm-col orange"><?php echo($data['upcomingMatches'][0]['midfielder']); ?> mid</div>
                                        <div class="perfrm-col dpink"><?php echo($data['upcomingMatches'][0]['attacker']); ?> for</div>
                                        <div class="perfrm-col gk"><?php echo($data['upcomingMatches'][0]['goalkeeper']); ?> GK</div>
                                        <div class="clearfix"></div>
                                      </div>
          <?php endif; ?>
                                      <?php if(isset($data['pbrank']['forwd'][0])) {?>
                                      <div class="top_plbox">
                                        <div class="top_plcol pl_img"><img  @if(@$data['pbrank']['forwd'][0]['local_image'] == 0)
      src="{{ @$data['pbrank']['forwd'][0]['image'] }}"
      @elseif($data['pbrank']['forwd'][0]['local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$data['pbrank']['forwd'][0]['local_image_path'])))
      src="{{ asset('uploads/player-images/'.@$data['pbrank']['forwd'][0]['local_image_path']) }}"
      @else
      src ="{{ asset('assets/images/no-image.jpeg')}}" 
      @endif></div>
                                        <div class="top_plcol pl_play dpink">Top Forward</div>
                                        <div class="top_plcol pl_scr dpink"><?php echo round($data['pbrank']['forwd'][0]['score']);?></div>
                                        <div class="top_plcol pl_name"><?php echo $data['pbrank']['forwd'][0]['name'];?></div>
                                        <div class="clearfix"></div>
                                        <div class="topl-halfcol ply-wins">Top player wins <span><?php echo $data['pbrank']['forwd'][0]['top_win'];?></span></div>
                                        <div class="topl-halfcol ply-type-wins">Top fwd wins <span><?php echo $data['pbrank']['forwd'][0]['pos_win'];?></span></div>
                                        <div class="clearfix"></div>
                                      </div>
                                      <?php } ?>

                                      <?php if(isset($data['pbrank']['mid'][0])) {?>
                                      <div class="top_plbox">
                                        <div class="top_plcol pl_img"><img  @if(@$data['pbrank']['mid'][0]['local_image'] == 0)
      src="{{ @$data['pbrank']['mid'][0]['image'] }}"
      @elseif($data['pbrank']['mid'][0]['local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$data['pbrank']['mid'][0]['local_image_path'])))
      src="{{ asset('uploads/player-images/'.@$data['pbrank']['mid'][0]['local_image_path']) }}"
      @else
      src ="{{ asset('assets/images/no-image.jpeg')}}" 
      @endif></div>
                                        <div class="top_plcol pl_play orange">Top Midfielder</div>
                                        <div class="top_plcol pl_scr orange"><?php echo round($data['pbrank']['mid'][0]['score']);?></div>
                                        <div class="top_plcol pl_name"><?php echo $data['pbrank']['mid'][0]['name'];?></div>
                                        <div class="clearfix"></div>
                                        <div class="topl-halfcol ply-wins">Top player wins <span><?php echo $data['pbrank']['mid'][0]['top_win'];?></span></div>
                                        <div class="topl-halfcol ply-type-wins">Top mid wins <span><?php echo $data['pbrank']['mid'][0]['pos_win'];?></span></div>
                                        <div class="clearfix"></div>
                                      </div>
                                      <?php } ?>

                                      <?php if(isset($data['pbrank']['def'][0])) {?>
                                      <div class="top_plbox">
                                        <div class="top_plcol pl_img"><img  @if(@$data['pbrank']['def'][0]['local_image'] == 0)
      src="{{ @$data['pbrank']['def'][0]['image'] }}"
      @elseif($data['pbrank']['def'][0]['local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$data['pbrank']['def'][0]['local_image_path'])))
      src="{{ asset('uploads/player-images/'.@$data['pbrank']['def'][0]['local_image_path']) }}"
      @else
      src ="{{ asset('assets/images/no-image.jpeg')}}" 
      @endif></div>
                                        <div class="top_plcol pl_play lblue">Top Defender</div>
                                        <div class="top_plcol pl_scr lblue"><?php echo round($data['pbrank']['def'][0]['score']);?></div>
                                        <div class="top_plcol pl_name"><?php echo $data['pbrank']['def'][0]['name'];?></div>
                                        <div class="clearfix"></div>
                                        <div class="topl-halfcol ply-wins">Top player wins <span><?php echo $data['pbrank']['def'][0]['top_win'];?></span></div>
                                        <div class="topl-halfcol ply-type-wins">Top def wins <span><?php echo $data['pbrank']['def'][0]['pos_win'];?></span></div>
                                        <div class="clearfix"></div>
                                      </div>
                                      <?php } ?>

                                      <?php if(isset($data['pbrank']['gol'][0])) {?>
                                      <div class="top_plbox">
                                        <div class="top_plcol pl_img"><img  @if(@$data['pbrank']['gol'][0]['local_image'] == 0)
      src="{{ @$data['pbrank']['gol'][0]['image'] }}"
      @elseif($data['pbrank']['gol'][0]['local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$data['pbrank']['gol'][0]['local_image_path'])))
      src="{{ asset('uploads/player-images/'.@$data['pbrank']['gol'][0]['local_image_path']) }}"
      @else
      src ="{{ asset('assets/images/no-image.jpeg')}}" 
      @endif></div>
                                        <div class="top_plcol pl_play gk">Top GK</div>
                                        <div class="top_plcol pl_scr gk"><?php echo round($data['pbrank']['gol'][0]['score']);?></div>
                                        <div class="top_plcol pl_name"><?php echo $data['pbrank']['gol'][0]['name'];?></div>
                                        <div class="clearfix"></div>
                                        <div class="topl-halfcol ply-wins">Top player wins <span><?php echo $data['pbrank']['gol'][0]['top_win'];?></span></div>
                                        <div class="topl-halfcol ply-type-wins">Top GK wins <span><?php echo $data['pbrank']['gol'][0]['pos_win'];?></span></div>
                                        <div class="clearfix"></div>
                                      </div>
                                      <?php } ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Piont Table -->
                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->

@endsection
@section('page_js')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

   

    selectedindex();
});
$(function () {
  $("#start_date").datepicker({
      dateFormat: 'yy-mm-dd',
      maxDate: 0,
      onClose: function (selectedDate) {
          $("#end_date").datepicker("option", "minDate", selectedDate);
      }
  });
  $("#end_date").datepicker({
      dateFormat: 'yy-mm-dd',
      maxDate: 0,
      onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
      }
  });
});

$(function () {
  $("#select_date").datepicker({
    dateFormat: 'yy-mm-dd',
    maxDate: 0,
    onClose: function (selectedDate) {
    }
  });
});

function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("search_player");
  filter = input.value.toUpperCase();
  table = document.getElementById("player_table");
  tr = table.getElementsByTagName("tr");

  if(filter.length > 0){
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
         tr[i].style.backgroundColor = "";
       
      } else {
        //tr[i].style.display = "none";
       tr[i].style.backgroundColor = "#ffffff";
      }
    } 
    }
  }
    else
    {
      for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
     
      //tr[i].style.display = "";
       tr[i].style.backgroundColor = "#ffffff";
      }
    } 
    }
    }      
  function selectedindex() 
  {
    <?php if (isset($data['pbrank']['mid'][0])) {?>
    var midname = '<?php echo $data['pbrank']['mid'][0]['name'];?>';
    var forname = '<?php echo $data['pbrank']['forwd'][0]['name'];?>';
    var topname = '<?php echo $data['pbrank']['def'][0]['name'];?>';
    <?php } ?>
    if (typeof(midname) !== 'undefined') 
    {
    var input, filter, table, tr, td, i;
  input = document.getElementById("search_player");
  filter = input.value.toUpperCase();
  table = document.getElementById("player_table");
  tr = table.getElementsByTagName("tr");

    if(midname.length > 2){
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    td1 = tr[i].getElementsByTagName("td")[2];
    if (td) {
       if (td.innerHTML == midname) {
        tr[i].style.display = "";
        tr[i].style.backgroundColor = "#0070D1";
      } 
      else if (td.innerHTML == forname) 
      {
        tr[i].style.display = "";
        tr[i].style.backgroundColor = "#C9007B";
      }
      else if (td.innerHTML == topname)
      {
        tr[i].style.display = "";
        tr[i].style.backgroundColor = "#00A185";
      }
      else {
        tr[i].style.display = "";
      }
    }
    if (td1) 
    {
      if (td1.innerHTML <= 30) {
        td1.style.backgroundColor = "#FF0000";
      } 
      else if (td1.innerHTML  >= 31 && td1.innerHTML  <= 70 ) 
      {
        
        td1.style.backgroundColor = "#FF5733";
      }
      else if (td1.innerHTML  >= 71 && td1.innerHTML  <= 110 ) 
      {
        
        td1.style.backgroundColor = "#eacd0e";
      }
      else if (td1.innerHTML   >= 111 && td1.innerHTML <= 150)
      {
       
        td1.style.backgroundColor = "#FFFF00";
      }
      else if (td1.innerHTML   >= 151 && td1.innerHTML <= 180)
      {
        td1.style.backgroundColor = "#aad77d";
      }
      else {
       
        td1.style.backgroundColor = "#39a02b";
      }
    }
    }
  }
  }
}
</script>

<style type="text/css">
.ttl_mbt {
text-align: center;
font-size: 25px;
line-height: 50px;
background-color: #373f63;
color: #fff;
border: 2px solid #ddd;
text-transform: uppercase;
}
.pl_row:nth-of-type(odd) {
    background-color: #b1d1dc;
}
.pl_row:nth-of-type(even) {
    background-color: #81bbe1;
}
.pl_row {
border-left: 2px solid #ddd;
border-right: 2px solid #ddd;
border-bottom: 2px solid #ddd;
}
.pl_col.pl_img {
float: left;
width: 16%;
}
.col-md-5.col-left {
    padding-left: 0;
}
.pl_col.pl_name {
float: left;
width: 64%;
font-size: 20px;
line-height: 53px;
padding-left: 10px;
}
.pl_col.pl_scr {
float: left;
width: 20%;
font-size: 20px;
line-height: 53px;
border-left: 2px solid #ddd;
text-align: center;
}
.dt_slc input#select_date {
background-color: transparent;
border: 2px solid #000;
text-align: center;
font-size: 24px;
color: #000;
height: 50px;
line-height: 50px;
}
.dt_slc {
margin: 0 0 15px;
}

.search-head {
    background: #0071c1;
}
.sp_col { float: left; }
.sp_col.player_num {
    width: 18%;
}
.sp_col.player_name {
    width: 60%;
    padding-left: 5px;
}
.sp_col.players_scr {
    width: 22%;
}
.search-row .sp_col {
    line-height: 40px;
}
.sp_col.player_num {
    text-align: center;
}
.sp_col.players_scr {
    text-align: center;
}
.sp_col {
    border-left: 1px solid #000;
    border-bottom: 1px solid #000;
}
.search-head .sp_col {
    border-left: 0;
    line-height: 64px;
    color: #000;
    font-size: 23px;
    font-weight: bold;
    text-transform: uppercase;
    text-align: center;
}
.sp_col.players_scr {
    font-size: 16px;
    border-right: 1px solid #000;
}

.cr_row1 {
    background: #b6b7bb;
    line-height: 68px;
    padding: 0 10px;
    text-transform: uppercase;
    font-size: 22px;
    color: #000;
    letter-spacing: 0.6px;
}
.perfrm-col {
    width: 25%;
    float: left;
    line-height: 60px;
    height: 60px;
    border-bottom: 2px solid #ddd;
    border-left: 2px solid #ddd;
    font-size: 22px;
    text-transform: uppercase;
    padding: 0 5px;
    color: #000;
    text-align: center;
}
.perfrm-col.dbl_top_plyr, .perfrm-col.dbl_positions {
    font-size: 16px;
}
.perfrm-col.dbl_top_plyr span, .perfrm-col.dbl_positions span {
    font-size: 22px;
}
.perfrm-col:last-child {
    border-right: 2px solid #ddd;
}
.lblue { background: #00A185; color: #fff; }
.dpink { background: #C9007B; color: #fff; }
.orange { background: #0070D1; color: #fff; }
.gk { background: #540D6E; color: #fff; }
.top_plcol.pl_scr {
    text-align: center;
}
.top_plcol {
    float: left;
}
.top_plcol.pl_img {
    width: 25.5%;
    border-right: 1px solid #000;
}
.top_plcol.pl_play {
    width: 55%;
    border-right: 1px solid #000;
    text-transform: uppercase;
    padding: 0 10px;
}
.top_plcol {
    float: left;
    width: 19.5%;
    line-height: 58px;
    font-size: 22px;
}
.top_plcol.pl_name {
    width: 74.5%;
    padding: 0 10px;
}
.top_plbox {
    border: 1px solid #000;
    margin-top: 20px;
    color: #000;
}
.search_player {
    color: #000;
    max-height: 411px;
    overflow-y: scroll;
    width: 100%;
}
.search_ply {
    margin: 20px 0;
}
.sidebarres .search_ply input[type="text"] {
    background: none;
    border: 2px solid #000;
    color: #000;
    font-size: 20px !important;
    text-align: center;
    height: 50px;
    line-height: 50px;
}
input[type="text"]::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #000;
}
input[type="text"]::-moz-placeholder { /* Firefox 19+ */
  color: #000;
}
input[type="text"]:-ms-input-placeholder { /* IE 10+ */
  color: #000;
}
input[type="text"]:-moz-placeholder { /* Firefox 18- */
  color: #000;
}
.topl-halfcol {
    width: 50%;
    float: left;
    line-height: 50px;
    font-size: 18px;
    padding: 0 10px;
    text-transform: uppercase;
    background: #d6d7d9;
    border-top: 1px solid #000;
}
.topl-halfcol.ply-wins {
    border-right: 1px solid #000;
}
.topl-halfcol span {
    float: right;
}
.sidebarres .last-matches.styel-3 {
    border: 0;
}
.mbt {
    color: #000;
}

@media screen and (max-width: 1024px) and (min-width: 961px) {
  .pl_col.pl_name, .pl_col.pl_scr { line-height: 61px; }
}

@media screen and (max-width: 960px) and (min-width: 769px) {
  .pl_col.pl_name, .pl_col.pl_scr { line-height: 140px; }
  .col-md-7.col_right { margin-top: 20px; padding-right: 30px;}
}

@media screen and (max-width: 768px) and (min-width: 641px) {
  .pl_col.pl_name, .pl_col.pl_scr { line-height: 110px; }
  .col-md-7.col_right { margin-top: 20px; padding-right: 30px;}
}

@media screen and (max-width: 640px) and (min-width: 560px) {
  .pl_col.pl_name, .pl_col.pl_scr { line-height: 94px; }
  .col-md-7.col_right { margin-top: 20px;}
  .cr_row1 { line-height: 30px; padding: 15px; font-size: 20px;}
  .top_plcol { line-height: 51px;}
}

@media screen and (max-width: 559px) and (min-width: 480px) {
  .pl_col.pl_name, .pl_col.pl_scr { line-height: 68px; }
  .col-md-7.col_right { margin-top: 20px;}
  .cr_row1 { line-height: 30px; padding: 15px; font-size: 20px;}
  .top_plcol { line-height: 51px;}
}
@media screen and (max-width: 479px) and (min-width: 320px) {
  .pl_col.pl_name, .pl_col.pl_scr { line-height: 48px; }
  .col-md-7.col_right { margin-top: 20px;}
  .cr_row1 { line-height: 30px; padding: 15px; font-size: 22px;}
  .perfrm-col { font-size: 10px; line-height: 50px; height: 50px; }
  .perfrm-col.dbl_top_plyr, .perfrm-col.dbl_positions { font-size: 10px; }
  .top_plcol { line-height: 36px; font-size: 12px;}
  .topl-halfcol { font-size: 12px;}
  .col-md-7.col_right { padding: 0;}
  .top_plcol{ border-bottom: 0; }
  .perfrm-col.dbl_top_plyr span, .perfrm-col.dbl_positions span { font-size: 14px; }
}
</style>
<script type="text/javascript">

 $('#select_date').datepicker({
    onSelect: function(dateText, inst) {
      $('#dateform').submit();
    },
   dateFormat: 'yy-mm-dd'
});
</script>
@endsection
