@extends('layouts.app')

@section('content')

<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>{{ Request::route()->getName() == 'articles.category' ? @$categoryDetail->title : 'Articles' }}</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>

            @if(Request::route()->getName() == 'articles.category')
            <li><a href="{{ url()->route('articles') }}">Articles</a></li>
            <li>{{ @$categoryDetail->title }}</li>
            @else
            <li>Articles</li>
            @endif

        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->
<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg') }}">
</div>

<!-- Main Content -->
<main class="main-content">

    <!-- Blog -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Blog Content -->
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 r-full-width"> 
                    @if(@Request::has('keyword'))
                    <h2>Search results for: {{ @Request::get('keyword') }}</h2>
                    @endif
                    <!-- Blog Grid View -->
                    <div class="blog-grid-view">
                        <!-- <div class="row"> -->
                            @if(@$articles->count() > 0)
                            <div id="articles_list">
                                @foreach(@$articles as $article)
                                <!-- Post Img -->
                                <div class="upcoming-fixture row articles-fixture"> 
                                    <p class="article-date"><strong>Posted on {{date('d F Y',strtotime(@$article->date))}}</strong></p>
                                    <div class="col-lg-6 col-xs-12">

                                        <div class="news-post-holder">                                            
                                            <!-- Widget -->
                                            <div class="news-post-widget">
                                                <a href="{{route('article.detail',[@$article->slug])}}">
                                                    <img
                                                        @if (file_exists('uploads/articles/' . @$article->image_path) &&  @$article->image_path != '')
                                                        src="{{ asset('uploads/articles/'.@$article->image_path) }}"
                                                        @else
                                                        src="{{ asset('assets/images/no-image.jpeg') }}"
                                                        @endif  
                                                        alt="">  
                                                </a>
                                            </div>
                                            <!-- Widget -->                                                   

                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-xs-12">
                                        <div class="news-post-holder">

                                            <h2><a href="{{route('article.detail',[@$article->slug])}}">
                                                    @if(strlen(@$article->title) >= 40) 
                                                    {!! substr(@$article->title,0,39) !!}..
                                                    @else
                                                    {!!@$article->title!!}
                                                    @endif
                                                </a></h2>
                                            <div class="article-content">
                                                @php
                                                $text = strip_tags(@$article->content);
                                                $this->orginal_content_count = str_word_count($text);
                                                @endphp

                                                @if(strlen(@$text) >= 360) 
                                                {!! substr(@$text,0,359) !!}....<p class="article_read_more"><strong><a href="{{route('article.detail',[@$article->slug])}}">Read More</a></strong></p>
                                                @else
                                                {!!@$text!!}
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- Post Img -->
                                @endforeach
                            </div>
                            <a class="btn red-btn load_all_articles_btn" href="{{route('all.articles')}}">See More</a>
                            @else
                            <h4>Sorry no articles found..!</h4>
                            @endif

                        <!-- </div> -->
                    </div>
                    <!-- Blog Grid View -->

                </div>
                <!-- Blog Content -->

                <!-- Aside -->
                @include ('front.articles.sidebar')
                <!-- Aside -->

            </div>
        </div>
    </div>
    <!-- Blog -->

</main>

@endsection
@section('page_js')
<script type="text/javascript">

    $(document).on("click", ".articles_search", function () {
        $("#articles_search_form").submit();
    });
</script>
@endsection