<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 r-full-width articles-sidebar">

    <!-- Search Bar -->
    <div class="aside-search-bar">
        @php $route = ""; @endphp
        @if(@$articlePage == "all_article")
        @php $route = route('all.articles'); @endphp
        @else
        @php $route = route('articles'); @endphp
        @endif
        <form action="{{$route}}" method="GET" id="articles_search_form">
            <input class="form-control" name="keyword" type="text" placeholder="Search..." value="{{ @Request::get('keyword') }}">
            <button><i class="fa fa-search articles_search"></i></button>
        </form>
    </div>
    <!-- Search Bar -->

    <!-- Aside Widget -->
    <div class="aside-widget">
        <h3><span>Top Categories</span></h3>
        <div class="top-categories">
            @if(!$articleCategories->isEmpty())
            <ul>                             
                @foreach($articleCategories as $category)
                <li>
                    <a href="{{route('articles.category',[$category->slug])}}" class="{{ @$categoryDetail->id == $category->id ? 'active' : '' }} categories">{{$category->title}}</a>
                </li>
                @endforeach
            </ul>
            @endif
        </div>
    </div>
    <!--                    <div class="aside-widget">
                            <a href="#"><img src="images/adds.jpg" alt=""></a>
                        </div>-->
    <!-- Aside Widget -->

</div>