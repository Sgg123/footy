<script type="text/javascript">
    $( function() {
        $( "#start_date" ).datepicker({
            dateFormat: 'yy-mm-dd',  
            maxDate: 0,          
            onClose: function (selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
            }
        });
        $( "#end_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,            
            onClose: function (selectedDate) {
                        
            }
        });
      } );
</script>
<script type="text/javascript">

    $(function () {
        
        oTable = $('#mediabuzz_team').DataTable({
           "processing": true,
            serverSide: true,
            "pageLength": 25,
            searching: false,
            "sDom": "tpr",
            "dom": 'difrtp',
            ordering: false,
            "bFilter": true,
            ajax: {
                url: '{!! route("media.buzz.team.data") !!}',
                data: function (d) {
                    d.start_date_filter = $("#start_date").val();
                    d.end_date_filter = $("#end_date").val();
                },
            },
            columns: [
             
                {data: 'mb_first', name: 'mb_first'},
                {data: 'mb_second', name: 'mb_second'},
                {data: 'mb_third', name: 'mb_third'},
                {data: 'pb_top_player', name: 'pb_top_player'},
                {data: 'pb_top_defender', name: 'pb_top_defender'},
                {data: 'pb_top_midfielder', name: 'pb_top_midfielder'},
                {data: 'pb_top_forwarder', name: 'pb_top_forwarder'},
                {data: 'created_at', name: 'created_at'},
            ]
        });
    });
    $('#search-form').on('submit', function (e) {
        oTable.draw();
        e.preventDefault();
    });
</script>