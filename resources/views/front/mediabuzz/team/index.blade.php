@extends('layouts.app')
@section('no_follow')
<meta name="robots" content="noindex, nofollow">
@endsection
@section('content')


<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Media Buzz Last Week Winner Team</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Media Buzz Last Week Winner Team</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg') }}">
</div>

<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                @include('front.pages.sidebar')
                <!-- Aside -->
                 @if(\Auth::user()->isPremium())
                      <!-- Search Form -->
                <div class="col-lg-9 col-sm-12 sidebarres">
                    <form name="mediabuzz_search_form" action="">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="team_name" placeholder="Team name" value="{{ @Request::get('team_name') }}">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="from" id="start_date" placeholder="From" value="{{ @Request::get('from') }}" readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" id="end_date" name="to" placeholder="To" value="{{ @Request::get('to') }}" readonly>
                        </div>
                        <div class="form-group col-md-3 pull-right search-new-add">
                            <button type="submit" class="btn red-btn">Search</button>
                             <a href="{{route('media.buzz.team')}}" class="btn red-btn">Reset</a>
                        </div>
                    </form>
                </div>
                <!-- Search Form -->
                   @endif
                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-12 sidebarres">

                    <!-- Piont Table -->
                    <div class="macth-fixture">
                        <div class="last-matches styel-3">
                            <!-- Table Style 3 -->
                            <div class="table-responsive padding-top-70">
                                <table class="table table-bordered table-striped table-dark-header mb-team-table">
                                    <thead>
                                        <tr>
                                            <th>Date</th> 
                                            <th>MB 1st Place</th>
                                            <th>MB 2nd Place</th>
                                            <th>MB 3rd Place</th>
<!--                                            <th>PB Top Player</th>                                                
                                            <th>PB Top GK/Def</th>                                                
                                            <th>PB Top Midfielder</th>                                                
                                            <th>PB Top Forward</th>    -->                                               
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($media_buzz_teams))
                                        @foreach($media_buzz_teams as $history)
                                        @php $player_team_name = explode(',',@$history['player_team_names']); @endphp
                                        <tr>
                                            <td>{{date('d M Y',strtotime($history->date))}}</td>
                                            <td>{{ @$player_team_name[0] }}</td>
                                            <td>{{ @$player_team_name[1] }}</td>
                                            <td>{{ @$player_team_name[2] }}</td>                            
                                        </tr>  
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="3">No data found</td>                                            
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-12">
                                <div class="mb-player-pagination">    

                @if (@Request::get('team_name') != '' || @Request::get('from') != '' || @Request::get('to') != '')
                {!! $media_buzz_teams->appends(['team_name' => @Request::get('team_name'), 'to' => @Request::get('to'),'from' => @Request::get('from')])->links() !!}
                @else
                {!! $media_buzz_teams->links() !!}
                @endif
                                </div>                               
                            </div>  
                            @if(!\Auth::user()->isPremium())
                                <div class="col-xs-12 text-center">
                                <br>
                                <table class="MsoTableLightShadingAccent1" style="border-collapse: collapse; border: medium none;display: inline-block;" cellspacing="0" cellpadding="0" border="1" align="center">
                                    <tbody>
                                        <tr style="mso-yfti-irow:12;height:45.5pt">
                                          <td style="width:155.95pt;border:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                                          background:#fff;mso-background-themecolor:accent1;mso-background-themetint:
                                          63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="208">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                          margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                          mso-yfti-cnfc:68" align="center"><b><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
                                          color:#365F91;mso-themecolor:accent1;mso-themeshade:191">100% Full Media and Performance Buzz Winners History</span></b></p>
                                          </td>
                                          <td style="width:111.7pt;border-left:none;
                                          border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                          mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                          mso-border-alt:solid windowtext .5pt;background:#fff;mso-background-themecolor:
                                          accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="149">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                          margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                          mso-yfti-cnfc:64" align="center"><b style="mso-bidi-font-weight:normal"><span style="font-size:20.0pt;mso-bidi-font-size:11.0pt;color:#365F91;mso-themecolor:
                                          accent1;mso-themeshade:191">PREMIUM</span></b></p>
                                          </td>
                                          <td style="width:200.3pt;border-left:none;
                                          border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                          mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                          mso-border-alt:solid windowtext .5pt;background:#fff;mso-background-themecolor:
                                          accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="267">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;margin-bottom:.0001pt;
                                          mso-add-space:auto;line-height:normal;mso-yfti-cnfc:64"><span style="color:#365F91;mso-themecolor:accent1;mso-themeshade:191">View the full and complete winners of both Media and Performance Buzz.</span></p>
                                          </td>
                                         </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="text-center">
                                    <a href="{{url('/upgrade-account')}}" class="text-uppercase btn btn-primary">Click here to upgrade</a>
                                </div> <br>
                        <div class="text-center" style="font-weight: bold;font-size: 13pt;">
                         PREMIUM MEMBERSHIP ONLY £5 A MONTH. THE FIRST 30 DAYS ARE FREE!
                        </div>
                                <br>
                                </div>
                                @endif
                        </div>
                    </div>
                    <!-- Piont Table -->

                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->

@endsection
@section('page_js')
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" charset="utf-8"></script>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>
@endsection