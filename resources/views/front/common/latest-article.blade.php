@php
$article = Helper::getLatestArticle();
@endphp
<div class="col-sm-12 col-xs-12 r-full-width">
    <h3><span><i class="red-color">Latest Article</i></span></h3>
    <div class="article-posted-date"><p class="article-date"><strong>Posted on {{date('d F Y',strtotime(@$article->date))}}</strong></p></div>
    <div class="upcoming-fixture row">
        <div class="col-lg-4 col-xs-12">
            <div class="news-post-holder">
                <!-- Widget -->
                <div class="news-post-widget">
                    <a href="{{route('article.detail',[@$article->slug])}}">
                        <img
                            @if (file_exists('uploads/articles/' . @$article->image_path) &&  @$article->image_path != '')
                            src="{{ asset('uploads/articles/'.@$article->image_path) }}"
                            @else
                            src="{{ asset('assets/images/no-image.jpeg') }}"
                            @endif  alt="">  
                    </a>
                </div>
                <!-- Widget -->
            </div>
        </div>

        <div class="col-lg-8 col-xs-12">
            <div class="news-post-holder">
                <h2><a href="{{route('article.detail',[@$article->slug])}}">
                        @if(strlen(@$article->title) >= 59) 
                        {!! substr(@$article->title,0,58) !!}..
                        @else
                        {!!@$article->title!!}
                        @endif
                    </a></h2>
                <div class="article-content">
                    <p>
                        @php
                        $text = strip_tags(@$article->content);
                        $this->orginal_content_count = str_word_count($text);
                        @endphp

                        @if(strlen(@$text) >= 725) 
                        {!! substr(@$text,0,724) !!}....<p class="article_read_more"><strong><a href="{{route('article.detail',[@$article->slug])}}">Read more</a></strong></p>
                    @else
                    {!!@$text!!}
                    @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>