
<nav id="menu" class="responive-nav">
    <a class="r-nav-logo" href="{{ url('/') }}"><img src="{{ asset('assets/front/images/Footy-Index-Scout-Logo-Main.png') }}" alt=""></a>
    @guest
  
    <ul class="respoinve-nav-list">
        <li class="{{ @$activeMenu == 'home' ? 'active' : ''}}">
            <a href="{{ url('') }}">Home</a>
        </li>
        <li>
            <a href="{{route('page', [@$pages[7]['slug']])}}">Money Back Guarantee</a>
        </li>
        <li>
            <a href="{{route('page', [@$pages[6]['slug']])}}" >Media</a>
        </li>  
        <li class="{{ @$activeSubMenu == 'articles' ? 'active' : ''}}">
            <a href="{{route('articles')}}">Article</a>
        </li> 

        <li>
            <a data-toggle="collapse" href="#list-1"><i class="pull-right fa fa-angle-down"></i>Media Buzz</a>
            <ul class="collapse" id="list-1">
                <li>
                    <a href="{{route('page', [@$pages[4]['slug']])}}">What is Media Buzz?</a></li>
                <li>
                    <a href="#" class="show-login-frm">Media Buzz Winner Player History</a>
                </li>
                <li>
                    <a href="#" class="show-login-frm">Media Buzz Winner Team History</a>
                </li>
            </ul>
        </li>
        <li>
            @php @$todayDate = Helper::encrypt(date('Y-m-d')); @endphp
            <a data-toggle="collapse" href="#list-2"><i class="pull-right fa fa-angle-down"></i>Performance Buzz</a>
            <ul class="collapse" id="list-2">
                <li class="{{ @$activeSubMenu == 'what-is-performance-buzz' ? 'active' : ''}}"><a href="{{route('page', [@$pages[5]['slug']])}}">What is Performance Buzz?</a></li>
                <li class="{{ @$activeSubMenu == 'performance-buzz-player' ? 'active' : ''}}"><a href="#" class="show-login-frm">Performance Buzz Winner Player History</a></li>
                <li class="{{ @$activeSubMenu == 'performance-buzz-team' ? 'active' : ''}}"><a href="#" class="show-login-frm">Performance Buzz Winner Team History</a></li>
                <li class="{{ @$activeSubMenu == 'pb-planner' ? 'active' : ''}}"><a href="#" class="show-login-frm">Performance Buzz Planner</a></li>
                <li class="{{ @$activeSubMenu == 'performance-buzz-statistics' ? 'active' : ''}}"><a href="#" class="show-login-frm">Performance Buzz Statistics</a></li>
                 <li class="{{ @$activeSubMenu == 'performance-buzz-stats-history' ? 'active' : ''}}"><a href="#" class="show-login-frm">Performance Buzz Scores History</a></li>
            </ul>
        </li>
        <li class="{{ @$activeMenu == 'database' ? 'active' : ''}}">
            <a href="#" class="show-login-frm">Database</a>
        </li>       
        <li class="{{ @$activeMenu == 'spreadsheet' ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#list-3" class="show-login-frm"><i class="pull-right fa fa-angle-down"></i>Spreadsheet</a>

                        <ul class="collapse" id="list-3">
                            @foreach($spreadsheets as $spreadsheet)
                            <li class="{{ @$activeSubMenu == 'spreadsheet' ? 'active' : ''}}">
                               <a href="#" class="show-login-frm">{{@$spreadsheet->title}}</a>
                            </li>
                            @endforeach
                        </ul>

                    </li>                                                              
    </ul>
    @else
    <ul class="respoinve-nav-list">
        <li class="{{ @$activeMenu == 'home' ? 'active' : ''}}">
            <a href="{{ url('') }}">Home</a>
        </li>
        <li>
            <a href="{{route('page', [@$pages[7]['slug']])}}">Money Back Guarantee</a>
        </li>
        <li>
            <a href="{{route('page', [@$pages[6]['slug']])}}">Media</a>
        </li>  
        <li class="{{ @$activeSubMenu == 'articles' ? 'active' : ''}}">
            <a href="{{route('articles')}}">Article</a>
        </li> 

        <li>
            <a data-toggle="collapse" href="#list-1"><i class="pull-right fa fa-angle-down"></i>Media Buzz</a>
            <ul class="collapse" id="list-1">
                <li>
                    <a href="{{route('page', [@$pages[4]['slug']])}}">What is Media Buzz?</a></li>
                <li>
                    <a href="{{route('media.buzz.player')}}">Media Buzz Winner Player History</a>
                </li>
                <li>
                    <a href="{{route('media.buzz.team')}}">Media Buzz Winner Team History</a>
                </li>
            </ul>
        </li>
        <li>
            @php @$todayDate = Helper::encrypt(date('Y-m-d')); @endphp
            <a data-toggle="collapse" href="#list-2"><i class="pull-right fa fa-angle-down"></i>Performance Buzz</a>
            <ul class="collapse" id="list-2">
                <li class="{{ @$activeSubMenu == 'what-is-performance-buzz' ? 'active' : ''}}"><a href="{{route('page', [@$pages[5]['slug']])}}">What is Performance Buzz?</a></li>
                <li class="{{ @$activeSubMenu == 'performance-buzz-player' ? 'active' : ''}}"><a href="{{route('performance.buzz.player')}}">Performance Buzz Winner Player History</a></li>
                <li class="{{ @$activeSubMenu == 'performance-buzz-team' ? 'active' : ''}}"><a href="{{route('performance.buzz.team')}}">Performance Buzz Winner Team History</a></li>
                <li class="{{ @$activeSubMenu == 'pb-planner' ? 'active' : ''}}"><a href="{{(\Auth::user()->isPremium()) ? route('pb.planner', @$todayDate) : route('pb.planner.upgrade')}}">Performance Buzz Planner</a></li>
                <li class="{{ @$activeSubMenu == 'performance-buzz-statistics' ? 'active' : ''}}"><a href="{{ (\Auth::user()->isPremium()) ? route('performance.buzz.statistics') : route('performance.buzz.statistics.upgrade')}}">Performance Buzz Statistics</a></li>
                 <li class="{{ @$activeSubMenu == 'performance-buzz-stats-history' ? 'active' : ''}}"><a href="{{ (\Auth::user()->isPremium()) ? route('performance.buzz.statshistory') : route('performance.buzz.statshistory.upgrade')}}">Performance Buzz Scores History</a></li>
            </ul>
        </li>
        <li class="{{ @$activeMenu == 'database' ? 'active' : ''}}">
            <a href="{{(\Auth::user()->isPremium()) ? route('database') : route('database.upgrade')}}">Database</a>
        </li>   

                    <li class="{{ @$activeMenu == 'spreadsheet' ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#list-3" class="show-login-frm"><i class="pull-right fa fa-angle-down"></i>Spreadsheet</a>

                        <ul class="collapse" id="list-3">
                            @foreach($spreadsheets as $spreadsheet)
                            <li class="{{ @$activeSubMenu == 'spreadsheet' ? 'active' : ''}}">
                               <a href="{{url('spreadsheets/'.@$spreadsheet->file_path)}}">{{@$spreadsheet->title}}</a>
                            </li>
                            @endforeach
                        </ul>

                    </li>                                                                  
    </ul>
    @endguest
</nav>
