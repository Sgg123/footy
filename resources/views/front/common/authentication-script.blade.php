<script type="text/javascript">
    $(".show-register-frm").on("click", function () {
        $("#registration-modal").modal();
    });

    $(".show-login-frm").on("click", function () {
        $("#login-modal").modal();
    });

    $("#do-login").on("click", function () {

        $formData = new FormData();
        $email = $("#login-email").val();
        $password = $("#login-password").val();
        $formData.append('email', $email);
        $formData.append('password', $password);
        $URL = "{{ route('login') }}";

        $.ajax({
            url: $URL,
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken
            },
            data: $formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {

                if (data.hasOwnProperty('errors')) {

                    if (data.errors.hasOwnProperty('email')) {

                        $("#login-email-error").text(data.errors.email[0]);
                    } else {
                        $("#login-email-error").text('');
                    }
                    if (data.errors.hasOwnProperty('password')) {
                        $("#login-password-error").text(data.errors.password[0]);
                    } else {
                        $("#login-password-error").text('');
                    }

                } else if (data.hasOwnProperty('status')) {

                    $alert = '<div class="alert alert-' + data.status + ' alert-dismissable">';
                    $alert += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    $alert += data.message;
                    $alert += '</div>';
                    $(".login-response").html($alert);

                    if (data.status == 'success') {

                        window.location.reload();
                    }

                } else {
                    window.location.reload();
                }

            }
        });
    });

    $("#do-reset").on("click", function () {

        $resetData = new FormData();
        $email = $("#forgot-email").val();
        $resetData.append('email', $email);

        $URL = "{{ route('password.email') }}";

        $.ajax({
            url: $URL,
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken
            },
            data: $resetData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {

                if (data.hasOwnProperty('errors')) {

                    if (data.errors.hasOwnProperty('email')) {

                        $("#forgot-email-error").text(data.errors.email[0]);
                    }
                } else {

                    $("#forgot-email-error").text('');

                    $alert = '<div class="alert alert-' + data.status + ' alert-dismissable">';
                    $alert += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    $alert += data.message;
                    $alert += '</div>';
                    $(".forgot-response").html($alert);

                    if (data.status == 'success') {

                        window.location.reload();
                    }
                }
            }
        });
    });
    $('input[name="register_type"]').click(function(e){
        if($('input[name="register_type"]:checked').val() == 'premium')
        {
            $('#registerCreditCard').show();
        }
        else
        {
            $('#registerCreditCard').hide();
        }
    });
    //
    $("#do-register").on("click", function () {
        $formData = new FormData();
        $error = 0;
        $firstname = $("#register-firstname").val();
        if ($firstname.trim() == "") {
            $("#register-firstname-error").text("The firstname field is required.");
            $error = 1;
        } else {
            $("#register-firstname-error").text('');
        }
        $lastname = $("#register-lastname").val();
        if ($lastname.trim() == "") {
            $("#register-lastname-error").text("The lastname field is required.");
            $error = 1;
        } else {
            $("#register-lastname-error").text('');
        }
        $phone = $("#register-phone").val();
        if ($phone.trim() == "") {
            $("#register-phone-error").text("The phone field is required.");
            $error = 1;
        } else if(!$.isNumeric( $phone )) {
            $("#register-phone-error").text("The phone field must be numeric.");
            $error = 1;
        } else {
            $("#register-phone-error").text('');
        }
        $email = $("#register-email").val();
        var emailReg = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if ($email.trim() == "") {
            $("#register-email-error").text("The email field is required.");
            $error = 1;
        } else if(!emailReg.test( $email )) {
            $("#register-email-error").text("Enter valid email.");
            $error = 1;
        } else {
            $("#register-email-error").text('');
        }
        $confirmPassword = $("#register-password").val();
        $password = $("#register-confirm-password").val();
        if ($password.trim() == "") {
            $("#register-password-error").text("The password field is required.");
            $error = 1;
        } else if($password.trim() != $confirmPassword.trim()) {
            $("#register-password-error").text("Both passwords must match.");
            $error = 1;
        } else {
            $("#register-password-error").text('');
        }
        $register_type = $('input[name="register_type"]:checked').val();
        $cardno = $('#register-cardnumber').val();
        if($register_type == 'premium')
        {
            if($cardno.trim() == "") {
                $("#register-cardnumber-error").text('The card number field is required.');
                $error = 1;
            } else if(!Stripe.card.validateCardNumber($cardno)) {
                $("#register-cardnumber-error").text('Enter valid card number.');
                $error = 1;
            } else {
                $("#register-cardnumber-error").text('');
            }
        }
        $ccexpirymonth = $('#register-expiry-month').val();
        $ccexpiryyear = $('#register-expiry-year').val();
        if($register_type == 'premium')
        {
            if(!Stripe.card.validateExpiry($ccexpirymonth, $ccexpiryyear)) {
                $("#register-expiry-month-error").text('Enter valid expiry month.');
                $("#register-expiry-year-error").text('Enter valid expiry year.');
                $error = 1;
            } else {
                $("#register-expiry-month-error").text('');
                $("#register-expiry-year-error").text();
            }
        }
        $cvvno = $('#register-cvv-number').val();
        $cardToken = '';
        if($register_type == 'premium')
        {
            if($cvvno.trim() == "") {
                $("#register-cvv-number-error").text('The cvv number field is required.');
                $error = 1;
            } else if(!Stripe.card.validateCVC($cvvno)) {
                $("#register-cvv-number-error").text('Enter valid cvv number.');
                $error = 1;
            } else {
                $("#register-cvv-number-error").text('');
            }
            Stripe.card.createToken({
              number: $cardno,
              cvc: $cvvno,
              exp_month: $ccexpirymonth,
              exp_year: $ccexpiryyear
            }, function(status, response){
                if(status == 200)
                {
                    if($error == 1)
                    {
                        return false;
                    }
                    $formData.append('firstname', $firstname);
                    $formData.append('lastname', $lastname);
                    $formData.append('phone', $phone);
                    $formData.append('email', $email);
                    $formData.append('password_confirmation', $confirmPassword);
                    $formData.append('password', $password);
                    $formData.append('account_type', $register_type);
                    $formData.append('card_no', $cardno);
                    $formData.append('ccExpiryMonth', $ccexpirymonth);
                    $formData.append('ccExpiryYear', $ccexpiryyear);
                    $formData.append('cvvNumber', $cvvno);
                    $formData.append('cardToken', response.id);
                    $URL = "{{ route('register') }}";

                    $.ajax({
                        url: $URL,
                        headers: {
                            'X-CSRF-TOKEN': window.Laravel.csrfToken
                        },
                        data: $formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function (data) {
                            if (data.hasOwnProperty('errors')) {

                                if (data.errors.hasOwnProperty('firstname')) {
                                    $("#register-firstname-error").text(data.errors.firstname[0]);
                                } else {
                                    $("#register-firstname-error").text('');
                                }
                                if (data.errors.hasOwnProperty('lastname')) {
                                    $("#register-lastname-error").text(data.errors.lastname[0]);
                                } else {
                                    $("#register-lastname-error").text('');
                                }
                                if (data.errors.hasOwnProperty('phone')) {
                                    $("#register-phone-error").text(data.errors.phone[0]);
                                } else {
                                    $("#register-phone-error").text('');
                                }
                                if (data.errors.hasOwnProperty('email')) {
                                    $("#register-email-error").text(data.errors.email[0]);
                                } else {
                                    $("#register-email-error").text('');
                                }
                                if (data.errors.hasOwnProperty('password')) {
                                    $("#register-password-error").text(data.errors.password[0]);
                                } else {
                                    $("#register-password-error").text('');
                                }
                                if (data.errors.hasOwnProperty('card_no')) {
                                    $("#register-cardnumber-error").text(data.errors.card_no[0]);
                                } else {
                                    $("#register-cardnumber-error").text('');
                                }
                                if (data.errors.hasOwnProperty('cardToken')) {
                                    $("#register-cardnumber-error").text(data.errors.cardToken[0]);
                                } else {
                                    $("#register-cardnumber-error").text('');
                                }
                                if (data.errors.hasOwnProperty('ccExpiryMonth')) {
                                    $("#register-expiry-month-error").text(data.errors.ccExpiryMonth[0]);
                                } else {
                                    $("#register-expiry-month-error").text('');
                                }
                                if (data.errors.hasOwnProperty('ccExpiryYear')) {
                                    $("#register-expiry-year-error").text(data.errors.ccExpiryYear[0]);
                                } else {
                                    $("#register-expiry-year-error").text('');
                                }
                                if (data.errors.hasOwnProperty('cvvNumber')) {
                                    $("#register-cvv-number-error").text(data.errors.cvvNumber[0]);
                                } else {
                                    $("#register-cvv-number-error").text('');
                                }

                            } else if (data.hasOwnProperty('status')) {
                                $("#modal-register-form")[0].reset();
                                $('#registerCreditCard').hide();
                                $alert = '<div class="alert alert-' + data.status + ' alert-dismissable">';
                                $alert += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                                $alert += data.message;
                                $alert += '</div>';
                                $(".register-response").html($alert);

            //                    if (data.status == 'success') {
            //
            //                        window.location.reload();
            //                    }

                            } else {
                                //window.location.reload();
                            }

                        }
                    });
                }
                else
                {
                    $("#register-cardnumber-error").text(response.error.message);
                    $error = 1;
                    if($error == 1)
                    {
                        return false;
                    }
                }
            });
        }
        else
        {
            if($error == 1)
            {
                return false;
            }
        
            $formData.append('firstname', $firstname);
            $formData.append('lastname', $lastname);
            $formData.append('phone', $phone);
            $formData.append('email', $email);
            $formData.append('password_confirmation', $confirmPassword);
            $formData.append('password', $password);
            $formData.append('account_type', $register_type);
            $formData.append('card_no', $cardno);
            $formData.append('ccExpiryMonth', $ccexpirymonth);
            $formData.append('ccExpiryYear', $ccexpiryyear);
            $formData.append('cvvNumber', $cvvno);
            $URL = "{{ route('register') }}";

            $.ajax({
                url: $URL,
                headers: {
                    'X-CSRF-TOKEN': window.Laravel.csrfToken
                },
                data: $formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    if (data.hasOwnProperty('errors')) {

                        if (data.errors.hasOwnProperty('firstname')) {
                            $("#register-firstname-error").text(data.errors.firstname[0]);
                        } else {
                            $("#register-firstname-error").text('');
                        }
                        if (data.errors.hasOwnProperty('lastname')) {
                            $("#register-lastname-error").text(data.errors.lastname[0]);
                        } else {
                            $("#register-lastname-error").text('');
                        }
                        if (data.errors.hasOwnProperty('phone')) {
                            $("#register-phone-error").text(data.errors.phone[0]);
                        } else {
                            $("#register-phone-error").text('');
                        }
                        if (data.errors.hasOwnProperty('email')) {
                            $("#register-email-error").text(data.errors.email[0]);
                        } else {
                            $("#register-email-error").text('');
                        }
                        if (data.errors.hasOwnProperty('password')) {
                            $("#register-password-error").text(data.errors.password[0]);
                        } else {
                            $("#register-password-error").text('');
                        }
                        if (data.errors.hasOwnProperty('card_no')) {
                            $("#register-cardnumber-error").text(data.errors.card_no[0]);
                        } else {
                            $("#register-cardnumber-error").text('');
                        }
                        if (data.errors.hasOwnProperty('ccExpiryMonth')) {
                            $("#register-expiry-month-error").text(data.errors.ccExpiryMonth[0]);
                        } else {
                            $("#register-expiry-month-error").text('');
                        }
                        if (data.errors.hasOwnProperty('ccExpiryYear')) {
                            $("#register-expiry-year-error").text(data.errors.ccExpiryYear[0]);
                        } else {
                            $("#register-expiry-year-error").text('');
                        }
                        if (data.errors.hasOwnProperty('cvvNumber')) {
                            $("#register-cvv-number-error").text(data.errors.cvvNumber[0]);
                        } else {
                            $("#register-cvv-number-error").text('');
                        }

                    } else if (data.hasOwnProperty('status')) {
                        $("#modal-register-form")[0].reset();
                        $alert = '<div class="alert alert-' + data.status + ' alert-dismissable">';
                        $alert += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                        $alert += data.message;
                        $alert += '</div>';
                        $(".register-response").html($alert);

    //                    if (data.status == 'success') {
    //
    //                        window.location.reload();
    //                    }

                    } else {
                        window.location.reload();
                    }

                }
            });
        }

    })
</script>

@if(Route::is('password.reset'))
<script type="text/javascript">
    $("#reset-password-modal").modal({
        backdrop: 'static',
        keyboard: false
    });

    $("#do-reset-password").on("click", function () {

        $resetData = new FormData();
        $email = $("#reset-email").val();
        $resetData.append('email', $email);

        $resetData.append('token', '{{ @$token }}');

        $password = $("#reset-password").val();
        $resetData.append('password', $password);

        $passwordConfirm = $("#reset-confirm-password").val();
        $resetData.append('password_confirmation', $passwordConfirm);

        $URL = "{{ route('password.request') }}";

        $.ajax({
            url: $URL,
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken
            },
            data: $resetData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {

                if (data.hasOwnProperty('errors')) {

                    if (data.errors.hasOwnProperty('email')) {

                        $("#reset-email-error").text(data.errors.email[0]);
                    } else {
                        $("#reset-email-error").text('');
                    }
                    if (data.errors.hasOwnProperty('password')) {
                        $("#reset-password-error").text(data.errors.password[0]);
                    } else {
                        $("#reset-password-error").text('');
                    }
                } else {

                    $("#reset-password-error,reset-email-error").text('');

                    $alert = '<div class="alert alert-' + data.status + ' alert-dismissable">';
                    $alert += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    $alert += data.message;
                    $alert += '</div>';
                    $(".reset-response").html($alert);

                    if (data.status == 'success') {

                        window.location.reload();
                    }
                }

            }
        });
    });
</script>
@endif

<script type="text/javascript">
    $("#modal-login-form").keypress(function (e) {
        if (e.which == 13) {
            $("#do-login").trigger('click');
        }
    });

    $("#modal-register-form").keypress(function (e) {
        if (e.which == 13) {
            $("#do-register").trigger('click');
        }
    });

    $("#modal-forgot-password-form").keypress(function (e) {
        if (e.which == 13) {
            $("#do-reset").trigger('click');
            return false;
        }
    });
</script>