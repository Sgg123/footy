@php
$bottomBenners=Helper::benner('bottom');
@endphp
@if(count($bottomBenners) > 0)
    <div class="">
        @foreach($bottomBenners as $key=> $b)
            <div class="bannerimg">
                <a href="{{$b->link}}">
                    <img @if($b->image != '' && file_exists(public_path('uploads/banner/'.$b->image)))
                        src="{{ asset('uploads/banner/'.$b->image) }}"
                        @else
                        src="{{ asset('assets/images/no-image.jpeg') }}"
                        @endif alt="">
                </a>
            </div>
        @endforeach
    </div>
@endif