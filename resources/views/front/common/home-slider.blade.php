@php
    $mainBenners=Helper::benner('main');
@endphp
<div class="slider-holder">

    <!-- Banner slider -->
    <ul id="main-slides" class="main-slides">

        <!-- Itme -->
        <li>
            <div id="animated-slider" class="carousel slide carousel-fade">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    @if(count($mainBenners) > 0)
                        @foreach($mainBenners as $key=> $b)
                            <div class="item @if($key == 0) {{'active'}} @endif">
                                <a href="{{$b->link}}">
                                    <img @if($b->image != '' && file_exists(public_path('uploads/banner/'.$b->image)))
                                        src="{{ asset('uploads/banner/'.$b->image) }}"
                                        @else
                                        src="{{ asset('assets/images/no-image.jpeg') }}"
                                        @endif alt="">
                                    </a>
                            </div> 
                        @endforeach
                    @else
                        <!-- Item -->
                        <div class="item active">
                            <img src="{{ asset('assets/front/images/banner-slider/FIS-Splash-Screen-1.jpg') }}" alt="">
                        </div> 
                        <!-- Item -->

                        <!-- Item -->
                        <div class="item">
                            <img src="{{ asset('assets/front/images/banner-slider/FIS-Splash-Screen-4a.jpg') }}" alt="">
                        </div> 
                        <!-- Item -->

                        <!-- Item -->
                        <div class="item">
                            <img src="{{ asset('assets/front/images/banner-slider/FIS-Splash-Screen-3.jpg') }}" alt="">
                        </div> 
                        <!-- Item -->       
                    @endif
                </div>
                <ul class="carousel-indicators">
                    @if(count($mainBenners) > 0)
                        @foreach($mainBenners as $key=> $b)
                            <li data-target="#animated-slider" data-slide-to="{{$key}}" class="@if($key == 0) {{'active'}} @endif"></li>
                        @endforeach
                    @else
                        <li data-target="#animated-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#animated-slider" data-slide-to="1"></li>
                        <li data-target="#animated-slider" data-slide-to="2"></li>
                    @endif
                </ul>
                <!-- Indicators -->

            </div>
        </li>
        <!-- Itme -->

    </ul>
    <!-- Banner slider -->

</div>