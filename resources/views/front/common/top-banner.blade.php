@php
$topBenners=Helper::benner('top');
@endphp
@if(count($topBenners) > 0)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topbanner">
        @foreach($topBenners as $key=> $b)
            <div class="bannerimg">
                <a href="{{$b->link}}">
                    <img @if($b->image != '' && file_exists(public_path('uploads/banner/'.$b->image)))
                        src="{{ asset('uploads/banner/'.$b->image) }}"
                        @else
                        src="{{ asset('assets/images/no-image.jpeg') }}"
                        @endif alt="">
                </a>
            </div>
        @endforeach
    </div>
@endif