@php
$leftBenners=Helper::benner('left');
@endphp
@if(count($leftBenners) > 0)
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 leftbanner">
        @foreach($leftBenners as $key=> $b)
            <div class="bannerimg">
                <a href="{{$b->link}}">
                    <img @if($b->image != '' && file_exists(public_path('uploads/banner/'.$b->image)))
                        src="{{ asset('uploads/banner/'.$b->image) }}"
                        @else
                        src="{{ asset('assets/images/no-image.jpeg') }}"
                        @endif alt="">
                </a>
            </div>
        @endforeach
    </div>
@endif