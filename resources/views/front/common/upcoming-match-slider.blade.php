<?php 
    if(isset($updatedmatches)){ 
        $updatedmatches = json_decode(json_encode($updatedmatches), True); 
    }
?>
<div class="products-holder gray-bg theme-padding">
    @if(isset($page) && $page == "homepage")
        <h1><span><i class="red-color">UPCOMING </i>FIXTURES</span></h1>
    @else
        <h3><span><i class="red-color">UPCOMING </i>FIXTURES</span></h3>
    @endif
    <div id="product-slider" class="product-slider nav-style-1">
        @if(count($upcomingMatches) > 0)
            @foreach($upcomingMatches as $match)
                @php 
                $triple_media_class = "";@$startingAt = ""; @endphp
                
                @if(@$match['title'] === "Triple Media")
                    @php $triple_media_class = "triple_media_class"; @endphp
                @else
                    @php $triple_media_class = ""; @endphp
                @endif                                                                                                                      

                @if(isset($updatedmatches))
                    @foreach($updatedmatches as $umatch)
                        @if($match['date'] ==  date('D jS M', strtotime($umatch['date'])))
                            @if($umatch['type_of_day'] == 1)
                            @php 
                                $match['title'] = 'Bronze Match Day';
                                $match['color_codes'] = '#CF5A0A';
                            @endphp
                            @endif
                            @if($umatch['type_of_day'] == 2)
                            @php 
                                $match['title'] = 'Silver Match Daye';
                                $match['color_codes'] = '#8C909C';
                            @endphp
                            @endif
                            @if($umatch['type_of_day'] == 3)
                            @php 
                                $match['title'] = 'Gold Match Day';
                                $match['color_codes'] = '#DDAB27';
                            @endphp
                            @endif
                            @if($umatch['type_of_day'] == 4)
                            @php 
                                $match['title'] = 'Media Day';
                                $match['color_codes'] = '#439F05';
                            @endphp
                            @endif
                            @if($umatch['type_of_day'] == 5)
                                @php 
                                    $match['title'] = 'Media Madness Top 5';
                                    $match['color_codes'] = '#19ADF3';
                                     //if($match['midfielder']  == '3p' && $umatch['media_buzz_second'] != ""){
                                @endphp
                            @endif
                            @if($umatch['type_of_day'] == 5 || $umatch['type_of_day'] == 4){
                                @php
                                    if($umatch['media_buzz_second'] != ""){
                                        $match['midfielder'] = ($umatch['media_buzz_second']*100).'p';
                                    }

                                    //if($match['defender']  == '5p' && $umatch['media_buzz_first'] != ""){
                                    if($umatch['media_buzz_first'] != ""){
                                        $match['defender'] = ($umatch['media_buzz_first']*100).'p';
                                    }

                                    //if($match['attacker']  == '1p' && $umatch['media_buzz_third'] != ""){
                                    if($umatch['media_buzz_third'] != ""){
                                        $match['attacker'] = ($umatch['media_buzz_third']*100).'p';
                                    }

                                    if($umatch['goalkeeper'] != ""){
                                        $match['goalkeeper'] = ($umatch['goalkeeper']*100).'p';
                                    }
                                @endphp
                            }
                        @endif
                    @endforeach
                @endif

                @if(@$match['title'] === 'Triple Media' || @$match['title'] === 'Media Madness Top 5' || @$match['title'] === 'Media Day')
                    @php
                        $route = route('page', [@$pages[6]['slug']]);
                        $title = "View News"
                    @endphp
                @else
                    @php
                        @$startingAt = Helper::encrypt(@$match['startingAt']);
                        $route = route('pb.planner', @$startingAt);
                        $title = "View Games"
                    @endphp
                @endif

                <div class="product-column" style="cursor: pointer;" onclick="location.href='{{ @$route }}'">
                    <div class="r-full-width">
                        <div class="next-matches">
                            <h4>{{ @$match['date'] }}</h4>

                        </div>
                    </div>
                    <div class="col-md-12 text-center media_box {{@$triple_media_class}}" style="background-color: {{ @$match['color_codes'] }}">

                        <h3>
                            {{ @$match['title'] }}
                        </h3>
                    </div>
                    <div class="product-column-inner">
                        <div class="row">
                            <div class="col-md-4 col-xs-4 position-title">
                                {{ @$match['title'] === 'Triple Media' || (@$match['title'] === 'Media Madness Top 5' || @$match['title'] === 'Media Day')? '1st' : 'DEF' }}
                                <div class="def_mid"> 
                                    {{@$match['defender'] }}
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-4 position-title">
                                {{ @$match['title'] === 'Triple Media' || (@$match['title'] === 'Media Madness Top 5' || @$match['title'] === 'Media Day') ? '2nd' : 'MID' }}  
                                <div class="def_mid">{{ @$match['midfielder'] }}
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-4 position-title">
                                {{ @$match['title'] === 'Triple Media'  || (@$match['title'] === 'Media Madness Top 5' || @$match['title'] === 'Media Day')? '3rd' : 'FWD' }}  
                                <div class="def_mid">{{ @$match['attacker'] }}</div>

                            </div>
                             <div class="col-md-4 col-xs-4 position-title">
                                {{ @$match['title'] === 'Triple Media'  || (@$match['title'] === 'Media Madness Top 5' || @$match['title'] === 'Media Day')? '3rd' : 'GK' }}  
                                <div class="def_mid">{{ @$match['goalkeeper'] }}</div>

                            </div>
                        </div>

                    </div> 
                    <div class="btm">    
                        <a class="btn btn-success btn-block" href="{{ @$route }}">{{ @$title }}</a>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>
<!-- Products -->