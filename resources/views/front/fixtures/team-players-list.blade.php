<div class="row" id="view_team_players">
    <div class="col-sm-6 detail-player">
        <table class="table table-bordered firstplayer-list">
            <tbody>
            <!--<h3><span><img src="{{@$localTeam[0]['logo_path']}}" class="team_logo"></span></h3>-->
            <h3><span>{{@$localTeam[0]['name']}}</span></h3>
            @if(@$localTeam[0]['get_team_players'])
            @foreach(@$localTeam[0]['get_team_players'] as $player)
            <tr>
                <td class="players-image-logo"><img class="player_image view-player" data-id="{{@$player['pid']}}"
                    @if(@$player['is_local_image'] == 0)
                       src="{{ @$player['image_path'] }}"
                    @elseif(@$player['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$player['local_image_path'])))
                       src="{{ asset('uploads/player-images/'.@$player['local_image_path']) }}"
                    @else
                      src ="{{ asset('assets/images/no-image.jpeg')}}" 
                    @endif
                    >

                   

                  </td>
          
                <td class="view-player" data-id="{{@$player['pid']}}" data-teamid="{{@$localTeam[0]['id']}}">{{ @$player['football_index_common_name'] != "" ? @$player['football_index_common_name'] : @$player['common_name'] }} - {{$player['football_index_position']}}
                    <br/>
                @php
                  $colorCode = "";
                  $fontcolor = "";
                  $playerScore = @number_format(@$player['total_score']); 
                @endphp
                @if(@$playerScore <= 30)
                  @php
                  $colorCode = "#FF0000";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @elseif(@$playerScore >= 31 && @$playerScore <= 70)
                  @php
                  $colorCode = "#FF5733";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @elseif(@$playerScore >= 71 && @$playerScore <= 110)
                  @php
                  $colorCode = "#eacd0e";
                  $fontcolor = "#000";
                  @endphp
                @elseif(@$playerScore >= 111 && @$playerScore <= 150)
                  @php
                  
                  $colorCode = "#FFFF00";
                  $fontcolor = "#000";
                  @endphp
                @elseif(@$playerScore >= 151 && @$playerScore <= 180)
                  @php
                  $colorCode = "#aad77d";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @elseif(@$playerScore >= 181)
                  @php
                  $colorCode = "#39a02b";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @endif
                <div style="background:{{$colorCode}}; color:{{ $fontcolor }}">
                  <span class="pb_player_score">{{ @number_format($player['total_score'],0.5) }}</span>
                </div>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td>No Api Data found.</td>
            </tr>
            @endif
            </tbody> 
        </table>
    </div>

    <div class="col-sm-6 detail-player ">
        <table class="table table-bordered secondplayer-list">
            <!--<h3><span><img src="{{@$visitorTeam[0]['logo_path']}}" class="team_logo"></span></h3>-->
            <h3><span>{{@$visitorTeam[0]['name']}}</span></h3>
            @if(@$visitorTeam[0]['get_team_players'])
            @foreach(@$visitorTeam[0]['get_team_players'] as $player)
            <tr>
                <td class="players-image-logo"><img class="player_image view-player" data-id="{{@$player['pid']}}"
                  @if(@$player['is_local_image'] == 0)
                    src="{{ @$player['image_path'] }}"
                  @elseif(@$player['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$player['local_image_path'])))
                    src="{{ asset('uploads/player-images/'.@$player['local_image_path']) }}"
                  @else
                    src ="{{ asset('assets/images/no-image.jpeg')}}" 
                  @endif
                ></td>

                <td class="view-player" data-id="{{@$player['pid']}}" data-teamid="{{@$visitorTeam[0]['id']}}">{{ 
            @$player['football_index_common_name'] != "" ? @$player['football_index_common_name'] : @$player['common_name'] }} - {{$player['football_index_position']}}
            <br/>
                @php
                  $colorCode = "";
                  $fontcolor = "";
                  $playerScore = @number_format(@$player['total_score']); 
                @endphp
                @if(@$playerScore <= 30)
                  @php
                  $colorCode = "#FF0000";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @elseif(@$playerScore >= 31 && @$playerScore <= 70)
                  @php
                  $colorCode = "#FF5733";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @elseif(@$playerScore >= 71 && @$playerScore <= 110)
                  @php
                  $colorCode = "#eacd0e";
                  $fontcolor = "#000";
                  @endphp
                @elseif(@$playerScore >= 111 && @$playerScore <= 150)
                  @php
                  $colorCode = "#FFFF00";
                  $fontcolor = "#000";
                  @endphp
                @elseif(@$playerScore >= 151 && @$playerScore <= 180)
                  @php
                  $colorCode = "#aad77d";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @elseif(@$playerScore >= 180)
                  @php
                  $colorCode = "#39a02b";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @endif
                <div style="background:{{$colorCode}}; color:{{ $fontcolor }};">
                  <span class="pb_player_score">{{ @number_format($player['total_score'],0.5) }}</span>
                </div>
           </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td>No Api Data found.</td>
            </tr>
            @endif

        </table>

    </div>

</div>

<div class="row" id="view_form_players" style="display:none;">
    <div class="col-sm-6 detail-player">
        <table class="table table-bordered firstplayer-list">
            <tbody>
            <!--<h3><span><img src="{{@$localTeam[0]['logo_path']}}" class="team_logo"></span></h3>-->
            <!-- <h3><span>{{@$localTeam[0]['name']}}</span></h3> -->
            @if(@$localTeam[0]['get_team_players'])
            @foreach(@$all_players as $player)
            <tr>
                <td class="players-image-logo"><img class="player_image view-player" data-id="{{@$player['pid']}}"
                  @if(@$player['is_local_image'] == 0)
                    src="{{ @$player['image_path'] }}"
                  @elseif(@$player['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$player['local_image_path'])))
                    src="{{ asset('uploads/player-images/'.@$player['local_image_path']) }}"
                  @else
                    src ="{{ asset('assets/images/no-image.jpeg')}}" 
                  @endif
                  ></td>
      
                <td class="view-player" data-id="{{@$player['pid']}}" data-teamid="{{@$player['team_id']}}">
                  {{ @$player['football_index_common_name'] != "" ? @$player['football_index_common_name'] : @$player['common_name'] }} - {{$player['football_index_position']}}
                <br/>
                @php
                  $colorCode = "";
                  $fontcolor = "";
                  $playerScore = @number_format(@$player['total_score']); 
                @endphp
                @if(@$playerScore <= 30)
                  @php
                  $colorCode = "#FF0000";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @elseif(@$playerScore >= 31 && @$playerScore <= 70)
                  @php
                  $colorCode = "#FF5733";
                  $fontcolor = "#FFFFFF"; 
                  @endphp
                @elseif(@$playerScore >= 71 && @$playerScore <= 110)
                  @php
                  $colorCode = "#eacd0e";
                  $fontcolor = "#000";
                  @endphp
                @elseif(@$playerScore >= 111 && @$playerScore <= 150)
                  @php
                  $colorCode = "#FFFF00";
                  $fontcolor = "#000";
                  @endphp
                @elseif(@$playerScore >= 151 && @$playerScore <= 180)
                  @php
                  $colorCode = "#aad77d";
                  $fontcolor = "#FFFFFF"; 
                  @endphp
                @elseif(@$playerScore >= 180)
                  @php
                  $colorCode = "#39a02b";
                  $fontcolor = "#FFFFFF";
                  @endphp
                @endif
                <div style="background:{{$colorCode}};color:{{ $fontcolor }}">
                  <span class="pb_player_score">{{ @number_format($player['total_score'],0.5) }}</span>
                </div>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td>No Api Data found.</td>
            </tr>
            @endif
            </tbody> 
        </table>
    </div>


</div>