<div class="row" id="view_all_players">
  <div class="col-sm-6 detail-player">
    <table class="table table-bordered firstplayer-list">
      <tbody>
        @if(@$all_players)
        @foreach(@$all_players as $player)
        <tr>
          <td class="players-image-logo"><img class="player_image view-player" data-id="{{@$player->id}}" data-teamid="{{@$player->team_id}}"
            @if(@$player->is_local_image == 0)
            src="{{ @$player->image_path }}"
            @elseif(@$player->is_local_image == 1 && file_exists(public_path('uploads/player-images/'.@$player->local_image_path)))
            src="{{ asset('uploads/player-images/'.@$player->local_image_path) }}"
            @else
            src ="{{ asset('assets/images/no-image.jpeg')}}" 
            @endif
            >
          </td>
          <td class="view-player" data-id="{{@$player->id}}" data-teamid="{{@$player->team_id}}">{{ @$player->football_index_common_name != "" ? @$player->football_index_common_name : @$player->common_name }} - {{$player->football_index_position}}
            <br/>
            @php
            $colorCode = "";
            $fontcolor = "";
            $playerScore = @number_format(@$player->total_score); 
            @endphp
            @if(@$playerScore <= 30)
            @php
            $colorCode = "#FF0000";
            $fontcolor = "#FFFFFF";
            @endphp
            @elseif(@$playerScore >= 31 && @$playerScore <= 70)
            @php
            $colorCode = "#FF5733";
            $fontcolor = "#FFFFFF";
            @endphp
            @elseif(@$playerScore >= 71 && @$playerScore <= 110)
            @php
            $colorCode = "#eacd0e";
            $fontcolor = "#000";
            @endphp
            @elseif(@$playerScore >= 111 && @$playerScore <= 150)
            @php

            $colorCode = "#FFFF00";
            $fontcolor = "#000";
            @endphp
            @elseif(@$playerScore >= 151 && @$playerScore <= 180)
            @php
            $colorCode = "#aad77d";
            $fontcolor = "#FFFFFF";
            @endphp
            @elseif(@$playerScore >= 181)
            @php
            $colorCode = "#39a02b";
            $fontcolor = "#FFFFFF";
            @endphp
            @endif
            <div style="background:{{$colorCode}}; color:{{ $fontcolor }};">
              <span class="pb_player_score">{{ @number_format($player->total_score,0.5) }}</span>
            </div>
          </td>
        </tr>
        @endforeach
        @endif
      </tbody> 
    </table>
  </div>
</div>