    @if(@$fixtures_list)
    <input type="hidden" id="localteam_id" value="{{@$fixtures_list[0]['localteam_id']}}">
    <input type="hidden" id="visitorteam_id" value="{{@$fixtures_list[0]['visitorteam_id']}}">
    <table>
        @foreach(@$fixtures_list as $list)
        <tr>
            <td><strong>{{$list->local_team}} v {{$list->visitor_team}} </strong></td>
            <td class="list_player_btn"> <a class="get-players-list" href="javascript:void(0);" data-visitorteam_id="{{$list->visitorteam_id}}" data-localteam_id="{{$list->localteam_id}}">LIST PLAYERS</a></td>
        </tr>
        @endforeach
    </table>                           
    @endif   


