@extends('layouts.app')

@section('content')

<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Fixtures</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Fixture</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<!-- Main Content -->
<main class="main-content"> 
    <!-- Upcoming match slider -->
    @include('front.common.upcoming-match-slider')
    <!-- Upcoming match slider -->

    <!-- Match -->
    <div class="theme-padding white-bg">
        <?php $updatedmatches = json_decode(json_encode($updatedmatches), True); ?>
        <div class="container">
            <div class="row">

                <div class="matches-shedule-holder">                
                    <!-- match Contenet -->
                    <div class="">
                        @if(@$fixturesCount != 0)
                        <!-- Page Heading banner -->

                        <div class="container">
                            <div class="team_section_main">
                                <div class="col-sm-8 team-ad-set">
                                    <div class="row">
                                        <div class="col-sm-3">
                                              @php 
                                        $position = ""; 
                                        $topPlayer = "";
                                        $title = "";
                                        @endphp

                                        @if ($totalMatches >= 1 && $totalMatches <= 4)                                             
                                        @php 
                                        $title = "Bronze Match Day";
                                        $position = "2p"; 
                                        $topPlayer = "2p";
                                        $colorCode = "#CF5A0A";
                                        @endphp
                                        @elseif ($totalMatches >= 5 && $totalMatches <= 14)

                                        @php 
                                        $title = "Silver Match Day";
                                        $position = "4p"; 
                                        $topPlayer = "4p";
                                        $colorCode = "#8C909C";
                                        @endphp

                                        @elseif ($totalMatches >= 15)

                                        @php
                                        $title = "Gold Match Day";
                                        $position = "8p"; 
                                        $topPlayer = "8p";
                                        $colorCode = "#DDAB27";
                                        @endphp

                                        @else

                                        @php
                                        $title = "Media Day";
                                        $colorCode = "#439F05";                                        
                                        @endphp
                                        @endif
                                        @foreach($updatedmatches as $umatch)
                                            @if(date('F d Y',strtotime($startDate) ) ==  date('F d Y', strtotime($umatch['date'])))
                                                @php
                                                    $position = ($umatch['top_positional_performance_player'])*100; 
                                                    $topPlayer = ($umatch['top_performance_player'])*100;
                                                    $goalkeeper = ($umatch['goalkeeper'])*100;
                                                    $position=$position.'p';
                                                    $topPlayer=$topPlayer.'p';
                                                    $goalkeeper=$goalkeeper.'p';
                                                @endphp
                                                @if($umatch['type_of_day'] == 1) 
                                                    @php 
                                                        $title = "Bronze Match Day";
                                                        $colorCode = "#CF5A0A";
                                                    @endphp
                                                @endif
                                                @if($umatch['type_of_day'] == 2)
                                                @php 
                                                    $title = "Silver Match Day";
                                                    $colorCode = "#8C909C";
                                                @endphp
                                                @endif
                                                @if($umatch['type_of_day'] == 3)
                                                @php
                                                    $title = "Gold Match Day";
                                                    $colorCode = "#DDAB27";
                                                @endphp
                                                @endif
                                                @if($umatch['type_of_day'] == 4)
                                                 @php
                                                    $title = "Media Day";
                                                    $colorCode = "#DDAB27";
                                                 @endphp
                                                @endif 
                                                @if($umatch['type_of_day'] == 5)
                                                 @php
                                                    $title = "Media Madness Top 5";
                                                    $colorCode = "#DDAB27";
                                                 @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        <div class="triple_box_media" style="background-color: {{$colorCode}}">
                                            {{$title}} {{date('F d Y',strtotime($startDate) )}}
                                        </div>        

                                        </div>
                                        <div class="col-sm-2">
                                            <div class="box_top_player">
                                            Top Player<br><span class="top-player">{{ $topPlayer }}</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                             <div class="box_position">
                                            Positional<br><span class="positional">{{ $position }} </span>            
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                             <div class="box_gk">
                                            GK<br><span class="gk">{{ $goalkeeper }} </span>            
                                            </div>
                                        </div>
                                       <div class="col-sm-2">
                                            <div class="box_fixtures">
                                            {{@$fixturesCount}} {{ @$fixturesCount > 1 ? 'Fixtures' : 'Fixture' }}<br>{{$totalPlayers}} Players             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 sec-team-ad">
                                    <div class="row">
                                      <div class="col-sm-4 v-t-play">
                                        <div class="view_team" id="vteam" value="show">
                                            VIEW TEAM PLAYERS             
                                        </div>
                                      </div>
                                      <div class="col-sm-4 v-t-play">
                                         <div class="view_form" id="vfteam" value="show">
                                            VIEW FORM PLAYERS            
                                        </div>
                                      </div>
                                      <div class="col-sm-4 v-t-play">
                                        <div class="view_form pb_top" id="vallpl" value="show">
                                            VIEW All PLAYERS            
                                        </div>
                                      </div>  
                                    </div> 
                                </div>
                                <div class="row">
                                </div>
                            </div>   
                        </div>
                        <!-- Page Heading banner -->
                        <div class="col-lg-2 leagues-list">
                            <!-- Matches Dates Shedule -->
                            <div class="matches-dates-shedule">
                                @if(@$fixturesCount != 0)
                                <table>
                                    @foreach($fixtures as $fixture)
                                    <tr>
                                        <td data-league_id="{{$fixture->league_id}}" class="leagues">{{$fixture->league}}</td>                                       
                                    </tr>
                                    @endforeach
                                </table>                           
                                @else
                                <h4>Sorry no Matches found..!</h4>
                                @endif                           
                            </div>
                            <!-- Matches Dates Shedule -->                            
                        </div>
                        <div class="col-lg-5 list-player" id="fixtures-data">
                            @include('front.fixtures.fixtures-list')
                        </div>
                        <div class="col-lg-5 col-lg-offset-1" id="data">
                            <div class="col-lg-12">
                                @include('front.fixtures.team-players-list')
                            </div>
                            <div class="col-lg-12">
                                @include('front.fixtures.all-players')
                            </div>
                        </div>
                        <div class="col-lg-5 col-lg-offset-1" id="data_all">
                            <div class="col-lg-12">
                                @include('front.fixtures.all-players')
                            </div>
                        </div>
                        <!-- match Contenet -->
                       
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- Match -->

</main>
@if(\Auth::user()->isPremium())
<div class="modal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg new-modal-dialog-v" role="document">
        <div class="modal-content bg-set-t">
            <div class="modal-header new-edit-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="playerdata">
                <div id="fixturedeta">

  <div>

            </div>

        </div>
    </div>
</div>
    @endif
@endsection
@section('page_js')
<script type="text/javascript">
    $(document).on("click", ".view-player", function(){
    $id = $(this).attr("data-id");
    $teamid = $(this).attr("data-teamid");
    $("#exampleModalCenter").modal();
    $.ajax({
    url: "{{ route('viewplayer') }}?id=" + $id,
            success: function (response) {
            $("#playerdata").html(response);
            $('#id2').hide();
            }
    });
    })
    $(document).on("click", "#idlast", function(){
    $('#id1').show();
    $(this).addClass('pb_top');
    $('#idtop').removeClass('pb_top');
    $('#id2').hide();
    });
    $(document).on("click", "#idtop", function(){
    $('#id2').show();
    $(this).addClass('pb_top');
    $('#idlast').removeClass('pb_top');
    /*$('#idtop').css('background-color', '#192851');
     $('#idlast').css('background-color', '#808080');*/
    $('#id1').hide();
    });
    $(document).on("click", "#vfteam", function(){
        $('#view_form_players').show();
        $(this).addClass('pb_top');
        $('#vteam').removeClass('pb_top');
        $('#vallpl').removeClass('pb_top');
        $('#view_team_players').hide();
        $("#data_all").hide();
    });
    $(document).on("click", "#vteam", function(){
        $('#view_team_players').show();
        $(this).addClass('pb_top');
        $('#vfteam').removeClass('pb_top');
        $('#vallpl').removeClass('pb_top');
        $('#view_form_players').hide();
        $("#data_all").hide();
    });
    $(document).on("click", "#vallpl", function(){
        $('#view_team_players').hide();
        $('#view_form_players').hide();
        $('#data_all').show();
        $('#vteam').removeClass('pb_top');
        $('#vfteam').removeClass('pb_top');
        $(this).addClass('pb_top');
    });
    $(document).ready(function(){ 
     $load = $('#vallpl'); 
        $(document).on('click', '.show-filter', function(){
            $("#div1").fadeToggle("slow");  
        });
        $("button").click(function(){  
        });
    });



</script>
<script type="text/javascript">
    $("#data").hide();
    // GET FIXTURES LIST
    function getFixtures($date, $league_id) {
    $.ajax({
    url: "{{ route('get_fixtures_list') }}?date=" + $date + "&league_id=" + $league_id,
            success: function (response) {
            $("#data").hide();
            $("#fixtures-data").html(response);
//                var localteam_id = $("#localteam_id").val();
//                var visitorteam_id = $("#visitorteam_id").val();               
//                getPlayers(localteam_id, visitorteam_id);
            }
    });
    }

    $(document).on('click', '.leagues', function () {
    var league_id = $(this).attr("data-league_id");
    var date = "{{$startDate}}";
    getFixtures(date, league_id);
    });
    $(function () {
    var league_id = {{ @$fixtures[0]['league_id']}};
    var date = "{{$startDate}}";
    getFixtures(date, league_id);
    });
    $(document).on('click', '.get-players-list', function () {
    $localteam_id = $(this).attr("data-localteam_id");
    $visitorteam_id = $(this).attr("data-visitorteam_id");
    getPlayers($localteam_id, $visitorteam_id);
    });
    function getPlayers($localteam_id, $visitorteam_id) {
    $.ajax({
    url: "{{ route('get_team_players') }}?localteam_id=" + $localteam_id + "&visitorteam_id=" + $visitorteam_id,
            success: function (response) {
            $("#data_all").hide();
            $('#vteam').addClass('pb_top');
            $('#vfteam').removeClass('pb_top');
            $('#vallpl').removeClass('pb_top');
            $("#data").show();
            $("#data").html(response);
            }
    });
    }
    function getfinalplayerfixturedata(year){
        $.ajax({
            url: "{{ route('getfinalplayerfixturedata') }}?player_id=" + $id + "&teamid=" + $teamid + "&year=" + year,
            success: function (response) {
                $('#player_fixture_score').html(response);
            }
        });
    }

    function getfixturemonthforyear(year){
        $.ajax({
            url: "{{ route('getfixturemonthforyear') }}?player_id=" + $id + "&year=" + year,
            success: function (response) {
                var data = JSON.parse(response);
                var html = '<option>Select</option>';
                // console.log(response);
                var arrayLength = data.length;
                for (var i = 0; i < arrayLength; i++) {
                    html+= '<option value="'+data[i]+'">'+getMonthName(data[i]-1)+'</option>';
                }
                $('#season_month').html(html);
            }
        });
    }

    function getfixtureoppositeteam(month){
        var year = $('#season_year').val();
        $.ajax({
            url: "{{ route('getfixtureoppositeteam') }}?player_id="+$id+"&teamid=" + $teamid + "&year=" + year + "&month="+month,
            success: function (response) {
                var data = JSON.parse(response);
                var html = '<option>Select</option>';
                var arrayLength = data.length;
                for (var i = 0; i < arrayLength; i++) {
                    html+= '<option value="'+data[i].starting_at+'" data-id="'+data[i].teamid+'" data-date="'+data[i].starting_at+'">'+data[i].teamname+' ('+data[i].played_at+')</option>';
                }
                $('#season_fixture').html(html);
            }
        });
    }

    function getfinalplayerfixturescore(date){
        var year = $('#season_year').val(), month = $('#season_month').val();
        $.ajax({
            url: "{{ route('getfinalplayerfixturescore') }}?player_id="+$id+"&date="+date,
            success: function (response) {

                $('#player_fixture_score').html(response.score).css('background', response.color);
                $('#player_fixture_score').css('color', response.fontcolor);
            }
        });
    }
    getMonthName = function (v) {
        var n = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return n[v]
    }
</script>
@endsection
@section('page_css')
<style type="text/css">
    #data_all{
        background: #d6d7d9;
        font-weight: 700;
        width: 39.5%;
        padding: 0;
        margin-left: 50px;
    }
</style>
@endsection