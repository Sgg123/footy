
<div class="row">
  <div class="col-sm-3">
    <div class="player-v">

      <img 
      @if(@$playerInfo['is_local_image'] == 0)
      src="{{ @$playerInfo['image_path'] }}"
      @elseif(@$playerInfo['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$playerInfo['local_image_path'])))
      src="{{ asset('uploads/player-images/'.@$playerInfo['local_image_path']) }}"
      @else
      src ="{{ asset('assets/images/no-image.jpeg')}}" 
      @endif
      >
    </div>
    <div class="set-player-btn">
      @php
      $colorCode = "";
      $title ="";
      @endphp
      @if(@$playerInfo->football_index_position == "Attacker" || @$playerInfo->football_index_position == "Forward")

      @php
      $colorCode = "#C9007B";
      $title ="Forward";
      @endphp

      @elseif(@$playerInfo->football_index_position == "Defender")

      @php
      $colorCode = "#00A185";
      $title ="Defender";
      @endphp

      @elseif(@$playerInfo->football_index_position == "Midfielder")

      @php
      $colorCode = "#0070D1";
      $title ="Midfielder";
      @endphp

      @elseif(@$playerInfo->football_index_position == "Goalkeeper")

      @php
      $colorCode = "#540D6E";
      $title ="Goalkeeper";
      @endphp
      @endif
      <div class="fd-btn-py" style="background:{{$colorCode}}">
        {{$title}}
      </div>
     
        @php
        $colorCodes = "";
        $fontcolor ="";
        $playerScore = @number_format(@$playerInfo->total_score); 
        @endphp
        @if(@$playerScore <= 30)
        @php
        $colorCodes = "#FF0000";
        $fontcolor = "#FFFFFF";
        @endphp
        @elseif(@$playerScore >= 31 && @$playerScore <= 70)
        @php
        $colorCodes = "#FF5733";
        $fontcolor = "#FFFFFF";
        @endphp
        @elseif(@$playerScore >= 71 && @$playerScore <= 110)
        @php
        $colorCodes = "#eacd0e";
        $fontcolor = "#000";
        @endphp
        @elseif(@$playerScore >= 111 && @$playerScore <= 150)
        @php
        $colorCodes = "#FFFF00";
        $fontcolor = "#000";
        @endphp
        @elseif(@$playerScore >= 151 && @$playerScore <= 180)
        @php
        $colorCodes = "#aad77d";
        $fontcolor = "#FFFFFF";
        @endphp
        @elseif(@$playerScore >= 180)
        @php
        $colorCodes = "#39a02b";
        $fontcolor = "#FFFFFF";
        @endphp
        @endif
         <div class="avrage-btn" style="background:{{$colorCodes}};color:{{$fontcolor}}">
        <div class="av-pb">
          Average<br>PB Score
        </div>
        <div class="sc-wo">
         {{ @number_format($playerInfo->total_score ,0.5) }}
       </div>
     </div>
     <div class="game-btn-v">
      <div class="game-text">
       Games<br>Played 
     </div>
     <div class="game-con">
       {{ @$playerInfo->total_count }}
     </div>
   </div>


 </div>
</div>
@php
$playerObj = \App\Player::find(@$playerInfo->playerid);
@endphp
<div class="col-sm-9 up-c-set">
  <div class="player-v-text">
   <div class="col-sm-9 hedading-s"> <h2>{{ @$playerInfo->football_index_common_name }}</h2>
    <div class="fiture-b">
      <h3>{{@$playerObj->getTeam->name}} <br/> {{ @$fixture->league_name }}</h3>
    </div>
  </div>
  <div class="col-sm-3 image-sv-set">
   <div class="harry-img-v">
     <img src="{{ asset('assets/front/images/FIS-Club-Badge-new.png')}}" alt="">
   </div>
 </div>
@php $days=''; @endphp
@if ($diff_in_days == -1)
    @php $days = "No Data"; @endphp
@elseif ($diff_in_days == 0)
    @php $days = "Today"; @endphp
@elseif ($diff_in_days == 1)
    @php $days = $diff_in_days." Day"; @endphp
@elseif ($diff_in_days > 1)
    @php $days = $diff_in_days." Days"; @endphp
@endif
 <div class="next-f">
  <h3>Next Fixture: {{@$teamObj->name}} ({{$days}}) </h3>          
  </div>
</div>
<div class="rank-box row"> 
  <div class="col-sm-3 p-frd-set">
    <div class="frd-rank" style="background:{{$colorCode}}">
      @php
      $colorCode = "";
      $title ="";
      @endphp
      @if(@$playersector == "Attacker" ||@$playersector == 'Forward')
      @php
      $colorCode = "#d52cbb";
      $title ="FWD";
      @endphp
      @elseif(@$playersector == 'Midfielder')
      @php
      $colorCode = "#00008B";
      $title ="MID";
      @endphp
      @elseif(@$playersector == 'Defender')
      @php
      $colorCode = "#00A185";
      $title ="DEF";
      @endphp
      @elseif(@$playersector == 'Goalkeeper')
      @php
      $colorCode = "#540D6E";
      $title ="GK";
      @endphp
      @endif
      {{$title}}<br/>Rank
      <span>{{ $playerdetail}}</span>
    </div>
  </div>
  <div class="col-sm-9">
    <div class="row">
      <div class="col-sm-4 wins-p">
        <div class="overl-1">
          <div class="sep-overrall">
            OVERALL<br>RANK
          </div>
          <span>{{ $overallrankdetail}}</span>
        </div>
      </div>
      <div class="col-sm-4 wins-p">
        <div class="overl-1">
          <div class="sep-overrall">
            @php
            $title ="";
            @endphp
            @if(@$playerInfo->football_index_position == "Attacker" ||@$playerInfo->football_index_position == 'Forward')
            @php
            $title ="FWD";
            @endphp
            @elseif(@$playerInfo->football_index_position == 'Midfielder')
            @php
            $title ="MID";
            @endphp
            @elseif(@$playerInfo->football_index_position == 'Defender')
            @php
            $title ="DEF";
            @endphp
            @elseif(@$playerInfo->football_index_position == 'Goalkeeper')
            @php
            $title ="GK";
            @endphp
            @endif
            TOP {{$title}}<br>WINS
          </div>
          <span>{{@$playerInfo->positional_wins}}</span>
        </div>
      </div>
      <div class="col-sm-4 wins-p">
        <div class="overl-1">
          <div class="sep-overrall">
            TOP PLAYER<br>WINS
          </div>
          <span>{{ @$playerInfo->top_player_wins}}</span>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="row media-wind-v">

          <div class="col-sm-3 ex-media">
            <div class="u-media-f">MEDIA PLACINGS</div>

          </div>
          <div class="col-sm-3">
            <div class="onest-p">
             1st Place<br>
             <span>{{@$playerInfo->mb_first_count}}</span>
           </div>
         </div>
         <div class="col-sm-3">
           <div class="onest-p">
             2nd Place<br>
             <span>{{@$playerInfo->mb_second_count}}</span>
           </div>
         </div>
         <div class="col-sm-3">
          <div class="onest-p">
           3rd Place<br>
           <span>{{@$playerInfo->mb_third_count}}</span>
         </div>
       </div>
     </div>
   </div>
 </div>    
</div>
</div>
</div>
</div>
<div class="row">
  <div class="last-f-s last-b">
    <div class="lst-s lst-s-sec pb_top" id="idlast">
      <a onclick="show('id1');">
        Last 5 Scores
      </a>
    </div>
    <div class="top-s top-s-sec" id="idtop">
      <a onclick="show('id2');">
        Top 5 Scores
      </a>
    </div>    
    <div class="top-s top-s-sec div_yield_pop">
      <span>
        @if(@$playerInfo->dividend_yield > 0)
        Dividend Yield : £{{ @$playerInfo->dividend_yield }}
        @else
        Dividend Yield : £0.00
        @endif
      </span>
    </div>    
  </div>
</div>
<div id="id1">
  <div class="row">
    <div class="different-b">                          
     @if(!$pbdemo->isEmpty())
     @foreach ($pbdemo as $object)
     @php
     $colorCode = "";
     $fontcolor ="";
     @endphp
     @if(@$object->score <= 30)
     @php
     $colorCode = "#FF0000";
     @endphp
     @elseif(@$object->score >= 31 && @$object->score <= 70)
     @php
     $colorCode = "#FF5733";
     @endphp
     @elseif(@$object->score >= 71 && @$object->score <= 110)
     @php
     $colorCode = "#eacd0e";
     $fontcolor = "#000";
     @endphp
     @elseif(@$object->score >= 111 && @$object->score <= 150)
     @php
     $colorCode = "#FFFF00";
     $fontcolor = "#000";
     @endphp
     @elseif(@$object->score >= 151 && @$object->score <= 180)
     @php
     $colorCode = "#aad77d";
     @endphp
     @elseif(@$object->score >= 180)
     @php
     $colorCode = "#39a02b";
     @endphp
     @endif
     <div class="main-d-v-set" >
      <div class="first-d-v" style="background:{{$colorCode}}; color:{{$fontcolor}}">
        <span>{{ $object->score }}</span>
      </div>
      <p>{{ date('d M y', strtotime($object->date)) }}</p>
    </div>
    @endforeach
    @else
    <div class="data-not">No Records Found</div>
    @endif
  </div>
</div>
</div>
<div id="id2">
  <div class="row">
    <div class="different-b">
     @if(!$pbhighestscore->isEmpty())
     @foreach ($pbhighestscore as $object)

     @php
     $colorCode = "";
     $fontcolor = "";
     @endphp
     @if(@$object->total_score <= 30)
     @php
     $colorCode = "#FF0000";
     @endphp
     @elseif(@$object->total_score >= 31 && @$object->total_score <= 70)
     @php
     $colorCode = "#FF5733";
     @endphp
     @elseif(@$object->total_score >= 71 && @$object->total_score <= 110)
     @php
     $colorCode = "#eacd0e";
     $fontcolor = "#000";
     @endphp
     @elseif(@$object->total_score >= 111 && @$object->total_score <= 150)
     @php
     $colorCode = "#FFFF00";
     $fontcolor = "#000";
     @endphp
     @elseif(@$object->total_score >= 151 && @$object->total_score <= 180)
     @php
     $colorCode = "#aad77d";
     @endphp
     @elseif(@$object->total_score >= 180)
     @php
     $colorCode = "#39a02b";
     @endphp
     @endif
     <div class="main-d-v-set">
       <div class="first-d-v" style="background:{{$colorCode}}; color:{{$fontcolor}}">
        <span>{{ $object->total_score }}</span>
      </div>
      <p>{{ date('d M y', strtotime($object->date)) }}</p>
    </div>
    @endforeach
    @else
    <div class="data-not">No Records Found</div>
    @endif
  </div>
</div>
</div>
<div class="row search-f-box">
  <div class="col-sm-4">
    <div class="f-search-f show-filter" style="cursor: pointer;">
      Search Fixtures
    </div>
  </div>
  <div class="col-sm-4">
    
  </div>
  <div class="col-sm-4 ft-tf-set">
    <div class="ft-play">
      @if($playerInfo->injured == "true")
      @php $pl_status="Unfit to play"; @endphp
      <img src="{{ asset('assets/front/images/plus.png') }}">
      @else
      @php $pl_status="Fit to play"; @endphp
      <img src="{{ asset('assets/front/images/minus.png') }}">
      @endif
    </div>
    <div class="ft-play-con">
      <span>{{ $pl_status }}</span>    
    </div>
  </div>
</div>
<div class="row">
<div class="col">
<div id="div1" class="col-sm-12" style="display: none; background-color: white;">
  <div class="row">
    <div class="col-md-5">
      <div class="row">
        <div class="form-group row season_dropdown">
          <label for="sort" class="col-sm-5 control-label"> SEASON </label>
          <div class="col-sm-7">
            <select class="form-control" id="season_year" onchange="getfinalplayerfixturedata(this.value);">
              <option>Select</option>
              @foreach(@$pbzsearch as $searchfix)
               
                <option value="{{ @$searchfix->year }}">{{ @$searchfix->year-1 }}/{{ @$searchfix->year }}</option>
               
              @endforeach
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-7">
      <div class="selected-f" id="player_fixture_score">
        Selected Fixture Score
      </div>
    </div>
  </div>
</div>
</div>

