@extends('layouts.app')

@section('page_css')
@endsection

@section('content')
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>{{ @$page->title }}</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Upgrade</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('uploads/pages/'.@$page->image_path) }}">
</div>


<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                @include('front.pages.sidebar')
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-8">

                    <div class="team-detail-content theme-padding-bottom">
                        <h1 class="text-center text-uppercase text-primary">Interactive Database</h1>
                        <div class="text-center">
                            <img src="{{ asset('assets/images/interactive_database_upgrade_screen.jpg')}}" alt="Database" style="max-width: 100%;">
                        </div>
                        <br>
                        <table class="MsoTableLightShadingAccent1" style="border-collapse: collapse; border: medium none;" cellspacing="0" cellpadding="0" border="1" align="center">
                            <tbody>
                                <tr style="mso-yfti-irow:12;height:45.5pt">
                                  <td style="width:155.95pt;border:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                                  background:#D3DFEE;mso-background-themecolor:accent1;mso-background-themetint:
                                  63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="208">
                                  <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                  margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                  mso-yfti-cnfc:68" align="center"><b><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
                                  color:#365F91;mso-themecolor:accent1;mso-themeshade:191">Interactive Database
                                  of All Media and Performance Stats</span></b></p>
                                  </td>
                                  <td style="width:111.7pt;border-left:none;
                                  border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                  mso-border-alt:solid windowtext .5pt;background:#D3DFEE;mso-background-themecolor:
                                  accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="149">
                                  <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                  margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                  mso-yfti-cnfc:64" align="center"><b style="mso-bidi-font-weight:normal"><span style="font-size:20.0pt;mso-bidi-font-size:11.0pt;color:#365F91;mso-themecolor:
                                  accent1;mso-themeshade:191">PREMIUM</span></b></p>
                                  </td>
                                  <td style="width:200.3pt;border-left:none;
                                  border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                  mso-border-alt:solid windowtext .5pt;background:#D3DFEE;mso-background-themecolor:
                                  accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="267">
                                  <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;margin-bottom:.0001pt;
                                  mso-add-space:auto;line-height:normal;mso-yfti-cnfc:64"><span style="color:#365F91;mso-themecolor:accent1;mso-themeshade:191">Fully
                                  interactive Database of every player and team including Buzz wins, Dividend
                                  Yields and Dates. All filterable by Seasons, Dates, Leagues and Positions.</span></p>
                                  </td>
                                 </tr>
                            </tbody>
                        </table>
                        <br>
                        <div class="text-center">
                            <a href="{{url('/upgrade-account')}}" class="text-uppercase btn btn-primary">Click here to upgrade</a>
                        </div><br>
                        <div class="text-center" style="font-weight: bold;font-size: 13pt;">
                         PREMIUM MEMBERSHIP ONLY £5 A MONTH. THE FIRST 30 DAYS ARE FREE!
                        </div>
                    </div>
                </div>
                <!-- Match Result Contenet -->
            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
@endsection

@section('page_js')

@endsection
