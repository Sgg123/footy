@extends('layouts.app')
@section('no_follow')
<meta name="robots" content="noindex, nofollow">
@endsection
@section('content')

<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Database</h2>
        <ul class="breadcrumbs">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Database</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{ asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg') }}">
</div>

<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">
                <div class="side-d-hiden">

                <!-- Aside -->
                @include('front.pages.sidebar')
                <!-- Aside -->
  </div>
                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-12">
                    <div class="row">
                        <div class="col-md-12 player-set-m">
                            <div class="form-group col-md-3">
                                <select class="form-control fg-py-drop" id="leagues_filter">
                                    <option value="">Select League</option>
                                    @if(!empty(@$leagues))
                                    @foreach(@$leagues as $league)
                                    <option value="{{@$league->id}}">{{@$league->name}}</option>
                                    @endforeach
                                    @endif

                                </select>
                            </div>
                            <div class="form-group col-md-3 py-drop">
                                <select class="form-control fg-py-drop" id="time_filter">
                                    <option value="" >Select Period</option>
                                    <option value="all" data-type="day">All Time</option>
                                    <option value="30" data-type="day">Last 30 days</option>
                                    <option value="100" data-type="day">Last 100 days</option>
                                    @if(!empty(@$seasons))
                                    @foreach(@$seasons as $season)
                                    <option value="{{@$season->id}}" data-type="season" data-start_date="{{@$season->start_date}}" data-end_date="{{@$season->end_date}}">{{@$season->name}}</option>
                                    @endforeach
                                    @endif
                                    @if(!empty(@$offSeasons))
                                    @foreach(@$offSeasons as $season)
                                    <option value="{{@$season->id}}" data-type="season" data-start_date="{{@$season->off_season_start_date}}" data-end_date="{{@$season->off_season_end_date}}">{{@$season->name}} - Off Season</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="text" class="form-control" name="from" id="start_date_datepicker" placeholder="From" value="{{ @$from }}" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="text" class="form-control" id="end_date_datepicker" name="to" placeholder="To" value="{{ @$to }}" readonly>
                            </div>                           
                            <div class="form-group col-md-2 pull-right">
                                <button type="button" id="search_datewise" class="btn red-btn">Search</button>
                            </div>
                            <input type="hidden" id="start_date" value="">
                            <input type="hidden" id="end_date" value="">
                            <input type="hidden" id="leagueId" value="">
                        </div>
                    </div>
                    <!-- Piont Table -->
                    <div class="macth-fixture">

                        <div class="last-matches styel-3">
                            <!-- Table Style 3 -->
                            <div class="table-responsive padding-top-70">
                                <!--<img src="{{ asset('assets/front/images/Interactive-Database-Full-Table-Combined.jpg') }}" class="database-img">-->                                                              
                            </div>
                        </div>
                    </div>
                    <!-- Piont Table -->

                    <div class="database">
                        <div class="row">
                            <div class="col-sm-5 col-xs-12 d-set-p">
                                <div class="dashbord-f">
                                    <ul class="first-dasbord">   
                                        <li class="search-d">
                                            <div class="dropdown search-dropdown">
                                                <input type="text" placeholder="Search..." id="search-token">
                                            </div>   
                                        </li>
                                        <li class="Players-d data-active" id="players_load_btn"><a>Players</a></li>
                                        <li class="teams-d data-inactive" id="teams_load_btn"><a>Teams</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-7 col-xs-12 d-bord-pd d-set-p-sec">
                                <ul class="second-dasbord">   
                                    <li class="mb-s-w"><span class="mb-w">MB<br>Winners</span><span class="w-b-count">{{@$totalMbWinners}}</span></li>
                                    <li class="mb-s-w"><span class="mb-w">PB<br>Winners</span><span class="w-b-count">{{@$totalPbWinners}}</span></li>
                                    <li class="mb-s-w"><span class="mb-w">Team<br>Winners</span><span class="w-b-count">{{@$totalTeamWinners}}</span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="row combinate-v">
                            <div class="col-sm-5 col-xs-12 d-set-p">
                                <div class="dashbord-f">
                                    <ul class="combinate combinate-player">   
                                        <li class="combinate1 data-active-sub" id="players_combined_load_btn"><a class="combined-a data-active-sub-a">Combined</a></li>
                                        <li class="Media1 data-inactive-sub" id="players_media_load_btn"><a class="media-a data-inactive-sub-a">Media</a></li>
                                        <li class="Performance data-inactive-sub" id="players_performance_load_btn"><a class="performance-a data-inactive-sub-a">Performance</a></li>
                                    </ul>
                                    <ul class="combinate combinate-team">   
                                        <li class="combinate-team data-active-sub"><a class="data-active-sub-a">Combined</a></li>                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-7 col-xs-12 d-bord-pd d-set-p d-set-p-sec">

                                <input type="hidden" value="dividend" id="orderBy">
                                <input type="hidden" value="" id="position">
                                <ul class="showing-d">   
                                    <li class="showing-d-f">
                                        <select class="positions-filter us-position-f" >
                                            <option value="">Select Position</option>
                                            <option value="Forward">Forward</option>
                                            <option value="Midfielder">Midfielder</option>
                                            <option value="Defender">Defender</option>                                            
                                            <option value="Goalkeeper">Goalkeeper</option>                                            
                                        </select>
                                    </li>
                                    <li class="view-us">View As</li>
                                    <li class="wins-v" id="wins-filter"><a class="wins filter-inactive">Wins</a></li>
                                    <li class="dividends" id="dividend-filter"><a class="dividend filter-active">Dividends</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive start-ply-w">
                        <div id="combined_player_table">
                            <table class="table datateble_player" id="combined_player" style="width: 100%;">
                                <thead>
                                    <tr class="mb-plauer-set">
                                        <th class="r-mb-v r-mb-v-sec">R</th>
                                        <th class="r-mb-v">Player</th>
                                        <th>MB 1st<br> Place</th>
                                        <th>MB 2nd<br> Place</th>
                                        <th>MB 3rd<br> Place</th>
                                        <th>PB Best<br> Player</th>
                                        <th>PB<br> Positional</th>
                                        <th class="since-t">Date of<br>last Placing</th>
                                        <th class="since-t">Dividend<br>Yield</th>
                                    </tr>

                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <div id="media_player_table">
                            <table class="table datateble_media_player" id="media_player"  style="width: 100%;">
                                <thead>
                                    <tr class="mb-plauer-set">
                                        <th class="r-mb-v r-mb-v-sec">R</th>
                                        <th class="r-mb-v">Player</th>
                                        <th>MB 1st<br> Place</th>
                                        <th>MB 2nd<br> Place</th>
                                        <th>MB 3rd<br> Place</th>                                    
                                        <th class="since-t">Date of<br>last Placing</th>
                                        <th class="since-t">Dividend<br>Yield</th>
                                    </tr>

                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <div id="performance_player_table">
                            <table class="table datateble_performance_player" id="performance_player" style="width: 100%;">
                                <thead>
                                    <tr class="mb-plauer-set">
                                        <th class="r-mb-v r-mb-v-sec">R</th>
                                        <th class="r-mb-v">Player</th>
                                        <th>PB Best<br> Player</th>
                                        <th>PB<br> Positional</th>                                  
                                        <th>Average<br> PB Score</th>                                  
                                        <th>Games Played</th>                                  
                                        <th class="since-t">Date of<br>last Placing</th>
                                        <th class="since-t">Dividend<br>Yield</th>
                                    </tr>

                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <div id="combined_team_table">
                            <table class="table datateble_team" id="combined_team" style="width: 100%;">
                                <thead>
                                    <tr class="mb-plauer-set">
                                        <th class="r-mb-v r-mb-v-sec">R</th>
                                        <th class="r-mb-v">Team</th>
                                        <th>MB 1st<br> Place</th>
                                        <th>MB 2nd<br> Place</th>
                                        <th>MB 3rd<br> Place</th>
                                        <th>PB Best<br> Player</th>
                                        <th>PB<br> Positional</th>                                
                                        <th class="since-t">Date of<br>last Placing</th>
                                        <th class="since-t">Dividend<br>Yield</th>
                                    </tr>

                                </thead>
                                <tbody>

                                </tbody>
                            </table>         
                        </div>

                    </div>
                    <!-- Match Result Contenet -->

                </div>
            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->


@endsection
@section('page_js')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
@include('front.database.common-script')
@endsection


