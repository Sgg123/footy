
<script type="text/javascript">
    var active_table = "combinedPlayer";
    $('.combinate-team').hide();
    $(function () {
        // DATEPICKER
        $("#start_date_datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            //maxDate: 0,
            onClose: function (selectedDate) {
                $("#end_date_datepicker").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end_date_datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            //maxDate: 0,
            onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
        // DATATABLES
        $('#combined_team_table').hide();
        $('#performance_player_table').hide();
        $('#media_player_table').hide();
        combinedPlayer = $('#combined_player').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 20,
            searching: false,
            "sDom": "tpr",
            "dom": 'difrtp',
            "pagingType": 'simple',
            ordering: true,
            order: [8, 'desc'],
            "bFilter": true,
            ajax: {
                url: '{!! route("database.combined.data") !!}',
                data: function (d) {
                    d.position = $("#position").val();
                    d.search_token = $("#search-token").val();
                    d.orderBy = $("#orderBy").val();
                    d.end_date = $("#end_date").val();
                    d.start_date = $("#start_date").val();
                    d.leagueId = $("#leagueId").val();
                },
            },
            columns: [
                {data: 'id', name: 'id', 'sClass': 'mb-mv-start', orderable: false, 'targets': [8], 'orderData': [8], "orderSequence": ["desc", "asc"]},
                {data: 'football_index_common_name', name: 'football_index_common_name', 'sClass': 'b-player-b', orderable: true, "orderSequence": ["desc","asc"]},
                {data: 'mb_first', name: 'mb_first', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'mb_second', name: 'mb_second', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"], },
                {data: 'mb_third', name: 'mb_third', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'top_player', name: 'top_player', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'positional', name: 'positional', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'last_mb_win_date', name: 'last_mb_win_date', 'sClass': 'mb-mv-start media_player_lastwin', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'combined_dividend', name: 'combined_dividend', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
            ]
        });
        mediaPlayer = $('#media_player').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 20,
            searching: false,
            "sDom": "tpr",
            "dom": 'difrtp',
            "pagingType": 'simple',
            ordering: true,
            order: [6, 'desc'],
            "bFilter": true,
            ajax: {
                url: '{!! route("database.media.data") !!}',
                data: function (d) {
                    d.position = $("#position").val();
                    d.search_token = $("#search-token").val();
                    d.orderBy = $("#orderBy").val();
                    d.end_date = $("#end_date").val();
                    d.start_date = $("#start_date").val();
                    d.leagueId = $("#leagueId").val();
                },
            },
            columns: [
                {data: 'id', name: 'id', 'sClass': 'mb-mv-start', orderable: false},
                {data: 'football_index_common_name', name: 'football_index_common_name', 'sClass': 'b-player-b', orderable: true, "orderSequence": ["asc", "desc"]},
                {data: 'mb_first', name: 'mb_first', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'mb_second', name: 'mb_second', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'mb_third', name: 'mb_third', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'last_mb_win_date', name: 'last_mb_win_date', 'sClass': 'mb-mv-start media_lastwin', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'media_dividend', name: 'media_dividend', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
            ]
        });
        performancePlayer = $('#performance_player').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 20,
            searching: false,
            "sDom": "tpr",
            "dom": 'difrtp',
            "pagingType": 'simple',
            ordering: true,
            order: [7, 'desc'],
            "bFilter": true,
            ajax: {
                url: '{!! route("database.performance.data") !!}',
                data: function (d) {
                    d.position = $("#position").val();
                    d.search_token = $("#search-token").val();
                    d.orderBy = $("#orderBy").val();
                    d.end_date = $("#end_date").val();
                    d.start_date = $("#start_date").val();
                    d.leagueId = $("#leagueId").val();
                },
            },
            columns: [
                {data: 'id', name: 'id', 'sClass': 'mb-mv-start', orderable: false},
                {data: 'football_index_common_name', name: 'football_index_common_name', 'sClass': 'b-player-b', orderable: true, "orderSequence": ["asc", "desc"]},
                {data: 'top_player', name: 'top_player', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'positional', name: 'positional', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'average_pb_score', name: 'average_pb_score', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'games_played', name: 'games_played', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'last_win_date', name: 'last_win_date', 'sClass': 'mb-mv-start performance_lastwin', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'performance_dividend', name: 'performance_dividend', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
            ]
        });
        combinedTeam = $('#combined_team').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 20,
            searching: false,
            "sDom": "tpr",
            "dom": 'difrtp',
            "pagingType": 'simple',
            ordering: true,
            order: [8, 'desc'],
            "bFilter": true,
            ajax: {
                url: '{!! route("database.team.data") !!}',
                data: function (d) {
                    d.search_token = $("#search-token").val();
                    d.orderBy = $("#orderBy").val();
                    d.end_date = $("#end_date").val();
                    d.start_date = $("#start_date").val();
                    d.leagueId = $("#leagueId").val();
                },
            },
            columns: [
                {data: 'id', name: 'rank', 'sClass': 'mb-mv-start', orderable: false},
                {data: 'name', name: 'name', 'sClass': 'b-player-b team_name', orderable: true, "orderSequence": ["asc", "desc"]},
                {data: 'mb_first', name: 'mb_first', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'mb_second', name: 'mb_second', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'mb_third', name: 'mb_third', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'top_player', name: 'top_player', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'positional', name: 'positional', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'last_mb_win_date', name: 'last_mb_win_date', 'sClass': 'mb-mv-start team_lastwin', orderable: true, "orderSequence": ["desc", "asc"]},
                {data: 'combined_dividend', name: 'combined_dividend', 'sClass': 'mb-mv-start', orderable: true, "orderSequence": ["desc", "asc"]},
            ]
        });
    });
    $(document).on('click', '#players_load_btn', function () {
        addClass();
        addSubClass('combined');
        combinedPlayer.draw();
        active_table = "combinedPlayer";
        $('.showing-d-f').show();
        $('#combined_team_table').hide();
        $('#performance_player_table').hide();
        $('#media_player_table').hide();
        $('#combined_player_table').show();

    });
    $(document).on('click', '#players_combined_load_btn', function () {
        addClass();
        addSubClass('combined');
        combinedPlayer.draw();
        active_table = "combinedPlayer";
        $('.showing-d-f').show();
        $('#combined_team_table').hide();
        $('#performance_player_table').hide();
        $('#media_player_table').hide();
        $('#combined_player_table').show();
    });
    $(document).on('click', '#players_media_load_btn', function () {
        addClass();
        addSubClass('media');
        mediaPlayer.draw();
        active_table = "mediaPlayer";
        $('.showing-d-f').show();
        $('#combined_team_table').hide();
        $('#performance_player_table').hide();
        $('#combined_player_table').hide();
        $('#media_player_table').show();
    });
    $(document).on('click', '#players_performance_load_btn', function () {
        addClass();
        addSubClass('performance');
        performancePlayer.draw();
        active_table = "performancePlayer";
        $('.showing-d-f').show();
        $('#combined_team_table').hide();
        $('#combined_player_table').hide();
        $('#media_player_table').hide();
        $('#performance_player_table').show();
    });
    $(document).on('click', '#teams_load_btn', function () {
        combinedTeam.draw();
        active_table = "combinedTeam";
        $('.combinate-team').show();
        $('.combinate-player').hide();
        $('.showing-d-f').hide();
        $('.teams-d').removeClass('data-inactive');
        $('.teams-d').addClass('data-active');
        $('.Players-d').removeClass('data-active');
        $('.Players-d').addClass('data-inactive');
        $('#combined_player_table').hide();
        $('#performance_player_table').hide();
        $('#media_player_table').hide();
        $('#combined_team_table').show();
        // CLEAR POSITION & DATES VALUE
        $("#position").val();
        $("#end_date").val();
        $("#start_date").val();
    });
    function addClass() {
        $('.combinate-team').hide();
        $('.combinate-player').show();
        $('.teams-d').removeClass('data-active');
        $('.teams-d').addClass('data-inactive');
        $('.Players-d').removeClass('data-inactive');
        $('.Players-d').addClass('data-active');
    }

    function addSubClass(token) {
//        $("#search-token").val("");
        if (token === 'combined') {
            $('.Media1').addClass('data-inactive-sub');
            $('.Media1').removeClass('data-active-sub');
            $('.combinate1').addClass('data-active-sub');
            $('.combinate1').removeClass('data-inactive-sub');
            $('.Performance').addClass('data-inactive-sub');
            $('.Performance').removeClass('data-active-sub');
            $('.media-a').addClass('data-inactive-sub-a');
            $('.media-a').removeClass('data-active-sub-a');
            $('.combined-a').addClass('data-active-sub-a');
            $('.combined-a').removeClass('data-inactive-sub-a');
            $('.performance-a').addClass('data-inactive-sub-a');
            $('.performance-a').removeClass('data-active-sub-a');
        } else if (token === 'media') {
            $('.Media1').removeClass('data-inactive-sub');
            $('.Media1').addClass('data-active-sub');
            $('.combinate1').removeClass('data-active-sub');
            $('.combinate1').addClass('data-inactive-sub');
            $('.Performance').removeClass('data-active-sub');
            $('.Performance').addClass('data-inactive-sub');
            $('.media-a').addClass('data-active-sub-a');
            $('.media-a').removeClass('data-inactive-sub-a');
            $('.combined-a').addClass('data-inactive-sub-a');
            $('.combined-a').removeClass('data-active-sub-a');
            $('.performance-a').addClass('data-inactive-sub-a');
            $('.performance-a').removeClass('data-active-sub-a');
        } else {
            $('.Media1').addClass('data-inactive-sub');
            $('.Media1').removeClass('data-active-sub');
            $('.combinate1').removeClass('data-active-sub');
            $('.combinate1').addClass('data-inactive-sub');
            $('.Performance').addClass('data-active-sub');
            $('.Performance').removeClass('data-inactive-sub');
            $('.media-a').addClass('data-inactive-sub-a');
            $('.media-a').removeClass('data-active-sub-a');
            $('.combined-a').addClass('data-inactive-sub-a');
            $('.combined-a').removeClass('data-active-sub-a');
            $('.performance-a').addClass('data-active-sub-a');
            $('.performance-a').removeClass('data-inactive-sub-a');
        }

    }
    //SEARCH FUNCTIONALITY
    $('.positions-filter').on('change', function (e) {
        var position = $(this).val();
        $("#position").val(position);
        if (active_table === "combinedPlayer") {
            combinedPlayer.draw();
        } else if (active_table === "mediaPlayer") {
            mediaPlayer.draw();
        } else if (active_table === "performancePlayer") {
            performancePlayer.draw();
        }
        e.preventDefault();
    });
    $('#search-token').on('keyup', function (e) {
        drawTables(e);
    });
    $('#wins-filter').on('click', function (e) {
        addClassWins();
        drawTables(e);
    });
    function addClassWins() {
        $('#orderBy').val('wins');
        $('.dividend').removeClass('filter-active');
        $('.dividend').addClass('filter-inactive');
        $('.wins').removeClass('filter-inactive');
        $('.wins').addClass('filter-active');
    }

    $('#dividend-filter').on('click', function (e) {
        $('#orderBy').val('dividend');
        drawTables(e);
        $('.wins').removeClass('filter-active');
        $('.wins').addClass('filter-inactive');
        $('.dividend').removeClass('filter-inactive');
        $('.dividend').addClass('filter-active');
    });
    function drawTables(e) {

        if (active_table === "combinedPlayer") {
            combinedPlayer.draw();
        } else if (active_table === "mediaPlayer") {
            mediaPlayer.draw();
        } else if (active_table === "performancePlayer") {
            performancePlayer.draw();
        } else if (active_table === "combinedTeam") {
            combinedTeam.draw();
        }
        e.preventDefault();
    }

    $("#search_datewise").on("click", function (e) {
        //addClassWins();
        var start_date = $("#start_date_datepicker").val();
        var end_date = $("#end_date_datepicker").val();
        var time_filter = $("#time_filter").val();
        var type = $("#time_filter").find(':selected').data('type');
        var leagueId = $('#leagues_filter').val();
        var start = '';
        var end = '';

        if (leagueId != '') {
            $("#leagueId").val(leagueId);
            start = '';
            end = '';
        } else if (start_date != '' && end_date != '') {
            $("#leagueId").val('');
            start = start_date;
            end = end_date;
        } else {
            $("#leagueId").val('');
            if (time_filter == 'all' && type == 'day') {
                start = '';
                end = '';
            } else if (time_filter == 100 && type == 'day') {
                start = moment().subtract(100, 'days').format("YYYY-MM-DD");
                end = moment().format("YYYY-MM-DD");
            } else if (time_filter == 30 && type == 'day') {
                start = moment().subtract(30, 'days').format("YYYY-MM-DD");
                end = moment().format("YYYY-MM-DD");
            } else if (type == 'season') {
                start = $("#time_filter").find(':selected').data('start_date');
                end = $("#time_filter").find(':selected').data('end_date');
            }
        }
        $("#end_date").val(end);
        $("#start_date").val(start);

        drawTables(e);
    });

    $("#time_filter").on('change', function () {
        var $dates = $('#start_date_datepicker, #end_date_datepicker').datepicker();
        $dates.datepicker('setDate', null);
    });
    $("#leagues_filter").on('change', function () {
        var $dates = $('#start_date_datepicker, #end_date_datepicker').datepicker();
        $dates.datepicker('setDate', null);
    });

</script>