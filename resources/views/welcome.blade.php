@extends('layouts.app')
@section('title', '- Free Tools and Premium Features')
@section('meta_desc')
<meta name="keywords" content="{{ @$pages[8]['meta_keywords'] }} {{ @$pages[9]['meta_keywords'] }}" />
<meta name="description" content="{{ @$pages[8]['meta_description'] }} {{ @$pages[9]['meta_keywords'] }}" />
@endsection

@section('page_css')
@endsection

@section('content')
<!-- Match Detail -->
@php
    $rightBenners=Helper::benner('right');
@endphp
<section class="theme-padding-bottom bg-fixed">
    @guest 
    @else
        <div class="activate-alert">
            @include ('alerts.alert')
        </div>    
        @php ($page = "homepage") @endphp
        @include('front.common.upcoming-match-slider')
    @endguest
    <div class="container">
        @guest
            <div class="logout-section latest-article-section row">
                <!-- Latest Article -->
                @include('front.common.latest-article')
                <!-- Latest Article -->
            </div>
        @else
            <div class="match-detail-content">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            @include('front.common.latest-article')
                            <div class="col-sm-12 col-xs-12 r-full-width p-full-width">
                                <h2><span><i class="red-color">Media Buzz Last Week Winner Player</i></span><!-- <a class="view-all pull-right" href="{{route('media.buzz.player')}}">view all<i class="fa fa-angle-double-right"></i></a> --></h2>
                                <div class="last-matches styel-3">
                                    <!-- Table Style 3 -->
                                    <div class="table-responsive padding-top-70">
                                        <table class="table table-bordered table-striped table-dark-header">
                                            <thead class="player_heading">
                                                <tr>
                                                    <th>DATE</th>
                                                    <th >MB 1st Place</th>
                                                    <th  >SCORE</th>
                                                    <th >MB 2nd Place</th>
                                                    <th >SCORE</th>
                                                    <th >MB 3rd Place</th>
                                                    <th >SCORE</th>
                                                </tr>
                                            </thead>
                                            <tbody class="player_alldetails">
                                                @if(!empty($data))
                                                @foreach($data as $history)
                                                @php $player_name = explode(',',@$history['player_names']); @endphp
                                                @php $player_common_name = explode(',',@$history['player_football_index_common_names']); @endphp
                                                @php $player_image = explode(',',@$history['player_images']); @endphp
                                                @php $player_is_local_image = explode(',',@$history['is_local_images']); @endphp
                                                @php $player_local_images = explode(',',@$history['player_local_images']); @endphp
                                                @php $player_team_name = explode(',',@$history['player_team_names']); @endphp
                                                @php $score = explode(',',@$history['scores']); @endphp
                                                @php $team_ids = explode(',',@$history['team_ids']); @endphp
                                                @php $player_ids = explode(',',@$history['player_ids']); @endphp
                                                <tr>
                                                    <td>{{date('d M',strtotime(@$history['date']))}}<br/>{{date('Y',strtotime(@$history['date']))}}</td>
                                                    <td class="mb-player-details view-player" data-id="{{@$player_ids[0]}}" data-teamid="{{@$team_ids[0]}}">
                                                        <div class="col-md-4 mb-player-image">
                                                            @if(@$player_image[0] != "" || @$player_local_images[0] != "")
                                                            <img 
                                                                @if(@$player_is_local_image[0] == 0)
                                                                src="{{ @$player_image[0] }}"
                                                                @else
                                                                src="{{ asset('uploads/player-images/'.@$player_local_images[0]) }}"
                                                                @endif
                                                                >
                                                                @endif
                                                        </div>
                                                        <div class="player_heading_title">
                                                            <div class="col-md-8 remove_player_space">
                                                                <div class="mb-player-name">
                                                                    {{ @$player_common_name[0] != "" ? @$player_common_name[0] : @$player_name[0] }}
                                                                </div>
                                                                <div class="mb-player-team-name">{{ @$player_team_name[0] }}</div>
                                                            </div> 
                                                        </div> 
                                                    </td>
                                                    <td class="mb-player-score">{{ @$score[0] }}</td>
                                                    <td class="mb-player-details view-player" data-id="{{@$player_ids[1]}}" data-teamid="{{@$team_ids[1]}}">
                                                        <div class="col-md-4 mb-player-image">
                                                            @if(@$player_image[1] != "" || @$player_local_images[1] != "")
                                                            <img 
                                                                @if(@$player_is_local_image[1] == 0)
                                                                src="{{ @$player_image[1] }}"
                                                                @else
                                                                src="{{ asset('uploads/player-images/'.@$player_local_images[1]) }}"
                                                                @endif
                                                                >
                                                                @endif
                                                        </div>
                                                        <div class="col-md-8 remove_player_space">
                                                            <div class="mb-player-name">
                                                                {{ @$player_common_name[1] != "" ? @$player_common_name[1] : @$player_name[1] }}
                                                            </div>
                                                            <div class="mb-player-team-name">{{ @$player_team_name[1] }}</div>
                                                        </div>                                                
                                                    </td>
                                                    <td class="mb-player-score">{{ @$score[1] }}</td>
                                                    <td class="mb-player-details view-player" data-id="{{@$player_ids[2]}}" data-teamid="{{@$team_ids[2]}}">
                                                        <div class="col-md-4 mb-player-image">
                                                            @if(@$player_image[2] != "" || @$player_local_images[2] != "")
                                                            <img 
                                                                @if(@$player_is_local_image[2] == 0)
                                                                src="{{ @$player_image[2] }}"
                                                                @else
                                                                src="{{ asset('uploads/player-images/'.@$player_local_images[2]) }}"
                                                                @endif
                                                                >
                                                                @endif
                                                        </div>
                                                        <div class="col-md-8 remove_player_space">
                                                            <div class="mb-player-name">
                                                                {{ @$player_common_name[2] != "" ? @$player_common_name[2] : @$player_name[2] }}
                                                            </div>
                                                            <div class="mb-player-team-name">{{ @$player_team_name[2] }}</div>
                                                        </div>                                                
                                                    </td>
                                                    <td class="mb-player-score">{{ @$score[2] }}</td>
                                                </tr>  
                                                @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="7">No data found</td>                                            
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12 r-full-width media_buzz_players_table">
                                <h3><span><i class="red-color">Performance Buzz Last Week Winners</i></span><!-- <a class="view-all pull-right" href="{{route('media.buzz.team')}}">view all<i class="fa fa-angle-double-right"></i></a> --></h3>
                                <div class="last-matches styel-3">
                                    <!-- Table Style 3 -->
                                    <div class="table-responsive padding-top-70">
                                        <table class="table table-bordered table-striped table-dark-header advance-v">
                                            <thead class="player_heading player_heading">
                                                <tr>
                                                    <th class="date-v-d new-p-set">DATE</th>
                                                    <th class="date-v-d">PB TOP<br>PLAYER</th>
                                                    <th class="date-v-d new-p-set">SCORE</th>
                                                    <th class="top-for">PB TOP<br>FORWARD</th>
                                                    <th class="top-for new-p-set-2">
                                                        <span class="sc-set">SCORE</span></th>
                                                    <th class="mide-v">PB TOP<br>MIDFIELDER</th>
                                                    <th class="mide-v sec-d-using"><span class="sc-set">SCORE</span></th>
                                                    <th class="defender-v">PB TOP<br>DEFENDER</th>
                                                    <th class="defender-v sec-d-using"><span class="sc-set">SCORE</span></th>
                                                    <th class="defender-goal">PB TOP<br>GOALKEEPER</th>
                                                    <th class="defender-goal sec-d-using"><span class="sc-set">SCORE</span></th>
                                                </tr>
                                            </thead>
                                            <tbody class="player_alldetails d-view-set">
                                                @if(!empty($PBWinners))
                                                @foreach($PBWinners as $history)                                 
                                                <tr>
                                                    <td>{{date('d M',strtotime(@$history['date']))}}<br/>{{date('Y',strtotime(@$history['date']))}}</td>

                                                    <!--TOP PLAYER DETAILS-->
                                                    <td class="mb-player-details view-player" data-id="{{ @$history['topPlayerPlayerId'] }}" data-teamid="{{ @$history['topPlayerTeamId'] }}">
                                                        <div class="col-md-4 mb-player-image">
                                                            @if(@$history['topPlayerLocalImagePath'] != "" || @$history['topPlayerImagePath'] != "")
                                                            <img 
                                                                @if(@$history['topPlayerIsLocalImage'] == 1 && @$history['topPlayerLocalImagePath'] != "")
                                                                src="{{ asset('uploads/player-images/'.@$history['topPlayerLocalImagePath'] ) }}"
                                                                @else
                                                                src="{{ @$history['topPlayerImagePath'] }}"
                                                                @endif
                                                                >
                                                                @endif
                                                        </div>
                                                        <div class="player_heading_title">
                                                            <div class="col-md-8 remove_player_space">
                                                                <div class="mb-player-name">
                                                                    {{ @$history['topPlayerFCommonName'] != "" ? @$history['topPlayerFCommonName'] : @$history['topPlayerName'] }}
                                                                </div>
                                                                <div class="mb-player-team-name">{{ @$history['topPlayerTeamName'] }}</div>
                                                            </div> 
                                                        </div> 
                                                    </td>

                                                    <td class="mb-player-score">{{ @$history['topPlayerScore'] }}</td>

                                                    <!--TOP FORWARD DETAILS-->
                                                    <td class="view-player" data-id="{{ @$history['forwardPlayerPlayerId'] }}" data-teamid="{{ @$history['forwardPlayerTeamId'] }}">
                                                        <div class="col-md-4 mb-player-image">
                                                            @if(@$history['forwardLocalImagePath'] != "" || @$history['forwardImagePath'] != "")
                                                            <img 
                                                                @if(@$history['forwardIsLocalImage'] == 1 && @$history['forwardLocalImagePath'] != "")
                                                                src="{{ asset('uploads/player-images/'.@$history['forwardLocalImagePath'] ) }}"
                                                                @else
                                                                src="{{ @$history['forwardImagePath'] }}"
                                                                @endif
                                                                >
                                                                @endif
                                                        </div>
                                                        <div class="player_heading_title">
                                                            <div class="col-md-8 remove_player_space">
                                                                <div class="mb-player-name">
                                                                    {{ @$history['forwardFCommonName'] != "" ? @$history['forwardFCommonName'] : @$history['forwardName'] }}
                                                                </div>
                                                                <div class="mb-player-team-name">{{ @$history['forwardTeamName'] }}</div>
                                                            </div> 
                                                        </div> 
                                                    </td>

                                                    <td class="mb-player-score">{{ @$history['forwardScore'] }}</td>

                                                    <!--TOP MIDFIELDER DETAILS-->
                                                    <td class="view-player" data-id="{{ @$history['midfielderPlayerPlayerId'] }}" data-teamid="{{ @$history['midfielderPlayerTeamId'] }}">
                                                        <div class="col-md-4 mb-player-image">
                                                            @if(@$history['midfielderLocalImagePath'] != "" || @$history['midfielderImagePath'] != "")
                                                            <img 
                                                                @if(@$history['midfielderIsLocalImage'] == 1 && @$history['midfielderLocalImagePath'] != "")
                                                                src="{{ asset('uploads/player-images/'.@$history['midfielderLocalImagePath'] ) }}"
                                                                @else
                                                                src="{{ @$history['midfielderImagePath'] }}"
                                                                @endif
                                                                >
                                                                @endif
                                                        </div>
                                                        <div class="player_heading_title">
                                                            <div class="col-md-8 remove_player_space">
                                                                <div class="mb-player-name">
                                                                    {{ @$history['midfielderFCommonName'] != "" ? @$history['midfielderFCommonName'] : @$history['midfielderName'] }}
                                                                </div>
                                                                <div class="mb-player-team-name">{{ @$history['midfielderTeamName'] }}</div>
                                                            </div> 
                                                        </div> 
                                                    </td>

                                                    <td class="mb-player-score">{{ @$history['midfielderScore'] }}</td>

                                                    <!--TOP DEFENDER DETAILS-->
                                                    <td class="view-player" data-id="{{ @$history['defenderPlayerPlayerId'] }}" data-teamid="{{ @$history['defenderPlayerTeamId'] }}">
                                                        <div class="col-md-4 mb-player-image">
                                                            @if(@$history['defenderLocalImagePath'] != "" || @$history['defenderImagePath'] != "")
                                                            <img 
                                                                @if(@$history['defenderIsLocalImage'] == 1 && @$history['defenderLocalImagePath'] != "")
                                                                src="{{ asset('uploads/player-images/'.@$history['defenderLocalImagePath'] ) }}"
                                                                @else
                                                                src="{{ @$history['defenderImagePath'] }}"
                                                                @endif
                                                                >
                                                                @endif
                                                        </div>
                                                        <div class="player_heading_title">
                                                            <div class="col-md-8 remove_player_space">
                                                                <div class="mb-player-name">
                                                                    {{ @$history['defenderFCommonName'] != "" ? @$history['defenderFCommonName'] : @$history['defenderName'] }}
                                                                </div>
                                                                <div class="mb-player-team-name">{{ @$history['defenderTeamName'] }}</div>
                                                            </div> 
                                                        </div> 
                                                    </td>

                                                    <td class="mb-player-score">{{ @$history['defenderScore'] }}</td>

                                                    <td class="view-player" data-id="{{ @$history['goalkeeperPlayerPlayerId'] }}" data-teamid="{{ @$history['goalkeeperPlayerTeamId'] }}">
                                                        <div class="col-md-4 mb-player-image">
                                                            @if(@$history['goalkeeperLocalImagePath'] != "" || @$history['goalkeeperImagePath'] != "")
                                                            <img 
                                                                @if(@$history['goalkeeperIsLocalImage'] == 1 && @$history['goalkeeperLocalImagePath'] != "")
                                                                src="{{ asset('uploads/player-images/'.@$history['goalkeeperLocalImagePath'] ) }}"
                                                                @else
                                                                src="{{ @$history['goalkeeperImagePath'] }}"
                                                                @endif
                                                                >
                                                                @endif
                                                        </div>
                                                        <div class="player_heading_title">
                                                            <div class="col-md-8 remove_player_space">
                                                                <div class="mb-player-name">
                                                                    {{ @$history['goalkeeperFCommonName'] != "" ? @$history['goalkeeperFCommonName'] : @$history['goalkeeperName'] }}
                                                                </div>
                                                                <div class="mb-player-team-name">{{ @$history['goalkeeperTeamName'] }}</div>
                                                            </div> 
                                                        </div> 
                                                    </td>


                                                    <td class="mb-player-score">{{ @$history['goalkeeperScore'] }}</td>
                                                </tr>  
                                                @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="7">No data found</td>                                            
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                        
        @endguest
    </div>
    <div class="container">
        <div class="row theme-padding">
            <div class="col-md-6">
                <div class="about-video-caption">
                    <h2><span class="red-color">{!! @$pages[8]['title'] !!}</span>   </h2>
                    {!! @$pages[8]['content'] !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="about-video-caption">
                    <h2><span class="red-color">{!! @$pages[9]['title'] !!}</span>   </h2>
                    {!! @$pages[9]['content'] !!}                   
                </div>

            </div> 
        </div>
    </div>
</section>
                    <!-- Match Detail -->
<!-- Modal -->

<div class="modal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg new-modal-dialog-v" role="document">
    <div class="modal-content bg-set-t">
      <div class="modal-header new-edit-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row">
            <div class="col-sm-3">
                <div class="player-v">
                <img src="https://cdn.sportmonks.com/images/soccer/players/26/31002.png">
                </div>
                <div class="set-player-btn">
                    <div class="fd-btn-py">
                        Forword
                    </div>
                    <div class="avrage-btn">
                        <div class="av-pb">
                            Average<br>PB Score
                        </div>
                        <div class="sc-wo">
                         154
                        </div>
                    </div>
                    
                    <div class="game-btn-v">
                        <div class="game-text">
                         Game<br>Played
                        </div>
                        <div class="game-con">
                        20
                        </div>
                    </div>


                </div>
            </div>
       
            <div class="col-sm-9 up-c-set">
                <div class="player-v-text">
                <h2>HARRY KANE</h2>
                 <div class="harry-img-v">
                <img src="https://footyindexscout.co.uk/public/assets/front/images/brand-icons/img-1-6.png" alt="">
                </div>
                <div class="fiture-b">
                <h3>Tottenham Hotspur Premier league</h3>
                </div>
                <div class="next-f">
                <h3>Next Fixture: V Juventus (5 days)</h3>
                </div>
               </div>
               <div class="rank-box row"> 
                <div class="col-sm-3 p-frd-set">
                    <div class="frd-rank">
                        FRW<br>Rank
                        <span>3</span>
                    </div>
                </div>
                <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-4 wins-p">
                        <div class="overl-1">
                            <div class="sep-overrall">
                            Overall<br>Rank
                            </div>
                            <span>15</span>
                        </div>
                    </div>
                     <div class="col-sm-4 wins-p">
                        <div class="overl-1">
                            <div class="sep-overrall">
                            Overall<br>Rank
                            </div>
                            <span>15</span>
                        </div>
                    </div>
                    <div class="col-sm-4 wins-p">
                        <div class="overl-1">
                            <div class="sep-overrall">
                            Overall<br>Rank
                            </div>
                            <span>15</span>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row media-wind-v">
                        <div class="col-sm-3">
                            <div class="u-media-f">
                             media winds
                           </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="onest-p">
                             1st Place<br>
                             <span>15</span>
                          </div>
                        </div>
                        <div class="col-sm-3">
                           <div class="onest-p">
                             2st Place<br>
                             <span>5</span>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="onest-p">
                             3st Place<br>
                             <span>4</span>
                          </div>
                        </div>
                    </div>
                </div>
                </div>    
               </div>
            </div>
       </div>
      </div>
      <div class="row">
        <div class="last-f-s">
            <div class="lst-s">
                Last 5 Scores
            </div>
            <div class="top-s">
                Top 5 Scores
            </div>
        </div>
      </div>
      <div class="row">
        <div class="different-b">
            <div class="main-d-v-set">
                <div class="first-d-v">
                <span>216</span>
                </div>
                <p>6 Nov 17</p>
            </div>
             <div class="main-d-v-set">
                <div class="sec-d-v">
                <span>92</span>
                </div>
                <p>6 Nov 17</p>
             </div>
             <div class="main-d-v-set">
                <div class="thurd-d-v">
                 <span>-18</span>
                </div>
                <p>6 Nov 17</p>
            </div>
            <div class="main-d-v-set">
                <div class="four-d-v">
                <span>216</span>    
                </div>
                <p>6 Nov 17</p>
             </div>
             <div class="main-d-v-set">
                <div class="five-d-v">
                <span>216</span>
                </div>
                <p>6 Nov 17</p>
            </div>
        </div>
      </div>
      <div class="row search-f-box">
        <div class="col-sm-4">
            <div class="f-search-f">
                Search Fixtures
            </div>
        </div>
        <div class="col-sm-4">
            <div class="selected-f">
                Selected Fixture<br>Score
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ft-play">
            <img src="http://127.0.0.1:8000/assets/front/images/minus.png">    
            </div>
            <div class="ft-play-con">
            <span>Fit to play</span>    
            </div>
        </div>
      </div>

   </div>
  </div>
</div>
@endsection
@section('page_js')
    @include('front.pages.popup')
    @include('front.pages.player-popup-script')
@endsection
