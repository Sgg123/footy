@extends('admin.layouts.app')

@section('page_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('day_and_dividends_list') }}">
                Day And Dividends
            </a>
        </li>
        <li class="breadcrumb-item active">
            Edit
        </li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Edit Day and Dividends
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('day_and_dividends_post', ['id' => Helper::encrypt(@$id)]) }}">
  
                    {{ csrf_field() }}                   

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Date <span class= "error">*</span></label>
                                <input type="text" class="form-control" name="date" value="{{ $date }}">
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputName">Day <span class= "error">*</span></label>
                                <select name="type_of_day" id="type_of_day" class="form-control" onchange="getdividends(this)">
                                    <option @if(@$type_of_day == 1) selected @endif value="1">Bronze Match Day</option>
                                    <option @if(@$type_of_day == 2) selected @endif value="2"> Silver Match Day</option>
                                    <option @if(@$type_of_day == 3) selected @endif value="3">Gold Match Day</option>
                                    <option @if(@$type_of_day == 4) selected @endif value="4">Media Day</option>
                                    <option @if(@$type_of_day == 5) selected @endif value="5">Media Madness Top 5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-2">
                                <label for="">Top Player PB</label>
                                <input type="text" class="form-control" name="top_performance_player" value="{{ $top_performance_player }}" id="top_performance_player">
                            </div>
                            <div class="col-md-2">
                                <label for="">Top PB Positional</label>
                                <input type="text" class="form-control" name="top_positional_performance_player" value="{{ $top_positional_performance_player }}" id="top_positional_performance_player">
                                
                            </div>
                            <div class="col-md-2">
                                <label for="">MB 1st</label>
                                <input type="text" class="form-control" name="media_buzz_first" value="{{ $media_buzz_first }}" id="media_buzz_first">
                                
                            </div>
                            <div class="col-md-2">
                                <label for="">MB 2nd</label>
                                <input type="text" class="form-control" name="media_buzz_second" value="{{ $media_buzz_second }}" id="media_buzz_second">
                            </div>
                            <div class="col-md-2">
                                <label for="">MB 3rd</label>
                                <input type="text" class="form-control" name="media_buzz_third" value="{{ $media_buzz_third }}" id="media_buzz_third">
                            </div>
                            <div class="col-md-2">
                                <label for="">Goalkeeper Dividend</label>
                                <input type="text" class="form-control" name="goalkeeper" value="{{ $goalkeeper }}" id="goalkeeper">
                            </div>
                           {{--  <div class="col-md-2">
                                <label for="">MB 4th</label>
                                <input type="text" class="form-control" name="media_buzz_fourth" value="{{ $media_buzz_fourth }}" id="media_buzz_fourth">
                            </div>
                            <div class="col-md-2">
                                <label for="">MB 5th</label>
                                <input type="text" class="form-control" name="media_buzz_fifth" value="{{ $media_buzz_fifth }}" id="media_buzz_fifth">
                            </div> --}}
                            {{-- <div class="col-md-2 pclass" style="@if(@$type_of_day == 5 || @$type_of_day == 4) {{'display: block'}} @else {{'display: none'}} @endif">
                                <label for="">First P</label>
                                <input type="text" class="form-control" name="first_p" value="{{ $first_p }}" id="first_p">
                            </div>
                            <div class="col-md-2 pclass" style="@if(@$type_of_day == 5 || @$type_of_day == 4) {{'display: block'}} @else {{'display: none'}} @endif">
                                <label for="">Second P</label>
                                <input type="text" class="form-control" name="second_p" value="{{ $second_p }}" id="second_p">
                            </div>
                            <div class="col-md-2 pclass" style="@if(@$type_of_day == 5 || @$type_of_day == 4) {{'display: block'}} @else {{'display: none'}} @endif">
                                <label for="">Fifth P</label>
                                <input type="text" class="form-control" name="fifth_p" value="{{ $fifth_p }}" id="fifth_p">
                            </div> --}}
                        </div>
                    </div>
                    
                    <button class="btn btn-primary" type="submit">Update</button>
                    <a class="btn btn-danger" href="{{ route('day_and_dividends_list') }}">Cancel</a>   
                </form>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $("#mb_date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });
    $('.select2').select2({dropdownCssClass: 'bigdrop'});
    function getdividends(obj){
        var day_type = $('#type_of_day').val();
        if(day_type==1){
            $('#top_performance_player').val(0.04);
            $('#goalkeeper').val(0.04);
            $('#top_positional_performance_player').val(0.04);
            $('#media_buzz_first').val(0.00);
            $('#media_buzz_second').val(0.00);
            $('#media_buzz_third').val(0.00);
             //$(".pclass").hide();
        }
        if(day_type==2){
            $('#top_performance_player').val(0.08);
            $('#goalkeeper').val(0.08);
            $('#top_positional_performance_player').val(0.08);
            $('#media_buzz_first').val(0.00);
            $('#media_buzz_second').val(0.00);
            $('#media_buzz_third').val(0.00);
             //$(".pclass").hide();
        }
        if(day_type==3){
            $('#top_performance_player').val(0.14);
            $('#goalkeeper').val(0.14);
            $('#top_positional_performance_player').val(0.14);
            $('#media_buzz_first').val(0.00);
            $('#media_buzz_second').val(0.00);
            $('#media_buzz_third').val(0.00);
             //$(".pclass").hide();
        }
        if(day_type==4){
            $('#top_performance_player').val(0.00);
            $('#goalkeeper').val(0.00);
            $('#top_positional_performance_player').val(0.00);
            $('#media_buzz_first').val(0.06);
            $('#media_buzz_second').val(0.04);
            $('#media_buzz_third').val(0.02);
             //$(".pclass").show();
        }
        if(day_type==5){
            $('#top_performance_player').val(0.00);
            $('#goalkeeper').val(0.00);
            $('#top_positional_performance_player').val(0.00);
            $('#media_buzz_first').val(0.08);
            $('#media_buzz_second').val(0.04);
            $('#media_buzz_third').val(0.02);
           /* $('#media_buzz_fourth').val(0.01);
            $('#media_buzz_fifth').val(0.01);*/
            //$(".pclass").show();
        }
    }
</script>
@endsection