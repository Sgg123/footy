@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Day and Dividends</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-tags"></i> Day and Dividends           
        </div>
        <div class="clearfix"></div>

        <div class="card-body">            
            <a href="{{ route('calcalldividends') }}" class="btn btn-primary pull-right" target="_blank" style="margin-left: 10px;">Update Player Dividends</a>
            <a href="{{ route('calcallteamsdividends') }}" class="btn btn-primary pull-right" target="_blank" style="margin-left: 10px;">Update Team Dividends</a>
            <a href="{{ route('update-positions') }}" class="btn btn-primary pull-right" target="_blank" >Update Pb Positions</a>
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="seasons-table">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Day</th>                            
                            <th>Top Player PB</th>                            
                            <th>Top PB Positional</th>
                            <th>GK</th>
                            <th>MB 1st</th>
                            <th>MB 2nd</th>
                            <th>MB 3rd</th>
                            <th>MB 4th</th>
                            <th>MB 5th</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>                        
                    </tbody> 
                </table>
            </div>            
        </div>
    </div>
</div>

@endsection

@section('page_js')
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function () {

        oTable = $('#seasons-table').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 25,
            searching: true,
            "dom": 'difrtp',
            ordering: false,
            "bInfo" : false,
            ajax: {
                url: '{!! route("day_and_dividends.data") !!}',
                data: function (d) {

                },
            },
            columns: [
                {data: 'date', name: 'date'},
                {data: 'type_of_day', name: 'type_of_day'},
                {data: 'top_performance_player', name: 'top_performance_player'},
                {data: 'top_positional_performance_player', name: 'top_positional_performance_player'},
                {data: 'goalkeeper', name: 'goalkeeper'},
                {data: 'media_buzz_first', name: 'media_buzz_first'},
                {data: 'media_buzz_second', name: 'media_buzz_second'},
                {data: 'media_buzz_third', name: 'media_buzz_third'},
                {data: 'media_buzz_fourth', name: 'media_buzz_fourth'},
                {data: 'media_buzz_fifth', name: 'media_buzz_fifth'},
                {data: 'action', name: 'action'},
            ]
        });
        
        $('#exampleInputName2').keyup(function () {
            $keyword = $(this).val();
            delay(function () {

                if ($keyword.length > 2) {

                    oTable.search($keyword).draw();
                } else {
                    oTable.search('').draw();
                }
            }, 1000);
        })
    });

    function change_standing_status(season_id,obj){
        var chkstatus = $(obj).is(":checked");
        var standing_active = (chkstatus==true) ? 1 : 0;
        $.ajax({
            url: "{{ route('setseasonstandingstatus') }}?season_id=" + season_id + "&standing_active=" + standing_active,
            success: function (response) {
            }
        });
    }
</script>
<!--@include('alerts.delete-confirm')-->
@endsection