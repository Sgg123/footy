@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Banner</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Banner      
            <a href="{{route('banner.create')}}" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Image</th>
                            <th>Type</th>
                            <th>Url</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($banner) <= 0)
                        <tr>
                            <td colspan="6" class="text-center">Sorry no banners!</td>
                        </tr>
                        @else  
                        @php $count = 1; @endphp
                        @foreach($banner as $key => $b)
                        <tr>
                            <td>{{$count}}</td>
                            <td>
                                <img
                                    @if($b['image'] != '' && file_exists(public_path('uploads/banner/'.$b['image'])))
                                    src="{{ asset('uploads/banner/'.$b['image']) }}"
                                    @else
                                    src="{{ asset('assets/images/no-image.jpeg') }}"
                                    @endif
                                    class="image-icon">
                            </td>
                            <td>{{$b['type']}}</td>
                            <td>{{$b['link']}}</td>
                            <td nowrap="">
                                <a href="{{route('banner.edit',$b['id'])}}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                <a href="javascript:void(0)" data-url="{{route('banner.destroy',$b['id'])}}" class="btn btn-sm btn-danger delete-list-record"><i class="fa fa-fw fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                        @php $count++;  @endphp
                        @endforeach
                        @endif 
                    </tbody> 
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    {{ $banner->links() }}
                </div> 
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_js')
@include('alerts.delete-confirm')
@endsection