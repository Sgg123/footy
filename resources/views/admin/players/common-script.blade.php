<script type="text/javascript">
    $(function () {

        oTable = $('#players-table').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 25,
            searching: true,
            "dom": 'difrtp',
            ordering: false,
            "bInfo": false,
            ajax: {
                url: '{!! route("teams.players.get", ["id" => @$teamID]) !!}',                
            },
            columns: [
                {data: 'firstname', name: 'firstname'},
                {data: 'lastname', name: 'lastname'},
                {data: 'common_name', name: 'common_name'},
                {data: 'team', name: 'team'},
                {data: 'position', name: 'position'},
                {data: 'nationality', name: 'nationality'},
                {data: 'birthdate', name: 'birthdate'},
                {data: 'height', name: 'height'},
                {data: 'weight', name: 'weight'},                
                {data: 'action', name: 'action'},
            ]
        });        
    });
</script>