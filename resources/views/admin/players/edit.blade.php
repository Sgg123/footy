@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('teams.players', ['id' => @$teamID])}}">
                Players
            </a>
        </li>
        <li class="breadcrumb-item active">{{ @$player ? 'Edit' : 'Add' }}</li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> {{ @$player ? 'Edit' : 'Add' }} Player
        </div>
        <div class="card-body">
            @if(@$player)
            <form method="POST" action="{{ route('teams.players.update', ['id' => Helper::encrypt(@$player->team_id), 'playerid' => Helper::encrypt(@$player->id)]) }}" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                @else
                <form method="POST" action="{{ route('teams.players.store', ['id' => @$teamID]) }}" enctype="multipart/form-data">
                    @endif
                    {{ csrf_field() }}
                    <label><b>Team:</b> {{ @$teamName }} </label>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">First Name <span class= "error">*</span></label>
                                <input class="form-control" name="firstname" type="text" aria-describedby="nameHelp" placeholder="Enter First Name " value ="{{ old('firstname', @$player->firstname) }}" id="firstname">
                                <label id="firstname-error" class="error" for="firstname">{{ @$errors->first('firstname') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputName">Middle Name <span class= "error">*</span></label>
                                <input class="form-control" name="middlename" type="text" aria-describedby="nameHelp" placeholder="Enter Middle Name " value ="{{ old('middlename', @$player->middlename) }}" id="middlename">
                                <label id="middlename-error" class="error" for="middlename">{{ @$errors->first('middlename') }}</label>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">                       

                            <div class="col-md-6">
                                <label for="exampleInputName">Last Name <span class= "error">*</span></label>
                                <input class="form-control" name="lastname" type="text" aria-describedby="nameHelp" placeholder="Enter Last Name" value ="{{ old('lastname', @$player->lastname) }}" id="lastname">
                                <label id="lastname-error" class="error" for="lastname">{{ @$errors->first('lastname') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputName">Common Name <span class= "error">*</span></label>
                                <input class="form-control" name="common_name" type="text" aria-describedby="nameHelp" placeholder="Enter Common Name" value ="{{ old('common_name', @$player->common_name) }}" id="common_name">
                                <label id="common_name-error" class="error" for="common_name">{{ @$errors->first('common_name') }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">                       
                            <div class="col-md-4">
                                <label for="exampleInputName">Football Index Name<span class= "error">*</span></label>
                                <input class="form-control" name="football_index_common_name" type="text" aria-describedby="nameHelp" placeholder="Enter Football Index Name" value ="{{ old('football_index_common_name', @$player->football_index_common_name) }}" id="football_index_common_name">
                                <label id="football_index_common_name-error" class="error" for="football_index_common_name">{{ @$errors->first('football_index_common_name') }}</label>
                            </div>
                            
                            <div class="col-md-4">
                                <label for="exampleInputName">Nationality <span class= "error">*</span></label>
                                <input class="form-control" name="nationality" type="text" aria-describedby="nameHelp" placeholder="Enter Nationality" value ="{{ old('nationality', @$player->nationality) }}" id="nationality">
                                <label id="nationality-error" class="error" for="nationality">{{ @$errors->first('nationality') }}</label>
                            </div>

                            <div class="col-md-4">
                                <label for="exampleInputName">Birth Date <span class= "error">*</span></label>
                                <input class="form-control" name="birthdate" type="text" aria-describedby="nameHelp" placeholder="Enter Birth Date " value ="{{ old('birthdate', @$player->birthdate) }}" id="birthdate" readonly>
                                <label id="birthdate-error" class="error" for="birthdate">{{ @$errors->first('birthdate') }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">                       
                            <div class="col-md-4">
                                <label for="exampleInputName">Height <span class= "error">*</span></label>
                                <input class="form-control" name="height" type="text" aria-describedby="nameHelp" placeholder="Enter Height " value ="{{ old('height', @$player->height) }}" id="height">
                                <label id="height-error" class="error" for="height">{{ @$errors->first('height') }}</label>
                            </div>

                            <div class="col-md-4">
                                <label for="exampleInputName">Weight <span class= "error">*</span></label>
                                <input class="form-control" name="weight" type="text" aria-describedby="nameHelp" placeholder="Enter Weight" value ="{{ old('weight', @$player->weight) }}" id="weight">
                                <label id="weight-error" class="error" for="weight">{{ @$errors->first('weight') }}</label>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputName">Position <span class= "error">*</span></label>
                                <select name="position" class="form-control">
                                    <option value="">Select Position</option>
                                    <option value="Defender" {{ @$player->position == "Defender" ? 'selected' : '' }}>Defender</option>
                                    <option value="Forward" {{ @$player->position == "Forward" ? 'selected' : '' }}>Forward</option>
                                    <option value="Goalkeeper" {{ @$player->position == "Goalkeeper" ? 'selected' : '' }}>Goalkeeper</option>
                                    <option value="Midfielder" {{ @$player->position == "Midfielder" ? 'selected' : '' }}>Midfielder</option>
                                </select>
                                <label id="position-error" class="error" for="weight">{{ @$errors->first('position') }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">                       
                            <div class="col-md-4">
                                <label for="football_index_position_id">Football Index Position</label>
                                
                                <select name="football_index_position" id="football_index_position_id" class="form-control">
                                    <option value="">Select Position</option>
                                    <option value="Defender" {{ @$player->football_index_position == "Defender" ? 'selected' : '' }}>Defender</option>
                                    <option value="Forward" {{ @$player->football_index_position == "Forward" ? 'selected' : '' }}>Forward</option>
                                    <option value="Goalkeeper" {{ @$player->football_index_position == "Goalkeeper" ? 'selected' : '' }}>Goalkeeper</option>
                                    <option value="Midfielder" {{ @$player->football_index_position == "Midfielder" ? 'selected' : '' }}>Midfielder</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Image </label>  
                                <small class="text-muted">( upload image of size 150 * 150 for better result. )</small>                        
                                <input name="local_image_path" id="update-thumbnail-image" accept=".jpg,.jpeg,.png" type="file" class="form-control
                                       @if (file_exists('uploads/player-images/' . @$player->local_image_path) &&  @$player->local_image_path != '' &&  @$player->is_local_image == 1)
                                       edit_image
                                       @else
                                       image
                                       @endif" />
                                <label id="local_image_path-error" class="error" for="local_image_path">{{ @$errors->first('local_image_path') }}</label>
                            </div>
                            <div class="col-md-6">
                                <div class="add-preview-main">
                                    <img  name="thumbnail-preview" id="thumbnail-preview" class="article-image"
                                          @if (file_exists('uploads/player-images/' . @$player->local_image_path) &&  @$player->local_image_path != '' &&  @$player->is_local_image == 1)
                                          src="{{ asset('uploads/player-images/'.@$player->local_image_path) }}"
                                          @else
                                          src="{{ @$player->image_path }}"
                                          @endif                                                 
                                          >
                                </div>
                            </div>

                        </div>
                    </div>

                    <input type="submit" value="{{ @$player ? 'Update' : 'Save' }}" class="btn btn-primary" />
                    <a class="btn btn-danger" href="{{ route('teams.players', ['id' => @$teamID])}}">Cancel</a>	
                </form>
        </div>
    </div>
</div>

@endsection

@section('page_js')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
    $("#birthdate").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: '0',
        autoclose: true,
        changeYear: true,
        changeMonth: true,
        yearRange: "-80:+00"
    });
    $("#update-image").change(function () {
        readURLEdit(this);
    });

    function readURLEdit(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#preview").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    //Thumbnail Image Preview
    $("#update-thumbnail-image").change(function () {
        readURLEdit2(this);
    });

    function readURLEdit2(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#thumbnail-preview").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection