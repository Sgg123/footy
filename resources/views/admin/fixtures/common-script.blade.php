<script type="text/javascript">
    $(function () {

        oTable = $('#seasons-table').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 25,
            searching: false,
            "dom": 'difrtp',
            ordering: false,
            "bInfo": false,
            "scrollX": true,
            ajax: {
                url: '{!! route("fixtures.data") !!}',
                data: function (d) {
                    d.season_id = $("#season_id").val();
                    d.league_id = $("#league_id").val();
                },
            },
            columns: [
                {data: 'league', name: 'league'},
                {data: 'season', name: 'season'},
                {data: 'local_team', name: 'local_team'},
                {data: 'visitor_team', name: 'visitor_team'},
                {data: 'pitch', name: 'pitch'},
                {data: 'status', name: 'status'},
                {data: 'starting_at', name: 'starting_at'},
                {data: 'minute', name: 'minute'},
                {data: 'action', name: 'action'},
            ]
        });

        $('#search-form').on('submit', function (e) {
            oTable.draw();
            e.preventDefault();
        });

        $("#league_id").on("change", function (e) {
            $seasonsOptions = $("#season_id")[0].selectize;
            $seasonsOptions.clearOptions();
            $seasonsOptions.renderCache = {};

            //Get seasons
            $URL = "{{ route('get_league_season') }}";
            $id = $("#league_id").val();
            $token = "{{ csrf_token() }}";

            var formData = new FormData();
            formData.append('_token', $token);
            formData.append('id', $id);

            $.ajax({
                type: "POST",
                url: $URL,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.length > 0) {
                        var i = 0;
                        for (var key in response) {
                            $seasonsOptions.addOption({value: response[key].id, text: response[key].name});
                        }
                    }
                    $seasonsOptions.refreshOptions();
                }
            });
        });

        $('.selectize-dropdowns-category').selectize({
            allowEmptyOption: false,
        });
    });
</script>