@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Article Categories</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Article Categories
            <a href="{{route('article-category.create')}}" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <div class="clearfix"></div>        
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>No. of Articles</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($articleCategories) <= 0)
                        <tr>
                            <td colspan="6" class="text-center">Sorry no category found!</td>
                        </tr>
                        @else  
                        @php $count = 1; @endphp
                        @foreach($articleCategories as $articleCategory)
                        @php $id = Helper::encrypt($articleCategory['id']); @endphp
                        <tr>
                            <td>{{$count}}</td>
                            <td>{{$articleCategory['title']}}</td>
                            <td>{{$articleCategory['getArticles']->count()}}</td>
                            <td>
                                <a href="{{route('article-category.edit',[$id])}}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                @if($articleCategory['getArticles']->count() == 0)
                                <a href="javascript:void(0)" data-url="{{route('article-category.destroy',$id)}}" class="btn btn-sm btn-danger delete-list-record" ><i class="fa fa-fw fa-trash"></i>Delete</a>
                                @endif
                            </td>
                        </tr>
                        @php $count++;  @endphp
                        @endforeach
                        @endif 
                    </tbody> 
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    {{ $articleCategories->links() }}
                </div> 
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_js')
@include('alerts.delete-confirm')
@endsection