@extends('admin.layouts.app')

@section('page_css') 
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Seasons</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-tags"></i> Seasons           
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="seasons-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>League</th>                            
                            <th>Type</th> 
                            <th>Start Date</th>
                            <th>End Date</th>                           
                            <th>Standing View</th>
                        </tr>
                    </thead>
                    <tbody>                        
                    </tbody> 
                </table>
            </div>            
        </div>
    </div>
</div>

@endsection

@section('page_js')
 
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
    $(function () {

        oTable = $('#seasons-table').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 25,
            searching: true,
            "dom": 'difrtp',
            ordering: false,
            "bInfo" : false,
            ajax: {
                url: '{!! route("seasons.data") !!}',
                data: function (d) {

                },
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'league_name', name: 'league_name'},
                {data: 'type', name: 'type'},
                {data: 'actions', name: 'actions',render: function (dataField,String) { return '<input type="text" name="date" class="start_date_season" value='+dataField.toString().split(".")[1]+' data-id='+dataField.toString().split(".")[0]+' size="30">'; }},
                {data: 'end_date', name: 'end_date',render: function (dataField,String) { return '<input type="text" name="date" class="end_date_season" size="30" value='+dataField.toString().split(".")[1]+' data-id='+dataField.toString().split(".")[0]+'>'; }},
                {data: 'action', name: 'action'},
            ]
        });
        
        $('#exampleInputName2').keyup(function () {
            $keyword = $(this).val();
            delay(function () {

                if ($keyword.length > 2) {

                    oTable.search($keyword).draw();
                } else {
                    oTable.search('').draw();
                }
            }, 1000);
        })
    });
    function change_standing_status(season_id,obj){
        var chkstatus = $(obj).is(":checked");
        var standing_active = (chkstatus==true) ? 1 : 0;
        $.ajax({
            url: "{{ route('setseasonstandingstatus') }}?season_id=" + season_id + "&standing_active=" + standing_active,
            success: function (response) {
            }
        });
    }
 
</script>
<script type="text/javascript">
var id1;
var value1;
var id2;
var value2;
function date(){
    console.log("as");
   $(".start_date_season").datepicker({
        dateFormat: 'yy-mm-dd',
    }).on("input change", function (e) {
        value1 = e.target.value;
        id1= $(this).attr("data-id");
        $.ajax({
            url: "{{ route('setstartdateseason') }}?season_id=" + id1 + "&standing_date=" + value1,
            success: function (response) {
            }
        });
       
    });
    $(".end_date_season").datepicker({
        dateFormat: 'yy-mm-dd',
    }).on("input change", function (e) {
        value2 = e.target.value;
        id2= $(this).attr("data-id");
        $.ajax({
            url: "{{ route('setenddateseason') }}?season_id=" + id2 + "&standing_date=" + value2,
            success: function (response) {
            }
        });
      
    });


}
setTimeout("date()", 1000);
</script>

<!--@include('alerts.delete-confirm')-->
@endsection