@extends('admin.layouts.app')

@section('page_css')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{route('articles.index')}}">
                Articles
            </a>
        </li>
        <li class="breadcrumb-item active">{{ @$articles === null ? 'Add' : 'Edit' }}</li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> {{ @$articles === null ? 'Add' : 'Edit' }} Article
        </div>
        <div class="card-body">
            @if(@$articles)
            <form method="POST" action="{{ route('articles.update', ['id' => Helper::encrypt(@$articles->id)]) }}" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                @else
                <form method="POST" action="{{ route('articles.store') }}" enctype="multipart/form-data">
                    @endif
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Title <span class= "error">*</span></label>
                                <input class="form-control" required name="title" type="text" aria-describedby="nameHelp" placeholder="Enter title" value ="{{ @$articles === null ? old('title') : @$articles->title }}" id="title">
                                <label id="title-error" class="error" for="title">{{ @$errors->first('title') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputName">Slug <span class= "error">*</span></label>
                                <input class="form-control pointer-disabled" name="slug" type="text" aria-describedby="nameHelp" placeholder="" value ="{{ @$articles === null ? old('slug') : @$articles->slug }}" id="slug" readonly>
                                <label id="slug-error" class="error" for="slug">{{ @$errors->first('slug') }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="exampleInputName">Article Category <span class= "error">*</span></label>
                                <select class="form-control" name="article_category_id">
                                    <option value="">Select article category</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category['id']}}" @if($category['id'] == old('article_category_id',@$articles->article_category_id)) selected @endif>{{$category['title']}}</option>
                                    @endforeach
                                </select>
                                <label id="article_category_id-error" class="error" for="article_category_id">{{ @$errors->first('article_category_id') }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Content Image <span class= "error">*</span></label>  
                                <small class="text-muted">( upload image of size 1280 * 768 for better result. )</small>                        
                                <input name="image" id="update-image" accept=".jpg,.jpeg,.png" type="file" class="form-control
                                       @if (file_exists('uploads/articles/' . @$articles->image_path) &&  @$articles->image_path != '')
                                       edit_image
                                       @else
                                       image
                                       @endif" />
                                <label id="image-error" class="error" for="image">{{ @$errors->first('image') }}</label>
                            </div>
                            <div class="col-md-6">
                                <div class="add-preview-main">
                                    <img  name="preview" id="preview" class="article-image"
                                          @if (file_exists('uploads/articles/' . @$articles->image_path) &&  @$articles->image_path != '')
                                          src="{{ asset('uploads/articles/'.@$articles->image_path) }}"
                                          @else
                                          src="{{ asset('assets/images/no-image.jpeg') }}"
                                          @endif                                                 
                                          >
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Thumbnail Image <span class= "error">*</span></label>  
                                <small class="text-muted">( upload image of size 320 * 200 for better result. )</small>                        
                                <input name="thumbnail_image" id="update-thumbnail-image" accept=".jpg,.jpeg,.png" type="file" class="form-control
                                       @if (file_exists('uploads/articles/' . @$articles->thumbnail_image) &&  @$articles->thumbnail_image != '')
                                       edit_image
                                       @else
                                       image
                                       @endif" />
                                <label id="thumbnail_image-error" class="error" for="thumbnail_image">{{ @$errors->first('thumbnail_image') }}</label>
                            </div>
                            <div class="col-md-6">
                                <div class="add-preview-main">
                                    <img  name="thumbnail-preview" id="thumbnail-preview" class="article-image"
                                          @if (file_exists('uploads/articles/' . @$articles->thumbnail_image) &&  @$articles->thumbnail_image != '')
                                          src="{{ asset('uploads/articles/'.@$articles->thumbnail_image) }}"
                                          @else
                                          src="{{ asset('assets/images/no-image.jpeg') }}"
                                          @endif                                                 
                                          >
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="">SEO Keywords</label> 
                            </div>
                            <div class="col-md-12">
                                <input class="form-control" id="meta_keywords" name="meta_keywords" type="text" placeholder="Slug" maxlength="255" value="{{ old('meta_keywords', @$articles->meta_keywords) }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="">SEO Description</label> 
                            </div>
                            <div class="col-md-12">
                                <input class="form-control" id="meta_description" name="meta_description" type="text" placeholder="Slug" maxlength="255" value="{{ old('meta_description', @$articles->meta_description) }}">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Youtube Video Link <span class= "error">*</span></label>
                                <input class="form-control" name="youtube_video" type="text" aria-describedby="nameHelp" placeholder="Enter youtube video link" value ="{{ @$articles === null ? old('youtube_video') : @$articles->youtube_video }}" id="youtube_video">
                                <label id="youtube_video-error" class="error" for="content">{{ @$errors->first('youtube_video') }}</label>
                            </div>

                            <div class="col-md-6">
                                <label for="exampleConfirmPassword">Date <span class= "error">*</span></label>
                                <input class="form-control" id="mb_date" name="date" type="text" placeholder="Date" value="{{ @$articles === null ? old('date') : @$articles->date }}">
                                <label id="date-error" class="error" for="date">{{ @$errors->first('date') }}</label>
                            </div>
                        </div>
                    </div>     

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="exampleInputName">Content <span class= "error">*</span></label>
                                <textarea name="content" id="summernote">{{ @$articles === null ? old('content') : @$articles->content }}</textarea>
                                <label id="content-error" class="error" for="content">{{ @$errors->first('content') }}</label>
                            </div>
                        </div>
                    </div>     

                    <input type="submit" value="{{ @$articles === null ? 'Save' : 'Update' }}" class="btn btn-primary" href=""/>
                    <a class="btn btn-danger" href="{{ route('articles.index')}}">Cancel</a>	
                </form>
        </div>
    </div>
</div>

@endsection

@section('page_js')
<script src="{{ asset('assets/admin/js/jquery.seourl.min.js') }}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script src="{{ asset('assets/front/js/summernote-image-attributes.js') }}"></script>

<script type="text/javascript">
    $("#mb_date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });

    $(document).ready(function () {
            $('#summernote').summernote({
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ],
            },            
            imageAttributes:{
                icon:'<i class="note-icon-pencil"/>',                
            },            
            fontNames: ['Segoe UI', 'Arial', 'Helvetica', 'Comic Sans MS', 'Calibri', 'Consolas', 'Courier New', 'Garamond', 'Georgia', 'Lucida Console', 'Lucida Sans', 'Segoe UI', 'Tahoma',
                'Tempus Sans ITC', 'Times New Roman', 'Trebuchet MS', 'Verdana'],
            height: 150,
            toolbar: [
                ['style', ['style']],
                ['group2', ['fontsize']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture', 'hr', 'link', 'video']],
                ['view', ['fullscreen', 'codeview']],
                ['table', ['table']]
            ],
        });

    });

    $("#update-image").change(function () {
        readURLEdit(this);
    });

    function readURLEdit(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#preview").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    //Thumbnail Image Preview
    $("#update-thumbnail-image").change(function () {
        readURLEdit2(this);
    });

    function readURLEdit2(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#thumbnail-preview").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    $(document).on("focusout", "#title", function () {
        var productTitle = $(this).val().toLowerCase();
        var productSlug = productTitle.seoURL();
        $("#slug").val(productSlug);
    });
</script>
@endsection