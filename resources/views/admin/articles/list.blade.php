@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Articles</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Articles      
            <a href="{{route('articles.create')}}" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <!--        <div class="card-header">
                    <form method="POST" action="{{route('articles.import_articles')}}" enctype="multipart/form-data" class="pull-right">
                        {{ csrf_field() }}
                        <input type="file" name="import_file">     
                        <label id="import_file-error" class="error" for="import_file">{{ @$errors->first('import_file') }}</label>
                        <input type="submit" value="Import Articles" class="btn btn-sm btn-primary">
                    </form>
                </div>-->
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Content Image</th>
                            <th>Thumbnail Image</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($articles) <= 0)
                        <tr>
                            <td colspan="6" class="text-center">Sorry no articles!</td>
                        </tr>
                        @else  
                        @php $count = 1; @endphp
                        @foreach($articles as $article)
                        @php $id = Helper::encrypt($article['id']); @endphp
                        <tr>
                            <td>{{$count}}</td>
                            <td>{{$article['title']}}</td>
                            <td>{{$article['getArticlesCategory']['title']}}</td>
                            <td>
                                <img
                                    @if($article['image_path'] != '' && file_exists(public_path('uploads/articles/'.$article['image_path'])))
                                    src="{{ asset('uploads/articles/'.$article['image_path']) }}"
                                    @else
                                    src="{{ asset('assets/images/no-image.jpeg') }}"
                                    @endif
                                    class="image-icon">
                            </td>
                            <td>
                                <img
                                    @if($article['thumbnail_image'] != '' && file_exists(public_path('uploads/articles/'.$article['thumbnail_image'])))
                                    src="{{ asset('uploads/articles/'.$article['thumbnail_image']) }}"
                                    @else
                                    src="{{ asset('assets/images/no-image.jpeg') }}"
                                    @endif
                                    class="image-icon">
                            </td>
                            <td>{{$article['date']}}</td>
                            <td nowrap="">
                                <a href="{{route('articles.edit',[$id])}}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                <a href="javascript:void(0)" data-url="{{route('articles.destroy',$id)}}" class="btn btn-sm btn-danger delete-list-record"><i class="fa fa-fw fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                        @php $count++;  @endphp
                        @endforeach
                        @endif 
                    </tbody> 
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    {{ $articles->links() }}
                </div> 
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_js')
@include('alerts.delete-confirm')
@endsection