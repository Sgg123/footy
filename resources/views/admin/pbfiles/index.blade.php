@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Performance Buzz Data Files</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-tags"></i> Performance Buzz Data Files           
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <a href="{{ route('performance_buzz_files_new') }}" class="btn btn-primary pull-right" style="margin-left: 10px;">Add New Data File</a>
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="seasons-table">
                    <thead>
                        <tr>
                            <th>File</th>
                            <th>Date</th>                            
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pbfiles as $pbfile)
                        <tr>
                            <td>{{ $pbfile->file_name }}</td>
                            <td>{{ $pbfile->date }}</td>
                            <td><a href="javascript:void(0)" onclick="delete_pb_files({{ $pbfile->id }})">Delete</a></td>
                        </tr>
                        @endforeach                     
                    </tbody> 
                </table>
            </div>            
        </div>
    </div>
</div>
@endsection
@section('page_js')
<script type="text/javascript">
    function delete_pb_files(id){
        var conf = confirm("Are you sure you want to delete all records of file as well as data?");
        if(conf==true){
            $.ajax({
                url: '{{ route('performance_buzz_files_del') }}',
                data: {id:id},
                success: function(data){
                    alert('File and data deleted');
                    location.reload();
                }
            });
        }
        return false;
    }
</script>
@endsection