<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Bootstrap core CSS-->
  <link href="{{ asset('assets/admin/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{ asset('assets/admin/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="{{ asset('assets/admin/css/sb-admin.css') }}" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Reset Password</div>
      <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/backoffice-fis/password/reset') }}">
                        {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
          <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Email address</label>
            <input class="form-control" name="email" value="{{ old('email') }}" type="email" aria-describedby="emailHelp" placeholder="Enter email">
            <label id="email-error" class="error" for="email">{{ @$errors->first('email') }}</label>
          </div>
          <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Password</label>
            <input class="form-control" name="password" type="password" placeholder="Password">
            <label id="password-error" class="error" for="password">{{ @$errors->first('password') }}</label>
          </div>
          <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Confirm Password</label>
            <input class="form-control" name="password_confirmation" type="password" placeholder="Confirm Password">
          
            <label id="password_confirmation-error" class="error" for="password_confirmation">{{ @$errors->first('password_confirmation') }}</label>
          </div>
          <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
        </form>
        
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('assets/admin/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- Core plugin JavaScript-->
  <script src="{{ asset('assets/admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
</body>
</html>