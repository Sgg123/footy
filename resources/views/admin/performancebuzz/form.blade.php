@extends('admin.layouts.app')

@section('page_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('performance-buzz.index') }}">
                Performance Buzz History
            </a>
        </li>
        <li class="breadcrumb-item active">
            {{@$pbData === null ? 'Add' : 'Edit' }}
        </li>
    </ol>

    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> {{@$pbData === null ? 'Add' : 'Edit' }} Performance Buzz History
        </div>
        @include('alerts.alert')
        <div class="card-body">
            @if(@$pbData)
            <form method="POST" action="{{ route('performance-buzz.update', ['id' => Helper::encrypt(@$pbData->id)]) }}">
                {{ method_field('PUT') }}                
                @else
                <form method="POST" action="{{ route('performance-buzz.store') }}">
                    @endif
                    {{ csrf_field() }}     

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">PLAYER NAME</label>
                                <select class="form-control player_id select2" name="player_id" id="player_id">
                                    <option value="">Select Player</option>
                                    @foreach(@$players as $player)
                                    <option value="{{ $player->id }}" @if(@$pbData->player_id == $player->id) selected @endif>{{ $player->football_index_common_name }}</option>
                                    @endforeach
                                </select>                               
                                <label id="player_id-error" class="error" for="player_id">{{ @$errors->first('player_id') }}</label>
                            </div>                            
                        </div>
                    </div>                    

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">TEAM NAME </label>
                                <select class="form-control player_id select2" name="team_id" id="team_id">
                                    <option value="">Select Team</option>
                                    @foreach(@$teams as $team)
                                    <option value="{{ $team->id }}" @if(@$pbData->team_id == $team->id) selected @endif>{{ $team->name }}</option>
                                    @endforeach
                                </select>                               
                                <label id="team_id-error" class="error" for="team_id">{{ @$errors->first('team_id') }}</label>
                            </div>                            
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputLastName">Score </label>
                                <input class="form-control number" id="score" name="score" type="text" placeholder="Score" maxlength="10" value="{{ old('score', @$pbData->score) }}">
                                <label id="score-error" class="error" for="score">{{ @$errors->first('score') }}</label>
                            </div>                           
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputLastName">DATE </label>
                                <input class="form-control number" id="date" name="date" type="text" placeholder="Date" maxlength="10" value="{{ old('date', @$pbData->date) }}" @if(@$pbData != "") readonly @endif>
                                       <label id="date-error" class="error" for="date">{{ @$errors->first('date') }}</label>
                            </div>                       
                        </div>
                    </div>

                    <button class="btn btn-primary" type="submit">{{ @$playerHistory === null ? 'Save' : 'Update' }}</button>
                    <a class="btn btn-danger" href="{{ route('performance-buzz-winner-player.index') }}">Cancel</a>	
                </form>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $('.select2').select2({dropdownCssClass: 'bigdrop'});

    $("#date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });
</script>
@endsection