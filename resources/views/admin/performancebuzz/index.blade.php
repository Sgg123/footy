@extends('admin.layouts.app')

@section('page_css')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Performance Buzz History</li>
    </ol>
    @include('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Performance Buzz History
            <a href="{{ route('performance-buzz.create') }}" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <div class="card-body">

            <form action="" class="row">
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="player_name" placeholder="Player name" value="{{ @Request::get('player_name') }}">
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="date" name="date" placeholder="Date" value="{{ @Request::get('date') }}">
                </div>
                <div class="col-sm-3">
                    <button type="submit" class="btn red-btn">Search</button>
                    <a href="{{route('performance-buzz.index')}}" class="btn btn-danger">Reset</a>
                </div>
            </form>
            <br>

            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Date</th>   
                            <th>PLAYER NAME</th>
                            <th>TEAM NAME</th>
                            <th>SCORE</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$pbData->isEmpty())
                        @foreach($pbData as $history)
                        <tr>
                            <td nowrap="">{{ $history->date }}</td>  
                            <td>
                                {{ @$history->football_index_common_name != '' ?  @$history->football_index_common_name : @$history->common_name }}
                            </td>
                            <td>{{ @$history->name }}</td>                                    
                            <td>{{ @$history->score }}</td>                                    
                            <td>
                                <a href="{{ route('performance-buzz.edit', ['id' => Helper::encrypt($history->id)]) }}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                <a href="javascript:destroy('{{ route('performance_buzz_data_delete', ['id' => Helper::encrypt($history->id)]) }}')" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="8" class="text-center">No performance buzz history found!</td>
                        <tr>
                            @endif
                    </tbody>
                </table>
                @if (@Request::get('player_name') != '' || @Request::get('date') != '')
                {!! $pbData->appends(['player_name' => @Request::get('player_name'), 'date' => @Request::get('date')])->links() !!}
                @else
                {!! $pbData->links() !!}
                @endif

            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')

@include('alerts.delete-confirm')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $('.select2').select2({dropdownCssClass: 'bigdrop'});

    $("#date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });

    function destroy(url){
        var conf=confirm('Do you want to delete this performance buzz record?');
        if(conf==true){
            $.ajax({
                url: url,
                data: {},
                success: function(data){
                    alert('deleted');
                    location.reload();
                }
            })
        }
    }
</script>
@endsection
