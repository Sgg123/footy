@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Media Buzz Winner Teams History</li>
    </ol>
    @include('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
<!--        <div class="card-header">
            <i class="fa fa-table"></i> Media Buzz Winner Teams History
            <a href="{{ route('media-buzz-winner-team.create') }}" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>-->
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>MB 1st Place</th>
                            <th>MB 2nd Place</th>
                            <th>MB 3rd Place</th>
                            <th>MB 4th Place</th>
                            <th>MB 5th Place</th> 
                            <th>Actions</th>                                  
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($winnerTeamHistory))
                        @foreach($winnerTeamHistory as $history)
                        @php $player_team_name = explode(',',@$history['player_team_names']); @endphp
                        <tr>
                            <td>{{@$history['date']}}</td>
                            <td>{{ @$player_team_name[0] }}</td>
                            <td>{{ @$player_team_name[1] }}</td>
                            <td>{{ @$player_team_name[2] }}</td>
                            <td>{{ @$player_team_name[3] }}</td>
                            <td>{{ @$player_team_name[4] }}</td>
                            <td>
                                <a href="{{ route('media-buzz-winner-team.edit', ['id' => Helper::encrypt($history->date)]) }}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                
                            </td>
                        </tr>  
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">No data found</td>                                         
                        </tr>
                        @endif
                    </tbody>
                </table>
                {{ $winnerTeamHistory->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')

@include('alerts.delete-confirm')

@endsection