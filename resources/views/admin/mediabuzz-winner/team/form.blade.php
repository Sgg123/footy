@extends('admin.layouts.app')

@section('page_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('media-buzz-winner-team.index') }}">
                Media Buzz Winner Team History
            </a>
        </li>
        <li class="breadcrumb-item active">
            {{@$teamHistory === null ? 'Add' : 'Edit' }}
        </li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> {{@$teamHistory === null ? 'Add' : 'Edit' }} Media Buzz Winner Team History
        </div>
        <div class="card-body">
            @if(@$teamHistory)
            <form method="POST" action="{{ route('media-buzz-winner-team.update', ['id' => Helper::encrypt(@$teamHistory->date)]) }}">
                {{ method_field('PUT') }}
                @else
                <form method="POST" action="{{ route('media-buzz-winner-team.store') }}">
                    @endif
                    <input type="hidden" name="ids" value="{{@$teamHistory->mb_ids}}">
                    {{ csrf_field() }}  
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-3  col-md-offset-1">
                                <label for="exampleInputName">MB 1st Place <span class= "error">*</span></label>
                                <select class="form-control select2" name="mb_first">
                                    <option value="">Select Team</option>
                                    @foreach(@$teams as $team)
                                    <option value="{{$team['id']}}" @if($team['id'] == old('mb_first',@$teamIds[0])) selected @endif>{{$team['name']}} </option>
                                    @endforeach
                                </select>
                                <label id="mb_first-error" class="error" for="mb_first">{{ @$errors->first('mb_first') }}</label>
                            </div>
                            <div class="col-md-3 col-md-offset-2">
                                <label for="exampleInputPassword1">MB 2nd Place</label>
                                <select class="form-control select2" name="mb_second">
                                    <option value="">Select Team</option>
                                    @foreach(@$teams as $team)
                                    <option value="{{$team['id']}}" @if($team['id'] == old('mb_second', @$teamIds[1])) selected @endif>{{$team['name']}} </option>
                                    @endforeach
                                </select>

                                <label id="mb_second-error" class="error" for="mb_second">{{ @$errors->first('mb_second') }}</label>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <label for="exampleInputPassword1">MB 3rd Place </label>
                                <select class="form-control select2" name="mb_third">
                                    <option value="">Select Team</option>
                                    @foreach(@$teams as $team)
                                    <option value="{{$team['id']}}" @if($team['id'] == old('mb_third', @$teamIds[2])) selected @endif>{{$team['name']}} </option>
                                    @endforeach
                                </select>
                                <label id="mb_third-error" class="error" for="mb_third">{{ @$errors->first('mb_third') }}</label>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <label for="exampleInputPassword1">MB 4th Place </label>
                                <select class="form-control select2" name="mb_fourth">
                                    <option value="">Select Team</option>
                                    @foreach(@$teams as $team)
                                    <option value="{{$team['id']}}" @if($team['id'] == old('mb_fourth', @$teamIds[3])) selected @endif>{{$team['name']}} </option>
                                    @endforeach
                                </select>
                                <label id="mb_fourth-error" class="error" for="mb_fourth">{{ @$errors->first('mb_fourth') }}</label>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <label for="exampleInputPassword1">MB 5th Place </label>
                                <select class="form-control select2" name="mb_fifth">
                                    <option value="">Select Team</option>
                                    @foreach(@$teams as $team)
                                    <option value="{{$team['id']}}" @if($team['id'] == old('mb_fifth', @$teamIds[4])) selected @endif>{{$team['name']}} </option>
                                    @endforeach
                                </select>
                                <label id="mb_fifth-error" class="error" for="mb_fifth">{{ @$errors->first('mb_fifth') }}</label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">{{ @$teamHistory === null ? 'Save' : 'Update' }}</button>
                    <a class="btn btn-danger" href="{{ route('media-buzz-winner-team.index') }}">Cancel</a> 
                </form>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $("#mb_date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });
    $('.select2').select2({dropdownCssClass: 'bigdrop'});

</script>
@endsection