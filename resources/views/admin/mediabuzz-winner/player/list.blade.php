@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Media Buzz Winner Players History</li>
    </ol>
    @include('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Media Buzz Winner Players History
            <a href="{{ route('media-buzz-winner-player.create') }}" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Date</th>   
                            <th>MB 1st Place</th>
                            <th>Score</th>
                            <th>MB 2nd Place</th>
                            <th>Score</th>
                            <th>MB 3rd Place</th>
                            <th>Score</th>
                            <th>MB 4th Place</th>
                            <th>Score</th>
                            <th>MB 5th Place</th>
                            <th>Score</th>
                                                     
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$winnerPlayerHistory->isEmpty())
                        @foreach($winnerPlayerHistory as $history)
                        <tr>
                            @php $player_common_name = explode(',',@$history->player_football_index_common_names); @endphp
                            @php $player_name = explode(',',@$history->player_names); @endphp
                            @php $score = explode(',',@$history->scores); @endphp
                            <td>{{ $history->date }}</td>         
                            <td>
                                {{ @$player_common_name[0] != "" ? @$player_common_name[0] : @$player_name[0] }}
                            </td>
                            <td>{{ @$score[0] }}</td>
                            <td>
                                {{ @$player_common_name[1] != "" ? @$player_common_name[1] : @$player_name[1] }}
                            </td>
                            <td>{{ @$score[1] }}</td>
                            <td>
                                {{ @$player_common_name[2] != "" ? @$player_common_name[2] : @$player_name[2] }}
                            </td>
                            <td>{{ @$score[2] }}</td>
                             <td>
                                {{ @$player_common_name[3] != "" ? @$player_common_name[3] : @$player_name[3] }}
                            </td>
                            <td>{{ @$score[3] }}</td>
                             <td>
                                {{ @$player_common_name[4] != "" ? @$player_common_name[4] : @$player_name[4] }}
                            </td>
                            <td>{{ @$score[4] }}</td>
                            <td>
                                <a href="{{ route('media-buzz-winner-player.edit', ['id' => Helper::encrypt($history->date)]) }}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                <a href="javascript:void(0)" class="btn btn-sm btn-danger delete-list-record" data-url="{{ route('media-buzz-winner-player.destroy', ['date' => Helper::encrypt($history->date)]) }}"><i class="fa fa-fw fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="8" class="text-center">No media buzz winner player history found!</td>
                        <tr>
                            @endif
                    </tbody>
                </table>
                {{ $winnerPlayerHistory->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')

@include('alerts.delete-confirm')

@endsection