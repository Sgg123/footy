@extends('admin.layouts.app')

@section('page_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('media-buzz-winner-player.index') }}">
                Media Buzz Winner Player History
            </a>
        </li>
        <li class="breadcrumb-item active">
            {{@$playerHistory === null ? 'Add' : 'Edit' }}
        </li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> {{@$playerHistory === null ? 'Add' : 'Edit' }} Media Buzz Winner Players History
        </div>
        <div class="card-body">
            @if(@$playerHistory)
            @php
            $player_id = explode(',',@$playerHistory->player_ids);
            $score = explode(',',@$playerHistory->scores);
            @endphp

            <form method="POST" action="{{ route('media-buzz-winner-player.update', ['id' => Helper::encrypt(@$playerHistory->date)]) }}">
                <input type="hidden" name="ids" value="{{@$playerHistory->ids}}">
                {{ method_field('PUT') }}                
                @else
                <form method="POST" action="{{ route('media-buzz-winner-player.store') }}">
                    @endif

                    {{ csrf_field() }}                   

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">MB 1st Place <span class= "error">*</span></label>
                                <select class="form-control player_id select2" name="mb_first" id="mb_first">
                                    <option value="">Select Player</option>
                                    @foreach(@$players as $player)
                                    <option value="{{@$player['id']}}" @if(@$player['id'] == old('mb_first',@$player_id[0])) selected @endif>{{$player['football_index_common_name']}}</option>
                                    @endforeach
                                </select>                               
                                <label id="mb_first-error" class="error" for="mb_first">{{ @$errors->first('mb_first') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputLastName">Score </label>
                                <input class="form-control" id="mb_first_score" name="mb_first_score" type="text" placeholder="Score" maxlength="10" value="{{ old('mb_first_score', @$playerHistory ? @$score[0] : '') }}">
                                <label id="mb_first_score-error" class="error" for="mb_first_score">{{ @$errors->first('mb_first_score') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputPassword1">MB 2nd Place</label>
                                <select class="form-control player_id select2" name="mb_second" id="mb_second">
                                    <option value="">Select Player</option>
                                    @foreach(@$players as $player)
                                    <option value="{{@$player['id']}}" @if(@$player['id'] == old('mb_second',@$player_id[1])) selected @endif>{{$player['football_index_common_name']}}</option>
                                    @endforeach
                                </select>
                                <label id="mb_second-error" class="error" for="mb_second">{{ @$errors->first('mb_second') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleConfirmPassword">Score</label>
                                <input class="form-control" id="mb_second_score" name="mb_second_score" type="text" placeholder="Score" maxlength="10" value="{{ old('mb_second_score', @$playerHistory ? @$score[1] : '') }}">
                                <label id="mb_second_score-error" class="error" for="mb_second_score">{{ @$errors->first('mb_second_score') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputPassword1">MB 3rd Place</label>
                                <select class="form-control player_id select2" name="mb_third" id="mb_third">
                                    <option value="">Select Player</option>
                                    @foreach(@$players as $player)
                                    <option value="{{@$player['id']}}" @if(@$player['id'] == old('mb_third',@$player_id[2])) selected @endif>{{$player['football_index_common_name']}}</option>
                                    @endforeach
                                </select>
                                <label id="mb_third-error" class="error" for="mb_third">{{ @$errors->first('mb_third') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleConfirmPassword">Score</label>
                                <input class="form-control" id="mb_third_score" name="mb_third_score" type="text" placeholder="Score" maxlength="10" value="{{ old('mb_third_score', @$playerHistory ? @$score[2] : '') }}">
                                <label id="mb_third_score-error" class="error" for="mb_third_score">{{ @$errors->first('mb_third_score') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputPassword1">MB 4rd Place</label>
                                <select class="form-control player_id select2" name="mb_fourth" id="mb_fourth">
                                    <option value="">Select Player</option>
                                    @foreach(@$players as $player)
                                    <option value="{{@$player['id']}}" @if(@$player['id'] == old('mb_fourth',@$player_id[3])) selected @endif>{{$player['football_index_common_name']}}</option>
                                    @endforeach
                                </select>
                                <label id="mb_fourth-error" class="error" for="mb_fourth">{{ @$errors->first('mb_fourth') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleConfirmPassword">Score</label>
                                <input class="form-control" id="mb_fourth_score" name="mb_fourth_score" type="text" placeholder="Score" maxlength="10" value="{{ old('mb_fourth_score', @$playerHistory ? @$score[3] : '') }}">
                                <label id="mb_fourth_score-error" class="error" for="mb_fourth_score">{{ @$errors->first('mb_fourth_score') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputPassword1">MB 5th Place</label>
                                <select class="form-control player_id select2" name="mb_fifth" id="mb_fifth">
                                    <option value="">Select Player</option>
                                    @foreach(@$players as $player)
                                    <option value="{{@$player['id']}}" @if(@$player['id'] == old('mb_fifth',@$player_id[4])) selected @endif>{{$player['football_index_common_name']}}</option>
                                    @endforeach
                                </select>
                                <label id="mb_fifth-error" class="error" for="mb_fifth">{{ @$errors->first('mb_fifth') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleConfirmPassword">Score</label>
                                <input class="form-control" id="mb_fifth_score" name="mb_fifth_score" type="text" placeholder="Score" maxlength="10" value="{{ old('mb_fifth_score', @$playerHistory ? @$score[4] : '') }}">
                                <label id="mb_fifth_score-error" class="error" for="mb_fifth_score">{{ @$errors->first('mb_fifth_score') }}</label>
                            </div>
                        </div>
                    </div>
                    @if(!@$playerHistory)
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleConfirmPassword">Date <span class= "error">*</span></label>
                                <input class="form-control" id="mb_date" name="date" type="text" placeholder="Date" value="{{ old('date', @$playerHistory ? @$playerHistory->date : '') }}">
                                <label id="date-error" class="error" for="date">{{ @$errors->first('date') }}</label>
                            </div>                        
                        </div>
                    </div>                             
                    @endif
                    <button class="btn btn-primary" type="submit">{{ @$playerHistory === null ? 'Save' : 'Update' }}</button>
                    <a class="btn btn-danger" href="{{ route('media-buzz-winner-player.index') }}">Cancel</a>   
                </form>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $("#mb_date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });
    $('.select2').select2({dropdownCssClass: 'bigdrop'});
</script>
@endsection