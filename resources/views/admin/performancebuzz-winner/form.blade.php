@extends('admin.layouts.app')

@section('page_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('performance-buzz-winner-player.index') }}">
                Performance Buzz Winner Player History
            </a>
        </li>
        <li class="breadcrumb-item active">
            {{@$date === null ? 'Add' : 'Edit' }}
        </li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> {{@$date === null ? 'Add' : 'Edit' }} Performance Buzz Winner Players History
        </div>
        <div class="card-body">
            @if(@$date)
            <form method="POST" action="{{ route('performance-buzz-winner-player.update', ['id' => Helper::encrypt(@$date)]) }}">
                {{ method_field('PUT') }}                
                @else
                <form method="POST" action="{{ route('performance-buzz-winner-player.store') }}">
                    @endif
                    {{ csrf_field() }}     

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">PB TOP FORWARD </label>
                                <select class="form-control player_id select2" name="top_forward_player" id="top_forward_player">
                                    <option value="">Select Player</option>
                                    @foreach(@$ForwardPlayers as $player)
                                    <option value="{{ $player->id }}" @if(@$forward->player_id == $player->id) selected @endif>{{ $player->common_name }}</option>
                                    @endforeach
                                </select>                               
                                <label id="top_forward_player-error" class="error" for="top_forward_player">{{ @$errors->first('top_forward_player') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputLastName">Score </label>
                                <input class="form-control number" id="top_forward_score" name="top_forward_score" type="text" placeholder="Score" maxlength="10" value="{{ old('top_forward_score', @$forward->score) }}">
                                <label id="top_forward_score-error" class="error" for="top_forward_score">{{ @$errors->first('top_forward_score') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">PB TOP MIDFIELDER </label>
                                <select class="form-control player_id select2" name="top_midfielder_player" id="top_midfielder_player">
                                    <option value="">Select Player</option>
                                    @foreach(@$MidfielderPlayers as $player)
                                    <option value="{{ $player->id }}" @if(@$midfielder->player_id == $player->id) selected @endif>{{ $player->common_name }}</option>
                                    @endforeach
                                </select>                               
                                <label id="top_midfielder_player-error" class="error" for="top_midfielder_player">{{ @$errors->first('top_midfielder_player') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputLastName">Score </label>
                                <input class="form-control number" id="top_midfielder_score" name="top_midfielder_score" type="text" placeholder="Score" maxlength="10" value="{{ old('top_midfielder_score', @$midfielder->score) }}">
                                <label id="top_midfielder_score-error" class="error" for="top_midfielder_score">{{ @$errors->first('top_midfielder_score') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">PB TOP DEFENDER </label>
                                <select class="form-control player_id select2" name="top_defender_player" id="top_defender_player">
                                    <option value="">Select Player</option>
                                    @foreach(@$DefenderPlayers as $player)
                                    <option value="{{ $player->id }}" @if(@$defender->player_id == $player->id) selected @endif >{{ $player->common_name }}</option>
                                    @endforeach
                                </select>                               
                                <label id="top_defender_player-error" class="error" for="top_defender_player">{{ @$errors->first('top_defender_player') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputLastName">Score </label>
                                <input class="form-control number" id="top_defender_score" name="top_defender_score" type="text" placeholder="Score" maxlength="10" value="{{ old('top_defender_score', @$defender->score) }}">
                                <label id="top_defender_score-error" class="error" for="top_defender_score">{{ @$errors->first('top_defender_score') }}</label>
                            </div>
                        </div>
                    </div>
                    @if(!@$date)
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleConfirmPassword">Date <span class= "error">*</span></label>
                                <input class="form-control" id="mb_date" name="date" type="text" placeholder="Date" value="{{ old('date', @$playerHistory ? @$playerHistory->date : '') }}">
                                <label id="date-error" class="error" for="date">{{ @$errors->first('date') }}</label>
                            </div>                        
                        </div>
                    </div>                             
                    @endif
                    <button class="btn btn-primary" type="submit">{{ @$playerHistory === null ? 'Save' : 'Update' }}</button>
                    <a class="btn btn-danger" href="{{ route('performance-buzz-winner-player.index') }}">Cancel</a>	
                </form>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $('.select2').select2({dropdownCssClass: 'bigdrop'});

    $("#mb_date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });
</script>
@endsection