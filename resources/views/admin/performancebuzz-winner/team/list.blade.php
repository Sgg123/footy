@extends('admin.layouts.app')

@section('page_css')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Performance Buzz Winner Team History</li>
    </ol>
    @include('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">

        <div class="card-body">
            <form name="performancebuzz_search_form" action="" class="row">
                <div class="form-group col-md-3">
                    <input type="text" class="form-control" name="team_name" placeholder="Team name" value="{{ @Request::get('team_name') }}">
                </div>
                <div class="form-group col-md-3">
                    <input type="text" class="form-control" name="from" id="start_date" placeholder="From" value="{{ @Request::get('from') }}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <input type="text" class="form-control" id="end_date" name="to" placeholder="To" value="{{ @Request::get('to') }}" readonly>
                </div>
                <div class="form-group col-md-3 pull-right">
                    <button type="submit" class="btn red-btn">Search</button>
                    <a href="{{route('performance-buzz-winner-team.index')}}" class="btn red-btn">Reset</a>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-dark-header mb-team-table">
                    <thead class="player_heading">
                        <tr>
                            <th>DATE</th>
                            <th >PB TOP PLAYER</th>
                            <th >PB TOP FORWARD</th>
                            <th >PB TOP MIDFIELDER</th>
                            <th >PB TOP DEFENDER</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($PBWinners))
                        @foreach($PBWinners as $history)                                        
                        <tr>
                            <td>{{date('d M Y',strtotime(@$history['date']))}}</td>

                            <!--TOP PLAYER DETAILS-->
                            <td>{{ @$history['topPlayerTeamName'] }}</td>
                            <!--TOP FORWARD DETAILS-->
                            <td>{{ @$history['forwardTeamName'] }}</td>
                            <!--TOP MIDFIELDER DETAILS-->
                            <td>{{ @$history['midfielderTeamName'] }}</td>
                            <!--TOP DEFENDER DETAILS-->
                            <td>{{ @$history['defenderTeamName'] }}</td>
                        </tr>  
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5">No data found</td>                                            
                        </tr>
                        @endif
                    </tbody>
                </table>
                @if (@Request::get('player_name') != '' || @Request::get('from') != '' || @Request::get('to') != '')
                {!! $links->appends(['player_name' => @Request::get('player_name'), 'to' => @Request::get('to'),'from' => @Request::get('from')])->links() !!}
                @else
                {!! $links->links() !!}
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
    });

</script>
@endsection