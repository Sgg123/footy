@extends('admin.layouts.app')

@section('page_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('media-buzz-winner-player.index') }}">
                Performance Buzz Winner Team History
            </a>
        </li>
        <li class="breadcrumb-item active">
            Edit
        </li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Edit Performance Buzz Winner Team History
        </div>
        <div class="card-body">

            <form method="POST" action="{{ route('performance-buzz-winner-team.update', ['id' => Helper::encrypt(@$date)]) }}">
                {{ method_field('PUT') }}                
                {{ csrf_field() }}     

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="exampleInputName">PB TOP FORWARD <span class= "error">*</span></label>
                            <select class="form-control player_id select2" name="top_forward_team" id="top_forward_team">
                                <option value="">Select Team</option>
                                @foreach(@$teams as $team)
                                <option value="{{ $team->id }}" @if(@$forward->team_id == $team->id) selected @endif>{{ $team->name }}</option>
                                @endforeach
                            </select>                               
                            <label id="top_forward_team-error" class="error" for="top_forward_team">{{ @$errors->first('top_forward_team') }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="exampleInputName">PB TOP MIDFIELDER <span class= "error">*</span></label>
                            <select class="form-control player_id select2" name="top_midfielder_team" id="top_midfielder_team">
                                <option value="">Select Team</option>
                                @foreach(@$teams as $team)
                                <option value="{{ $team->id }}" @if(@$midfielder->team_id == $team->id) selected @endif>{{ $team->name }}</option>
                                @endforeach
                            </select>                               
                            <label id="top_midfielder_team-error" class="error" for="top_midfielder_team">{{ @$errors->first('top_midfielder_team') }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="exampleInputName">PB TOP DEFENDER <span class= "error">*</span></label>
                            <select class="form-control player_id select2" name="top_defender_team" id="top_defender_team">
                                <option value="">Select Team</option>
                                @foreach(@$teams as $team)
                                <option value="{{ $team->id }}" @if(@$defender->team_id == $team->id) selected @endif>{{ $team->name }}</option>
                                @endforeach
                            </select>                               
                            <label id="top_defender_team-error" class="error" for="top_defender_team">{{ @$errors->first('top_defender_team') }}</label>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">{{ @$playerHistory === null ? 'Save' : 'Update' }}</button>
                <a class="btn btn-danger" href="{{ route('performance-buzz-winner-team.index') }}">Cancel</a>	
            </form>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script type="text/javascript">
    $('.select2').select2({dropdownCssClass: 'bigdrop'});
</script>

@endsection