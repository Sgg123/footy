@extends('admin.layouts.app')

@section('page_css')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Performance Buzz Winner Players History</li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Performance Buzz Winner Players History
        </div>
        <div class="card-body">
            <form name="performancebuzz_search_form" action="" class="row">
                <div class="form-group col-md-3">
                    <input type="text" class="form-control" name="player_name" placeholder="Player name" value="{{ @Request::get('player_name') }}">
                </div>
                <div class="form-group col-md-3">
                    <input type="text" class="form-control" name="from" id="start_date" placeholder="From" value="{{ @Request::get('from') }}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <input type="text" class="form-control" id="end_date" name="to" placeholder="To" value="{{ @Request::get('to') }}" readonly>
                </div>
                <div class="form-group col-md-3 pull-right">
                    <button type="submit" class="btn red-btn">Search</button>
                    <a href="{{route('performance-buzz-winner-player.index')}}" class="btn red-btn">Reset</a>
                </div>
            </form>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Date</th>   
                            <th>PB TOP PLAYER</th>
                            <th>SCORE</th>
                            <th>PB TOP FORWARD</th>
                            <th>SCORE</th>
                            <th>PB TOP MIDFIELDER</th>
                            <th>SCORE</th>
                            <th>PB TOP DEFENDER</th>
                            <th>SCORE</th>           
                        </tr>
                    </thead>
                    <tbody class="player_alldetails d-view-set">
                        @if(!empty($PBWinners))
                        @foreach($PBWinners as $history)                                        
                        <tr>
                            <td>{{@$history['date']}}</td>

                            <!--TOP PLAYER DETAILS-->
                            <td>                                                
                                {{ @$history['topPlayerFCommonName'] != "" ? @$history['topPlayerFCommonName'] : @$history['topPlayerName'] }}
                            </td>

                            <td>{{ @$history['topPlayerScore'] }}</td>

                            <!--TOP FORWARD DETAILS-->
                            <td>
                                {{ @$history['forwardFCommonName'] != "" ? @$history['forwardFCommonName'] : @$history['forwardName'] }}
                            </td>

                            <td>{{ @$history['forwardScore'] }}</td>

                            <!--TOP MIDFIELDER DETAILS-->
                            <td>
                                {{ @$history['midfielderFCommonName'] != "" ? @$history['midfielderFCommonName'] : @$history['midfielderName'] }}
                            </td>

                            <td>{{ @$history['midfielderScore'] }}</td>

                            <!--TOP DEFENDER DETAILS-->
                            <td>
                                {{ @$history['defenderFCommonName'] != "" ? @$history['defenderFCommonName'] : @$history['defenderName'] }}
                            </td>

                            <td>{{ @$history['defenderScore'] }}</td>
                        </tr>  
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6">No data found</td>                                            
                        </tr>
                        @endif
                    </tbody>
                </table>
                @if (@Request::get('player_name') != '' || @Request::get('from') != '' || @Request::get('to') != '')
                {!! $links->appends(['player_name' => @Request::get('player_name'), 'to' => @Request::get('to'),'from' => @Request::get('from')])->links() !!}
                @else
                {!! $links->links() !!}
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
    });

</script>
@endsection