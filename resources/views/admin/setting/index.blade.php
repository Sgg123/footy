@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{route('settings')}}">
                Settings
            </a>
        </li>        
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Manage Settings
        </div>
        <div class="card-body">
            
            <form method="POST" action="{{ route('settings') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Title </label>
                                <input class="form-control pointer-disabled" name="title" type="text" aria-describedby="nameHelp" placeholder="Enter title" value ="{{ @$settings->title }}" readonly>
                            </div>
                        </div>
                    </div>   
                    
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Value <span class= "error">*</span></label>
                                <input class="form-control" name="value" type="text" aria-describedby="nameHelp" placeholder="Enter subscription charge" value ="{{ @$settings->value }}" autofocus>
                                <label id="value-error" class="error" for="value">{{ @$errors->first('value') }}</label>
                            </div>
                        </div>
                    </div>         
                    <input type="submit" value="Update" class="btn btn-primary"/>
                    <a class="btn btn-danger" href="{{ url('backoffice-fis') }}">Cancel</a>	
                </form>
        </div>
    </div>
</div>

@endsection

@section('page_js')
@endsection