@extends('admin.layouts.app')

@section('page_css')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{route('advertisement.index')}}">
                Advertisement
            </a>
        </li>
        <li class="breadcrumb-item active">Edit</li>
    </ol>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Edit Advertisement
        </div>
        @include ('alerts.alert')
        <div class="card-body">
            <form method="POST" action="{{ route('advertisement.update') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="link">Url<span class= "error">*</span></label>
                            <input class="form-control" required name="link" type="text" aria-describedby="link" placeholder="Enter link" value ="{{ old('link',$banner->link) }}" id="link">
                            <label id="link-error" class="error" for="link">{{ @$errors->first('link') }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="banner_image">Banner Image <span class= "error">*</span></label>  
                            @if($banner->type == 'left' || $banner->type == 'right')
                                <small class="text-muted">( upload image of size 300 * 600 for better result. )</small>
                            @elseif($banner->type == 'top' || $banner->type == 'bottom')
                                <small class="text-muted">( upload image of size 1070 * 145 for better result. )</small>
                            @endif
                            <input name="banner_image" id="banner_image" accept=".jpg,.jpeg,.png" type="file" class="form-control image"/>
                            <label id="banner_image-error" class="error" for="banner_image">{{ @$errors->first('banner_image') }}</label>
                        </div>
                        <div class="col-md-6">
                            <div class="add-preview-main">
                                <img  name="preview" id="preview" class="article-image"
                                  @if (file_exists('uploads/banner/' . @$banner->image) &&  @$banner->image != '')
                                  src="{{ asset('uploads/banner/'.@$banner->image) }}"
                                  @else
                                  src="{{ asset('assets/images/no-image.jpeg') }}"
                                  @endif                                                 
                                  >
                            </div>
                        </div>
                    </div>
                </div>    
                 @php $id = Helper::encrypt($banner->id); @endphp
                <input type="hidden" value="{{$id}}" name="id" />
                <input name="type" id="type" type="hidden" value="{{$banner->type}}" class="form-control image"/>
                <input type="submit" value="Update" class="btn btn-primary" href=""/>
                <a class="btn btn-danger" href="{{ route('advertisement.index')}}">Cancel</a>  
            </form>
        </div>
    </div>
</div>

@endsection
@section('page_js')
<script type="text/javascript">
    $("#banner_image").change(function () {
        readURLEdit2(this);
    });

    function readURLEdit2(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#preview").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
