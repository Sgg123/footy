@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Banner</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
       <div class="card-header row">
            <div class="text-left col-md-3">
                <i class="fa fa-table"></i> Advertisement     
            </div>
            <div class="text-right col-md-9">
                @if(Helper::bennerCount('bottom') == 0)    
                    <a href="{{route('advertisement.create','bottom')}}" class="btn btn-sm btn-primary">
                        <i class="fa fa-fw fa-plus"></i>Add Bottom Banner
                    </a>
                @endif
                @if(Helper::bennerCount('top') == 0)    
                    <a href="{{route('advertisement.create','top')}}" class="btn btn-sm btn-info">
                        <i class="fa fa-fw fa-plus"></i>Add Top Banner
                    </a>
                @endif
                @if(Helper::bennerCount('left') == 0)    
                    <a href="{{route('advertisement.create','left')}}" class="btn btn-sm btn-danger">
                        <i class="fa fa-fw fa-plus"></i>Add Left Banner
                    </a>
                @endif
                @if(Helper::bennerCount('right') == 0)    
                    <a href="{{route('advertisement.create','right')}}" class="btn btn-sm btn-warning">
                        <i class="fa fa-fw fa-plus"></i>Add Right Banner
                    </a>
                @endif
             </div>
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Image</th>
                            <th>Type</th>
                            <th>Url</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($banner) <= 0)
                        <tr>
                            <td colspan="6" class="text-center">Sorry no banners!</td>
                        </tr>
                        @else  
                        @php $count = 1; @endphp
                        @foreach($banner as $key => $b)
                             @php $id = Helper::encrypt($b['id']); @endphp
                        <tr>
                            <td>{{$count}}</td>
                            <td>
                                <img
                                    @if($b['image'] != '' && file_exists(public_path('uploads/banner/'.$b['image'])))
                                    src="{{ asset('uploads/banner/'.$b['image']) }}"
                                    @else
                                    src="{{ asset('assets/images/no-image.jpeg') }}"
                                    @endif
                                    class="image-icon">
                            </td>
                           
                            <td>{{$b['type']}}</td>
                            <td>{{$b['link']}}</td>
                            <td nowrap="">
                                <a href="{{route('advertisement.edit',$id)}}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                <a href="javascript:void(0)" data-url="{{route('advertisement.destroy',$id)}}" class="btn btn-sm btn-danger delete-list-record"><i class="fa fa-fw fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                        @php $count++;  @endphp
                        @endforeach
                        @endif 
                    </tbody> 
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    {{ $banner->links() }}
                </div> 
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_js')
<div class="modal fade" id="deleteBannerConfirmModal" tabindex="-1" role="dialog" aria-labelledby="deleteBannerConfirmModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="POST" action="" id="delete-list-form">
    {{ csrf_field() }}
      <div class="modal-header">
        <h5 class="modal-title">Are you sure you want to delete this record?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">You can not recover deleted item.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
        <button class="btn btn-danger">Yes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function() {
    $(".delete-list-record").on("click", function(){
        $deleteUrl = $(this).attr('data-url');
        $("#delete-list-form").attr('action', $deleteUrl);
        $("#deleteBannerConfirmModal").modal();
    });
});
</script>
@endsection