<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{ url('backoffice-fis') }}">{{ config('app.name', 'Laravel') }}</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link {{ @$activeMenu == 'dashboard' ? 'active' : '' }}" href="{{ url('backoffice-fis') }}">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Dashboard</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Manage Articles">
                <a class="nav-link nav-link-collapse {{ @$activeMenu == 'manage-article' ? '' : 'collapsed' }}" data-toggle="collapse" href="#collapseExamplePages">
                    <i class="fa fa-fw fa-newspaper-o"></i>
                    <span class="nav-link-text">Manage Articles</span>
                </a>
                <ul class="sidenav-second-level collapse {{ @$activeMenu == 'manage-article' ? 'show' : '' }}" id="collapseExamplePages">
                    <li class="{{ @$activeSubMenu == 'article-category' ? 'active' : ''}}">
                        <a href="{{route('article-category.index')}}"><i class="fa fa-fw fa-tags"></i> Categories</a>
                    </li>
                    <li class="{{ @$activeSubMenu == 'articles' ? 'active' : ''}}">
                        <a href="{{route('articles.index')}}">
                            <i class="fa fa-fw fa-newspaper-o"></i>
                            Articles</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Manage Banner">
                <a class="nav-link nav-link-collapse {{ @$activeMenu == 'manage-banner' ? '' : 'collapsed' }}" data-toggle="collapse" href="#collapsebannerPages">
                    <i class="fa fa-fw fa-picture-o"></i>
                    <span class="nav-link-text">Manage Banner</span>
                </a>
                <ul class="sidenav-second-level collapse {{ @$activeMenu == 'manage-banner' ? 'show' : '' }}" id="collapsebannerPages">
                    <li class="{{ @$activeSubMenu == 'banner' ? 'active' : ''}}">
                        <a href="{{route('banner.index')}}">
                            <i class="fa fa-fw fa-picture-o"></i>
                            Banner</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Manage Banner">
                <a class="nav-link nav-link-collapse {{ @$activeMenu == 'manage-advertisement' ? '' : 'collapsed' }}" data-toggle="collapse" href="#collapsebannerPages">
                    <i class="fa fa-fw fa-picture-o"></i>
                    <span class="nav-link-text">Manage Banner</span>
                </a>
                <ul class="sidenav-second-level collapse {{ @$activeMenu == 'manage-advertisement' ? 'show' : '' }}" id="collapsebannerPages">
                    <li class="{{ @$activeSubMenu == 'advertisement' ? 'active' : ''}}">
                        <a href="{{route('advertisement.index')}}">
                            <i class="fa fa-fw fa-picture-o"></i>
                            Banner</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Manage Articles">
                <a class="nav-link nav-link-collapse {{ @$activeMenu == 'media-buzz' ? '' : 'collapsed'}}" data-toggle="collapse" href="#collapseMediaBuzzPages" data-parent="#exampleAccordion" {{ @$activeMenu == 'media-buzz' ? 'aria-expanded="true"' : ''}}>
                    <i class="fa fa-fw fa-video-camera"></i>
                    <span class="nav-link-text">Media Buzz Winners</span>
                </a>
                <ul class="sidenav-second-level collapse {{ @$activeMenu == 'media-buzz' ? 'show' : ''}}" id="collapseMediaBuzzPages">
                    <li class="{{ @$activeSubMenu == 'media-buzz-player-history' ? 'active' : ''}}">
                        <a href="{{ route('media-buzz-winner-player.index') }}"><i class="fa fa-fw fa-history"></i> Player History</a>
                    </li>
                    <li class="{{ @$activeSubMenu == 'media-buzz-team-history' ? 'active' : ''}}">
                        <a href="{{ route('media-buzz-winner-team.index') }}">
                            <i class="fa fa-fw fa-history"></i>
                            Team History</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Performance Buzz">
                <a class="nav-link nav-link-collapse {{ @$activeMenu == 'performance-buzz' ? '' : 'collapsed'}}" data-toggle="collapse" href="#collapsePerformanceBuzzPages" data-parent="#exampleAccordion" {{ @$activeMenu == 'performance-buzz' ? 'aria-expanded="true"' : ''}}>
                    <i class="fa fa-fw fa-video-camera"></i>
                    <span class="nav-link-text">Performance Buzz</span>
                </a>
                <ul class="sidenav-second-level collapse {{ @$activeMenu == 'performance-buzz' ? 'show' : ''}}" id="collapsePerformanceBuzzPages">
                    <li class="{{ @$activeSubMenu == 'performance-buzz-player-history' ? 'active' : ''}}">
                        <a href="{{ route('performance-buzz-winner-player.index') }}"><i class="fa fa-fw fa-history"></i> Player History</a>
                    </li>
                    <li class="{{ @$activeSubMenu == 'performance-buzz-team-history' ? 'active' : ''}}">
                        <a href="{{ route('performance-buzz-winner-team.index') }}">
                            <i class="fa fa-fw fa-history"></i>
                            Team History</a>
                    </li>
                    <li class="{{ @$activeSubMenu == 'performance_buzz_files_list' ? 'active' : ''}}">
                        <a href="{{ route('performance_buzz_files_list') }}">
                            <i class="fa fa-fw fa-history"></i>
                            Add PB Data File</a>
                    </li>
                    <li class="{{ @$activeSubMenu == 'performance-buzz-datapoints' ? 'active' : ''}}">
                        <a href="{{ route('performance-buzz.index') }}">
                            <i class="fa fa-fw fa-history"></i>
                            Performance Buzz History</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item {{ @$activeMenu == 'cahsback-emails' ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="Cashback Offer Emails">
                <a class="nav-link" href="{{ route('cashback-offer.index') }}">
                    <i class="fa fa-fw fa-envelope"></i>
                    <span class="nav-link-text">Cashback Offer Emails</span>
                </a>
            </li>
           <li class="nav-item {{ @$activeMenu == 'contactus-emails' ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="Contact Emails">
                <a class="nav-link" href="{{ route('contactus-emails.index') }}">
                    <i class="fa fa-fw fa-envelope"></i>
                    <span class="nav-link-text">ContactUs Emails</span>
                </a>
            </li>
            <li class="nav-item {{ @$activeMenu == 'pages' ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="Pages">
                <a class="nav-link" href="{{ route('pages.index') }}">
                    <i class="fa fa-fw fa-navicon"></i>
                    <span class="nav-link-text">Pages</span>
                </a>
            </li>
            <li class="nav-item {{ @$activeMenu == 'spreadsheets' ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="Spreadsheets">
                <a class="nav-link" href="{{ route('spreadsheets.index') }}">
                    <i class="fa fa-fw fa-navicon"></i>
                    <span class="nav-link-text">Spreadsheets</span>
                </a>
            </li>
            <li class="nav-item {{ @$activeMenu == 'users' ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="Users">
                <a class="nav-link" href="{{ route('users') }}">
                    <i class="fa fa-fw fa-users"></i>
                    <span class="nav-link-text">Users</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="SportMonks Data">
                <a class="nav-link nav-link-collapse {{ @$activeMenu == 'games' ? '' : 'collapsed' }}" data-toggle="collapse" href="#collapseSeasonsPages">
                    <i class="fa fa-fw fa-newspaper-o"></i>
                    <span class="nav-link-text">SportMonks Data</span>
                </a>
                <ul class="sidenav-second-level collapse {{ @$activeMenu == 'games' ? 'show' : '' }}" id="collapseSeasonsPages">
                    <li class="{{ @$activeSubMenu == 'seasons' ? 'active' : ''}}">
                        <a href="{{route('seasons')}}"><i class="fa fa-fw fa-tags"></i> Seasons</a>
                    </li>            
                    <li class="{{ @$activeSubMenu == 'fixtures' ? 'active' : ''}}">
                        <a href="{{route('fixtures')}}"><i class="fa fa-fw fa-tags"></i> Fixtures</a>
                    </li>            
                    <li class="{{ @$activeSubMenu == 'fetch-api-data' ? 'active' : ''}}">
                        <a href="{{route('sportmonk.fetch.data')}}"><i class="fa fa-fw fa-tags"></i> Sports Monk Api</a>
                    </li>            
                    <li class="{{ @$activeSubMenu == 'teams' ? 'active' : ''}}">
                        <a href="{{route('teams')}}"><i class="fa fa-fw fa-tags"></i> Teams</a>
                    </li>   
                    <li class="{{ @$activeSubMenu == 'day_and_dividends_list' ? 'active' : ''}}">
                        <a href="{{route('day_and_dividends_list')}}"><i class="fa fa-fw fa-tags"></i> Days and Dividends</a>
                    </li>       
                </ul>
            </li>
            <li class="nav-item {{ @$activeMenu == 'settings' ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="Settings">
                <a class="nav-link" href="{{ route('settings') }}">
                    <i class="fa fa-fw fa-cog"></i>
                    <span class="nav-link-text">Settings</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('change-password') }}">
                    <i class="fa fa-fw fa-cog"></i>Reset Password</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-fw fa-sign-out"></i>Logout</a>
            </li>
        </ul>
    </div>
</nav>