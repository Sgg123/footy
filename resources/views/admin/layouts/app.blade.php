<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'FOOTY INDEX SCOUT') }}</title>
  <!-- Bootstrap core CSS-->
  <link href="{{ asset('assets/admin/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{ asset('assets/admin/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="{{ asset('assets/admin/css/sb-admin.css') }}" rel="stylesheet">
  @yield('page_css')
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  @include('admin.layouts.header')
  <div class="content-wrapper">
    @yield('content')
    @include('admin.layouts.footer')
  </div>
</body>
@yield('page_js')
</html>

