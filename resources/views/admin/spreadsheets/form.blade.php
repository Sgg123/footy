@extends('admin.layouts.app')

@section('page_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">
            <a href="{{ url()->route('spreadsheets.index') }}">Spreadsheets</a>
        </li>
        <li class="breadcrumb-item">{{ @$spreadsheet->title }}</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Spreadsheets
        </div>
        <div class="clearfix"></div>

        <div class="card-body">

          @if(isset($spreadsheet->id) && $spreadsheet->id != "")
            <form method="POST" action="{{ route('spreadsheets.update', ['id' => @$spreadsheet->id]) }}" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{@$spreadsheet->id}}">
                {{ method_field('PUT') }}
                @else
                <form method="POST" action="{{ route('spreadsheets.store') }}" enctype="multipart/form-data">
                    @endif
                    {{ csrf_field() }}  
                    <input type="hidden" name="id" value="{{@$spreadsheet->id}}">
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Title <span class= "error">*</span></label>
                                <input class="form-control" id="title" name="title" type="text" placeholder="Title" maxlength="255" value="{{ old('title', @$spreadsheet->title) }}">
                                <label id="title-error" class="error" for="title">{{ @$errors->first('title') }}</label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputLastName">Slug <span class= "error">*</span></label>
                                <input class="form-control" id="slug" name="slug" type="text" placeholder="Slug" maxlength="255" value="{{ old('slug', @$spreadsheet->slug) }}">
                                <label id="slug-error" class="error" for="slug">{{ @$errors->first('slug') }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Banner Image <span class= "error">*</span></label> 
                                <small class="text-muted">( Use 1920 X 1280 size for better result.)</small>
                                <input class="form-control" id="file_path" name="file_path" type="file">
                                <label id="file_path-error" class="error" for="file_path">{{ @$errors->first('image') }}</label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Update</button>
                    <a class="btn btn-danger" href="{{ route('spreadsheets.index') }}">Cancel</a> 
                </form>

        </div>
    </div>
</div>

@endsection

@section('page_js')
<script src="{{ asset('assets/admin/js/jquery.seourl.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script src="{{ asset('assets/front/js/summernote-image-attributes.js') }}"></script>

<script type="text/javascript">
    $(function () {
        $('.summernote').summernote({
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ],
            },            
            imageAttributes:{
                icon:'<i class="note-icon-pencil"/>',                
            },
            height: 250,
            toolbar: [
                ['style', ['style']],
                ['group2', ['fontsize']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture', 'hr', 'link', 'video']],
                ['view', ['fullscreen', 'codeview']],
                ['table', ['table']]
            ],
            fontNames: ['Segoe UI', 'Arial', 'Helvetica', 'Comic Sans MS', 'Calibri', 'Consolas', 'Courier New', 'Garamond', 'Georgia', 'Lucida Console', 'Lucida Sans', 'Segoe UI', 'Tahoma',
                'Tempus Sans ITC', 'Times New Roman', 'Trebuchet MS', 'Verdana'],
        });
    });
    $(document).on("focusout", "#title", function () {
        var pageTitle = $(this).val().toLowerCase();
        var pageSlug = pageTitle.seoURL();
        $("#slug").val(pageSlug);
    });
</script>
@endsection