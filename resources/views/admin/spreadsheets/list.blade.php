@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Spreadsheets</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Spreadsheets
            <a href="{{route('spreadsheets.create')}}" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        	<th>#</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>File Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                       @foreach($spreadsheets as $spreadsheet)
                       <tr>
                       		<td>{{ $spreadsheet->id }}</td>
                       		<td>{{ $spreadsheet->title }}</td>
                       		<td>{{ $spreadsheet->slug }}</td>
                          <td>{{ $spreadsheet->file_name }}</td>
                       		<td>
                       			<a class="btn btn-sm btn-success" href="{{ route('spreadsheets.edit', [$spreadsheet->id]) }}"><i class="fa fa-fw fa-pencil"></i> Edit</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-danger delete-list-record" data-url="{{ route('spreadsheets.destroy', ['id' => $spreadsheet->id]) }}"><i class="fa fa-fw fa-trash"></i>Delete</a>
                       		</td>
                       </tr>
                       @endforeach
                    </tbody> 
                </table>
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('page_js')

@include('alerts.delete-confirm')

@endsection