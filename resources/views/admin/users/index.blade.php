@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Users</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-users"></i> Users   <a href="{{ route('users.add') }}" class="btn btn-sm btn-success pull-right text-white"><i class="fa fa-plus"></i> Add</a>
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Account Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($users) <= 0)
                        <tr>
                            <td colspan="6" class="text-center">Sorry no users!</td>
                        </tr>
                        @else                          
                        @foreach($users as $user)
                        @php $id = Helper::encrypt($user['id']); @endphp
                        <tr>
                            <td>{{$user['firstname']}}</td>
                            <td>{{$user['lastname']}}</td>
                            <td>{{$user['email']}}</td>
                            <td>{{$user['phone']}}</td>
                            <td>{{str_replace('_',' ',$user['account_type'])}}</td>
                            <td nowrap="">
                                @php $id = Helper::encrypt($user['id']); @endphp
                                 <a href="{{ route('users.edit', $id) }}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                <a href="javascript:void(0)" data-url="{{route('users.delete',$id)}}" class="btn btn-sm btn-danger delete-list-record"><i class="fa fa-fw fa-trash"></i>Delete</a>                                
                            </td>
                        </tr>
                        @endforeach
                        @endif 
                    </tbody> 
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    {{ $users->links() }}
                </div> 
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_js')
@include('alerts.delete-confirm')
@endsection
