@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{route('users')}}">
                Users
            </a>
        </li>
        <li class="breadcrumb-item active">Edit</li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Edit User
        </div>
        <div class="card-body">
            <form class="form-horizontal" method="POST" action="{{ route('users.update',Helper::encrypt(@$data->id)) }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">First Name</label>

                    <div class="col-md-6">
                        <input id="firstname" type="text" class="form-control" name="firstname" value="{{@$data->firstname}}" required autofocus>

                        @if ($errors->has('firstname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Last Name</label>

                    <div class="col-md-6">
                        <input id="lastname" type="text" class="form-control" name="lastname" value="{{@$data->lastname}}" required autofocus>

                        @if ($errors->has('lastname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lastname') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{@$data->email}}" required>

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">Phone</label>

                    <div class="col-md-6">
                        <input id="phone" type="text" class="form-control" name="phone" value="{{@$data->phone}}" required>

                        @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('account_type') ? ' has-error' : '' }}">
                    <label for="account_type" class="col-md-4 control-label">Account Type</label>

                    <div class="col-md-6">
                        <select class="form-control" name="account_type">
                            <option value="">Select Account type </option>
                            <option value="tier_2" {{ @$data->account_type == 'tier_2' ? 'selected' : ''}}>Tier 2</option>
                            <option value="tier_3" {{ @$data->account_type == 'tier_3' ? 'selected' : ''}}>Tier 3</option>
                        </select>
                        
                        @if ($errors->has('account_type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('account_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>
                         <a class="btn btn-danger" href="{{ route('users')}}">Cancel</a>
                    </div>
                </div>
            </form>           
        </div>
    </div>
</div>

@endsection

@section('page_js')

@endsection
