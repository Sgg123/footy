@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Cashback Offer Emails</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Cashback Offer Emails
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Source</th>
                            <th>Referral Code</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count(@$emails) <= 0)
                        <tr>
                            <td colspan="4" class="text-center">Sorry no cashback emails found!</td>
                        </tr>
                        @else  
                        @foreach(@$emails as $email)
                        @php 
                        	$id = Helper::encrypt($email->id);
                        @endphp
                        <tr>
                            <td>{{ $email->email }}</td>
                            <td>{{ $email->source }}</td>
                            <td>{{ $email->reference_code }}</td>
                            <td nowrap="">
                              <a href="javascript:void(0)" data-url="{{route('cashback-offer.destroy', $id)}}" class="btn btn-sm btn-danger delete-list-record"><i class="fa fa-fw fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        @endif 
                    </tbody> 
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    {{ @$emails->links() }}
                </div> 
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_js')
@include('alerts.delete-confirm')
@endsection