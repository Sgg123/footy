@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Pages</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Pages
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        	<th>#</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                       @foreach($pages as $page)
                       <tr>
                       		<td>{{ $page->id }}</td>
                       		<td>{{ $page->title }}</td>
                       		<td>{{ $page->slug }}</td>
                       		<td>
                       			<a class="btn btn-sm btn-success" href="{{ route('pages.edit', [ Helper::encrypt($page->id)]) }}"><i class="fa fa-fw fa-pencil"></i> Edit</a>
                       		</td>
                       </tr>
                       @endforeach
                    </tbody> 
                </table>
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('page_js')
@endsection