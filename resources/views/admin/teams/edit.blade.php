@extends('admin.layouts.app')

@section('page_css')
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{route('teams')}}">
                Teams
            </a>
        </li>
        <li class="breadcrumb-item active">Edit</li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Edit Team
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('teams.update', ['id' => Helper::encrypt(@$team->id)]) }}" enctype="multipart/form-data">

                {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="exampleInputName">Name <span class= "error">*</span></label>
                            <input class="form-control" required name="name" type="text" aria-describedby="nameHelp" placeholder="Enter Name " value ="{{ @$team->name }}" id="name">
                            <label id="name-error" class="error" for="name">{{ @$errors->first('name') }}</label>
                        </div>

                        <div class="col-md-6">
                            <label for="exampleInputName">Twitter <span class= "error">*</span></label>
                            <input class="form-control" name="twitter" type="text" aria-describedby="nameHelp" placeholder="Enter Twitter Account" value ="{{ @$team->twitter }}" id="twitter">
                            <label id="twitter-error" class="error" for="twitter">{{ @$errors->first('twitter') }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">                       
                        <div class="col-md-6">
                            <label for="exampleInputName">Founded <span class= "error">*</span></label>
                            <input class="form-control" name="founded" type="text" aria-describedby="nameHelp" placeholder="Enter Founded Year" value ="{{ @$team->founded }}" id="founded">
                            <label id="founded-error" class="error" for="founded">{{ @$errors->first('founded') }}</label>
                        </div>

                        <div class="col-md-6">
                            <label for="exampleInputName">National Team <span class= "error">*</span></label>
                            <input class="form-control" name="national_team" type="text" aria-describedby="nameHelp" placeholder="Enter Common Name" value ="{{ @$team->national_team }}" id="national_team">
                            <label id="national_team-error" class="error" for="national_team">{{ @$errors->first('national_team') }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="exampleInputName">Image </label>  
                            <small class="text-muted">( upload image of size 150 * 150 for better result. )</small>                        
                            <input name="local_image_path" id="update-thumbnail-image" accept=".jpg,.jpeg,.png" type="file" class="form-control
                                   @if (file_exists('uploads/team-images/' . @$team->local_image_path) &&  @$team->local_image_path != '' &&  @$team->is_local_image == 1)
                                   edit_image
                                   @else
                                   image
                                   @endif" />
                            <label id="local_image_path-error" class="error" for="local_image_path">{{ @$errors->first('local_image_path') }}</label>
                        </div>
                        <div class="col-md-6">
                            <div class="add-preview-main">
                                <img  name="thumbnail-preview" id="thumbnail-preview" class="article-image"
                                      @if (file_exists('uploads/team-images/' . @$team->local_image_path) &&  @$team->local_image_path != '' &&  @$team->is_local_image == 1)
                                      src="{{ asset('uploads/team-images/'.@$team->local_image_path) }}"
                                      @else
                                      src="{{ @$team->logo_path }}"
                                      @endif                                                 
                                      >
                            </div>
                        </div>

                    </div>
                </div>

                <input type="submit" value="Update" class="btn btn-primary" />
                <a class="btn btn-danger" href="{{ route('teams')}}">Cancel</a>	
            </form>
        </div>
    </div>
</div>

@endsection

@section('page_js')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">

    $("#update-image").change(function () {
        readURLEdit(this);
    });

    function readURLEdit(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#preview").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    //Thumbnail Image Preview
    $("#update-thumbnail-image").change(function () {
        readURLEdit2(this);
    });

    function readURLEdit2(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#thumbnail-preview").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection