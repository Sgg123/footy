<div>
    <p>Hi </p>
    <p>
        A new user has completed the Cashback Sign Up Form:
    </p>
    <br>
    <p>Paypal Email: {{@$cashback['email']}} </p>
    <p>Source: {{@$cashback['source']}} </p>
    @if(@$cashback['reference_code'] != "")
    <p>FIS Refer a Friend Code: {{@$cashback['reference_code']}} </p>    
    @endif
    <p>Accepted Terms and Conditions: Yes </p>
    <p>Date and Time submitted: {{ @$cashback['updated_at']}}</p>
</div>