<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;
use App\Page;
use App\League;
use App\Fixture;
use App\PerformanceBuzz;
use App\Player;
use DB;
use Helper;

class PerformanceBuzzStatisticsController extends Controller {

    protected $perPage = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'performance-buzz', 'activeSubMenu' => 'performance-buzz-statistics']);
    }

    public function index(Request $request) {

        if (\Auth::user()->isPremium()) {

            $data['minGame'] = $minGame = $request->has('min_games') && $request->min_games >= 3 ? $request->min_games : 0;

            $data['isDay'] = $isDay = false;
            $data['isLeague'] = $isLeague = false;
            $data['isDayStart'] = $isDayStart = '';
            $data['isDayEnd'] = $isDayEnd = date('Y-m-d', strtotime('now'));
            $data['last'] = '';

            $data['from'] = '';
            $data['to'] = '';
            $all_league_dates = '';
            $data['league_dates'] = '';

            if ($request->has('league_id') && $request->league_id != '') {
                $data['isLeague'] = $isLeague = true;
                $league_dates = Fixture::where('league_id', $request->league_id)->select(DB::raw('DISTINCT DATE(starting_at) AS dates'))->get();
                // $league_dates=Fixture::where('league_id',$request->league_id)->select(DB::raw('group_concat(DISTINCT DATE(starting_at),"") AS dates'))->get();
                foreach ($league_dates as $league_date) {
                    $all_dates[] = '\'' . $league_date->dates . '\'';
                }
                $all_league_dates = implode(',', $all_dates);
                $data['league_dates'] = $all_league_dates;
            }
            if (($request->has('from') && $request->has('to')) && ($request->from != '' && $request->to != '')) {

                $data['isDay'] = $isDay = true;
                $data['from'] = $isDayStart = $data['isDayStart'] = $request->from;
                $data['to'] = $isDayEnd = $data['isDayEnd'] = $request->to;
            } elseif ($request->has('last') && $request->last == 30) {
                $data['isDay'] = $isDay = true;
                $data['isDayStart'] = $isDayStart = date('Y-m-d', strtotime('now -30 days'));
                $data['last'] = 30;
            } elseif ($request->has('last') && $request->last == 100) {
                $data['isDay'] = $isDay = true;
                $data['isDayStart'] = $isDayStart = date('Y-m-d', strtotime('now -100 days'));
                $data['last'] = 100;
            } elseif ($request->has('last') && $request->last == 20152016) {
                $data['isDay'] = $isDay = true;
                $data['from'] = $isDayStart = $data['isDayStart'] = '2015-10-03';
                $data['to'] = $isDayEnd = $data['isDayEnd'] = '2016-05-28';
            } elseif ($request->has('last') && $request->last == 20162017) {
                $data['isDay'] = $isDay = true;
                $data['from'] = $isDayStart = $data['isDayStart'] = '2016-08-13';
                $data['to'] = $isDayEnd = $data['isDayEnd'] = '2017-06-03';
            } elseif ($request->has('last') && $request->last == 20172018) {
                $data['isDay'] = $isDay = true;
                $data['from'] = $isDayStart = $data['isDayStart'] = '2017-08-11';
                $data['to'] = $isDayEnd = $data['isDayEnd'] = '2018-05-26';
             }elseif ($request->has('last') && $request->last == 20182019) {
                $data['isDay'] = $isDay = true;
                $data['from'] = $isDayStart = $data['isDayStart'] = '2018-08-10';
                $data['to'] = $isDayEnd = $data['isDayEnd'] = '2019-06-01';
            }
            elseif ($request->has('last') && $request->last == 20192020) {
               $data['isDay'] = $isDay = true;
               $data['from'] = $isDayStart = $data['isDayStart'] = '2019-08-09';
               $data['to'] = $isDayEnd = $data['isDayEnd'] = '2020-08-23';
           }elseif ($request->has('last') && $request->last == 20202021) {
               $data['isDay'] = $isDay = true;
               $data['from'] = $isDayStart = $data['isDayStart'] = '2020-08-21';
               $data['to'] = $isDayEnd = $data['isDayEnd'] = '2021-05-29';
           }


            if ($request->has('sort') && $request->sort == "form_guide") {
                $order_column = DB::raw('IF((SELECT COUNT(*) FROM performance_buzz WHERE  performance_buzz.player_id = players.id)>5, round(players.form_guide / 5), round(players.form_guide / (SELECT COUNT(*) FROM performance_buzz WHERE  performance_buzz.player_id = players.id)))');
                $order = $request->order;
            } elseif ($request->has('sort') && $request->sort == "last_score") {
                $order_column = 'last_score';
                $order = $request->order;
            } elseif ($request->has('sort') && $request->sort == "lineups") {
                $order_column = 'lineups';
                $order = $request->order;
            } elseif ($request->has('sort') && $request->sort == "top_player_wins") {
                $order_column = 'top_player_wins';
                $order = $request->order;
            } elseif ($request->has('sort') && $request->sort == "positional_wins") {
                $order_column = 'positional_wins';
                $order = $request->order;
            } elseif ($request->has('sort') && $request->sort == "player_name") {
                $order_column = 'football_index_common_name';
                $order = $request->order;
            } else {
                $order_column = DB::raw('IF((SELECT COUNT(*) FROM performance_buzz WHERE  performance_buzz.player_id = players.id)>5, round(players.form_guide / 5), round(players.form_guide / (SELECT COUNT(*) FROM performance_buzz WHERE  performance_buzz.player_id = players.id)))');
                $order = 'DESC';
            }
            // echo $order_column; exit;

            $data['page'] = Page::find(6);

            if(($request->has('from') && $request->has('to')) && ($request->from != '' && $request->to != ''))
            {
              $scoreval = DB::raw("(SELECT score from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id` AND date BETWEEN '" . $request->from . "' AND '" . $request->to . "' order by date DESC LIMIT 1) AS last_score");
            }
            else
            {
              $scoreval = DB::raw("(SELECT score from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id` order by date DESC LIMIT 1) AS last_score");
            }

            $data['performanceBuzzStates'] = Player::
                    orderBy($order_column, $order)
                    ->join('teams', 'teams.id', 'players.team_id')
                    ->select('players.common_name as player_name', 'players.id as player_id', 'players.injured', 'players.football_index_common_name as football_index_common_name', 'players.position', 'players.football_index_position', 'players.injured', 'players.local_image_path', 'players.is_local_image', 'players.image_path', 'teams.name as team', 'teams.id as team_id', DB::raw('IF((SELECT COUNT(*) FROM performance_buzz WHERE  performance_buzz.player_id = players.id)>5, round(players.form_guide / 5), round(players.form_guide / (SELECT COUNT(*) FROM performance_buzz WHERE  performance_buzz.player_id = players.id))) as form_guide'), $scoreval)
                    ->when($isDay, function($query) use ($isDayStart, $isDayEnd) {
                        $query->addSelect(DB::raw("(SELECT COUNT(id) from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id` AND date BETWEEN '" . $isDayStart . "' AND '" . $isDayEnd . "')  AS lineups"));
                    })
                    ->when(!$isDay, function($query) {
                        $query->addSelect(DB::raw("(SELECT COUNT(id) from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id`) AS lineups"));
                    })
                    ->having('lineups', '>=', $minGame)
                    ->where('players.football_index_common_name', '!=', NULL)
                    ->when($request->keyword != "" || $request->position != "", function($query) use ($request) {

                        if ($request->keyword != "" && $request->position != "") {

                            $query->whereRaw("(players.football_index_common_name like '%" . $request->keyword . "%' OR teams.name like '%" . $request->keyword . "%')")->where('players.football_index_position', $request->position);
                        } elseif ($request->keyword != "") {

                            $query->whereRaw("(players.football_index_common_name like '%" . $request->keyword . "%' OR teams.name like '%" . $request->keyword . "%')");
                        } elseif ($request->position != "") {

                            $query->where('players.football_index_position', $request->position);
                        }
                    })
                    ->when($request->league_id != '', function($query) use ($request){
                        $query->where('players.league_id',$request->league_id);
                    })
                    ->when($isDay, function($query) use ($isDayStart, $isDayEnd, $minGame) {
                        return $query->whereRaw("(SELECT COUNT(id) from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id` AND date BETWEEN '" . $isDayStart . "' AND '" . $isDayEnd . "') >= 0");
                    })
                    ->when(!$isDay, function($query) use($minGame) {
                        return $query->whereRaw("(SELECT COUNT(id) from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id`) >= 0");
                    })
                    //      CALCULATE POSITION WINS & TOP PLAYER WINS
                    ->when(($request->has('from') && $request->has('to')) && ($request->from != '' && $request->to != '') || ($request->has('last') && $request->last != ''), function($query) use($isDayStart, $isDayEnd) {
                            return $query->addSelect(\DB::raw("(SELECT COUNT(id) from performance_buzz where performance_buzz.player_id = players.id AND
                         performance_buzz.position IN (1,2,3,4) AND date(performance_buzz.date) BETWEEN '" . $isDayStart . "' AND  '" . $isDayEnd . "') as positional_wins"))
                                    ->addSelect(\DB::raw("(SELECT COUNT(id) FROM `performance_buzz`
                                WHERE player_id = players.id AND position = 4 AND
                                date(performance_buzz.date) BETWEEN '" . $isDayStart . "' AND '" . $isDayEnd . "')
                                AS top_player_wins"));
                    })
                    ->when($request->from == '' && $request->to == '' && $request->last == '', function($query) {
                        $query->addSelect('players.positional_wins', 'players.top_player_wins');
                    })
                    // ->toSql(); exit;
                    ->simplePaginate($this->perPage);

//        $data['performanceBuzzStates'] = DB::table(
//                        DB::raw("( SELECT @row_number:=CASE
//                WHEN @player_id = player_id THEN @row_number + 1
//                ELSE 1 END AS rn,
//                @player_id:=player_id as player_id, score
//                FROM
//                performance_buzz,(SELECT @player_id:=0,@row_number:=0) as t
//                order by player_id, id desc ) a")
//                )
//                ->select('player_id', DB::raw('sum(score) score'), 'score as last_score', 'players.common_name as player_name', 'players.football_index_common_name as football_index_common_name', 'players.position', 'players.injured', 'players.lineups', 'players.local_image_path', 'players.is_local_image', 'players.image_path', 'teams.name as team', 'teams.id as team_id',
//                 DB::raw("( SELECT COUNT(*) MAX_SCORE_COUNT FROM ( SELECT player_id PLAYER_ID FROM performance_buzz a WHERE a.score = ( SELECT MAX(b.score) FROM performance_buzz b WHERE b.date = a.date) ) s WHERE s.player_id = a.player_id  GROUP BY s.player_id
//    ) as top_wins"), DB::raw("(
//
//SELECT COUNT(*) MAX_SCORE_COUNT FROM ( SELECT player_id PLAYER_ID FROM performance_buzz a WHERE a.score = ( SELECT MAX(b.score) FROM performance_buzz b WHERE b.date = a.date AND b.sector = a.sector) ) s WHERE s.player_id = a.player_id GROUP BY s.player_id
//
//) as positional_wins"))
//                ->join('players', 'players.id', 'player_id')
//                ->join('teams', 'teams.id', 'players.team_id')
//                ->where('rn', '<=', 5)
//                ->groupBy('player_id')
//                ->orderBy('score', 'DESC')
//                ->paginate($this->perPage);
            // /dd($data['performanceBuzzStates']);
            $data['leagues'] = League::all();
            return view('front.performance-buzz.statistics.index')->with($data);
        } else {
            return redirect('/performance-buzz-statistics/upgrade');
        }
    }

    public function getUpgrade() {
        if (\Auth::user()->isPremium()) {
            return redirect('performance-buzz-statistics');
        }
        else
        {
            return view('front.performance-buzz.statistics.upgrade');
        }
    }
}
