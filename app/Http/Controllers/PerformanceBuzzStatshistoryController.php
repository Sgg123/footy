<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;
use App\Page;
use App\League;
use App\Fixture;
use App\PerformanceBuzz;
use App\DayAndDividends;
use App\MediaBuzzPlayer;
use App\Player;
use DB;
use Helper;

class PerformanceBuzzStatshistoryController extends Controller {

    protected $perPage = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'performance-buzz', 'activeSubMenu' => 'performance-buzz-stats-history']);
    }

    public function index(Request $request) {

       if (\Auth::user()->isPremium()) {
            $isDate = false;
            $startDate = '';
            if ($request->has('from') && $request->from != '') {
                $isDate = true;
                $startDate = $request->from;
            }
            if ($startDate == null){
                $date = date('Y-m-d');
            }else{
                $date = $startDate;
            }
            if($isDate == true){
                $data['pbrank']['forwd'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id,sc.image,sc.local_image,sc.local_image_path,sc.name,sc.pos_win,sc.top_win, sc.score FROM(SELECT players.football_index_position AS sector,players.image_path AS image,players.is_local_image as local_image,players.local_image_path as local_image_path,players.football_index_common_name AS name ,players.top_player_wins AS top_win ,players.positional_wins AS pos_win ,performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON players.id = performance_buzz.player_id where players.football_index_position="forward" AND performance_buzz.date="'.$startDate.'" GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

                $data['pbrank']['def'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id,sc.image,sc.local_image,sc.local_image_path,sc.name,sc.pos_win,sc.top_win, sc.score FROM(SELECT players.football_index_position AS sector,players.image_path AS image ,players.is_local_image as local_image,players.local_image_path as local_image_path,players.football_index_common_name AS name ,players.top_player_wins AS top_win ,players.positional_wins AS pos_win ,performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON players.id = performance_buzz.player_id where players.football_index_position="Defender" AND performance_buzz.date="'.$startDate.'" GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                
                $data['pbrank']['gol'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id,sc.image,sc.local_image,sc.local_image_path,sc.name,sc.pos_win,sc.top_win, sc.score FROM(SELECT players.football_index_position AS sector,players.image_path AS image ,players.is_local_image as local_image,players.local_image_path as local_image_path,players.football_index_common_name AS name ,players.top_player_wins AS top_win ,players.positional_wins AS pos_win ,performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON players.id = performance_buzz.player_id where players.football_index_position="Goalkeeper" AND performance_buzz.date="'.$startDate.'" GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');


                 $data['pbrank']['mid'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id,sc.image,sc.local_image,sc.local_image_path,sc.name,sc.pos_win,sc.top_win, sc.score FROM(SELECT players.football_index_position AS sector,players.image_path AS image ,players.is_local_image as local_image,players.local_image_path as local_image_path,players.football_index_common_name AS name ,players.top_player_wins AS top_win ,players.positional_wins AS pos_win ,performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON players.id = performance_buzz.player_id where players.football_index_position="Midfielder" AND performance_buzz.date="'.$startDate.'" GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

                $data['pbrank']['allover'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id,sc.image,sc.local_image_path,sc.local_image,sc.name,sc.pos_win,sc.top_win, sc.score FROM(SELECT players.football_index_position AS sector,players.image_path AS image ,players.is_local_image as local_image,players.local_image_path as local_image_path,players.football_index_common_name AS name ,players.top_player_wins AS top_win ,players.positional_wins AS pos_win ,performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON players.id = performance_buzz.player_id where performance_buzz.date="'.$startDate.'" GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                 
                $data['mdrank']['allover'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id,sc.image,sc.local_image,sc.name,sc.pos_win,sc.top_win, sc.score FROM(SELECT players.football_index_position AS sector,players.image_path AS image ,players.is_local_image as local_image,players.local_image_path as local_image_path,players.football_index_common_name AS name ,players.top_player_wins AS top_win ,players.positional_wins AS pos_win ,media_buzz_players.player_id, avg(media_buzz_players.score) AS score FROM `media_buzz_players` LEFT JOIN `players` ON players.id = media_buzz_players.player_id  where media_buzz_players.date="'.$startDate.'" GROUP BY media_buzz_players.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

                $data['upcomingMatches'] = app('App\Http\Controllers\HomeController')->getUpmatchbydate($startDate);
                $day_and_dividends=DayAndDividends::where('date','=',$startDate)->first();
            } else{
                $day_and_dividends=DayAndDividends::where('date','=',$date)->first();
                $data['upcomingMatches'] = app('App\Http\Controllers\HomeController')->getUpmatchbydate(null);
            } 
            return view('front.performance-buzz.stats.index')->with('data',$data)->with('day_and_dividends',$day_and_dividends);
        } else {
            return redirect('/performance-buzz-statistics/upgrade');
        }
    }

    public function getUpgrade() {
        if (\Auth::user()->isPremium()) {
            return redirect('performance-buzz-statshistory');
        }
        else
        {
            return view('front.performance-buzz.stats.upgrade');
        }
    }
}