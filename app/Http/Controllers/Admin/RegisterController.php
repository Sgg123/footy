<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    //shows registration form to seller
	public function showRegistrationForm()
	{
	    return view('admin.auth.register');
	}
}
