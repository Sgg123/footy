<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Spreadsheet;
use Helper;
use Auth;

class SpreadsheetsController extends Controller {
    
    public function __construct() {

        View::share(['activeMenu' => 'spreadsheets']);
    }

    public function index() {
        $data['spreadsheets'] = Spreadsheet::get();
        return view('admin.spreadsheets.list', $data);
    }

    public function create() {
        return view('admin.spreadsheets.form');
    }

    public function store(Request $request) {
        $this->validate($request, $this->validationRules());

        $spreadsheet = new Spreadsheet();
        $spreadsheet->title = $request->title;
        $spreadsheet->slug = $request->slug;
        $spreadsheet->file_path = $request->file_path;

        if ($request->hasFile('file_path')) {
            $fileName = time() . '.' . $request->file_path->getClientOriginalExtension();
            $request->file_path->move(public_path('/uploads/spreadsheets/'), $fileName);
            $spreadsheet->file_name = $request->file_path->getClientOriginalName();
            $spreadsheet->file_path = $fileName;
        }

        if ($spreadsheet->save()) {

            $alert['status'] = 'success';
            $alert['message'] = 'Spreadsheet created successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('spreadsheets.index')->with($alert);
    }

    
    public function edit($id) {
        $data['spreadsheet'] = Spreadsheet::findOrFail($id);

        return view('admin.spreadsheets.form', $data);
    }


    public function update(Request $request, $id) {
        $this->validate($request, $this->validationRules());

        $spreadsheet = Spreadsheet::findOrFail($id);

        if ($request->hasFile('file_path')) {

            $this->removeImage($spreadsheet->file_path);
            $fileName = time() . '.' . $request->file_path->getClientOriginalExtension();
            $request->file_path->move(public_path('/uploads/spreadsheets/'), $fileName);
            $spreadsheet->file_name = $request->file_path->getClientOriginalName();
            $spreadsheet->file_path = $fileName;
        }

        $spreadsheet->title = $request->title;
        $spreadsheet->slug = $request->slug;

        if ($spreadsheet->save()) {

            $alert['status'] = 'success';
            $alert['message'] = 'Spreadsheet updated successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('spreadsheets.index')->with($alert);
    }

    public function removeImage($spreadfile) {

        @unlink(public_path('/uploads/spreadsheets/') . $spreadfile);
    }

    public function destroy($id) {

        $spreadsheet = Spreadsheet::findOrFail($id);

        $this->removeImage($spreadsheet->file_path);

        if ($spreadsheet->delete())
        {
            $alert['status'] = 'success';
            $alert['message'] = 'Spreadsheet deleted successfully!';
        }
        else
        {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('spreadsheets.index')->with($alert);
    }


    public function validationRules() {

        return [
            'title' => 'required|max:255',
            'slug' => 'required|max:255',
        ];
    }

}
