<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Yajra\Datatables\Datatables;
use App\Player;
use App\Team;
use Helper;

class PlayerController extends Controller {

    public function __construct() {

        View::share(['activeMenu' => 'games', 'activeSubMenu' => 'teams']);
    }

    /**
     * This function is used to show specific player data
     * 
     * @param  Request $request - Request Object
     * 
     * @return Response
     */
    public function index(Request $request) {

        $data['teamID'] = $request->id;

        return view('admin.players.index', $data);
    }

    /**
     * This function is used to create player
     * 
     * @param  Request $request - Request Object
     * 
     * @return Response
     */
    public function create(Request $request) {

        $data['teamID'] = $request->id;
        $data['teamName'] = Team::find(Helper::decrypt($request->id))->name;
        return view('admin.players.edit', $data);
    }

    /**
     * This function is used to store player detail
     * 
     * @param  Request $request - Request Object
     * 
     * @return Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'middlename' => 'required|string|max:255',
            'common_name' => 'required|string|max:255',
            'football_index_common_name' => 'required|string|max:255',
            'nationality' => 'required|string|max:255',
            'birthdate' => 'required|date',
            'height' => 'required|string|max:255',
            'weight' => 'required|string|max:255',
        ]);

        $player = new Player();
        $player->fill($request->all());
        $player->team_id = Helper::decrypt($request->id);


        if ($player->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Player created successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('teams.players', ['id' => $request->id])->with($alert);
    }

    /**
     * This function is used to edit player detail
     * 
     * @param  Request $request - Request Object
     * 
     * @return Response
     */
    public function edit(Request $request) {

        $data['teamName'] = Team::find(Helper::decrypt($request->id))->name;
        $id = Helper::decrypt($request->playerid);

        $data['player'] = Player::findorfail($id);
        $data['teamID'] = $request->id;

        return view('admin.players.edit')->with($data);
    }

    /**
     * This function is used to update player detail
     * 
     * @param  Request $request - Request Object
     * 
     * @return Response
     */
    public function update(Request $request) {

        $this->validate($request, [

            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'middlename' => 'required|string|max:255',
            'common_name' => 'required|string|max:255',
            'football_index_common_name' => 'required|string|max:255',
            'nationality' => 'required|string|max:255',
            'birthdate' => 'required|date|date_format:Y-m-d|before:tomorrow',
            'height' => 'required|string|max:255',
            'weight' => 'required|string|max:255',
            'position' => 'required',
        ]);

        $id = Helper::decrypt($request->playerid);
        $player = Player::findorfail($id);

        $player->firstname = $request->firstname;
        $player->lastname = $request->lastname;
        $player->middlename = $request->middlename;
        $player->common_name = $request->common_name;
        $player->football_index_common_name = $request->football_index_common_name;
        $player->nationality = $request->nationality;
        $player->birthdate = $request->birthdate;
        $player->height = $request->height;
        $player->weight = $request->weight;
        $player->position = $request->position;
        $player->football_index_position = $request->football_index_position;
        
        if ($request->hasFile('local_image_path')) {
            $file = $request->file('local_image_path');
            $destinationPath = 'uploads/player-images';
            $filename = $file->hashName();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $player->local_image_path = $filename;
            $player->is_local_image = 1;
        }
        
        if ($player->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Player updated successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('teams.players', ['id' => $request->id])->with($alert);
    }

    /**
     * This function is used to get players data
     */
    public function getPlayers(Request $request) {

        $team = Team::find(Helper::decrypt($request->id));
        $data1 = $team->getTeamPlayers()->orderBy('id', 'DESC');

        $datatables = Datatables::of($data1)
                ->addColumn('action', function($data) {
                    return '<a class="btn btn-sm btn-success" href="' . route('teams.players.edit', ['id' => Helper::encrypt($data->team_id), 'playerid' => Helper::encrypt($data->id)]) . '"><i class="fa fa-fw fa-pencil"></i>Edit </a>';
                })
                ->addColumn('team', function($data) {
                    return @$data->getTeam->name;
                })
                ->make(true);

        return $datatables;
    }

}
