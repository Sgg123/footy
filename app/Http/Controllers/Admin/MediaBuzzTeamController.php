<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Fixture;
use App\MediaBuzzPlayer;
use Helper;
use Yajra\Datatables\Datatables;
use App\Page;
use App\Player;
use App\Team;
use DB;

class MediaBuzzTeamController extends Controller {

    protected $perPage = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'media-buzz', 'activeSubMenu' => 'media-buzz-team-history']);
    }

    /**
     * Display a listing of the media buzz winner team history
     *
     * @return Response
     */
    public function index(Request $request) {
        $data['winnerTeamHistory'] = $this->mbTeamData($request)->paginate($this->perPage);

        return view('admin.mediabuzz-winner.team.list', $data);
    }

    /**
     * This function is used to show create form
     *
     * @return View
     */
    public function create() {
        $data['players'] = Player::get();
        $data['teams'] = Team::get();
        return view('admin.mediabuzz-winner.team.form')->with($data);
    }

    /**
     * This function is used to save medizbuzz winner team data
     *
     * @param  Request  $request - Request Object
     * 
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, $this->validationRules(), $this->validationMessages());

        $mediaBuzzObject = new MediaBuzzTeam();
        $mediaBuzzObject->fill($request->except(['_token']));

        if ($mediaBuzzObject->save()) {

            $alert['status'] = 'success';
            $alert['message'] = 'Media buzz winner team added successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('media-buzz-winner-team.index')->with($alert);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id - ID
     * @return Response 
     */
    public function show($id) {
        return abort('404');
    }

    /**
     * This function is used for edit specific record
     *
     * @param  int  $id - ID
     * 
     * @return View
     */
    public function edit($id) {

        $data['teams'] = Team::get();
        $data['teamHistory'] = $teamHistory = $this->getMbTeam(Helper::decrypt($id));
        $data['teamIds'] = explode(',', @$teamHistory->team_ids);

        return view('admin.mediabuzz-winner.team.form', $data);
    }

    /**
     * This function is used to get MB team
     * 
     * @param  integer $id - Media Buzz team 
     * 
     * @return Response 
     */
    public function getMbTeam($id) {

        $mbTeam = MediaBuzzPlayer::select('media_buzz_players.date', 'media_buzz_players.id', \DB::raw('group_concat(CASE WHEN media_buzz_players.team_id != "null" THEN media_buzz_players.team_id ELSE "" END ORDER BY media_buzz_players.position ASC) as team_ids'), \DB::raw('group_concat(IFNULL(media_buzz_players.id,"")) as mb_ids'))
                        ->where('media_buzz_players.date', $id)
                        ->groupBy('media_buzz_players.date')->first();

        return $mbTeam;
    }

    /**
     * Update the media buzz team history.
     *
     * @param  Request  $request - Request Object
     * @param  int  $id - ID
     * 
     * @return Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, $this->validationRules(), $this->validationMessages());
        $ids = explode(',', $request->ids);

        $mediaBuzzTeam1 = $this->getMediaBuzzTeamHistory(Helper::encrypt($ids[0]));
        $mediaBuzzTeam2 = $this->getMediaBuzzTeamHistory(Helper::encrypt($ids[1]));
        $mediaBuzzTeam3 = $this->getMediaBuzzTeamHistory(Helper::encrypt($ids[2]));
        $oldFirst = $oldSecond = $oldThird = '';

        if ($mediaBuzzTeam1->team_id != $request->mb_first) {
            if ($mediaBuzzTeam1->team_id != '') {
                $oldFirst = $mediaBuzzTeam1->team_id;
            }
        }

        if ($mediaBuzzTeam2->team_id != $request->mb_second) {
            if ($mediaBuzzTeam2->team_id != '') {
                $oldSecond = $mediaBuzzTeam2->team_id;
            }
        }

        if ($mediaBuzzTeam3->team_id != $request->mb_third) {
            if ($mediaBuzzTeam3->team_id != '') {
                $oldThird = $mediaBuzzTeam3->team_id;
            }
        }

//        SAVE UPDATES HERE
        $mediaBuzzTeam1->team_id = $request->mb_first;
        $mediaBuzzTeam1->position = 1;

        $mediaBuzzTeam2->team_id = $request->mb_second;
        $mediaBuzzTeam2->position = 2;

        $mediaBuzzTeam3->team_id = $request->mb_third;
        $mediaBuzzTeam3->position = 3;

        if ($mediaBuzzTeam1->save() && $mediaBuzzTeam2->save() && $mediaBuzzTeam3->save()) {
//        UPDATE MB COUNT OF OLD TEAMS
            if ($oldFirst != '') {
                Helper::calculateTeamDividend($oldFirst);
            }
            if ($oldSecond != '') {
                Helper::calculateTeamDividend($oldSecond);
            }
            if ($oldThird != '') {
                Helper::calculateTeamDividend($oldThird);
            }

//        UPDATE MB COUNT OF NEW TEAMS
            if ($request->mb_first != '') {
                Helper::calculateTeamDividend($request->mb_first);
            }

            if ($request->mb_second != '') {
                Helper::calculateTeamDividend($request->mb_second);
            }

            if ($request->mb_third != '') {
                Helper::calculateTeamDividend($request->mb_third);
            }

            $alert['status'] = 'success';
            $alert['message'] = 'Media buzz winner team updated successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('media-buzz-winner-team.index')->with($alert);
    }

    /**
     * This function is used to update teams
     * 
     * @param  Array $mbIds   - MB IDS
     * @param  Array $teamIds - Team Id's
     * 
     * @return Response
     */
    public function updateTeamIds($mbIds, $teamIds) {

        if (@$mbIds[0] != '') {

            $mbFirst = MediaBuzzPlayer::find($mbIds[0]);

            if ($mbFirst) {
                $mbFirst->team_id = $teamIds[0];
                $mbFirst->save();
            }
        }

        if (@$mbIds[1] != '') {

            $mbSecond = MediaBuzzPlayer::find($mbIds[1]);

            if ($mbSecond) {
                $mbSecond->team_id = $teamIds[1];
                $mbSecond->save();
            }
        }

        if (@$mbIds[2] != '') {

            $mbThird = MediaBuzzPlayer::find($mbIds[2]);
            if ($mbThird) {

                $mbThird->team_id = $teamIds[2];
                $mbThird->save();
            }
        }
    }

    /**
     * This function is used to 
     * 
     * @param  Array $arr - Array of ids
     * 
     * @return Response
     */
    public function updateMbWinsCount($arr, $type) {

        if ($type == 'dec') {

            if (@$arr[0] != '') {

                $mbFirst = Team::find($arr[0]);
                $mbFirst->mb_first_count = @$mbFirst->mb_first_count <= 0 ? 0 : @$mbFirst->mb_first_count - 1;
                $mbFirst->save();
            }

            if (@$arr[1] != '') {

                $mbSecond = Team::find($arr[0]);
                $mbSecond->mb_second_count = @$mbSecond->mb_second_count <= 0 ? 0 : @$mbSecond->mb_second_count - 1;
                $mbSecond->save();
            }

            if (@$arr[2] != '') {

                $mbThird = Team::find($arr[0]);
                $mbThird->mb_third_count = @$mbThird->mb_third_count <= 0 ? 0 : @$mbThird->mb_third_count - 1;
                $mbThird->save();
            }
        }

        if ($type == 'inc') {

            if ($arr[0] != '') {

                $mbFirst = Team::find($arr[0]);
                $mbFirst->mb_first_count = @$mbFirst->mb_first_count + 1;
                $mbFirst->save();
            }

            if ($arr[1] != '') {

                $mbSecond = Team::find($arr[0]);
                $mbSecond->mb_second_count = @$mbSecond->mb_second_count + 1;
                $mbSecond->save();
            }

            if ($arr[2] != '') {

                $mbThird = Team::find($arr[0]);
                $mbThird->mb_third_count = @$mbThird->mb_third_count + 1;
                $mbThird->save();
            }
        }
    }


    /**
     * This function is used to get team history
     * 
     * @param  string $id - ID
     * 
     * @return Object
     */
    public function getMediaBuzzTeamHistory($id) {

        return MediaBuzzPlayer::findOrFail(Helper::decrypt($id));
    }

    /**
     * This function is used to get validation rules
     * 
     * @return Array
     */
    public function validationRules($token = "", $id = "") {
        $rules = [
            'mb_first' => 'required|max:50',
//            'mb_second' => 'required|max:50',
//            'mb_third' => 'required|max:50',
        ];
        return $rules;
    }

    /**
     * This function is used to get validation messages
     * 
     * @return Array
     */
    public function validationMessages() {
        return [
            'mb_first.required' => 'The MB first team name is required.',
            'mb_second.required' => 'The MB second team name is required.',
            'mb_third.required' => 'The MB third team name is required.',
            'pb_top_player.required' => 'The PB top player name is required.',
            'pb_top_defender.required' => 'The PB top defender name is required.',
            'pb_top_midfielder.required' => 'The PB top midfielder name is required.',
            'pb_top_forwarder.required' => 'The PB top forwarder name is required.',
        ];
    }

    /**
     * This function is used to get media buzz team view
     */
    public function mediaBuzzTeamIndex(request $request) {
        $data['page'] = Page::find(5);

        $data['media_buzz_teams'] = $this->mbTeamData($request)
                ->when((!\Auth::user()->isPremium()), function($query) {
                    $endDate = date('Y-m-d');
                    $startDate = date('Y-m-d', strtotime("-30 day", strtotime($endDate)));
                    $query->whereBetween(\DB::raw('DATE(date)'), [$startDate, $endDate]);
                })
                ->simplePaginate($this->perPage);

        return view('front.mediabuzz.team.index', $data);
    }

    /**
     * This function is used to get media buzz team data
     */
    public function mediaBuzzTeamData(Request $request) {

        $data = MediaBuzzTeam::select('*');
        $datatables = Datatables::of($data)
                ->editColumn('created_at', function($data) {
                    return date('d-m-Y', strtotime($data->created_at));
                })
                ->filter(function ($query) use ($request) {

                    $startDate = @$request->start_date_filter;
                    $endDate = @$request->end_date_filter;

                    if ($startDate != '' && $endDate != '') {

                        $query->whereBetween(\DB::raw('DATE(created_at)'), [$startDate, $endDate]);
                    } elseif ($startDate != '') {
                        $query->where(\DB::raw('DATE(created_at)'), $startDate);
                    } elseif ($endDate != '') {

                        $query->where(\DB::raw('DATE(created_at)'), $endDate);
                    }
                })
                ->make(true);

        return $datatables;
    }

    /**
     * This function is used to show create form
     *
     * @return View
     */
    public function mbTeamData(Request $request) {
        $isTeamName = false;
        $team_name = '';
        if ($request->has('team_name') && $request->team_name != '') {
            $isTeamName = true;
            $team_name = $request->team_name;
        }
        $isDate = false;
        $startDate = '';
        $endDate = '';
        if ($request->has('from') && $request->from != '') {
            $isDate = true;
            $startDate = $request->from;
            $endDate = $request->to;
        }

        $query = MediaBuzzPlayer::select('media_buzz_players.date', 'media_buzz_players.id', DB::raw('group_concat(CASE WHEN media_buzz_players.score != "null" THEN media_buzz_players.score ELSE "" END ORDER BY media_buzz_players.position ASC) as scores'), DB::raw('group_concat(CASE WHEN media_buzz_players.player_id != "null" THEN media_buzz_players.player_id ELSE "" END ORDER BY media_buzz_players.position ASC) as player_ids'), DB::raw('group_concat(CASE WHEN teams.name != "null" THEN teams.name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_team_names')
                )
                ->leftJoin('players', 'players.id', 'media_buzz_players.player_id')
                ->leftJoin('teams', 'teams.id', 'media_buzz_players.team_id')
                ->when($isTeamName, function($query) use ($team_name) {
                    $query->havingRaw('LOCATE(\'' . $team_name . '\',player_team_names)>0');
                })
                ->when($isDate, function($query) use ($startDate, $endDate) {
                    $query->whereRaw(DB::raw('DATE(date) BETWEEN \'' . $startDate . '\' AND \'' . $endDate . '\''));
                })
                ->groupBy('media_buzz_players.date');
        return $query;
    }

}
