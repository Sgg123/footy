<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\ArticleCategories;
use App\Article;
use App\Helpers\Helper;
use Excel;

class ArticlesController extends Controller {

    protected $perPage = 20;

    public function __construct() {
        View::share(['activeMenu' => 'manage-article', 'activeSubMenu' => 'articles']);
    }

    public function index() {

        $data['articles'] = Article::with('getArticlesCategory')
                ->paginate($this->perPage);
        return view('admin.articles.list')->with($data);
    }

    public function create() {
        $categories = Helper::getArticleCategories();
        return view('admin.articles.form')->with('categories', $categories);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required|string|max:255|unique:articles',
            'slug' => 'required|unique:articles',
            'date' => 'required',
            'article_category_id' => 'required|max:2048',
            'image' => 'required|mimes:jpg,jpeg,png,gif,jpeg',
            'thumbnail_image' => 'required|mimes:jpg,jpeg,png,gif,jpeg',
            'content' => 'required',
                ], [
            'article_category_id.required' => 'The article category is required.',
        ]);

        $filename = $this->uploadArticleFile($request->file('image'));
        $thumnail_filename = $this->uploadArticleFile($request->file('thumbnail_image'));
        $articles = new Article();
        $articles->title = $request->title;
        $articles->slug = $request->slug;
        $articles->date = $request->date;
        $articles->article_category_id = $request->article_category_id;
        $articles->content = $request->content;
        $articles->youtube_video = $request->youtube_video;
        $articles->image_path = $filename;
        $articles->thumbnail_image = $thumnail_filename;
        $articles->meta_keywords = $request->meta_keywords;
        $articles->meta_description = $request->meta_description;

        if ($articles->save()) {

            $alert['status'] = 'success';
            $alert['message'] = 'Article added successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('articles.index')->with($alert);
    }

    public function edit($id) {
        $categories = Helper::getArticleCategories();
        $id = Helper::decrypt($id);
        $data['articles'] = $this->findArticle($id);
        $data['categories'] = $categories;
        return view('admin.articles.form')->with($data);
    }

    public function update($id, Request $request) {
        $id = Helper::decrypt($id);
        $a = $this->validate($request, [
            'title' => 'required|string|max:255|unique:articles,title,' . $id,
            'slug' => 'required|unique:articles,slug,' . $id,
            'date' => 'required',
            'article_category_id' => 'required',
            'image' => 'mimes:jpg,jpeg,png,gif,jpeg|max:2048',
            'thumbnail_image' => 'mimes:jpg,jpeg,png,gif,jpeg|max:2048',
            'content' => 'required',
        ]);

        $articles = $this->findArticle($id);

        if ($request->hasFile('image')) {
            // UPLOAD NEW IMAGE
            $filename = $this->uploadArticleFile($request->file('image'));
            // REMOVE OLD IMAGE
            $this->removeArticleFile($articles->image_path);
            $articles->image_path = $filename;
        }
        if ($request->hasFile('thumbnail_image')) {
            // UPLOAD NEW IMAGE
            $thumnail_filename = $this->uploadArticleFile($request->file('thumbnail_image'));
            // REMOVE OLD IMAGE
            $this->removeArticleFile($articles->thumbnail_image);
            $articles->thumbnail_image = $thumnail_filename;
        }
        $articles->title = $request->title;
        $articles->slug = $request->slug;
        $articles->date = $request->date;
        $articles->article_category_id = $request->article_category_id;
        $articles->content = $request->content;
        $articles->youtube_video = $request->youtube_video;
        $articles->meta_keywords = $request->meta_keywords;
        $articles->meta_description = $request->meta_description;

        if ($articles->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Article updated successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('articles.index')->with($alert);
    }

    public function destroy($id) {

        $id = Helper::decrypt($id);
        $articles = $this->findArticle($id);
        $articles->status = 'deleted';

        if ($articles->save()) {
            $alert['message'] = "Article deleted successfully!";
            $alert['status'] = "success";
        } else {
            $alert['message'] = "Something went wrong!";
            $alert['status'] = "danger";
        }

        return redirect()->back()->with($alert);
    }

    public function uploadArticleFile($file) {
        $destinationPath = 'uploads/articles';
        $filename = $file->hashName();
        $file->move($destinationPath, $filename);
        return $filename;
    }

    public function removeArticleFile($file) {
        if (file_exists('uploads/articles/' . $file)) {
            @unlink(public_path('/uploads/articles/') . $file);
        }
    }

    public function findArticle($id) {
        return Article::findorfail($id);
    }

    public function importArticles(Request $request) {

        $titles = Article::pluck('title')->toArray();
        $this->validate($request, [
            'import_file' => 'required|mimes:csv,txt,xls'
        ]);

        if ($request->hasFile('import_file')) {
            $path = $request->file('import_file')->getRealPath();

            $data = Excel::load($path, function($reader) {
                        
                    })->get();

            if (!empty($data) && $data->count()) {
                $insert = array();
                foreach ($data as $value) {

                    // Skip title previously added using in_array
                    if (in_array($value->title, $titles))
                        continue;

                    $insert[] = ['id' => $value['id'], 'article_category_id' => $value['article_category_id'], 'title' => $value['title'], 'slug' => $value['slug'], 'image_path' => $value['image_path'], 'content' => $value['content'], 'status' => $value['status']];
                    // Add new title to array
                    $titles[] = $value['title'];
                }
                if (!empty($insert)) {
                    Article::insert($insert);
                    $alert['status'] = 'success';
                    $alert['message'] = 'Import completed successfully!';
                } else {
                    $alert['status'] = 'success';
                    $alert['message'] = 'Articles already exist!';
                }
            }
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        } return redirect()->route('articles.index')->with($alert);
    }

}
