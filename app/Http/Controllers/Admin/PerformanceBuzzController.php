<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\PerformanceBuzz;
use App\Player;
use App\Team;
use Helper;

class PerformanceBuzzController extends Controller {

    protected $perPage = 50;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'performance-buzz', 'activeSubMenu' => 'performance-buzz-datapoints']);
    }

    /**
     * This function is used to show perofrmance buzz listing
     *
     * @return Response
     */
    public function index(Request $request) {
        $data['pbData'] = $pbDates = PerformanceBuzz::leftJoin('players', 'players.id', 'performance_buzz.player_id')
                ->leftJoin('teams', 'teams.id', 'performance_buzz.team_id')
                ->when($request->player_name != "", function($query)use($request) {
                    $query->where('players.football_index_common_name', 'like', '%' . $request->player_name . '%');
                })
                ->when($request->date != "", function($query)use($request) {
                    $query->whereDate('performance_buzz.date', $request->date);
                })
                ->select('performance_buzz.*', 'players.football_index_common_name', 'players.common_name', 'players.football_index_position', 'teams.name')
                ->orderBy('date', 'DESC')
                ->paginate($this->perPage);

        return view('admin.performancebuzz.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['players'] = Player::where('football_index_common_name', '!=', '')
                ->select('football_index_common_name', 'common_name', 'id')
                ->get();
        $data['teams'] = Team::select('name', 'id')->get();
        return view('admin.performancebuzz.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, $this->validationRules());

        $history = $this->checkPerformanceHistory($request->player_id, $request->date);

        if ($history == 0) {
            $player = Player::find($request->player_id);

            $pbData = new PerformanceBuzz();
            $pbData->player_id = $request->player_id;
            $pbData->team_id = $request->team_id;
            $pbData->score = $request->score;
            $pbData->date = $request->date;
            $pbData->sector = $player->football_index_position;
            $pbData->position = 0;
            $pbData->save();

            if ($pbData->save()) {

                //  UPDATE DIVIDEND & POSITIONS
                Helper::updatePBPosition($pbData->date);
                Helper::calculatePlayerDividend($pbData->player_id);
                if ($pbData->team_id != '') {
                    Helper::calculateTeamDividend($pbData->team_id);
                }


                $alert['status'] = 'success';
                $alert['message'] = 'Performance buzz history added successfully!';
            } else {

                $alert['status'] = 'danger';
                $alert['message'] = 'Something went wrong!';
            }

            return redirect()->route('performance-buzz.index')->with($alert);
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Player with same historical date already exists!';
            return redirect()->route('performance-buzz.create')->with($alert);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $id = Helper::decrypt($id);

        $data['players'] = Player::where('football_index_common_name', '!=', '')
                ->select('football_index_common_name', 'common_name', 'id')
                ->get();
        $data['teams'] = Team::select('name', 'id')->get();
        $data['pbData'] = PerformanceBuzz::where('id', '=', $id)->first();

        return view('admin.performancebuzz.form', $data);
    }

    /**
     * This function is used to update performance buzz record
     *
     * @param  Request  $request - Request Object
     * @param  string  $id - Encrypted date
     * 
     * @return Response
     */
    public function update(Request $request, $id) {

        $id = Helper::decrypt($id);

        $this->validate($request, $this->validationRules());

        $history = $this->checkPerformanceHistory($request->player_id, $request->date, $id);

        $oldPlayer = $oldTeam = '';

        if ($history == 0) {
            $pbData = PerformanceBuzz::findOrFail($id);
            if ($pbData->player_id != $request->player_id) {
                $oldPlayer = $pbData->player_id;
            }
            if ($pbData->team_id != $request->team_id) {
                $oldTeam = $pbData->team_id;
            }
            $player = Player::find($request->player_id);
            $pbData->player_id = $request->player_id;
            $pbData->team_id = $request->team_id;
            $pbData->score = $request->score;
            $pbData->date = $request->date;
            $pbData->sector = $player->football_index_position;
            $pbData->position = 0;

            if ($pbData->save()) {

                //  UPDATE DIVIDEND & POSITIONS OF NEW PLAYER & TEAM
                PerformanceBuzz::whereDate('date', $request->date)->update(['position' => 0]);
                Helper::updatePBPosition($request->date);
                Helper::calculatePlayerDividend($request->player_id);
                if ($request->team_id != "") {
                    Helper::calculateTeamDividend($request->team_id);
                }

                //  UPDATE DIVIDEND & POSITIONS OF OLD PLAYER & TEAM
                if ($oldPlayer != '') {
                    Helper::calculatePlayerDividend($oldPlayer);
                }
                if ($oldTeam != '') {
                    Helper::calculateTeamDividend($oldTeam);
                }
                //  UPDATE ALL PLAYERS COUNT FOR THIS DATE
                Helper::updateCounts($request->date);
                Helper::updateDividends($request->date);
                $alert['status'] = 'success';
                $alert['message'] = 'Performance buzz history updated successfully!';
            } else {

                $alert['status'] = 'danger';
                $alert['message'] = 'Something went wrong!';
            }

            return redirect()->route('performance-buzz.index')->with($alert);
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Player with same historical date already exists!';
            return redirect()->route('performance-buzz.edit')->with($alert);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * This function is used to return validation rules array
     */
    public function validationRules() {
        $rules['player_id'] = 'required';
        $rules['score'] = 'required';
        $rules['date'] = 'required';
        return $rules;
    }

    /**
     * This function is used to check if player exists for same date on PB History or not
     */
    public function checkPerformanceHistory($player_id, $date, $id = '') {

        return PerformanceBuzz::where(['player_id' => $player_id])
                        ->whereDate('date', $date)
                        ->when($id != "", function($query)use($id) {
                            $query->where('id', '!=', $id);
                        })->count();
    }

    /**
     * This function is used to delete performance buzz history delete
     */
    public function performance_buzz_data_delete(Request $request) {
        $id = Helper::decrypt($request->id);
        $pb = PerformanceBuzz::find($id);
        $pbdate = $pb->date;
        $player_id = $pb->player_id;
        $team_id = $pb->team_id;
        if ($pb->delete()) {
            Helper::updatePBPosition($pbdate);
            if ($player_id != "") {
                Helper::calculatePlayerDividend($player_id);
            }
            if ($team_id != "") {
                Helper::calculateTeamDividend($team_id);
            }
            $alert['status'] = 'success';
            $alert['message'] = 'Performance buzz history deleted successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }
        return redirect()->route('performance-buzz.index')->with($alert);
    }

}
