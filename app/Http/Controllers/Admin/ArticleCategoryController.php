<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\ArticleCategories;
use App\Article;
use App\Helpers\Helper;

class ArticleCategoryController extends Controller {

    protected $perPage = 20;

    public function __construct() {
        View::share(['activeMenu' => 'manage-article', 'activeSubMenu' => 'article-category']);
    }

    public function index() {
        $data['articleCategories'] = ArticleCategories::paginate($this->perPage);

        return view('admin.articleCategory.list')->with($data);
    }

    public function create() {

        return view('admin.articleCategory.form');
    }

    public function store(Request $request) {
        $this->validate($request, array(
            'title' => 'required|string|max:255|unique:article_categories',
            'slug' => 'required|unique:article_categories'
            ));

        $articleCategories = new ArticleCategories();
        $articleCategories->fill($request->except(['_token']));

        if ($articleCategories->save()) {

            $alert['status'] = 'success';
            $alert['message'] = 'Article category added successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('article-category.index')->with($alert);
    }

    public function edit($id) {

        $id = Helper::decrypt($id);
        $data['categories'] = $this->findCategory($id);
        return view('admin.articleCategory.form')->with($data);
    }

    public function update($id, Request $request) {
        $id = Helper::decrypt($id);
        $this->validate($request, array(
            'title' => 'required|string|max:255|unique:article_categories,title,' . $id,
            'slug' => 'required|unique:article_categories,slug,' . $id,
        ));

        $articleCategories = $this->findCategory($id);
        $articleCategories->title = $request->title;
        $articleCategories->slug = $request->slug;

        if ($articleCategories->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Article category updated successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('article-category.index')->with($alert);
    }

    public function destroy($id) {

        $id = Helper::decrypt($id);

        $articleCategories = $this->findCategory($id);
        $articles = $articleCategories->getArticles()->count();

        if ($articles > 0) {
            $alert['message'] = "Article found under this category! Cannot delete this category!";
            $alert['status'] = "danger";
        } else {
            $articleCategories->update(['status' => 'deleted']);
            $alert['message'] = "Article category deleted successfully!";
            $alert['status'] = "success";
        }


        return redirect()->back()->with($alert);
    }

    public function findCategory($id) {
        return ArticleCategories::findorfail($id);
    }

}
