<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\LengthAwarePaginator;
use App\PerformanceBuzz;
use App\Player;
use App\Team;
use App\PBFiles;
use Helper;

class PerformanceBuzzPlayerAdminController extends Controller {

    protected $perPage = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'performance-buzz', 'activeSubMenu' => 'performance-buzz-player-history']);
    }

    /**
     * This function is used to show perofrmance buzz listing
     *
     * @return Response
     */
    public function index(Request $request) {
        $performance = Helper::performanceBuzzPlayers_new($request,$token='admin');
        $data['PBWinners'] = Helper::dispatchObject($performance); 
        
        $data['links'] = $performance;

        return view('admin.performancebuzz-winner.list', $data);
    }

    /**
     * This function is used to seed performance buzz data
     * 
     * @param  Collection $pbDates - dates collection
     * 
     * @return Collection
     */
    public function seedPbData($pbDates) {

        $pbDates = $pbDates->map(function ($dates) {

            $defender = $this->getPbByDate($dates->date, 'Defender');
            $dates['defender'] = $defender;

            $forward = $this->getPbByDate($dates->date, 'Forward');
            $dates['forward'] = $forward;

            $midfielder = $this->getPbByDate($dates->date, 'Midfielder');
            $dates['midfielder'] = $midfielder;

            $topplayer = $this->getPbByDate($dates->date, null);
            $dates['topplayer'] = $topplayer;

            return $dates;
        });

        return $pbDates;
    }

    /**
     * This function is used to get specific date data for pb
     * 
     * @param  string $date   - Date
     * @param  string $sector - Player position
     * 
     * @return Collection
     */
    public function getPbByDate($date, $sector = '') {

        return PerformanceBuzz::join('players', 'players.id', 'performance_buzz.player_id')
                        ->join(\DB::raw('( SELECT player_id FROM performance_buzz GROUP BY player_id ) Temp'), 'Temp.player_id', 'performance_buzz.player_id')
                        ->selectRaw('performance_buzz.*, players.football_index_common_name, players.common_name, players.top_player_wins, players.positional_wins')
                        ->where('date', $date)
                        ->when($sector != '', function($query) use($sector) {
                            return $query->where('sector', $sector);
                        })
                        ->when($sector == '', function($query) use($sector) {
                            return $query->whereIn('sector', ['Defender', 'Forward', 'Midfielder']);
                        })
                        ->orderBy('performance_buzz.score', 'DESC')
                        ->take(1)
                        ->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $player['ForwardPlayers'] = Player::where('football_index_position', 'Forward')->get(['id', 'common_name']);

        $player['MidfielderPlayers'] = Player::where('football_index_position', 'Midfielder')->get(['id', 'common_name']);

        $player['DefenderPlayers'] = Player::where('football_index_position', 'Defender')->orWhere('football_index_position', 'Goalkeeper')->get(['id', 'common_name']);

        return view('admin.performancebuzz-winner.form', $player);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules['date'] = 'required|unique:performance_buzz';
        $this->validate($request, $rules);

        $pbForward = new PerformanceBuzz();
        $pbForward->player_id = $request->top_forward_player;
        $pbForward->score = $request->top_forward_score;
        $pbForward->date = $request->date;
        $pbForward->sector = 'Forward';
        $pbForward->position = 1;
        $pbForward->save();

        $pbMidfielder = new PerformanceBuzz();
        $pbMidfielder->player_id = $request->top_midfielder_player;
        $pbMidfielder->score = $request->top_midfielder_score;
        $pbMidfielder->date = $request->date;
        $pbMidfielder->sector = 'Midfielder';
        $pbMidfielder->position = 2;
        $pbMidfielder->save();

        $pbDefender = new PerformanceBuzz();
        $pbDefender->player_id = $request->top_defender_player;
        $pbDefender->score = $request->top_defender_score;
        $pbDefender->date = $request->date;
        $pbDefender->sector = 'Defender';
        $pbDefender->position = 3;
        $pbDefender->save();

        $this->updateNewPlayerData($request->date);

        $alert['status'] = 'success';
        $alert['message'] = 'Performance buzz player updated successfully!';

        return redirect()->route('performance-buzz-winner-player.index')->with($alert);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $date = Helper::decrypt($id);
        $player['date'] = $date;

        $forward = $this->getPbByDate($date, 'Forward');
        $player['forward'] = $forward;

        $defender = $this->getPbByDate($date, 'Defender');
        $player['defender'] = $defender;

        $midfielder = $this->getPbByDate($date, 'Midfielder');

        $player['midfielder'] = $midfielder;

        $player['ForwardPlayers'] = Player::where('football_index_position', 'Forward')->get(['id', 'common_name']);

        $player['MidfielderPlayers'] = Player::where('football_index_position', 'Midfielder')->get(['id', 'common_name']);

        $player['DefenderPlayers'] = Player::where('football_index_position', 'Defender')->orWhere('football_index_position', 'Goalkeeper')->get(['id', 'common_name']);

        return view('admin.performancebuzz-winner.form', $player);
    }

    /**
     * This function is used to update performance buzz record
     *
     * @param  Request  $request - Request Object
     * @param  string  $id - Encrypted date
     * 
     * @return Response
     */
    public function update(Request $request, $id) {

        $date = Helper::decrypt($id);

        $forward = $oldForward = $this->getPbByDate($date, 'Forward');
        $defender = $oldDefender = $this->getPbByDate($date, 'Defender');
        $midfielder = $oldMidfielder = $this->getPbByDate($date, 'Midfielder');
        $topplayer = $this->getPbByDate($date, null);

        $this->updateOldPlayerData($date);

        PerformanceBuzz::where('player_id', '=', NULL)->where('date', '=', $date)->delete();
        //Forward Player
        if ($forward) {

            $forward->player_id = $request->top_forward_player;
            $forward->score = $request->top_forward_score;
            $forward->save();
        } else {

            $pbForward = new PerformanceBuzz();
            $pbForward->player_id = $request->top_forward_player;
            $pbForward->score = $request->top_forward_score;
            $pbForward->date = $date;
            $pbForward->sector = 'Forward';
            $pbForward->position = 1;
            $pbForward->save();
        }


        //Middlefielder Player
        if ($midfielder) {

            $midfielder->player_id = $request->top_midfielder_player;
            $midfielder->score = $request->top_midfielder_score;
            $midfielder->save();
        } else {

            $pbMidfielder = new PerformanceBuzz();
            $pbMidfielder->player_id = $request->top_midfielder_player;
            $pbMidfielder->score = $request->top_midfielder_score;
            $pbMidfielder->date = $date;
            $pbMidfielder->sector = 'Midfielder';
            $pbMidfielder->position = 2;
            $pbMidfielder->save();
        }

        //Defender Player
        if ($defender) {

            $defender->player_id = $request->top_defender_player;
            $defender->score = $request->top_defender_score;
            $defender->save();
        } else {

            $pbDefender = new PerformanceBuzz();
            $pbDefender->player_id = $request->top_defender_player;
            $pbDefender->score = $request->top_defender_score;
            $pbDefender->date = $date;
            $pbDefender->sector = 'Defender';
            $pbDefender->position = 3;
            $pbDefender->save();
        }


        $this->updateNewPlayerData($date);

//        $this->updatePlayerLastWinDate($oldForward, $oldDefender, $oldMidfielder, $date);

        $alert['status'] = 'success';
        $alert['message'] = 'Performance buzz player updated successfully!';

        return redirect()->route('performance-buzz-winner-player.index')->with($alert);
    }

    /**
     * This function is used to update players last win date
     * 
     * @param  Collection $oldForward    - Forward 
     * @param  Collection $oldDefender   - Defender
     * @param  Collection $oldMidfielder - Midfielder
     * 
     * @return Boolean
     */
    public function updatePlayerLastWinDate($oldForward, $oldDefender, $oldMidfielder) {

        if ($oldForward) {

            $forwardPlayer = Player::find($oldForward->player_id);

            if ($forwardPlayer) {

                $date = $this->getLastWinDate($forwardPlayer->id);
                $forwardPlayer->last_win_date = $date;
                $forwardPlayer->save();
            }
        }

        if ($oldDefender) {

            $defenderPlayer = Player::find($oldDefender->player_id);
            $date = $this->getLastWinDate($defenderPlayer->id);
            $defenderPlayer->last_win_date = $date;
            $defenderPlayer->save();
        }

        if ($oldMidfielder) {

            $midfielderPlayer = Player::find($oldMidfielder->player_id);
            $date = $this->getLastWinDate($midfielderPlayer->id);
            $midfielderPlayer->last_win_date = $date;
            $midfielderPlayer->save();
        }
    }

    /**
     * This function is used to get player last win date
     * 
     * @param  integer $playerId - Player ID
     * 
     * @return string
     */
    public function getLastWinDate($playerId) {

        $lastWinDate = \DB::select("SELECT PLAYER_ID, COUNT(*) MAX_SCORE_COUNT, GROUP_CONCAT(date) as dates, date FROM ( SELECT player_id PLAYER_ID, date FROM performance_buzz a WHERE a.score = ( SELECT MAX(b.score) FROM performance_buzz b WHERE b.date = a.date)) s WHERE PLAYER_ID = " . $playerId . " GROUP BY PLAYER_ID ORDER BY date DESC");

        $date = '';

        if (count($lastWinDate) > 0) {

            $dates = explode(',', $lastWinDate[0]->dates);

            $lastEl = array_values(array_slice($dates, -1))[0];

            $date = $lastEl;
        }

        return $date;
    }

    /**
     * This function is used to update player top wins and positonal wins
     * 
     * @param  string $data - PB date
     * 
     * @return Response
     */
    public function updateOldPlayerData($date) {

        $forward = $this->getPbByDate($date, 'Forward');
        $defender = $this->getPbByDate($date, 'Defender');
        $midfielder = $this->getPbByDate($date, 'Midfielder');
        $topplayer = $this->getPbByDate($date, null);

        //Top Player
        $tPlayer = Player::find(@$topplayer->player_id);

        if ($tPlayer) {

            $tPlayer->top_player_wins = $tPlayer->top_player_wins > 0 ? $tPlayer->top_player_wins - 1 : 0;
            $tPlayer->save();
        }


        $fPlayer = Player::find(@$forward->player_id);
        if ($fPlayer) {

            $fPlayer->positional_wins = $fPlayer->positional_wins > 0 ? $fPlayer->positional_wins - 1 : 0;
            $fPlayer->save();
        }

        $mPlayer = Player::find(@$midfielder->player_id);
        if ($mPlayer) {

            $mPlayer->positional_wins = $mPlayer->positional_wins > 0 ? $mPlayer->positional_wins - 1 : 0;
            $mPlayer->save();
        }

        $dPlayer = Player::find(@$defender->player_id);
        if ($dPlayer) {

            $dPlayer->positional_wins = $dPlayer->positional_wins > 0 ? $dPlayer->positional_wins - 1 : 0;
            $dPlayer->save();
        }
    }

    /**
     * This function is used to update player top wins and positonal wins
     * 
     * @param  string $data - PB date
     * 
     * @return Response
     */
    public function updateNewPlayerData($date) {

        $forward = $this->getPbByDate($date, 'Forward');
        $defender = $this->getPbByDate($date, 'Defender');
        $midfielder = $this->getPbByDate($date, 'Midfielder');
        $topplayer = $this->getPbByDate($date, null);

        //Top Player
        $tPlayer = Player::find(@$topplayer->player_id);
        if ($tPlayer) {

            $tPlayer->top_player_wins = $tPlayer->top_player_wins + 1;
            $tPlayer->save();
        }


        $fPlayer = Player::find(@$forward->player_id);
        if ($fPlayer) {

            $fPlayer->positional_wins = $fPlayer->positional_wins + 1;
            $fPlayer->last_win_date = $date;
            $fPlayer->save();
        }

        $mPlayer = Player::find(@$midfielder->player_id);
        if ($mPlayer) {
            $mPlayer->positional_wins = $mPlayer->positional_wins + 1;
            $mPlayer->last_win_date = $date;
            $mPlayer->save();
        }

        $dPlayer = Player::find(@$defender->player_id);
        if ($dPlayer) {

            $dPlayer->positional_wins = $dPlayer->positional_wins + 1;
            $dPlayer->last_win_date = $date;
            $dPlayer->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * List the Files Uploaded for Performance Buzz
     *
     * @param
     * @return
     */
    public function performance_buzz_files_list(Request $request) {
        $data['activeSubMenu'] = 'performance_buzz_files_list';
        $data['pbfiles'] = PBFiles::select('*')->where('is_delete', 0)
                        ->orderBy('date', 'DESC')->get();
        return view('admin.pbfiles.index', $data);
    }

    public function performance_buzz_files_new(Request $request) {
        $data['activeSubMenu'] = 'performance_buzz_files_list';
        return view('admin.pbfiles.details', $data);
    }

    public function performance_buzz_files_post(Request $request) {
        $filename = $this->uploadPBFile($request->file('file_name'));
        $pbfile = new PBFiles();
        $pbfile->date = $request->date;
        $pbfile->file_name = $filename;
        if ($pbfile->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Record updated successfully!';
            $this->getCsvData($filename, $request->date);
            Helper::updatePBPosition($request->date);
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }
        return redirect()->route('performance_buzz_files_list')->with($alert);
    }

    public function performance_buzz_files_del(Request $request) {
        $pbfile = PBFiles::find($request->id);
        $pbfile->is_delete = 1;
        $date = $pbfile->date;
        $file_name = $pbfile->file_name;
        if (PerformanceBuzz::where('date', 'like', $date)->delete() && $pbfile->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Record updated successfully!';
            // $this->removePBFile($file_name);
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }
        return redirect()->route('performance_buzz_files_list')->with($alert);
    }

    public function getCsvData($filename, $date) {

        $filename = public_path('uploads/pb-files/' . $filename);

        $file_handle = fopen($filename, 'r');

        while (!feof($file_handle)) {
            $array[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);

        $pbcount = $fwd = $mid = $def = 0;
        $c = 0;
        foreach ($array as $arr) {
            if ($c != 0) {

                $name = $arr[0];
                $score = $arr[1];
                $sector = $arr[2];
                $team = $arr[3];
                $player = Player::where('football_index_common_name', '=', $name)->first();
                if ($player != "" && $date != "" && $score != "") {
                    if ($sector == '') {
                        $sector = $player->football_index_position;
                    }
                    if ($team != '') {
                        $teamArr = Team::where('name', '=', $name)->first();
                        if ($teamArr != '') {
                            $team_id = $teamArr->id;
                        } else {
                            $team_id = $player->team_id;
                        }
                    } else {
                        $team_id = $player->team_id;
                    }
                    $query = PerformanceBuzz::where(['date' => $date, 'player_id' => $player->id,])->count();
                    if ($query == 0) {
                        $pbData = new PerformanceBuzz();
                        $pbData->player_id = $player->id;
                        $pbData->team_id = $team_id;
                        $pbData->score = $score;
                        $pbData->sector = $sector;
                        $pbData->date = $date;
                        $pbData->save();

                        //                   UPDATE PLAYER FORM GUIDE SCORE
                        $form_guide = PerformanceBuzz::where(['player_id' => $player->id])->select("score")->orderBy('date', 'DESC')->limit(5)->get();
                        $player->form_guide = $form_guide->sum('score');
                        $player->save();
                    }
                }
            }
            $c++;
        }
    }

    public function uploadPBFile($file) {
        $destinationPath = 'uploads/pb-files';
        $filename = $file->hashName();
        $file->move($destinationPath, $filename);
        return $filename;
    }

    public function removePBFile($file) {
        if (file_exists('uploads/pb-files/' . $file)) {
            @unlink(public_path('/uploads/pb-files/') . $file);
        }
    }

}
