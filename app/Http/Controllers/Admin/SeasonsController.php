<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Yajra\Datatables\Datatables;
use App\Season;
use App\DayAndDividends;
use App\PerformanceBuzz;
use App\Player;
use App\Team;
use Helper;

class SeasonsController extends Controller {

    public function __construct() {
        View::share(['activeMenu' => 'games', 'activeSubMenu' => 'seasons']);
    }

    /**
     * This function is used to get Seasons view
     */
    public function index() {

        return view('admin.seasons.index');
    }

    /**
     * This function is used to get seasons data
     */
    public function seasonsData(Request $request) {
        $data = Season::select('seasons.*', 'leagues.name AS league_name')
                ->orderBy('id', 'DESC')
                ->join('leagues', 'leagues.id', 'seasons.league_id');

        $datatables = Datatables::of($data)
                ->editColumn('league', function($data) {
                    return $data->getLeagueData['name'];
                })
                ->addColumn('action', function($data) {
                    // return '<a class="btn btn-sm btn-success" href="' . route('seasons.fixtures', [ Helper::encrypt($data->id)]) . '">View Fixtures </a>';
                    if ($data->standing_active == 1) {
                        return '<input type="checkbox" name="standing_active" checked value="' . $data->id . '" onclick="change_standing_status(' . $data->id . ',this)" />';
                    } else {
                        return '<input type="checkbox" name="standing_active" value="' . $data->id . '" onclick="change_standing_status(' . $data->id . ',this)" />';
                    }
                })
                ->addColumn('actions', function($data) {
                     if(isset($data->start_date) || $data->start_date != null){
                        $datas= $data->id.".".$data->start_date;
                    }else{
                        $datas= $data->id.".&nbsp;";
                    }
                    return $datas;
                })
                ->addColumn('end_date', function($data) {
                    if(isset($data->end_date) || $data->end_date != null){
                        $datas = $data->id.".".$data->end_date;
                    }else{
                        $datas = $data->id.".&nbsp;";
                    }
                    return $datas;
                })
                ->filter(function ($query) use ($request) {
                    if (@$request->search['value']) {
                        $query->where('seasons.name', 'like', "%{$request->search['value']}%")
                        ->orWhere('leagues.name', 'like', "%{$request->search['value']}%");
                    }
                })
                ->make(true);

        return $datatables;
    }

    /**
     * Function to set the standing status of the season
     */
    public function setseasonstandingstatus(Request $request) {
        $season = Season::find($request->season_id);
        $season->standing_active = $request->standing_active;
        if ($season->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     *
     */
    public function day_and_dividends_list() {
        $data['activeSubMenu'] = 'day_and_dividends_list';
        return view('admin.day-and-dividends.index', $data);
    }

    public function day_and_dividends_details(Request $request) {
        $id = Helper::decrypt($request->id);
        $data = DayAndDividends::find($id);
        return view('admin.day-and-dividends.details', $data);
    }

    public function day_and_dividends_post(Request $request) {
        $id = Helper::decrypt($request->id);
        $data = DayAndDividends::find($id);
        $data->date = $request->date;
        $data->type_of_day = $request->type_of_day;
        $data->top_performance_player = $request->top_performance_player;
        $data->top_positional_performance_player = $request->top_positional_performance_player;
        $data->media_buzz_first = $request->media_buzz_first;
        $data->media_buzz_second = $request->media_buzz_second;
        $data->goalkeeper = $request->goalkeeper;
        $data->media_buzz_third = $request->media_buzz_third;
        $data->media_buzz_fourth = $request->media_buzz_fourth;
        $data->media_buzz_fifth = $request->media_buzz_fifth;
        $data->first_p = $request->first_p;
        $data->second_p = $request->second_p;
        $data->fifth_p = $request->fifth_p;
        $data->is_updated = "1";
        if ($data->save()) {

            $alert['status'] = 'success';
            $alert['message'] = 'Record updated successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }
        return redirect()->route('day_and_dividends_list')->with($alert);
    }

    public function dayanddividendsData(Request $request) {
        $data = DayAndDividends::select('*')
                ->orderBy('date', 'DESC');

        $datatables = Datatables::of($data)
                ->editColumn('type_of_day', function($data) {
                    if ($data->type_of_day == 1) {
                        return 'Single Performance';
                    } elseif ($data->type_of_day == 2) {
                        return 'Double Performance';
                    } elseif ($data->type_of_day == 3) {
                        return 'Triple Performance';
                    } elseif ($data->type_of_day == 4) {
                        return 'Triple Media';
                    } elseif ($data->type_of_day == 5) {
                        return 'Media Madness Top 5';
                    } else {
                        return 'Not Updated';
                    }
                })
                ->addColumn('action', function($data) {
                    return '<a class="btn btn-sm btn-success" href="' . route('day_and_dividends_details', [ Helper::encrypt($data->id)]) . '">Edit Day</a>';
                })
                ->filter(function ($query) use ($request) {
                    if (@$request->search['value']) {
                        $query->where('day_and_dividends.date', 'like', "%{$request->search['value']}%");
                    }
                })
                ->make(true);

        return $datatables;
    }

    /**
     * Function to calculte all players dividends
     */
    public function calcalldividends(Request $request) {
        $players = Player::where('football_index_common_name', '!=', '')->get();
        echo '<pre>';
        foreach ($players as $player) {
            $update = Helper::calculatePlayerDividend($player->id);
            print_r($update);
        }
        exit;
    }

    /**
     * Function to calculte all teams dividends
     */
    public function calcallteamsdividends(Request $request) {
        $teams = Team::all();
        echo '<pre>';
        foreach ($teams as $team) {
            $update = Helper::calculateTeamDividend($team->id);
            print_r($update);
        }
        exit;
    }

    /**
     * Function to update Pb positions for all dates
     */
    public function updatePbPositions(Request $request) {
        ini_set('max_execution_time', 600); //600 seconds = 10 minutes    
        $pbDates = PerformanceBuzz::select('date')
                ->groupBy('date')
                ->get();
        foreach ($pbDates as $pbDate) {
            Helper::updatePBPosition($pbDate->date);
        }
    }

    /**
     * Function to update players leagues. To be called in cron everyday once fixtures are been updated from sport monks api.
     */
    public function updatePlLeague(){
        $players = Player::where('football_index_common_name', '!=', '')->get();
        foreach ($players as $player) {
            $update = Helper::updatePlayerLeague($player->id);
        }
        exit;
    }

    /**
     * Function to update team leagues. To be called in cron everyday once fixtures are been updated from sport monks api.
     */
    public function updateTmLeague(){
        $teams = Team::all();
        foreach ($teams as $team) {
            $update = Helper::updateTeamLeague($team->id);
        }
        exit;
    }
    public function setstartdateseason(Request $request){
         $season = Season::find($request->season_id);
        $season->start_date = $request->standing_date;
        if ($season->save()) {
            return 1;
        } else {
            return 0;
        }
    }
    public function setenddateseason(Request $request){
         $season = Season::find($request->season_id);
        $season->end_date = $request->standing_date;
        if ($season->save()) {
            return 1;
        } else {
            return 0;
        }
    }

}
