<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\ArticleCategories;
use App\Article;
use App\Banner;
use App\Helpers\Helper;
use Excel;

class BannerController extends Controller {

    protected $perPage = 20;

    public function __construct() {
        View::share(['activeMenu' => 'manage-banner', 'activeSubMenu' => 'banner']);
    }

    public function index() {
        $data['banner'] = Banner::where('type','=','main')->paginate($this->perPage);
        return view('admin.banner.index')->with($data);
    }

    public function create() {
        return view('admin.banner.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'link' => 'required|url',
            'type' => 'required',
            'banner_image' => 'required|mimes:jpg,jpeg,png,gif,jpeg',
        ]);

        $filename = $this->uploadBannerFile($request->file('banner_image'));
        $banner = new Banner();
        $banner->type = $request->type;
        $banner->image = $filename;
        $banner->link = $request->link;

        if ($banner->save()) {

            $alert['status'] = 'success';
            $alert['message'] = 'Banner added successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }
        return redirect()->route('banner.index')->with($alert);
    }

    public function edit($id) {
        $id = Helper::decrypt($id);
        $banner = $this->findBanner($id);
        return view('admin.banner.edit',compact('banner'));
    }

    public function update(Request $request) {
        $id = Helper::decrypt($request->id);
        $a = $this->validate($request, [
            'link' => 'required|url',
            'type' => 'required',
            'banner_image' => 'nullable|mimes:jpg,jpeg,png,gif,jpeg',
        ]);

        $banner = $this->findBanner($id);

        if ($request->hasFile('banner_image')) {
            // UPLOAD NEW IMAGE
            $filename = $this->uploadBannerFile($request->file('banner_image'));
            // REMOVE OLD IMAGE
            $this->removeBannerFile($banner->image);
            $banner->image = $filename;
        }
        
        $banner->type = $request->type;
        $banner->link = $request->link;

        if ($banner->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Banner updated successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('banner.index')->with($alert);
    }

    public function destroy($id) {
        $id = Helper::decrypt($id);
        $delbanner = $this->findBanner($id);
        $this->removeBannerFile($delbanner->image);
        $banner = Banner::where('id','=',$id)->delete();
        if ($banner) {
            $alert['message'] = "Banner deleted successfully!";
            $alert['status'] = "success";
        } else {
            $alert['message'] = "Something went wrong!";
            $alert['status'] = "danger";
        }

        return redirect()->back()->with($alert);
    }

    public function findBanner($id) {
        return Banner::findorfail($id);
    }

    public function uploadBannerFile($file) {
        $destinationPath = public_path('uploads/banner');
        $filename = $file->getClientOriginalName().time().'.'.$file->getClientOriginalExtension();
        $file->move($destinationPath, $filename);
        return $filename;
    }

    public function removeBannerFile($file) {
        if (file_exists('uploads/banner/' . $file)) {
            \File::delete(public_path('/uploads/banner/') . $file);
        }
    }
}