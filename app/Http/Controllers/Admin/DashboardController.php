<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use Helper;

class DashboardController extends Controller
{
    public function index() {

    	$data['teams'] = $this->getTeamsWithMissingPlayer();


    	return view('admin.dashboard.home', $data);
    }

    /**
     * This function is used to get team with missing player
     * 
     * @return Response
     */
    public function getTeamsWithMissingPlayer() {

    	return Team::whereRaw(\DB::raw("NOT EXISTS ( SELECT 1 FROM players WHERE players.team_id = teams.id )"))->get();
    }
}
