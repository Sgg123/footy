<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\LengthAwarePaginator;
use App\PerformanceBuzz;
use App\Team;
use Helper;

class PerformanceBuzzTeamAdminController extends Controller {

    protected $perPage = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'performance-buzz', 'activeSubMenu' => 'performance-buzz-team-history']);
    }

    /**
     * This function is used to show perofrmance buzz team listing
     *
     * @return Response
     */
    public function index(Request $request) {
        $performance = Helper::performanceBuzzPlayers_new($request,$token='admin');
        $data['PBWinners'] = Helper::dispatchObject($performance); 
        
        $data['links'] = $performance;

        return view('admin.performancebuzz-winner.team.list', $data);
    }

    /**
     * This function is used to seed performance buzz data
     * 
     * @param  Collection $pbDates - dates collection
     * 
     * @return Collection
     */
    public function seedPbData($pbDates) {

        $pbDates = $pbDates->map(function ($dates) {

            $defender = $this->getPbByDate($dates->date, 'Defender');
            $dates['defender'] = $defender;

            $forward = $this->getPbByDate($dates->date, 'Forward');
            $dates['forward'] = $forward;

            $midfielder = $this->getPbByDate($dates->date, 'Midfielder');
            $dates['midfielder'] = $midfielder;

            $topplayer = $this->getPbByDate($dates->date, null);
            $dates['topplayer'] = $topplayer;

            return $dates;
        });

        return $pbDates;
    }

    /**
     * This function is used to get specific date data for pb
     * 
     * @param  string $date   - Date
     * @param  string $sector - Player position
     * 
     * @return Collection
     */
    public function getPbByDate($date, $sector = '') {

        return PerformanceBuzz::leftJoin('teams', 'teams.id', 'performance_buzz.team_id')
                        ->leftJoin(\DB::raw('( SELECT team_id FROM performance_buzz GROUP BY team_id ) Temp'), 'Temp.team_id', 'performance_buzz.team_id')
                        ->selectRaw('performance_buzz.*, teams.name')
                        ->where('date', $date)
                        ->when($sector != '', function($query) use($sector) {
                            return $query->where('sector', $sector);
                        })
                        ->when($sector == '', function($query) use($sector) {
                            return $query->whereIn('sector', ['Defender', 'Forward', 'Midfielder']);
                        })
                        ->orderBy('performance_buzz.score', 'DESC')
                        ->take(1)
                        ->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $date = Helper::decrypt($id);
        $team['date'] = $date;

        $forward = $this->getPbByDate($date, 'Forward');
        $team['forward'] = $forward;

        $defender = $this->getPbByDate($date, 'Defender');
        $team['defender'] = $defender;

        $midfielder = $this->getPbByDate($date, 'Midfielder');

        $team['midfielder'] = $midfielder;

        $team['teams'] = Team::get(['id', 'name']);

        return view('admin.performancebuzz-winner.team.form', $team);
    }

    /**
     * This function is used to update performance buzz record
     *
     * @param  Request  $request - Request Object
     * @param  string  $id - Encrypted date
     * 
     * @return Response
     */
    public function update(Request $request, $id) {

        $date = Helper::decrypt($id);

        $forward = $this->getPbByDate($date, 'Forward');
        $defender = $this->getPbByDate($date, 'Defender');
        $midfielder = $this->getPbByDate($date, 'Midfielder');
        $topplayer = $this->getPbByDate($date, null);

        //Forward Team
        if ($forward) {

            $forward->team_id = $request->top_forward_team;
            $forward->save();
        }


        //Middlefielder Team
        if ($midfielder) {

            $midfielder->team_id = $request->top_midfielder_team;
            $midfielder->save();
        }


        //Defender Team
        if ($defender) {

            $defender->team_id = $request->top_defender_team;
            $defender->save();
        }
        $this->updateOldTeamData($date);
        $this->updateNewTeamData($date);

        $alert['status'] = 'success';
        $alert['message'] = 'Performance buzz team updated successfully!';

        return redirect()->route('performance-buzz-winner-team.index')->with($alert);
    }

    /**
     * This function is used to update player top wins and positonal wins
     * 
     * @param  string $data - PB date
     * 
     * @return Response
     */
    public function updateOldTeamData($date) {


        $forward = $this->getPbByDate($date, 'Forward');
        $defender = $this->getPbByDate($date, 'Defender');
        $midfielder = $this->getPbByDate($date, 'Midfielder');
        $topTeam = $this->getPbByDate($date, null);

        //Top Team
        $tTeam = Team::find(@$topTeam->team_id);
        if ($tTeam) {

            $tTeam->top_player_wins = $tTeam->top_player_wins > 0 ? $tTeam->top_player_wins - 1 : 0;
            $tTeam->save();
        } else {
            
        }


        $fTeam = Team::find(@$forward->team_id);
        if ($fTeam) {

            $fTeam->positional_wins = $fTeam->positional_wins > 0 ? $fTeam->positional_wins - 1 : 0;
            $fTeam->save();
        }

        $mTeam = Team::find(@$midfielder->team_id);
        if ($mTeam) {
            $mTeam->positional_wins = $mTeam->positional_wins > 0 ? $mTeam->positional_wins - 1 : 0;
            $mTeam->save();
        }

        $dTeam = Team::find(@$defender->team_id);
        if ($dTeam) {

            $dTeam->positional_wins = $dTeam->positional_wins > 0 ? $dTeam->positional_wins - 1 : 0;
            $dTeam->save();
        }
    }

    /**
     * This function is used to update player top wins and positonal wins
     * 
     * @param  string $data - PB date
     * 
     * @return Response
     */
    public function updateNewTeamData($date) {

        $forward = $this->getPbByDate($date, 'Forward');
        $defender = $this->getPbByDate($date, 'Defender');
        $midfielder = $this->getPbByDate($date, 'Midfielder');
        $topTeam = $this->getPbByDate($date, null);

        //Top Team
        $tTeam = Team::find(@$topTeam->team_id);
        if ($tTeam) {

            $tTeam->top_player_wins = $tTeam->top_player_wins + 1;
            $tTeam->save();
        }


        $fTeam = Team::find(@$forward->team_id);
        if ($fTeam) {

            $fTeam->positional_wins = $fTeam->positional_wins + 1;
            $fTeam->last_win_date = $date;
            $fTeam->save();
        }

        $mTeam = Team::find(@$midfielder->team_id);
        if ($mTeam) {
            $mTeam->positional_wins = $mTeam->positional_wins + 1;
            $mTeam->last_win_date = $date;
            $mTeam->save();
        }

        $dTeam = Team::find(@$defender->team_id);
        if ($dTeam) {

            $dTeam->positional_wins = $dTeam->positional_wins + 1;
            $dTeam->last_win_date = $date;
            $dTeam->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
