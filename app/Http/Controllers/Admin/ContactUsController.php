<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\ContactEmail as ContactEmail;
use Helper;
use Mail;
use DB;

class ContactUsController extends Controller {

    protected $perPage = 20;

    public function __construct() {
        View::share(['activeMenu' => 'contactus-emails']);
    }

    /**
     * This function is used to show email listing
     *
     * @return View
     */
    public function index() {
        $data['emails'] = ContactEmail::paginate($this->perPage);
        return view('admin.contactus-emails.list', $data);
    }

    /**
     * This function is used to delete contactus email.
     *
     * @param  int  $id - ID
     * 
     * @return Response
     */
    public function destroy($id) {

        $email = ContactEmail::findOrFail(Helper::decrypt($id));
        $email->delete();

        $alert['status'] = 'success';
        $alert['message'] = 'Email deleted successfully!';

        return redirect()->route('contactus-emails.index')->with($alert);
    }


}

?>