<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Yajra\Datatables\Datatables;
use App\Fixture;
use App\League;
use App\Season;
use App\Player;
use App\Page;
use App\Team;
use Helper;
use DB;

class FixturesController extends Controller {

    protected $perPage = 20;

    public function __construct() {

        if (\Route::current()->getName() == 'players' || \Route::current()->getName() == 'players.edit' || \Route::current()->getName() == 'players.update') {
            $activeSubMenu = 'players';
            $activeMenu = 'games';
        } else if (\Route::current()->getName() == 'teams' || \Route::current()->getName() == 'teams.edit' || \Route::current()->getName() == 'teams.update') {
            $activeSubMenu = 'teams';
            $activeMenu = 'games';
        } else if (\Route::current()->getName() == 'pb.planner' || \Route::current()->getName() == 'pb.planner.upgrade') {
            $activeSubMenu = 'pb-planner';
            $activeMenu = 'performance-buzz';
        } else {
            $activeSubMenu = 'fixtures';
            $activeMenu = 'games';
        }

        View::share(['activeMenu' => $activeMenu, 'activeSubMenu' => $activeSubMenu]);
    }

    /**
     * This function is used to get fixtures view
     */
    public function index() {

        $data['leagues'] = League::get();
        return view('admin.fixtures.index')->with($data);
    }

    /**
     * This function is used to get fixtures data
     */
    public function fixturesData(Request $request) {

        $data = Fixture::select('fixtures.*', 'leagues.name as league', 'seasons.name as season')
                ->addSelect(DB::raw("(select `name` from `teams` where `id` = fixtures.localteam_id) AS local_team"))
                ->addSelect(DB::raw("(select `name` from `teams` where `id` = fixtures.visitorteam_id) AS visitor_team"))
                ->orderBy('id', 'DESC')
                ->join('leagues', 'leagues.id', 'fixtures.league_id')
                ->join('seasons', 'seasons.id', 'fixtures.season_id');

        $datatables = Datatables::of($data)
                ->addColumn('action', function($data) {
                    return '<a class="btn btn-sm btn-success" href="' . route('fixture.details', [Helper::encrypt($data->id)]) . '">Fixture Details </a>';
                })
                ->filter(function ($query) use ($request) {

                    if (@$request->league_id) {
                        $query->where('fixtures.league_id', '=', @$request->league_id);
                    }
                    if (@$request->season_id) {
                        $query->where('fixtures.season_id', '=', @$request->season_id);
                    }
                })
                ->make(true);

        return $datatables;
    }

    /**
     * This function is used to get season league wise
     */
    public function getLeagueSeasons(Request $request) {

        $seasons = League::find($request->id)->getSeasons;
        return response()->json($seasons);
    }

    /**
     * This function is used to get fixtures details
     */
    public function fixturesDetails($id) {
        $id = Helper::decrypt($id);

        $data['fixture_details'] = Fixture::where('fixtures.id', '=', $id)
                ->select('fixtures.*')
                ->first();
        $data['localTeam'] = Team::where('id', '=', $data['fixture_details']['localteam_id'])
                        ->select('name', 'id', 'logo_path')
                        ->with(['getTeamPlayers' => function($query) {
                                return $query->select('team_id', 'firstname', 'lastname', 'common_name');
                            }])->get()->toArray();
        $data['visitorTeam'] = Team::where('id', '=', $data['fixture_details']['visitorteam_id'])
                        ->select('name', 'id', 'logo_path')
                        ->with(['getTeamPlayers' => function($query) {
                                return $query->select('team_id', 'firstname', 'lastname', 'common_name');
                            }])->get()->toArray();

        return view('admin.fixtures.details')->with($data);
    }

    /**
     * This function is used to get edit Fixture
     */
    public function editFixture($id) {
        $id = Helper::decrypt($id);
        $data['player'] = Fixture::findorfail($id);
        return view('admin.fixtures.edit')->with($data);
    }

    public function getPbPlannerData($startingAt) {
        if (\Auth::user()->isPremium()) {
            $date = Helper::decrypt($startingAt);
            $data['startDate'] = $date;
            $query = Fixture::join('seasons','seasons.id','=','fixtures.season_id')
                    ->where(\DB::raw('DATE(fixtures.starting_at)'),$date)
                    ->where(\DB::raw('DATE(seasons.start_date)'),'<=',$date)
                    ->where(\DB::raw('DATE(seasons.end_date)'),'>=',$date);
            $query_all_matches = Fixture::join('seasons','seasons.id','=','fixtures.season_id')
                    ->where(\DB::raw('DATE(fixtures.starting_at)'),$date)
                    ->where(\DB::raw('DATE(seasons.start_date)'),'<=',$date)
                    ->where(\DB::raw('DATE(seasons.end_date)'),'>=',$date);
            $data['totalMatches'] = $data['fixturesCount'] = $query->count();

            $data['fixtures'] = $query
                            ->join('leagues', 'leagues.id', 'fixtures.league_id')
                            ->select('fixtures.*', 'leagues.name as league', 'seasons.name as season')
                            ->orderBy('leagues.name', 'ASC')
                            ->groupBy('leagues.id')->get();

            $data['upcomingMatches'] = app('App\Http\Controllers\HomeController')->getUpComingMatchesStats();
            $sql = "SELECT * FROM day_and_dividends AS dd WHERE dd.date >= CURDATE()"; 
            $data['updatedmatches'] = DB::select($sql);
            $data['totalPlayers'] = $this->getTotalPlayers($date);
            $activeSubMenu = 'pb-planner';
            $activeMenu = 'performance-buzz';

            $data['all_matches'] = $query_all_matches->get();
            $teams = array();
            foreach ($data['all_matches'] as $fixture) {
                $teams[] = $fixture->localteam_id;
                $teams[] = $fixture->visitorteam_id;
            }
            if (!empty($teams)) {
                $teams_str = implode(',', $teams);
            } else {
                $teams_str = '';
            }
            if (!empty($teams)) {
                $sql = "SELECT pl.*, AVG(pb.score) as total_score FROM players AS pl JOIN performance_buzz AS pb ON pl.id=pb.player_id WHERE pl.team_id IN (" . $teams_str . ") GROUP BY pl.id ORDER BY total_score DESC LIMIT 50";
                $data['all_players'] = DB::select($sql);
            } else {
                $data['all_players'] = '';
            }

            View::share(['activeMenu' => $activeMenu, 'activeSubMenu' => $activeSubMenu]);
            return view('front.fixtures.index')->with($data);
        } else {
            return redirect('/pb-planner-upgrade');
        }
    }

    public function getPbPlannerDataUpgrade()
    {
        if (\Auth::user()->isPremium()) {
            return redirect()->route('pb.planner', Helper::encrypt(date('Y-m-d')));
        }
        else
        {
            return view('front.fixtures.upgrade');
        }
    }

    public function getPbPlannerFixtures(Request $request) {
        $data['fixtures_list'] = Fixture::whereDate('starting_at', '=', $request->date)
                ->where('fixtures.league_id', $request->league_id)
                ->join('leagues', 'leagues.id', 'fixtures.league_id')
                ->select('fixtures.*')
                ->addSelect(DB::raw("(select `name` from `teams` where `id` = fixtures.localteam_id) AS local_team"))
                ->addSelect(DB::raw("(select `logo_path` from `teams` where `id` = fixtures.localteam_id) AS local_team_logo"))
                ->addSelect(DB::raw("(select `name` from `teams` where `id` = fixtures.visitorteam_id) AS visitor_team"))
                ->addSelect(DB::raw("(select `logo_path` from `teams` where `id` = fixtures.visitorteam_id) AS visitor_team_logo"))
                ->orderBy('local_team', 'ASC')
                ->get();


        $view = \View::make('front.fixtures.fixtures-list')->with($data);
        echo $view;
    }

    public function getTotalPlayers($date) {
        $localTeam = Fixture::whereDate('starting_at', '=', $date)->pluck('localteam_id')->toArray();
        $visitorTeam = Fixture::whereDate('starting_at', '=', $date)->pluck('visitorteam_id')->toArray();
        $count = 0;

        $localTeamPlayer = Player::whereIn('team_id', $localTeam)->where('players.football_index_common_name', '!=', NULL)->count();
        $visitorTeamPlayer = Player::whereIn('team_id', $visitorTeam)->where('players.football_index_common_name', '!=', NULL)->count();
        $count = $localTeamPlayer + $visitorTeamPlayer;
        return $count;
    }

    public function getTeamPlayers(Request $request) {
        $localteam_id = $request->localteam_id;
        $visitorteam_id = $request->visitorteam_id;
        $all_data = array();
        $data['localTeam'] = Team::where('id', '=', $localteam_id)
                        ->select('name', 'id', 'logo_path', 'local_image_path', 'is_local_image')
                        ->with(['getTeamPlayers' => function($query) {
                                return $query->select('players.id as pid', 'players.team_id', 'firstname', 'lastname', 'common_name', 'football_index_common_name', 'players.position', 'players.football_index_position', 'local_image_path', 'is_local_image', 'image_path', DB::raw('COUNT(performance_buzz.player_id) as total_count, AVG(performance_buzz.score) as total_score'))
                                        ->where('players.football_index_common_name', '!=', NULL)
                                        ->leftJoin('performance_buzz', 'players.id', '=', 'performance_buzz.player_id')
                                        ->groupBy('players.id')
                                        ->orderBy('total_score', 'DESC')
                                        ->get();
                            }])->get()->toArray();
        $data['visitorTeam'] = Team::where('id', '=', $visitorteam_id)
                        ->select('name', 'id', 'logo_path', 'local_image_path', 'is_local_image')
                        ->with(['getTeamPlayers' => function($query) {
                                return $query->select('players.id as pid', 'players.team_id', 'firstname', 'lastname', 'common_name', 'football_index_common_name', 'players.position', 'players.football_index_position', 'local_image_path', 'is_local_image', 'image_path', DB::raw('COUNT(performance_buzz.player_id) as total_count, AVG(performance_buzz.score) as total_score'))
                                        ->where('players.football_index_common_name', '!=', NULL)
                                        ->leftJoin('performance_buzz', 'players.id', '=', 'performance_buzz.player_id')
                                        ->groupBy('players.id')
                                        ->orderBy('total_score', 'DESC')
                                        ->get();
//                                        ->addSelect(DB::raw("(CASE WHEN (players.is_local_image = 1) THEN (select players.local_image_path)
//                                                        WHEN (players.is_local_image = 0) THEN (select players.image_path)
//                                                        end) AS image_path"));
                            }])->get()->toArray();
        $data['all_players'] = array_merge($data['localTeam'][0]['get_team_players'], $data['visitorTeam'][0]['get_team_players']);
        usort($data['all_players'], function($a, $b) {
            return $b['total_score'] <=> $a['total_score'];
        });

        $view = \View::make('front.fixtures.team-players-list')->with($data);
        echo $view;
    }

    /**
     * This function is used to get players view
     */
    public function getPlayers() {

        return view('admin.players.index');
    }

    /**
     * This function is used to get players data
     */
    public function getPlayersData(Request $request) {

        $data = Player::select('players.*', 'teams.name as team')
                ->orderBy('id', 'DESC')
                ->join('teams', 'teams.id', 'players.team_id');

        $datatables = Datatables::of($data)
                ->addColumn('action', function($data) {
                    return '<a class="btn btn-sm btn-success" href="' . route('players.edit', [Helper::encrypt($data->id)]) . '"><i class="fa fa-fw fa-pencil"></i>Edit </a>';
                })
                ->filter(function ($query) use ($request) {

                    if (@$request->search['value']) {

                        $query->where('players.firstname', 'like', "%{$request->search['value']}%")
                        ->orWhere('players.lastname', 'like', "%{$request->search['value']}%")
                        ->orWhere('players.middlename', 'like', "%{$request->search['value']}%")
                        ->orWhere('players.common_name', 'like', "%{$request->search['value']}%")
                        ->orWhere('players.football_index_position', 'like', "%{$request->search['value']}%")
                        ->orWhere('players.nationality', 'like', "%{$request->search['value']}%")
                        ->orWhere('teams.name', 'like', "%{$request->search['value']}%");
                    }
                })
                ->make(true);

        return $datatables;
    }

    /**
     * This function is used to get edit players
     */
    public function editPlayer($id) {
        $id = Helper::decrypt($id);
        $data['player'] = Player::findorfail($id);
        return view('admin.players.edit')->with($data);
    }

    /**
     * This function is used to get edit players
     */
    public function updatePlayer(Request $request, $id) {
        $this->validate($request, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'middlename' => 'required|string|max:255',
            'common_name' => 'required|string|max:255',
        ]);

        $id = Helper::decrypt($id);
        $player = Player::findorfail($id);

        $player->firstname = $request->firstname;
        $player->lastname = $request->lastname;
        $player->middlename = $request->middlename;
        $player->common_name = $request->common_name;
        $player->nationality = $request->nationality;
        $player->birthdate = $request->birthdate;
        $player->height = $request->height;
        $player->weight = $request->weight;

        if ($player->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Player updated successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('players')->with($alert);
    }

    /**
     * This function is used to get teams view
     */
    public function getTeams() {

        return view('admin.teams.index');
    }

    /**
     * This function is used to get teams data
     */
    public function getTeamsData(Request $request) {

        $data = Team::select('teams.*')
                ->orderBy('id', 'DESC');

        $datatables = Datatables::of($data)
                ->addColumn('action', function($data) {
                    return '<a class="btn btn-sm btn-success" href="' . route('teams.edit', [Helper::encrypt($data->id)]) . '"><i class="fa fa-fw fa-pencil"></i>Edit </a>' . ' <a class="btn btn-sm btn-info  " href="' . route('teams.players', [Helper::encrypt($data->id)]) . '"><i class="fa fa-fw fa-eye"></i>View Players </a>';
                })
                ->filter(function ($query) use ($request) {

                    if (@$request->search['value']) {

                        $query->where('teams.name', 'like', "%{$request->search['value']}%")
                        ->orWhere('teams.twitter', 'like', "%{$request->search['value']}%")
                        ->orWhere('teams.founded', 'like', "%{$request->search['value']}%");
                    }
                })
                ->make(true);

        return $datatables;
    }

    /**
     * This function is used to get edit players
     */
    public function editTeam($id) {
        $id = Helper::decrypt($id);
        $data['team'] = Team::findorfail($id);
        return view('admin.teams.edit')->with($data);
    }

    /**
     * This function is used to get edit players
     */
    public function updateTeam(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);

        $id = Helper::decrypt($id);
        $team = Team::findorfail($id);

        $team->name = $request->name;
        $team->national_team = $request->national_team;
        $team->twitter = $request->twitter;
        $team->founded = $request->founded;
        if ($request->hasFile('local_image_path')) {
            $file = $request->file('local_image_path');
            $destinationPath = 'uploads/team-images';
            $filename = $file->hashName();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $team->local_image_path = $filename;
            $team->is_local_image = 1;
        }
        if ($team->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Team updated successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('teams')->with($alert);
    }
}
