<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\User;
use App\Admin;
use App\Setting;
use App\Helpers\Helper;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Auth\Events\Registered;
use Stripe\Error\Card;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Mail;


class UserController extends Controller {

    protected $perPage = 20;

    public function __construct() {
        View::share(['activeMenu' => 'users', 'activeSubMenu' => 'users']);
    }

    public function index() {

        $data['users'] = User::where('status', '=', 'active')->paginate($this->perPage);

        return view('admin.users.index')->with($data);
    }

    /**
     * Delete user account from admin section
     * */
    public function deleteUser(Request $request, $id) {

        $id = Helper::decrypt($id);
        $user = User::findorfail($id);
        $user->status = 'deleted';

        if ($user->save()) {
            $alert['message'] = "User deleted successfully!";
            $alert['status'] = "success";
        } else {
            $alert['message'] = "Something went wrong!";
            $alert['status'] = "danger";
        }

        return redirect()->back()->with($alert);
    }

    public function store(Request $request)
    {
        $rule = [
                'firstname' => 'required|string|max:50',
                'lastname' => 'required|string|max:50',
                'phone' => 'required|digits_between:1,11',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'confirmed|required|min:6'
            ];
        $request->validate($rule);

        if ($request->validate($rule)) {

            $user = User::create([
                    'firstname' => $request->get('firstname'),
                    'lastname' => $request->get('lastname'),
                    'phone' => $request->get('phone'),
                    'email' => $request->get('email'),
                    'password' => bcrypt($request->get('password')),
                    'stripe_id' => '',
                    'stripe_card_id' => '',
                    'card_brand' => '',
                    'card_last_four' => '',
                    'account_type' => $request->get('account_type'),
                    'stripe_subscription_id' => '',
                    'create_by_admin' => 1
                ]);
            event(new Registered($user));
            Mail::send('emails.user-create', array('user' => $user, 'password' => $request->get('password')), function($message)use ($request) {
                $message->to($request->email, $request->first_name . ' ' . $request->last_name)->subject('Welcome to Footy Index Scout');
                $message->from(config('mail.from.address'), config('mail.from.name'));
            });

            if ($user) {
                $alert['message'] = "User created successfully!";
                $alert['status'] = "success";
            } else {
                $alert['message'] = "Something went wrong!";
                $alert['status'] = "danger";
            }

            return redirect()->route('users')->with($alert);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addUser() {
        return view('admin.users.add');
    }

    /**
     * Show the view paywith stripe.
     * */
    public function payWithStripe() 
    {
        if (\Auth::user()->isPremium()) {
            \Session::put('success', 'You have premium subscription.');
        }
        $data['subscription_charge'] = Setting::where('name', '=', 'subscription_charge')->first();
        return view('front.pages.paywithstripe', $data);
    }

    /**
     * Store a newly created resource in storage.     
     * */
    public function postPaymentWithStripe(Request $request) 
    {
        $s = $this->validate($request, [
            'card_no' => 'required|numeric',
            'ccExpiryMonth' => 'required|numeric|between:1,12',
            'ccExpiryYear' => 'required|numeric|digits_between:2,2',
            'cvvNumber' => 'required|numeric|digits_between:3,3',
            'amount' => 'required|integer',
                ], [
            'ccExpiryYear.digits_between' => 'The expiry year must be a valid year.',
            'cvvNumber.digits_between' => 'The cvv number must be of 3 digits only.',
        ]);

        $stripe = Stripe::make(env('STRIPE_SECRET'));
        if (\Auth::user()->account_type === 'tier_2') {
            try {
                $stripe_customer = $stripe->customers()->create([
                    'email' => \Auth::user()->email,
                ]);
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $request->card_no,
                        'exp_month' => $request->ccExpiryMonth,
                        'cvc'       => $request->cvvNumber,
                        'exp_year'  => $request->ccExpiryYear,
                    ],
                ]);
                if (!isset($token['id'])) {
                    \Session::put('error', 'The Stripe Token was not generated correctly');
                    return redirect()->route('paywithstripe');
                }
                $card = $stripe->cards()->create($stripe_customer['id'], $token['id']);
                $stripe_plan_count = Setting::where('name', '=', 'stripe_subscription_plan')->count();
                if($stripe_plan_count > 0)
                {
                    $stripe_plan = Setting::where('name', '=', 'stripe_subscription_plan')->first();
                    $stripe_subscription = $stripe->subscriptions()->create($stripe_customer['id'], [
                        'plan' => $stripe_plan->value,
                    ]);
                    \Auth::user()->account_type = 'tier_3';
                    \Auth::user()->stripe_id = $stripe_customer['id'];
                    \Auth::user()->stripe_subscription_id = $stripe_subscription['id'];
                    \Auth::user()->stripe_card_id = $card['id'];
                    \Auth::user()->card_brand = $card['brand'];
                    \Auth::user()->card_last_four = $card['last4'];
                    \Auth::user()->payment_date = date('Y-m-d H:i:s');
                    \Auth::user()->save();
                }       
                \Session::put('success', 'Payment completed successfully.');
                return redirect()->route('stripe-payment-form');         
            } catch (\Exception $e) {
                \Session::put('error', $e->getMessage());
                return redirect()->route('stripe-payment-form');
            } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
                \Session::put('error', $e->getMessage());
                return redirect()->route('stripe-payment-form');
            } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                \Session::put('error', $e->getMessage());
                return redirect()->route('stripe-payment-form');
            }
        }
        else
        {
            try {
                if(empty(\Auth::user()->stripe_id))
                {
                    $stripe_customer = $stripe->customers()->create([
                        'email' => \Auth::user()->email,
                    ]);
                    $token = $stripe->tokens()->create([
                        'card' => [
                            'number'    => $request->card_no,
                            'exp_month' => $request->ccExpiryMonth,
                            'cvc'       => $request->cvvNumber,
                            'exp_year'  => $request->ccExpiryYear,
                        ],
                    ]);
                    if (!isset($token['id'])) {
                        \Session::put('error', 'The Stripe Token was not generated correctly');
                        return redirect()->route('paywithstripe');
                    }
                    $card = $stripe->cards()->create($stripe_customer['id'], $token['id']);
                    \Auth::user()->stripe_id = $stripe_customer['id'];
                    \Auth::user()->stripe_card_id = $card['id'];
                    \Auth::user()->card_brand = $card['brand'];
                    \Auth::user()->card_last_four = $card['last4'];
                    \Auth::user()->save();
                    $stripe_plan_count = Setting::where('name', '=', 'stripe_subscription_plan')->count();
                    if($stripe_plan_count > 0)
                    {
                        $stripe_plan = Setting::where('name', '=', 'stripe_subscription_plan')->first();
                        $stripe_subscription = $stripe->subscriptions()->create($stripe_customer['id'], [
                            'plan' => $stripe_plan->value,
                        ]);
                        \Auth::user()->account_type = 'tier_3';
                        \Auth::user()->stripe_subscription_id = $stripe_subscription['id'];
                        \Auth::user()->payment_date = date('Y-m-d H:i:s');
                        \Auth::user()->save();
                    }
                }
                else if(empty(\Auth::user()->stripe_card_id))
                {
                    $token = $stripe->tokens()->create([
                        'card' => [
                            'number'    => $request->card_no,
                            'exp_month' => $request->ccExpiryMonth,
                            'cvc'       => $request->cvvNumber,
                            'exp_year'  => $request->ccExpiryYear,
                        ],
                    ]);
                    if (!isset($token['id'])) {
                        \Session::put('error', 'The Stripe Token was not generated correctly');
                        return redirect()->route('paywithstripe');
                    }
                    $card = $stripe->cards()->create(\Auth::user()->stripe_id, $token['id']);
                    \Auth::user()->stripe_card_id = $card['id'];
                    \Auth::user()->card_brand = $card['brand'];
                    \Auth::user()->card_last_four = $card['last4'];
                    \Auth::user()->save();

                    $stripe_plan_count = Setting::where('name', '=', 'stripe_subscription_plan')->count();
                    if($stripe_plan_count > 0)
                    {
                        $stripe_plan = Setting::where('name', '=', 'stripe_subscription_plan')->first();
                        $stripe_subscription = $stripe->subscriptions()->create($stripe_customer['id'], [
                            'plan' => $stripe_plan->value,
                        ]);
                        \Auth::user()->account_type = 'tier_3';
                        \Auth::user()->stripe_subscription_id = $stripe_subscription['id'];
                        \Auth::user()->payment_date = date('Y-m-d H:i:s');
                        \Auth::user()->save();
                    }
                }
                else
                {
                    $card = $stripe->cards()->delete(\Auth::user()->stripe_id, \Auth::user()->stripe_card_id);
                    $token = $stripe->tokens()->create([
                        'card' => [
                            'number'    => $request->card_no,
                            'exp_month' => $request->ccExpiryMonth,
                            'cvc'       => $request->cvvNumber,
                            'exp_year'  => $request->ccExpiryYear,
                        ],
                    ]);
                    if (!isset($token['id'])) {
                        \Session::put('error', 'The Stripe Token was not generated correctly');
                        return redirect()->route('paywithstripe');
                    }
                    $card = $stripe->cards()->create(\Auth::user()->stripe_id, $token['id']);
                    \Auth::user()->stripe_card_id = $card['id'];
                    \Auth::user()->card_brand = $card['brand'];
                    \Auth::user()->card_last_four = $card['last4'];
                    \Auth::user()->save();

                    $stripe_plan_count = Setting::where('name', '=', 'stripe_subscription_plan')->count();
                    if($stripe_plan_count > 0 && !\Auth::user()->isPremium())
                    {
                        $stripe_plan = Setting::where('name', '=', 'stripe_subscription_plan')->first();
                        $stripe_subscription = $stripe->subscriptions()->create($stripe_customer['id'], [
                            'plan' => $stripe_plan->value,
                        ]);
                        \Auth::user()->account_type = 'tier_3';
                        \Auth::user()->stripe_subscription_id = $stripe_subscription['id'];
                        \Auth::user()->payment_date = date('Y-m-d H:i:s');
                        \Auth::user()->save();
                    }
                }
               
                \Session::put('success', 'Payment completed successfully.');
                return redirect()->route('stripe-payment-form');
            } catch (\Exception $e) {
                \Session::put('error', $e->getMessage());
                return redirect()->route('stripe-payment-form');
            } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
                \Session::put('error', $e->getMessage());
                return redirect()->route('stripe-payment-form');
            } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                \Session::put('error', $e->getMessage());
                return redirect()->route('stripe-payment-form');
            }
        }

        return redirect()->route('stripe-payment-form');
    }

    /**
     * Show the view Update paywith stripe.
     * */
    public function updatePayWithStripe() 
    {   
        if(!\Auth::user()->isPremium())
        {
            \Session::put('error', 'You are not a premium member.');
            return redirect()->route('stripe-payment-form');
        }
        $data['subscription_charge'] = Setting::where('name', '=', 'subscription_charge')->first();
        return view('front.pages.updatepaymentmethod', $data);
    }

    /**
     * Store a newly created resource in storage.     
     * */
    public function postUpdatePaymentWithStripe(Request $request) 
    {
        $s = $this->validate($request, [
            'card_no' => 'required|numeric',
            'ccExpiryMonth' => 'required|numeric|between:1,12',
            'ccExpiryYear' => 'required|numeric|digits_between:2,2',
            'cvvNumber' => 'required|numeric|digits_between:3,3',
                ], [
            'ccExpiryYear.digits_between' => 'The expiry year must be a valid year.',
            'cvvNumber.digits_between' => 'The cvv number must be of 3 digits only.',
        ]);
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        try 
        {
            try
            {
                $card = $stripe->cards()->delete(\Auth::user()->stripe_id, \Auth::user()->stripe_card_id);
            }
            catch (\Exception $e) { }

            $token = $stripe->tokens()->create([
                'card' => [
                    'number'    => $request->card_no,
                    'exp_month' => $request->ccExpiryMonth,
                    'cvc'       => $request->cvvNumber,
                    'exp_year'  => $request->ccExpiryYear,
                ],
            ]);
            if (!isset($token['id'])) 
            {
                \Session::put('error', 'The Stripe Token was not generated correctly');
                return redirect()->route('updatepaymentmethod');
            }
            $card = $stripe->cards()->create(\Auth::user()->stripe_id, $token['id']);
            \Auth::user()->stripe_card_id = $card['id'];
            \Auth::user()->card_brand = $card['brand'];
            \Auth::user()->card_last_four = $card['last4'];
            \Auth::user()->save();

            $stripe_plan_count = Setting::where('name', '=', 'stripe_subscription_plan')->count();
            if($stripe_plan_count > 0 && \Auth::user()->isPremium())
            {
                $stripe_plan = Setting::where('name', '=', 'stripe_subscription_plan')->first();
                $stripe_subscription = $stripe->subscriptions()->create($stripe_customer['id'], [
                    'plan' => $stripe_plan->value,
                ]);
                \Auth::user()->account_type = 'tier_3';
                \Auth::user()->stripe_subscription_id = $stripe_subscription['id'];
                \Auth::user()->payment_date = date('Y-m-d H:i:s');
                \Auth::user()->save();
            }
               
            \Session::put('success', 'Update Payment Method completed successfully.');
            return redirect()->route('updatepaymentmethod');
        } 
        catch (\Exception $e) 
        {
            \Session::put('error', $e->getMessage());
            return redirect()->route('updatepaymentmethod');
        } 
        catch (\Cartalyst\Stripe\Exception\CardErrorException $e) 
        {
            \Session::put('error', $e->getMessage());
            return redirect()->route('updatepaymentmethod');
        } 
        catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) 
        {
            \Session::put('error', $e->getMessage());
            return redirect()->route('updatepaymentmethod');
        }
        return redirect()->route('updatepaymentmethod');
    }    

    public function getCancelSubscription()
    {
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        try 
        {
            $subscription = $stripe->subscriptions()->find(\Auth::user()->stripe_id, \Auth::user()->stripe_subscription_id);
            return view('front.pages.cancel-subscription', $subscription);
        
        } catch (\Exception $e) {
            \Session::put('error', $e->getMessage());
            return redirect()->route('stripe-payment-form');
        }
    }

    /**
     * Show the view Cancel Subscription for user.
     * */
    public function cancelSubscription() 
    {
        if(!\Auth::user()->isPremium())
        {
            \Session::put('error', 'Your account is not premium account.');
            return redirect()->route('stripe-payment-form');
        }
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        try 
        {
            $subscription = $stripe->subscriptions()->cancel(\Auth::user()->stripe_id, \Auth::user()->stripe_subscription_id, true);
            \Auth::user()->account_type = 'tier_2';
            \Auth::user()->save();
            \Session::put('success', "Your subscription canceled successfully");
            return redirect()->route('cancel-premium-subcription');
        }
        catch (\Exception $e) 
        {
            \Auth::user()->account_type = 'tier_2';
            \Auth::user()->save();
            \Session::put('error', $e->getMessage());
            return redirect()->route('cancel-premium-subcription');
        }
    }

    /**
     * Show the view reset password for user.
     * */
    public function resetPassword() {
        return view('front.pages.reset-password');
    }


    /**
     * Update user password for user.
     * */
    public function updatePassword(Request $request) {
        $validator = Validator::make($request->all(), [
                    'password' => 'confirmed|required|min:6',
                    'old_password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->route('reset-password')->with(['errors' => $validator->errors()]);
        } else {
            $user = User::findorFail(Auth::user()->id);
            $old_password = $request->old_password;
            if (\Hash::check($old_password, $user->password)) {
                $user->password = bcrypt($request->password);
                $user->save();
                \Session::put('success', 'Password updated successfully.');
            } else {
                \Session::put('error', 'Incorrect old password.');
            }

            return redirect()->route('reset-password');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editUser($id) {
        $id = Helper::decrypt($id);
        $data['data'] = User::findorfail($id);

        return view('admin.users.edit', $data);
    }

    /**
     * This function is used to update user data
     *
     * @param  Request  $request - Request Object
     * @param  string  $id - Encrypted date
     * 
     * @return Response
     */
    public function updateUser(Request $request, $id) {

        $id = Helper::decrypt($id);

        $this->validate($request, array(
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:users,email,' . $id,
            'account_type' => 'required',
        ));

        $user = User::findorFail($id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->account_type = $request->account_type;

        if ($user->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'User details updated successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('users')->with($alert);
    }

    public function editAcc() {
        $id = \Auth::user()->id;
        $data['data'] = User::findorfail($id);

        return view('front.pages.myaccount', $data);
    }

    public function updateAcc(Request $request) {

        $id = \Auth::user()->id;

        $this->validate($request, array(
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required|digits_between:1,11',
            'email' => 'required|unique:users,email,' . $id,
        ));

        $user = User::findorFail($id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->phone = $request->phone;
        $user->email = $request->email;

        if ($user->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'User details updated successfully!';
        } else {
            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('myaccount')->with($alert);
    }

    /**
     * Show the view for settings admin section.
     * */
    public function viewSettings() {
        View::share(['activeMenu' => 'settings']);
        $data['settings'] = Setting::where('name', '=', 'subscription_charge')->first();
        return view('admin.setting.index', $data);
    }

    /**
     * Update settings admin section.
     * */
    public function updateSettings(Request $request) {
        View::share(['activeMenu' => 'settings']);
        $this->validate($request, array(
            'value' => 'required',
        ));
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        try {
            $plan = $stripe->plans()->create([
                'id'                   => 'subscription_charge_'.time(),
                'name'                 => 'Premium Subscription',
                'amount'               => $request->value,
                'currency'             => env('CURRENCY_CODE', 'GBP'),
                'interval'             => env('SUBSCRIPTION_INTERVAL', 'month'),
                'statement_descriptor' => 'Premium Subscription',
                'trial_period_days'    => env('SUBSCRIPTION_TRIAL_PERIOD_DAYS', 30),
            ]);
            $stripe_plan_count = Setting::where('name', '=', 'stripe_subscription_plan')->count();
            if($stripe_plan_count > 0)
            {
                $stripe_plan = Setting::where('name', '=', 'stripe_subscription_plan')->first();
                $stripe_plan->value = $plan['id'];
                $stripe_plan->updated_at = Carbon::now();
                $stripe_plan->save();
            }
            else
            {
                Setting::create([
                    'title' => 'stripe_subscription_plan',
                    'name' => 'stripe_subscription_plan',
                    'value' => $plan['id'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        } catch (\Exception $e) {
            $alert['status'] = 'error';
            $alert['message'] = $e->getMessage();
            return redirect()->route('settings')->with($alert);
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            $alert['status'] = 'error';
            $alert['message'] = $e->getMessage();
            return redirect()->route('settings')->with($alert);
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            $alert['status'] = 'error';
            $alert['message'] = $e->getMessage();
            return redirect()->route('settings')->with($alert);
        }

        $setting = Setting::where('name', '=', 'subscription_charge')->first();
        $setting->value = $request->value;

        if ($setting->save()) {
            $alert['status'] = 'success';
            $alert['message'] = 'Subscription settings updated successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('settings')->with($alert);
    }

    /**
     * Show the view reset password for admin.
     * */
    public function resetPasswordAdmin() {
        return view('admin.setting.reset-password');
    }

    /**
     * Update user password for admin.
     * */
    public function updatePasswordAdmin(Request $request) {
        $validator = Validator::make($request->all(), [
                    'password' => 'confirmed|required|min:6',
                    'old_password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->route('change-password')->with(['errors' => $validator->errors()]);
        } else {
            $admin = Admin::findorFail(\Auth::guard('admin')->user()->id);

            $old_password = $request->old_password;

            if (\Hash::check($old_password, $admin->password)) {
                $admin->password = bcrypt($request->password);
                $admin->save();
                
                $alert['status'] = 'success';
                $alert['message'] = 'Password updated successfully.';
            } else {
                $alert['status'] = 'danger';
                $alert['message'] = 'Incorrect old password.';
            }

            return redirect()->route('change-password')->with($alert);
        }
    }
    public function cancelsubscriptionforalluser(){
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        $users = User::where('account_type','tier_3')->get();
        foreach ($users as $user) {
            try 
            {
                echo $user->id;
                $subscription = $stripe->subscriptions()->cancel($user->stripe_id, $user->stripe_subscription_id, true);
                User::where('id',$user->id)->update(['account_type'=>'tier_2']);
            }
            catch (\Exception $e) 
            {
               User::where('id',$user->id)->update(['account_type'=>'tier_2']);            }
        }
    }

}
