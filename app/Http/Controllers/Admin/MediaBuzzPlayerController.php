<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Pagination\LengthAwarePaginator;
use App\MediaBuzzPlayer;
use App\Player;
use App\Fixture;
use App\Team;
use App\Page;
use App\DayAndDividends;
use Helper;
use DB;
use Yajra\Datatables\Datatables;

class MediaBuzzPlayerController extends Controller {

    protected $perPage = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'media-buzz', 'activeSubMenu' => 'media-buzz-player-history']);
    }

    /**
     * Display a listing of the media buzz winner player history
     *
     * @return Response
     */
    public function index() {
        $data['winnerPlayerHistory'] = MediaBuzzPlayer::select('media_buzz_players.date', 'media_buzz_players.id', DB::raw('group_concat(CASE WHEN players.common_name != "null" THEN players.common_name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_names'), DB::raw('group_concat(CASE WHEN players.football_index_common_name != "null" THEN players.football_index_common_name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_football_index_common_names'), DB::raw('group_concat(media_buzz_players.position ORDER BY media_buzz_players.position ASC) as positions'), DB::raw('group_concat(CASE WHEN media_buzz_players.score != "null" THEN media_buzz_players.score ELSE "" END ORDER BY media_buzz_players.position ASC) as scores'), DB::raw('group_concat(CASE WHEN media_buzz_players.player_id != "null" THEN media_buzz_players.player_id ELSE "" END ORDER BY media_buzz_players.position ASC) as player_ids'), DB::raw('group_concat(CASE WHEN media_buzz_players.id != "null" THEN media_buzz_players.id ELSE "" END ORDER BY media_buzz_players.position ASC) as ids'))
                ->join('players', 'players.id', 'media_buzz_players.player_id')
                ->groupBy('media_buzz_players.date')
                ->paginate($this->perPage);

        return view('admin.mediabuzz-winner.player.list', $data);
    }

    /**
     * This function is used to show create form
     *
     * @return View
     */
    public function create() {

        $players = Player::get();
        return view('admin.mediabuzz-winner.player.form')->with('players', $players);
    }

    /**
     * This function is used to save medizbuzz winner player data
     *
     * @param  Request  $request - Request Object
     * 
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, $this->validationRules('create'), $this->validationMessages());

        $mediaBuzzPlayer1 = new MediaBuzzPlayer();
        $mediaBuzzPlayer1->player_id = $request->mb_first;
        $mediaBuzzPlayer1->score = $request->mb_first_score;
        $mediaBuzzPlayer1->date = $request->date;
        $mediaBuzzPlayer1->position = 1;

        $mediaBuzzPlayer2 = new MediaBuzzPlayer();
        $mediaBuzzPlayer2->player_id = $request->mb_second;
        $mediaBuzzPlayer2->score = $request->mb_second_score;
        $mediaBuzzPlayer2->date = $request->date;
        $mediaBuzzPlayer2->position = 2;

        $mediaBuzzPlayer3 = new MediaBuzzPlayer();
        $mediaBuzzPlayer3->player_id = $request->mb_third;
        $mediaBuzzPlayer3->score = $request->mb_third_score;
        $mediaBuzzPlayer3->date = $request->date;
        $mediaBuzzPlayer3->position = 3;

        $mediaBuzzPlayer4 = new MediaBuzzPlayer();
        $mediaBuzzPlayer4->player_id = $request->mb_fourth;
        $mediaBuzzPlayer4->score = $request->mb_fourth_score;
        $mediaBuzzPlayer4->date = $request->date;
        $mediaBuzzPlayer4->position = 4;

        $mediaBuzzPlayer5 = new MediaBuzzPlayer();
        $mediaBuzzPlayer5->player_id = $request->mb_fifth;
        $mediaBuzzPlayer5->score = $request->mb_fifth_score;
        $mediaBuzzPlayer5->date = $request->date;
        $mediaBuzzPlayer5->position = 5;

        if ($mediaBuzzPlayer1->save() && $mediaBuzzPlayer2->save() && $mediaBuzzPlayer3->save() && $mediaBuzzPlayer4->save() && $mediaBuzzPlayer5->save()) {
//        ADD PLAYERS WIN COUNT & UPDATE DIVIDEND
            Helper::calculatePlayerDividend($request->mb_first);

            if ($request->mb_second) {
                Helper::calculatePlayerDividend($request->mb_second);
            }

            if ($request->mb_third) {
                Helper::calculatePlayerDividend($request->mb_third);
            }

            if ($request->mb_fourth) {
                Helper::calculatePlayerDividend($request->mb_fourth);
            }

            if ($request->mb_fifth) {
                Helper::calculatePlayerDividend($request->mb_fifth);
            }

            $alert['status'] = 'success';
            $alert['message'] = 'Media buzz winner player added successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('media-buzz-winner-player.index')->with($alert);
    }

    /**
     * Display media buzz winner player detail.
     *
     * @param  int  $id - id
     * @return Response
     */
    public function show($id) {
        return abort('404');
    }

    /**
     * This function is used for edit specific record
     *
     * @param  int  $id - ID
     * 
     * @return View
     */
    public function edit($date) {
        $date_new = Helper::decrypt($date);
        $first = MediaBuzzPlayer::where(\DB::raw('DATE(date)'), $date_new)->where('position', 1)->count();
        $second = MediaBuzzPlayer::where(\DB::raw('DATE(date)'), $date_new)->where('position', 2)->count();
        $third = MediaBuzzPlayer::where(\DB::raw('DATE(date)'), $date_new)->where('position', 3)->count();
        $fourth = MediaBuzzPlayer::where(\DB::raw('DATE(date)'), $date_new)->where('position', 4)->count();
        $fifth = MediaBuzzPlayer::where(\DB::raw('DATE(date)'), $date_new)->where('position', 5)->count();

        // CREATE NULL ENTRIES IF NOT FOUND IN DB
        if ($first == 0) {
            $mediaBuzzPlayer = new MediaBuzzPlayer();
            $mediaBuzzPlayer->date = $date_new;
            $mediaBuzzPlayer->position = 1;
            $mediaBuzzPlayer->save();
        }
        if ($second == 0) {
            $mediaBuzzPlayer = new MediaBuzzPlayer();
            $mediaBuzzPlayer->date = $date_new;
            $mediaBuzzPlayer->position = 2;
            $mediaBuzzPlayer->save();
        }
        if ($third == 0) {
            $mediaBuzzPlayer = new MediaBuzzPlayer();
            $mediaBuzzPlayer->date = $date_new;
            $mediaBuzzPlayer->position = 3;
            $mediaBuzzPlayer->save();
        }
        if ($fourth == 0) {
            //echo "12";exit();
            $mediaBuzzPlayer = new MediaBuzzPlayer();
            $mediaBuzzPlayer->date = $date_new;
            $mediaBuzzPlayer->position = '4';
         //   echo "<pre>";print_r($mediaBuzzPlayer);exit();
            $mediaBuzzPlayer->save();

        }
        if ($fifth == 0) {
            $mediaBuzzPlayer = new MediaBuzzPlayer();
            $mediaBuzzPlayer->date = $date_new;
            $mediaBuzzPlayer->position = 5;
            $mediaBuzzPlayer->save();
        }
        $data['playerHistory'] = MediaBuzzPlayer::select('media_buzz_players.date', 'media_buzz_players.id', DB::raw('group_concat(CASE WHEN players.football_index_common_name != "null" THEN players.football_index_common_name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_names'), DB::raw('group_concat(media_buzz_players.position ORDER BY media_buzz_players.position ASC) as positions'), DB::raw('group_concat(CASE WHEN media_buzz_players.score != "null" THEN media_buzz_players.score ELSE 0 END ORDER BY media_buzz_players.position ASC) as scores'), DB::raw('group_concat(CASE WHEN media_buzz_players.player_id != "null" THEN media_buzz_players.player_id ELSE 0 END ORDER BY media_buzz_players.position ASC) as player_ids'), DB::raw('group_concat(CASE WHEN media_buzz_players.id != "null" THEN media_buzz_players.id ELSE 0 END ORDER BY media_buzz_players.position ASC) as ids'))
                ->leftJoin('players', 'players.id', 'media_buzz_players.player_id')
                ->where(\DB::raw('DATE(date)'), Helper::decrypt($date))
                ->groupBy('media_buzz_players.date')
                ->first();
        $data['players'] = Player::get();

        return view('admin.mediabuzz-winner.player.form', $data);
    }

    /**
     * Update the media buzz player history.
     *
     * @param  Request  $request - Request Object
     * @param  int  $id - ID
     * 
     * @return Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, $this->validationRules(), $this->validationMessages());
        $ids = explode(',', $request->ids);
        $mediaBuzzPlayer1 = $this->getMediaBuzzPlayerHistory(Helper::encrypt($ids[0]));
        $mediaBuzzPlayer2 = $this->getMediaBuzzPlayerHistory(Helper::encrypt($ids[1]));
        $mediaBuzzPlayer3 = $this->getMediaBuzzPlayerHistory(Helper::encrypt($ids[2]));
        $mediaBuzzPlayer4 = $this->getMediaBuzzPlayerHistory(Helper::encrypt($ids[3]));
        $mediaBuzzPlayer5 = $this->getMediaBuzzPlayerHistory(Helper::encrypt($ids[4]));
        $oldFirst = $oldSecond = $oldThird = $oldFourth = $oldFifth = '';

        if ($mediaBuzzPlayer1->player_id != $request->mb_first) {
            if ($mediaBuzzPlayer1->player_id != '') {
                $oldFirst = $mediaBuzzPlayer1->player_id;
            }
        }

        if ($mediaBuzzPlayer2->player_id != $request->mb_second) {
            if ($mediaBuzzPlayer2->player_id != '') {
                $oldSecond = $mediaBuzzPlayer2->player_id;
            }
        }

        if ($mediaBuzzPlayer3->player_id != $request->mb_third) {
            if ($mediaBuzzPlayer3->player_id != '') {
                $oldThird = $mediaBuzzPlayer3->player_id;
            }
        }

        if ($mediaBuzzPlayer4->player_id != $request->mb_fourth) {
            if ($mediaBuzzPlayer4->player_id != '') {
                $oldFourth = $mediaBuzzPlayer4->player_id;
            }
        }

        if ($mediaBuzzPlayer5->player_id != $request->mb_fifth) {
            if ($mediaBuzzPlayer5->player_id != '') {
                $oldFifth = $mediaBuzzPlayer5->player_id;
            }
        }

//        SAVE UPDATES HERE
        $mediaBuzzPlayer1->player_id = $request->mb_first;
        $mediaBuzzPlayer1->score = $request->mb_first_score;
        $mediaBuzzPlayer1->position = 1;

        $mediaBuzzPlayer2->player_id = $request->mb_second;
        $mediaBuzzPlayer2->score = $request->mb_second_score;
        $mediaBuzzPlayer2->position = 2;

        $mediaBuzzPlayer3->player_id = $request->mb_third;
        $mediaBuzzPlayer3->score = $request->mb_third_score;
        $mediaBuzzPlayer3->position = 3;

        $mediaBuzzPlayer4->player_id = $request->mb_fourth;
        $mediaBuzzPlayer4->score = $request->mb_fourth_score;
        $mediaBuzzPlayer4->position = 4;

        $mediaBuzzPlayer5->player_id = $request->mb_fifth;
        $mediaBuzzPlayer5->score = $request->mb_fifth_score;
        $mediaBuzzPlayer5->position = 5;

        if ($mediaBuzzPlayer1->save() && $mediaBuzzPlayer2->save() && $mediaBuzzPlayer3->save() && $mediaBuzzPlayer4->save() && $mediaBuzzPlayer5->save()) {
//        UPDATE MB COUNT OF OLD PLAYERS  
            if ($oldFirst != '') {
                Helper::calculatePlayerDividend($oldFirst);
            }
            if ($oldSecond != '') {
                Helper::calculatePlayerDividend($oldSecond);
            }
            if ($oldThird != '') {
                Helper::calculatePlayerDividend($oldThird);
            }
            if ($oldFourth != '') {
                Helper::calculatePlayerDividend($oldFourth);
            }
            if ($oldFifth != '') {
                Helper::calculatePlayerDividend($oldFifth);
            }

//        UPDATE MB COUNT OF NEW PLAYERS
            if ($request->mb_first != '') {
                Helper::calculatePlayerDividend($request->mb_first);
            }

            if ($request->mb_second != '') {
                Helper::calculatePlayerDividend($request->mb_second);
            }

            if ($request->mb_third != '') {
                Helper::calculatePlayerDividend($request->mb_third);
            }

            if ($request->mb_fourth != '') {
                Helper::calculatePlayerDividend($request->mb_fourth);
            }

            if ($request->mb_fifth != '') {
                Helper::calculatePlayerDividend($request->mb_fifth);
            }

            $alert['status'] = 'success';
            $alert['message'] = 'Media buzz winner player updated successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('media-buzz-winner-player.index')->with($alert);
    }

    /**
     * This function is used to delete media buzz player history
     *
     * @param  int  $id - ID
     * 
     * @return Response
     */
    public function destroy($date) {
        $mediaBuzzObject = MediaBuzzPlayer::where(\DB::raw('DATE(date)'), Helper::decrypt($date));
        $ids = $mediaBuzzObject->pluck('id');
        $mediaBuzzPlayerObject = MediaBuzzPlayer::whereIn('id', $ids)->pluck('player_id');
        $mediaBuzzTeamObject = MediaBuzzPlayer::whereIn('id', $ids)->pluck('team_id');

        if ($mediaBuzzObject->delete()) {
            foreach ($mediaBuzzPlayerObject as $player) {
                if ($player != '')
                    Helper::calculatePlayerDividend($player);
            }
            foreach ($mediaBuzzTeamObject as $team) {
                if ($team != '')
                    Helper::calculateTeamDividend($team);
            }

            $alert['status'] = 'success';
            $alert['message'] = 'Media buzz winner player deleted successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('media-buzz-winner-player.index')->with($alert);
    }

    /**
     * This function is used to get validation rules
     * 
     * @return Array
     */
    public function validationRules($token = NULL) {
        $rules['mb_first'] = 'required';
//        $rules['mb_first_score'] = 'required|digits_between:1,10';
        // $rules['mb_second'] = 'required';
        // $rules['mb_second_score'] = 'required|digits_between:1,10';
        // $rules['mb_third'] = 'required';
        // $rules['mb_third_score'] = 'required|digits_between:1,10';
        if ($token == "create") {
            $rules['date'] = 'required|unique:media_buzz_players';
        }
        return $rules;
    }

    /**
     * This function is used to get validation messages
     * 
     * @return Array
     */
    public function validationMessages() {
        return [
            'mb_first.required' => 'The MB first player name is required.',
            'mb_first_score.required' => 'The MB first player score is required.',
            'mb_second.required' => 'The MB second player name is required.',
            'mb_second_score.required' => 'The MB second player score is required.',
            'mb_third.required' => 'The MB third player name is required.',
            'mb_third_score.required' => 'The MB third player score is required.',
            'date.required' => 'The Date is required.',
        ];
    }

    /**
     * This function is used to get player history
     * 
     * @param  string $id - ID
     * 
     * @return Object
     */
    public function getMediaBuzzPlayerHistory($id) {

        return MediaBuzzPlayer::findOrFail(Helper::decrypt($id));
    }

    /**
     * This function is used to get media buzz player view
     * @param  string $isPlayerName - Whether to check player name filter or not [Request Parameter]
     * @param  string $player_name - Player football index common name [Request Parameter]
     * @param  boolean $isDate     - Whether to check date filter or not [Request Parameter]
     * @param  string $startDate  - Start date [Request Parameter]
     * @param  string $endDate    - End date [Request Parameter]
     * @return View - Mediabuzz Player History View
     */
    public function mediaBuzzPLayerIndex(request $request) {
        $isPlayerName = false;
        $player_name = '';
        if ($request->has('player_name') && $request->player_name != '') {
            $isPlayerName = true;
            $player_name = $request->player_name;
        }
        $isDate = false;
        $startDate = '';
        $endDate = '';
        if ($request->has('from') && $request->from != '') {
            $isDate = true;
            $startDate = $request->from;
            $endDate = $request->to;
        }

        $data['page'] = Page::find(5);
        $data['data'] = MediaBuzzPlayer::select('media_buzz_players.date', 'media_buzz_players.id', DB::raw('group_concat(CASE WHEN players.common_name != "null" THEN players.common_name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_names'), DB::raw('group_concat(media_buzz_players.position ORDER BY media_buzz_players.position ASC) as positions'), DB::raw('group_concat(CASE WHEN media_buzz_players.score != "null" THEN media_buzz_players.score ELSE "" END ORDER BY media_buzz_players.position ASC) as scores'), DB::raw('group_concat(CASE WHEN media_buzz_players.player_id != "null" THEN media_buzz_players.player_id ELSE "" END ORDER BY media_buzz_players.position ASC) as player_ids'), DB::raw('group_concat(CASE WHEN media_buzz_players.id != "null" THEN media_buzz_players.id ELSE "" END ORDER BY media_buzz_players.position ASC) as ids'), DB::raw('group_concat(CASE WHEN players.image_path != "null" THEN players.image_path ELSE "" END ORDER BY media_buzz_players.position ASC) as player_images'), DB::raw('group_concat(CASE WHEN players.football_index_common_name != "null" THEN players.football_index_common_name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_football_index_common_names'), DB::raw('group_concat(CASE WHEN players.is_local_image != "null" THEN players.is_local_image ELSE "" END ORDER BY media_buzz_players.position ASC) as is_local_images'), DB::raw('group_concat(CASE WHEN players.local_image_path != "null" THEN players.local_image_path ELSE "" END ORDER BY media_buzz_players.position ASC) as player_local_images'), DB::raw('group_concat(CASE WHEN teams.name != "null" THEN teams.name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_team_names'), DB::raw('group_concat(CASE WHEN media_buzz_players.team_id != "null" THEN media_buzz_players.team_id ELSE "" END ORDER BY media_buzz_players.position ASC) as team_ids'))
                        ->when($isPlayerName, function($query) use ($player_name) {
                            $query->havingRaw('LOCATE(\'' . $player_name . '\',player_football_index_common_names)>0');
                        })
                        ->when($isDate, function($query) use ($startDate, $endDate) {
                            $query->whereRaw(DB::raw('DATE(date) BETWEEN \'' . $startDate . '\' AND \'' . $endDate . '\''));
                        })
                        ->leftJoin('players', 'players.id', 'media_buzz_players.player_id')
                        ->leftJoin('teams', 'teams.id', 'media_buzz_players.team_id')
                        ->groupBy('media_buzz_players.date')
                        ->when((!\Auth::user()->isPremium()), function($query) {
                            $endDate = date('Y-m-d');
                            $startDate = date('Y-m-d', strtotime("-30 day", strtotime($endDate)));
                            $query->whereBetween(\DB::raw('DATE(date)'), [$startDate, $endDate]);
                        })->simplePaginate($this->perPage);
        return view('front.mediabuzz.player.index', $data);
    }

    /**
     * This function is used to get media buzz player data
     */
    public function mediaBuzzPlayerData(Request $request) {

        $data = MediaBuzzPlayer::select('media_buzz_players.*');

        $datatables = Datatables::of($data)
                        ->editColumn('created_at', function($data) {
                            return date('d-m-Y', strtotime($data->created_at));
                        })
                        ->filter(function ($query) use ($request) {
                            $start_date = @$request->start_date_filter;
                            $end_date = @$request->end_date_filter;
                            if ($start_date != '' && $end_date != '') {
                                $query->whereBetween(\DB::raw('DATE(created_at)'), [$start_date, $end_date]);
                            } elseif ($start_date != '') {
                                $query->where(\DB::raw('DATE(created_at)'), $start_date);
                            } elseif ($end_date != '') {
                                $query->where(\DB::raw('DATE(created_at)'), $end_date);
                            }
                        })->make(true);
        return $datatables;
    }

    /**
     * This function is used to fetch MB Player data from Football Index Site
     */
    public function getMbPlayerData() {
        $sql = "SELECT dd.date FROM day_and_dividends AS dd WHERE dd.date >= CURDATE()"; 
        $get_all_date = DB::select($sql);
        $available_date = array();
        foreach ($get_all_date as $get_all_date) {
           $available_date[] =  $get_all_date->date;
        }
        for($i =0 ;$i<=21;$i++){
            $day_and_dividends = strtotime("+".$i." day");
            $day_and_dividends_date = date('Y-m-d', $day_and_dividends);
            if(!in_array($day_and_dividends_date, $available_date)){
                $daydividend = new DayAndDividends;
                $daydividend->date = $day_and_dividends_date;
                $daydividend->save();
            }
        }
        $date = date('Ymd');
        $url = 'https://api-prod.footballindex.co.uk/rankedpage/footballuk.all:' . $date . '?page=1&per_page=20&sort=asc';

        $items = Helper::executeGet($url);

        if (!empty($items->items)) {
            for ($i = 0; $i < 5; $i++) {
                $playerName = $items->items[$i]->name;
                $score = $items->items[$i]->score;

                $player = Player::where('football_index_common_name', '=', $playerName)
                        ->first();
                $position = $i + 1;
                if ($player != "") {
                    $mbDate = date('Y-m-d');

                    $query = MediaBuzzPlayer::where(['date' => $mbDate, 'position' => $position]);
                    if ($query->count() != 0) {
                        $oldPlayerId = 0;
                        $oldTeamId = 0;
                        $query = $query->first();

                        if ($query->player_id != "") {
                            if ($query->player_id != $player->id) {
                                $oldPlayer = Player::where('id', '=', $query->player_id)->first();
                                $oldPlayerId = $oldPlayer->id;
//                          UPDATE PLAYERS MB COUNT - MINUS COUNT FROM OLD PLAYER
                                if ($query->position == 1) {
                                    $oldPlayer->mb_first_count = $oldPlayer->mb_first_count - 1;
                                } else if ($query->position == 2) {
                                    $oldPlayer->mb_second_count = $oldPlayer->mb_second_count - 1;
                                } else if ($query->position == 3) {
                                    $oldPlayer->mb_third_count = $oldPlayer->mb_third_count - 1;
                                }else if ($query->position == 4) {
                                    $oldPlayer->mb_fourth_count = $oldPlayer->mb_fourth_count - 1;
                                }
                                else if ($query->position == 5) {
                                    $oldPlayer->mb_fifth_count = $oldPlayer->mb_fifth_count - 1;
                                }
                                $oldPlayer->save();
//                  UPDATE PLAYERS MB COUNT - ADD COUNT                        
                                if ($position == 1) {
                                    $player->mb_first_count = $player->mb_first_count + 1;
                                } else if ($position == 2) {
                                    $player->mb_second_count = $player->mb_second_count + 1;
                                } else if ($position == 3) {
                                    $player->mb_third_count = $player->mb_third_count + 1;
                                }else if ($position == 4) {
                                    $player->mb_fourth_count = $player->mb_fourth_count + 1;
                                }else if ($position == 5) {
                                    $player->mb_fifth_count = $player->mb_fifth_count + 1;
                                }
                            }
                        }
                        //
                        if ($query->team_id != "") {
                            if ($query->team_id != $player->team_id) {
                                $oldTeam = Team::where('id', '=', $query->team_id)->first();
                                $oldTeamId = $oldTeam->id;
                                if ($query->position == 1) {
                                    $oldTeam->mb_first_count = $oldTeam->mb_first_count - 1;
                                } else if ($query->position == 2) {
                                    $oldTeam->mb_second_count = $oldTeam->mb_second_count - 1;
                                } else if ($query->position == 3) {
                                    $oldTeam->mb_third_count = $oldTeam->mb_third_count - 1;
                                }else if ($query->position == 4) {
                                    $oldTeam->mb_fourth_count = $oldTeam->mb_fourth_count - 1;
                                }else if ($query->position == 5) {
                                    $oldTeam->mb_fifth_count = $oldTeam->mb_fifth_count - 1;
                                }
                                $oldTeam->save();

                                $team = Team::where('id', '=', $player->team_id)->first();
                                if ($position == 1) {
                                    $team->mb_first_count = $team->mb_first_count + 1;
                                } else if ($position == 2) {
                                    $team->mb_second_count = $team->mb_second_count + 1;
                                } else if ($position == 3) {
                                    $team->mb_third_count = $team->mb_third_count + 1;
                                }else if ($position == 4) {
                                    $team->mb_fourth_count = $team->mb_fourth_count + 1;
                                }else if ($position == 5) {
                                    $team->mb_fifth_count = $team->mb_fifth_count + 1;
                                }
                                $team->save();
                            }
                        }
//                  UPDATE OLD PLAYERS/TEAMS MB DIVIDEND
                        if ($oldPlayerId != 0) {
                            Helper::calculatePlayerDividend($oldPlayerId);
                        }

                        if ($oldTeamId != 0) {
                            Helper::calculateTeamDividend($oldTeamId);
                        }
                        $mbData = $query->update(['player_id' => $player->id, 'score' => $score, 'team_id' => $player->team_id]);
                    } else {
                        $mediaBuzzPlayer = new MediaBuzzPlayer();
                        $mediaBuzzPlayer->player_id = $player->id;
                        $mediaBuzzPlayer->team_id = $player->team_id;
                        $mediaBuzzPlayer->score = $score;
                        $mediaBuzzPlayer->date = $mbDate;
                        $mediaBuzzPlayer->position = $position;
                        $mediaBuzzPlayer->save();

//                  UPDATE TEAMS MB COUNT - ADD COUNT                        
                        $team = Team::where('id', '=', $player->team_id)->first();
                        if ($position == 1) {
                            $team->mb_first_count = $team->mb_first_count + 1;
                        } else if ($position == 2) {
                            $team->mb_second_count = $team->mb_second_count + 1;
                        } else if ($position == 3) {
                            $team->mb_third_count = $team->mb_third_count + 1;
                        }   else if ($position == 4) {
                            $team->mb_fourth_count = $team->mb_fourth_count + 1;
                        }else if ($position == 5) {
                            $player->mb_fifth_count = $player->mb_fifth_count + 1;
                        }
                        $team->save();

//                  UPDATE PLAYERS MB COUNT - ADD COUNT                        
                        if ($position == 1) {
                            $player->mb_first_count = $player->mb_first_count + 1;
                        } else if ($position == 2) {
                            $player->mb_second_count = $player->mb_second_count + 1;
                        } else if ($position == 3) {
                            $player->mb_third_count = $player->mb_third_count + 1;
                        } else if ($position == 4) {
                            $player->mb_fourth_count = $player->mb_fourth_count + 1;
                        }else if ($position == 5) {
                            $player->mb_fifth_count = $player->mb_fifth_count + 1;
                        }
                    }
//                  UPDATE PLAYERS & TEAMS MB DIVIDEND
                    if ($player->id != 0) {
                        Helper::calculatePlayerDividend($player->id);
                    }

                    if ($player->team_id != 0) {
                        Helper::calculateTeamDividend($player->team_id);
                    }

                    $player->save();
                } else {
                    $mediaBuzzPlayer = new MediaBuzzPlayer();
                    $mediaBuzzPlayer->date = $mbDate;
                    $mediaBuzzPlayer->position = $position;
                    $mediaBuzzPlayer->save();
                }
            }
        }
    }

    /**
     * This function is used to fetch MB Player data from Football Index Site
     */
    public function getMbPlayerDataSelectedDate() {

         $start_date = '2018-07-04';
        $end_date = '2018-07-27';

        while (strtotime($start_date) <= strtotime($end_date)) {
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            $date = $start_date;

            $date1 = str_replace('-', '', $start_date);
            $sql = "SELECT dd.date FROM day_and_dividends AS dd WHERE dd.date >= '".$start_date."'"; 
            $get_all_date = DB::select($sql);
            $available_date = array();
            foreach ($get_all_date as $get_all_date) {
               $available_date[] =  $get_all_date->date;
            }
            for($i =0 ;$i<=20;$i++){
                $day_and_dividends = strtotime("+".$i." day", strtotime($start_date));
                $day_and_dividends_date = date('Y-m-d', $day_and_dividends);
                if(!in_array($day_and_dividends_date, $available_date)){
                    $daydividend = new DayAndDividends;
                    $daydividend->date = $day_and_dividends_date;
                    $daydividend->save();
                }
            }
            $url = 'https://api-prod.footballindex.co.uk/rankedpage/footballuk.all:' . $date1 . '?page=1&per_page=20&sort=asc';

            $items = Helper::executeGet($url);

            if (!empty($items->items)) {
                for ($i = 0; $i < 5; $i++) {
                    $playerName = $items->items[$i]->name;
                    $score = $items->items[$i]->score;

                    $player = Player::where('football_index_common_name', '=', $playerName)
                            ->first();
                    $position = $i + 1;
                    if ($player != "") {
                        $mbDate = $start_date;

                        $query = MediaBuzzPlayer::where(['date' => $mbDate, 'position' => $position]);
                        if ($query->count() != 0) {
                            $oldPlayerId = 0;
                            $oldTeamId = 0;
                            $query = $query->first();

                            if ($query->player_id != "") {
                                if ($query->player_id != $player->id) {
                                    $oldPlayer = Player::where('id', '=', $query->player_id)->first();
                                    $oldPlayerId = $oldPlayer->id;
//                          UPDATE PLAYERS MB COUNT - MINUS COUNT FROM OLD PLAYER
                                    if ($query->position == 1) {
                                        $oldPlayer->mb_first_count = $oldPlayer->mb_first_count - 1;
                                    } else if ($query->position == 2) {
                                        $oldPlayer->mb_second_count = $oldPlayer->mb_second_count - 1;
                                    } else if ($query->position == 3) {
                                        $oldPlayer->mb_third_count = $oldPlayer->mb_third_count - 1;
                                    } else if ($query->position == 4) {
                                        $oldPlayer->mb_fourth_count = $oldPlayer->mb_fourth_count - 1;
                                    } else if ($query->position == 5) {
                                        $oldPlayer->mb_fifth_count = $oldPlayer->mb_fifth_count - 1;
                                    }
                                    $oldPlayer->save();
//                  UPDATE PLAYERS MB COUNT - ADD COUNT                        
                                    if ($position == 1) {
                                        $player->mb_first_count = $player->mb_first_count + 1;
                                    } else if ($position == 2) {
                                        $player->mb_second_count = $player->mb_second_count + 1;
                                    } else if ($position == 3) {
                                        $player->mb_third_count = $player->mb_third_count + 1;
                                    } else if ($position == 4) {
                                        $player->mb_fourth_count = $player->mb_fourth_count + 1;
                                    }else if ($position == 5) {
                                        $player->mb_fifth_count = $player->mb_fifth_count + 1;
                                    }
                                }
                            }
                            //
                            if ($query->team_id != "") {
                                if ($query->team_id != $player->team_id) {
                                    $oldTeam = Team::where('id', '=', $query->team_id)->first();
                                    $oldTeamId = $oldTeam->id;
                                    if ($query->position == 1) {
                                        $oldTeam->mb_first_count = $oldTeam->mb_first_count - 1;
                                    } else if ($query->position == 2) {
                                        $oldTeam->mb_second_count = $oldTeam->mb_second_count - 1;
                                    } else if ($query->position == 3) {
                                        $oldTeam->mb_third_count = $oldTeam->mb_third_count - 1;
                                    }else if ($query->position == 4) {
                                        $oldTeam->mb_fourth_count = $oldTeam->mb_fourth_count - 1;
                                    }else if ($query->position == 5) {
                                        $oldTeam->mb_fifth_count = $oldTeam->mb_fifth_count - 1;
                                    }
                                    $oldTeam->save();

                                    $team = Team::where('id', '=', $player->team_id)->first();
                                    if ($position == 1) {
                                        $team->mb_first_count = $team->mb_first_count + 1;
                                    } else if ($position == 2) {
                                        $team->mb_second_count = $team->mb_second_count + 1;
                                    } else if ($position == 3) {
                                        $team->mb_third_count = $team->mb_third_count + 1;
                                    }else if ($position == 4) {
                                        $team->mb_fourth_count = $team->mb_fourth_count + 1;
                                    }else if ($position == 5) {
                                        $team->mb_fifth_count = $team->mb_fifth_count + 1;
                                    }
                                    $team->save();
                                }
                            }
//                  UPDATE OLD PLAYERS/TEAMS MB DIVIDEND
                            if ($oldPlayerId != 0) {
                                Helper::calculatePlayerDividend($oldPlayerId);
                            }

                            if ($oldTeamId != 0) {
                                Helper::calculateTeamDividend($oldTeamId);
                            }
                            $mbData = $query->update(['player_id' => $player->id, 'score' => $score, 'team_id' => $player->team_id]);
                        } else {
                            $mediaBuzzPlayer = new MediaBuzzPlayer();
                            $mediaBuzzPlayer->player_id = $player->id;
                            $mediaBuzzPlayer->team_id = $player->team_id;
                            $mediaBuzzPlayer->score = $score;
                            $mediaBuzzPlayer->date = $mbDate;
                            $mediaBuzzPlayer->position = $position;
                            $mediaBuzzPlayer->save();

//                  UPDATE TEAMS MB COUNT - ADD COUNT                        
                            $team = Team::where('id', '=', $player->team_id)->first();
                            if ($position == 1) {
                                $team->mb_first_count = $team->mb_first_count + 1;
                            } else if ($position == 2) {
                                $team->mb_second_count = $team->mb_second_count + 1;
                            } else if ($position == 3) {
                                $team->mb_third_count = $team->mb_third_count + 1;
                            }else if ($position == 4) {
                                $team->mb_fourth_count = $team->mb_fourth_count + 1;
                            }else if ($position == 5) {
                                $team->mb_fifth_count = $team->mb_fifth_count + 1;
                            }
                            $team->save();

//                  UPDATE PLAYERS MB COUNT - ADD COUNT                        
                            if ($position == 1) {
                                $player->mb_first_count = $player->mb_first_count + 1;
                            } else if ($position == 2) {
                                $player->mb_second_count = $player->mb_second_count + 1;
                            } else if ($position == 3) {
                                $player->mb_third_count = $player->mb_third_count + 1;
                            }else if ($position == 4) {
                                $player->mb_fourth_count = $player->mb_fourth_count + 1;
                            }else if ($position == 5) {
                                $player->mb_fifth_count = $player->mb_fifth_count + 1;
                            }
                        }
//                  UPDATE PLAYERS & TEAMS MB DIVIDEND
                        if ($player->id != 0) {
                            Helper::calculatePlayerDividend($player->id);
                        }

                        if ($player->team_id != 0) {
                            Helper::calculateTeamDividend($player->team_id);
                        }

                        $player->save();
                    } else {
                        $mediaBuzzPlayer = new MediaBuzzPlayer();
                        $mediaBuzzPlayer->date = $mbDate;
                        $mediaBuzzPlayer->position = $position;
                        $mediaBuzzPlayer->save();
                    }
                }
            }
        }
    }

    /**
     * This function is used to calculate dividend of players
     */
    public function calculateMbDividend() {
//        $this->updateMbCounts();
        $player_ids = Player::where(function($query) {
                            $query->where('mb_first_count', '!=', 0)
                            ->orWhere('mb_second_count', '!=', 0)
                            ->orWhere('mb_third_count', '!=', 0);
                        })
                        ->select('id', 'media_dividend')->get();

        foreach ($player_ids as $id) {
            Helper::calculatePlayerDividend($id->id);
        }
    }

    /**
     * This function is used to update win count of players
     */
    public function updateMbCounts() {
        DB::table('teams')->update(array('mb_first_count' => 0, 'mb_second_count' => 0, 'mb_third_count' => 0));
        DB::table('players')->update(array('mb_first_count' => 0, 'mb_second_count' => 0, 'mb_third_count' => 0));
        $query = MediaBuzzPlayer::get();

        foreach ($query as $q) {
            $position = $q->position;
            if ($q->team_id != null) {
                $team = Team::where('id', '=', $q->team_id)->first();
                if ($position == 1) {
                    $team->mb_first_count = $team->mb_first_count + 1;
                } else if ($position == 2) {
                    $team->mb_second_count = $team->mb_second_count + 1;
                } else if ($position == 3) {
                    $team->mb_third_count = $team->mb_third_count + 1;
                }else if ($position == 4) {
                    $team->mb_fourth_count = $team->mb_fourth_count + 1;
                }else if ($position == 5) {
                    $team->mb_fifth_count = $team->mb_fifth_count + 1;
                }
                $team->save();
            }

//                  UPDATE PLAYERS MB COUNT - ADD COUNT                        
            if ($q->player_id != null) {
                $player = Player::where('id', '=', $q->player_id)->first();
                if ($position == 1) {
                    $player->mb_first_count = $player->mb_first_count + 1;
                } else if ($position == 2) {
                    $player->mb_second_count = $player->mb_second_count + 1;
                } else if ($position == 3) {
                    $player->mb_third_count = $player->mb_third_count + 1;
                }else if ($position == 4) {
                    $player->mb_fourth_count = $player->mb_fourth_count + 1;
                }else if ($position == 5) {
                    $player->mb_fifth_count = $player->mb_fifth_count + 1;
                }
                $player->save();
            }
        }
    }

    /**
     * This function is used to calculate dividend of teams
     */
    public function calculateMbDividendTeam() {

        $team_ids = Team::where(function($query) {
                            $query->where('mb_first_count', '!=', 0)
                            ->orWhere('mb_second_count', '!=', 0)
                            ->orWhere('mb_third_count', '!=', 0);
                        })
                        ->select('id', 'media_dividend')->get();

        foreach ($team_ids as $id) {
            Helper::calculateTeamDividend($id->team_id);
        }
    }

    /**
     * This function is used to fetch MB Data
     */
    public function getMbPlayerData1() {

        $start_date = date('2016-12-31');
        $end_date = '2018-01-18';

        while (strtotime($start_date) <= strtotime($end_date)) {
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
            $date = $start_date;

            $date1 = str_replace('-', '', $start_date);

            $url = 'https://api-prod.footballindex.co.uk/rankedpage/footballuk.all:' . $date1 . '?page=1&per_page=20&sort=asc';

            $items = Helper::executeGet($url);

            if (!empty($items->items)) {

                for ($i = 0; $i < 5; $i++) {
                    $playerName = $items->items[$i]->name;
                    $score = $items->items[$i]->score;
                    $teamName = $items->items[$i]->team;
                    $player = Player::where('football_index_common_name', '=', $playerName)->first();
                    $position = $i + 1;
                    if ($player != "") {
                        $mbDate = $date;

                        $query = MediaBuzzPlayer::where(['date' => $mbDate, 'position' => $position]);
                        if ($query->count() != 0) {
                            $oldPlayerId = 0;
                            $oldTeamId = 0;
                            $newTeamId = 0;
                            $query = $query->first();
                            if ($query->player_id != "") {
                                if ($query->player_id != $player->id) {
                                    $oldPlayer = Player::where('id', '=', $query->player_id)->first();
                                    $oldPlayerId = $oldPlayer->id;
//                          UPDATE PLAYERS MB COUNT - MINUS COUNT FROM OLD PLAYER
                                    if ($query->position == 1) {
                                        $oldPlayer->mb_first_count = $oldPlayer->mb_first_count - 1;
                                    } else if ($query->position == 2) {
                                        $oldPlayer->mb_second_count = $oldPlayer->mb_second_count - 1;
                                    } else if ($query->position == 3) {
                                        $oldPlayer->mb_third_count = $oldPlayer->mb_third_count - 1;
                                    }else if ($query->position == 4) {
                                        $oldPlayer->mb_fourth_count = $oldPlayer->mb_fourth_count - 1;
                                    }else if ($query->position == 5) {
                                        $oldPlayer->mb_fifth_count = $oldPlayer->mb_fifth_count - 1;
                                    }
                                    $oldPlayer->save();

//                  UPDATE PLAYERS MB COUNT - ADD COUNT                        
                                    if ($position == 1) {
                                        $player->mb_first_count = $player->mb_first_count + 1;
                                    } else if ($position == 2) {
                                        $player->mb_second_count = $player->mb_second_count + 1;
                                    } else if ($position == 3) {
                                        $player->mb_third_count = $player->mb_third_count + 1;
                                    }else if ($position == 4) {
                                        $player->mb_fourth_count = $player->mb_fourth_count + 1;
                                    }else if ($position == 5) {
                                        $player->mb_fifth_count = $player->mb_fifth_count + 1;
                                    }
                                }
                            }
                            $array = ['player_id' => $player->id, 'score' => $score];
                            //
                            if ($query->team_id != "") {
                                $team = Team::where('name', '=', $teamName)->first();
                                $newTeamId = $team->id;
                                if ($team != "") {
                                    if ($query->team_id != $team->id) {
                                        $oldTeam = Team::where('id', '=', $query->team_id)->first();
                                        $oldTeamId = $oldTeam->id;
                                        if ($query->position == 1) {
                                            $oldTeam->mb_first_count = $oldTeam->mb_first_count - 1;
                                        } else if ($query->position == 2) {
                                            $oldTeam->mb_second_count = $oldTeam->mb_second_count - 1;
                                        } else if ($query->position == 3) {
                                            $oldTeam->mb_third_count = $oldTeam->mb_third_count - 1;
                                        }else if ($query->position == 4) {
                                            $oldTeam->mb_fourth_count = $oldTeam->mb_fourth_count - 1;
                                        }else if ($query->position == 5) {
                                            $oldTeam->mb_fourth_count = $oldTeam->mb_fourth_count - 1;
                                        }
                                        $oldTeam->save();

                                        if ($position == 1) {
                                            $team->mb_first_count = $team->mb_first_count + 1;
                                        } else if ($position == 2) {
                                            $team->mb_second_count = $team->mb_second_count + 1;
                                        } else if ($position == 3) {
                                            $team->mb_third_count = $team->mb_third_count + 1;
                                        }else if ($position == 4) {
                                            $team->mb_fourth_count = $team->mb_fourth_count + 1;
                                        }else if ($position == 5) {
                                            $team->mb_fourth_count = $team->mb_fourth_count + 1;
                                        }
                                    }
                                    $array['team_id'] = $team->id;
                                }
                            }
                            //                            
                            $mbData = $query->update($array);
//                  UPDATE OLD PLAYERS/TEAMS MB DIVIDEND
                            if ($oldPlayerId != 0) {
                                $this->updateMbDividend($oldPlayerId);
                            }

                            if ($oldTeamId != 0) {
                                $this->updateMbDividendTeam($oldTeamId);
                            }
                            if ($newTeamId != 0) {
                                $this->updateMbDividendTeam($newTeamId);
                            }
                        } else {
                            $team_id = 0;
                            $mediaBuzzPlayer = new MediaBuzzPlayer();
                            $mediaBuzzPlayer->player_id = $player->id;
                            $mediaBuzzPlayer->score = $score;
                            $mediaBuzzPlayer->date = $mbDate;
                            $mediaBuzzPlayer->position = $position;
                            $team = Team::where('name', '=', $teamName)->first();
                            if ($team != "") {
                                $team_id = $team->id;
                                $mediaBuzzPlayer->team_id = $team->id;
//                  UPDATE PLAYERS MB COUNT - ADD COUNT                        
                                if ($position == 1) {
                                    $team->mb_first_count = $team->mb_first_count + 1;
                                } else if ($position == 2) {
                                    $team->mb_second_count = $team->mb_second_count + 1;
                                } else if ($position == 3) {
                                    $team->mb_third_count = $team->mb_third_count + 1;
                                }else if ($position == 4) {
                                    $team->mb_fourth_count = $team->mb_fourth_count + 1;
                                }else if ($position == 5) {
                                    $team->mb_fifth_count = $team->mb_fifth_count + 1;
                                }
                            }
                            $mediaBuzzPlayer->save();
//                  UPDATE PLAYERS MB COUNT - ADD COUNT                        
                            if ($position == 1) {
                                $player->mb_first_count = $player->mb_first_count + 1;
                            } else if ($position == 2) {
                                $player->mb_second_count = $player->mb_second_count + 1;
                            } else if ($position == 3) {
                                $player->mb_third_count = $player->mb_third_count + 1;
                            }else if ($position == 4) {
                                $player->mb_fourth_count = $player->mb_fourth_count + 1;
                            }else if ($position == 5) {
                                $player->mb_fifth_count = $player->mb_fifth_count + 1;
                            }
                        }
                        $player->save();

//                  UPDATE PLAYERS MB DIVIDEND
                        $this->updateMbDividend($player->id);
                        if ($team_id != 0) {
                            $this->updateMbDividendTeam($team_id);
                        }
                    } else {
                        $query = MediaBuzzPlayer::where(['date' => $mbDate, 'position' => $position]);
                        if ($query->count() == 0) {
                            echo 'yes';
                            $mediaBuzzPlayer = new MediaBuzzPlayer();
                            $mediaBuzzPlayer->date = $mbDate;
                            $mediaBuzzPlayer->position = $position;
                            $mediaBuzzPlayer->save();
                        }
                    }
                }
            }
        }
    }

}

// MB --- https://api-buzz.footballindex.co.uk/rankedpage/footballuk.all:20171223?page=1&per_page=20&sort=asc
// PB --- https://api-prod.footballindex.co.uk/football.perfomancebuzz.all?page=1&per_page=20&sort=asc

