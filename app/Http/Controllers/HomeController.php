<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MediaBuzzPlayer;
use Illuminate\Support\Facades\View;
use App\MediaBuzzTeam;
use App\PerformanceBuzz;
use App\ArticleCategories;
use App\DayAndDividends;
use App\Fixture;
use App\Player;
use App\Page;
use App\Banner;
use App\Spreadsheet;
use Helper;
use App\ContactEmail as ContactEmail;
use Mail;
use Auth;
use DB;
use Validator;


class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'home']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $startDate = date("Y-m-d");
        $endDate = date("Y-m-d", strtotime('-15 days'));

        $data = $mainBenners=[];

        if (Auth::check()) {

            $performance = $this->performanceBuzzPlayers($startDate, $endDate);
            $data['PBWinners'] = Helper::dispatchObject($performance);

            $data['page'] = Page::find(5);
            $data['data'] = MediaBuzzPlayer::select('media_buzz_players.date', 'media_buzz_players.id', DB::raw('group_concat(media_buzz_players.position ORDER BY media_buzz_players.position ASC) as positions'), DB::raw('group_concat(CASE WHEN media_buzz_players.score != "null" THEN media_buzz_players.score ELSE "" END ORDER BY media_buzz_players.position ASC) as scores'), DB::raw('group_concat(CASE WHEN media_buzz_players.player_id != "null" THEN media_buzz_players.player_id ELSE "" END ORDER BY media_buzz_players.position ASC) as player_ids'), DB::raw('group_concat(CASE WHEN media_buzz_players.id != "null" THEN media_buzz_players.id ELSE "" END ORDER BY media_buzz_players.position ASC) as ids'), DB::raw('group_concat(CASE WHEN players.image_path != "null" THEN players.image_path ELSE "" END ORDER BY media_buzz_players.position ASC) as player_images'), DB::raw('group_concat(CASE WHEN players.football_index_common_name != "null" THEN players.football_index_common_name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_football_index_common_names'), DB::raw('group_concat(CASE WHEN players.common_name != "null" THEN players.common_name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_names'), DB::raw('group_concat(CASE WHEN players.is_local_image != "null" THEN players.is_local_image ELSE "" END ORDER BY media_buzz_players.position ASC) as is_local_images'), DB::raw('group_concat(CASE WHEN players.local_image_path != "null" THEN players.local_image_path ELSE "" END ORDER BY media_buzz_players.position ASC) as player_local_images'), DB::raw('group_concat(CASE WHEN teams.name != "null" THEN teams.name ELSE "" END ORDER BY media_buzz_players.position ASC) as player_team_names'), DB::raw('group_concat(CASE WHEN media_buzz_players.team_id != "null" THEN media_buzz_players.team_id ELSE "" END ORDER BY media_buzz_players.position ASC) as team_ids'))
                            ->leftJoin('players', 'players.id', 'media_buzz_players.player_id')
                            ->leftJoin('teams', 'teams.id', 'media_buzz_players.team_id')
                            ->groupBy('media_buzz_players.date')
                            ->whereBetween(\DB::raw('DATE(date)'), [ $endDate, $startDate])
                            ->take(7)->get();



            $data['upcomingMatches'] = $this->getUpComingMatchesStats();
            $sql = "SELECT * FROM day_and_dividends AS dd WHERE dd.date >= CURDATE()"; 
            $mainBenners =Banner::where('type','=','main')->get();
            $data['updatedmatches'] = DB::select($sql);
        }


        return view('welcome')->with($data);
    }

   
    public function performanceBuzzPlayers($startDate, $endDate) {
        $performance = PerformanceBuzz::leftJoin('players', 'players.id', 'performance_buzz.player_id')
                        ->leftJoin('teams', 'teams.id', 'performance_buzz.team_id')
                        ->where('performance_buzz.position', '!=', 0)
                        ->whereBetween(\DB::raw('DATE(date)'), [ $endDate, $startDate])
                        ->select('performance_buzz.date AS date', DB::raw('group_concat(IFNULL(performance_buzz.id,"")) as ids'), DB::raw('group_concat(IFNULL(performance_buzz.score,"")) as scores'), DB::raw('group_concat(IFNULL(players.id,"")) as player_ids'), DB::raw('group_concat(IFNULL(players.team_id,"")) as team_ids'), DB::raw('group_concat(IFNULL(players.common_name,"")) as player_names'), DB::raw('group_concat(IFNULL(performance_buzz.sector,""))as player_positions'), DB::raw('group_concat(IFNULL(players.football_index_common_name,"")) as player_football_index_common_names'), DB::raw('group_concat(IFNULL(players.image_path,"")) as player_images'), DB::raw('group_concat(IFNULL(players.is_local_image,"")) as is_local_images'), DB::raw('group_concat(IFNULL(players.local_image_path,"")) as player_local_images'), DB::raw('group_concat(IFNULL(teams.name,"")) as player_team_names'))
                        ->orderBy('performance_buzz.date', 'DESC')
                        ->groupBy('performance_buzz.date')
                        ->take(7)->get();
        return $performance;
    }

    /**
     * This function is used to send 
     * @return [type] [description]
     */
    public function getUpComingMatchesStats() {
        $date = date('Y-m-d'); //today date
        $weekOfdays = array();
        if (\Auth::user()->isPremium()) {
          $fixcount = 20;
        }
        else
        {
          $fixcount = 6;
        }
        for ($i = 0; $i <= $fixcount; $i++) {
            $weekOfdays[] = date('Y-m-d', strtotime("+$i day", strtotime($date)));
        }
        $upcomingMatches = [];
        foreach ($weekOfdays as $day) {
            $day_and_dividends=DayAndDividends::where('date','=',$day)->first();
            $fixtures = Fixture::select(\DB::raw("COUNT(fixtures.id) as totalmatch, GROUP_CONCAT(fixtures.localteam_id SEPARATOR ',') as localteams, GROUP_CONCAT(fixtures.visitorteam_id SEPARATOR ',') as visitorteams, fixtures.starting_at as starting_at, seasons.start_date as start_date, seasons.end_date as end_date"))
//                ->whereRaw("DATE(starting_at) BETWEEN adddate(now(),-10) AND DATE_ADD(NOW(), INTERVAL 11 DAY)")
                    ->join('seasons','seasons.id','=','fixtures.season_id')
                    ->where(\DB::raw('DATE(fixtures.starting_at)'),$day)
                    ->where(\DB::raw('DATE(seasons.start_date)'),'<=',$day)
                    ->where(\DB::raw('DATE(seasons.end_date)'),'>=',$day)
                    ->groupBy(\DB::raw('DATE(fixtures.starting_at)'))
                    ->get();

            if (!$fixtures->isEmpty()) {
                foreach ($fixtures as $fixture) {

                    $teamsIds = array_merge(@explode(',', @$fixture->localteams), @explode(',', @$fixture->visitorteams));

                    $teamsIds = @array_unique($teamsIds);

                    $midfielder = Player::whereIn('team_id', @$teamsIds)
                            ->where('football_index_position', 'Midfielder')
                            ->where('players.football_index_common_name', '!=', NULL)
                            ->selectRaw("COUNT(id) as total")
                            ->first();

                    $defender = Player::whereIn('team_id', @$teamsIds)
                            ->where('football_index_position', 'Defender')
                            ->where('players.football_index_common_name', '!=', NULL)
                            ->selectRaw("COUNT(id) as total")
                            ->first();

                    $goalkeeper = Player::whereIn('team_id', @$teamsIds)
                            ->where('football_index_position', 'Goalkeeper')
                            ->where('players.football_index_common_name', '!=', NULL)
                            ->selectRaw("COUNT(id) as total")
                            ->first();

                    $attacker = Player::whereIn('team_id', @$teamsIds)
                            ->where('football_index_position', 'Forward')
                            ->where('players.football_index_common_name', '!=', NULL)
                            ->selectRaw("COUNT(id) as total")
                            ->first();

                    $array = [];
                    $array['totalmatch'] = $fixture->totalmatch;
                    if(count($day_and_dividends)>0 && $day_and_dividends->type_of_day != ""){
                        $array['title'] = $this->typeofDay($day_and_dividends->type_of_day);
                    }else{
                        $array['title'] = $this->getUpcomingMatchTitle($fixture->totalmatch);
                    }
                    $array['color_codes'] = $this->getUpcomingMatchColorCodes($fixture->totalmatch);
                    $array['date'] = date('D jS M', strtotime($fixture->starting_at));
                    $array['startingAt'] = date('Y-m-d', strtotime($fixture->starting_at));
                    $array['midfielder'] = $midfielder->total;
                    $array['defender'] = $defender->total;
                    $array['attacker'] = $attacker->total;
                    $array['goalkeeper'] = $goalkeeper->total;

                    array_push($upcomingMatches, $array);
                    unset($array);
                }
            } else {
                $array['totalmatch'] = 0;
                if(count($day_and_dividends)>0 && $day_and_dividends->type_of_day != ""){
                    $array['title'] = $this->typeofDay($day_and_dividends->type_of_day);
                }else{
                    $array['title'] = $this->getUpcomingMatchTitle(0);
                }
                $array['color_codes'] = $this->getUpcomingMatchColorCodes(0);
                $array['date'] = date('D jS M', strtotime($day));
                $array['startingAt'] = date('Y-m-d', strtotime($day));
                $array['midfielder'] = "3p";
                $array['defender'] = "5p";
                $array['attacker'] = "1p";
                array_push($upcomingMatches, $array);
            }
        }

        return $upcomingMatches;
    }

    /**
     * This function is used to get upcoming match title
     * 
     * @param  Integer $count - Total number of match
     * 
     * @return String
     */
    public function getUpcomingMatchTitle($count) {

        if ($count >= 1 && $count <= 4) {

            return "Bronze Match Day";
        } else if ($count >= 5 && $count <= 14) {

            return "Silver Match Day";
        } else if ($count >= 15) {

            return "Gold Match Day";
        } else {

            return "Media Day";
        }
    }

    /**
     * This function is used to get color codes for slider
     * 
     * @param  Integer $count - Total number of match
     * 
     * @return String
     */
    public function getUpcomingMatchColorCodes($count) {

        if ($count >= 1 && $count <= 4) {

            return "#CF5A0A";
        } else if ($count >= 5 && $count <= 14) {

            return "#8C909C";
        } else if ($count >= 15) {

            return "#DDAB27";
        } else {

            return "#439F05";
        }
    }

    /*
     * Funciton to call contact us page
     */

    public function getContact() {
        return view('contact-us');
    }

    /*
     * Funciton to submit contact us request
     */

    public function postContact(Request $request) {
        $data = $request->all();
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'g-recaptcha-response'=>'required',
        ];
       
        $validator = Validator::make($data, $rules, [
            'name.*' => 'This field is required.',
            'Email.required' => 'This field is required.',
            'Email.email' => 'Email formate required', 
            'g-recaptcha-response.*' => 'Please ensure that you are a human!'
        ]);
        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput();
        }
        ContactEmail::create($request->only([
                    'name',
                    'email',
                    'message',
        ]));
        $contact = ContactEmail::firstOrNew(['email' => $request->email]);
        $contact_data['name'] = $request->name;
        $contact_data['email'] = $request->email;
        $contact_data['message'] = $request->message;

        // SEND EMAIL TO USER
        Mail::send('emails.contact', array('contact' => $contact_data), function($message)use ($request) {
            $message->to($request->email)->subject('Contact User Mail');
            $message->from(config('mail.from.address'), config('mail.from.name'));
        });

        // SEND EMAIL TO ADMIN
        Mail::send('emails.contact-admin', array('contact' => $contact_data), function($message)use ($request) {
            $message->to('info@footyindexscout.co.uk')->subject('Contact User Mail');
            $message->from(config('mail.from.address'), config('mail.from.name'));
        });
        return redirect()->back()->with(['status' => 'success', 'message' => 'Thanks for contacting us!']);
    }

    public function getUpmatchbydate($date_get) {
       if ($date_get == null) 
        {
           $date = date('Y-m-d'); //today date
        }
        else
        {
            $date = $date_get;
        }
       
        $weekOfdays = array();
        if (\Auth::user()->isPremium()) {
          $fixcount = 20;
        }
        else
        {
          $fixcount = 6;
        }
        
        $upcomingMatches = [];
        if ($date != "") 
         {
            $fixtures = Fixture::select(\DB::raw("COUNT(id) as totalmatch, GROUP_CONCAT(localteam_id SEPARATOR ',') as localteams, GROUP_CONCAT(visitorteam_id SEPARATOR ',') as visitorteams, starting_at"))
//                ->whereRaw("DATE(starting_at) BETWEEN adddate(now(),-10) AND DATE_ADD(NOW(), INTERVAL 11 DAY)")
                    ->where(\DB::raw('DATE(starting_at)'), $date)
                    ->groupBy(\DB::raw('DATE(starting_at)'))
                    ->get();
            $day_and_dividends=DayAndDividends::where('date','=',$date)->first();
            if (!$fixtures->isEmpty()) {
                foreach ($fixtures as $fixture) {

                    $teamsIds = array_merge(@explode(',', @$fixture->localteams), @explode(',', @$fixture->visitorteams));

                    $teamsIds = @array_unique($teamsIds);

                    $midfielder = Player::whereIn('team_id', @$teamsIds)
                            ->where('football_index_position', 'Midfielder')
                            ->where('players.football_index_common_name', '!=', NULL)
                            ->selectRaw("COUNT(id) as total")
                            ->first();

                    $defender = Player::whereIn('team_id', @$teamsIds)
                            ->where('football_index_position', 'Defender')
                            ->where('players.football_index_common_name', '!=', NULL)
                            ->selectRaw("COUNT(id) as total")
                            ->first();

                    $goalkeeper = Player::whereIn('team_id', @$teamsIds)
                            ->where('football_index_position', 'Goalkeeper')
                            ->where('players.football_index_common_name', '!=', NULL)
                            ->selectRaw("COUNT(id) as total")
                            ->first();

                    $attacker = Player::whereIn('team_id', @$teamsIds)
                            ->where('football_index_position', 'Forward')
                            ->where('players.football_index_common_name', '!=', NULL)
                            ->selectRaw("COUNT(id) as total")
                            ->first();

                    $array = [];
                    $array['totalmatch'] = $fixture->totalmatch;
                    if(count($day_and_dividends)>0 && $day_and_dividends->type_of_day != ""){
                        $array['title'] = $this->typeofDay($day_and_dividends->type_of_day);
                    }else{
                        $array['title'] = $this->getUpcomingMatchTitle($fixture->totalmatch);
                    }
                    $array['color_codes'] = $this->getUpcomingMatchColorCodes($fixture->totalmatch);
                    $array['date'] = date('D jS M', strtotime($fixture->starting_at));
                    $array['startingAt'] = date('Y-m-d', strtotime($fixture->starting_at));
                    
                    if(count($day_and_dividends)>0 && $day_and_dividends->type_of_day != ""){
                        if($day_and_dividends->type_of_day == 4 || $day_and_dividends->type_of_day == 5){
                            if(($day_and_dividends->media_buzz_second) != ""){
                                $array['midfielder'] = (($day_and_dividends->media_buzz_second)*100).'p';
                            }else{
                                $array['midfielder'] = "3p";
                            }

                            //if($match['defender']  == '5p' && $umatch['media_buzz_first'] != ""){
                            if(($day_and_dividends->media_buzz_first) != ""){
                                $array['defender'] = (($day_and_dividends->media_buzz_first)*100).'p';
                            }else{
                                $array['defender'] = "5p";
                            }
                            if(($day_and_dividends->goalkeeper) != ""){
                                $array['goalkeeper'] = (($day_and_dividends->goalkeeper)*100).'p';
                            }else{
                                $array['goalkeeper'] = "5p";
                            }

                            //if($match['attacker']  == '1p' && ($day_and_dividends->media_buzz_third) != ""){
                            if(($day_and_dividends->media_buzz_third) != ""){
                                $array['attacker'] = (($day_and_dividends->media_buzz_third)*100).'p';
                            }else{
                                $array['attacker'] = "1p";       
                            }
                        }else{
                            $array['midfielder'] = $midfielder->total;
                            $array['defender'] = $defender->total;
                            $array['attacker'] = $attacker->total;
                            $array['goalkeeper'] = $goalkeeper->total;
                        }
                    }else{
                        $array['midfielder'] = $midfielder->total;
                        $array['defender'] = $defender->total;
                        $array['attacker'] = $attacker->total;
                        $array['goalkeeper'] = $goalkeeper->total;
                    }
                        array_push($upcomingMatches, $array);
                        unset($array);
                }
            } else {
                $array['totalmatch'] = 0;
                if(count($day_and_dividends)>0 && $day_and_dividends->type_of_day != ""){
                    $array['title'] = $this->typeofDay($day_and_dividends->type_of_day);
                }else{
                    $array['title'] = $this->getUpcomingMatchTitle(0);
                }
                $array['color_codes'] = $this->getUpcomingMatchColorCodes(0);
                $array['date'] = date('D jS M', strtotime($date));
                $array['startingAt'] = date('Y-m-d', strtotime($date));
                if(count($day_and_dividends)>0 && $day_and_dividends->type_of_day != ""){
                    if($day_and_dividends->type_of_day == 4 || $day_and_dividends->type_of_day == 5){
                        if(($day_and_dividends->media_buzz_second) != ""){
                            $array['midfielder'] = (($day_and_dividends->media_buzz_second)*100).'p';
                        }else{
                            $array['midfielder'] = "3p";
                        }

                        //if($match['defender']  == '5p' && $umatch['media_buzz_first'] != ""){
                        if(($day_and_dividends->media_buzz_first) != ""){
                            $array['defender'] = (($day_and_dividends->media_buzz_first)*100).'p';
                        }else{
                            $array['defender'] = "5p";
                        }

                        if(($day_and_dividends->goalkeeper) != ""){
                            $array['goalkeeper'] = (($day_and_dividends->goalkeeper)*100).'p';
                        }else{
                            $array['goalkeeper'] = "5p";
                        }

                        //if($match['attacker']  == '1p' && ($day_and_dividends->media_buzz_third) != ""){
                        if(($day_and_dividends->media_buzz_third) != ""){
                            $array['attacker'] = (($day_and_dividends->media_buzz_third)*100).'p';
                        }else{
                            $array['attacker'] = "1p";       
                        }
                    }else{
                        $array['midfielder'] = "3p";
                        $array['defender'] = "5p";
                        $array['goalkeeper'] = "5p";
                        $array['attacker'] = "1p";
                    }
                }else{
                    $array['midfielder'] = "3p";
                    $array['defender'] = "5p";
                    $array['goalkeeper'] = "5p";
                    $array['attacker'] = "1p";
                }
                array_push($upcomingMatches, $array);
            }
        }
        return $upcomingMatches;
    }

    public function downloadSpreadsheet($file_path) {

        $spreadsheet = Spreadsheet::where('file_path', '=', $file_path)->first();
        $filepath = public_path('/uploads/spreadsheets/');
        return response()->download($filepath.'/'.$spreadsheet->file_path, $spreadsheet->file_path);
    }

    public static function typeofDay($no){
        $name="";
        if($no == 1){
            $name="Bronze Match Day";
        }elseif($no == 2){
            $name="Silver Match Day";
        }elseif($no == 3){
            $name="Gold Match Day";
        }elseif($no == 4){
            $name="Media Day";
        }elseif($no == 5){
            $name="Media Madness Top 5";
        }
        return $name;
    }
}