<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;
use App\Player;
use App\Team;
use App\Season;
use App\League;
use App\Fixture;
use App\PerformanceBuzz;
use App\MediaBuzzPlayer;
use Yajra\Datatables\Datatables;
use DB;

class DatabaseController extends Controller {

    protected $perPage = 5;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        if (\Route::current()->getName() == 'portfolio.assistant') {
            $activeMenu = 'portfolio-assistant';
        } else if (\Route::current()->getName() == 'database' || \Route::current()->getName() == 'database.combined.data' || \Route::current()->getName() == 'database.media.data' || \Route::current()->getName() == 'database.performance.data' || \Route::current()->getName() == 'database.team.data') {
            $activeMenu = 'database';
        } else if (\Route::current()->getName() == 'performance.buzz.statistics') {
            $activeMenu = 'database';
        } else if (\Route::current()->getName() ==  'database.upgrade') {
            $activeMenu = 'database';
        }
        View::share(['activeMenu' => $activeMenu]);
    }

    /**
     * Show the database view.     
     */
    public function index() {
        if (\Auth::user()->isPremium()) {
            // TOTAL MB WINNERS COUNT POSITION WISE - PLAYERS
            $firstPositionMbPlayers = count(MediaBuzzPlayer::where('position', '=', 1)->groupBy('player_id')->get());
            $secondPositionMbPlayers = count(MediaBuzzPlayer::where('position', '=', 2)->groupBy('player_id')->get());
            $thirdPositionMbPlayers = count(MediaBuzzPlayer::where('position', '=', 3)->groupBy('player_id')->get());
            $data['totalMbWinners'] = $firstPositionMbPlayers + $secondPositionMbPlayers + $thirdPositionMbPlayers;

            // TOTAL PB WINNERS COUNT POSITION WISE - PLAYERS
            $firstPositionPbPlayers = count(PerformanceBuzz::where('position', '=', 1)->groupBy('player_id')->get());
            $secondPositionPbPlayers = count(PerformanceBuzz::where('position', '=', 2)->groupBy('player_id')->get());
            $thirdPositionPbPlayers = count(PerformanceBuzz::where('position', '=', 3)->groupBy('player_id')->get());
            $data['totalPbWinners'] = $firstPositionPbPlayers + $secondPositionPbPlayers + $thirdPositionPbPlayers;

            // TOTAL PB WINNERS COUNT POSITION WISE - TEAMS
            $firstPositionMbTeams = count(MediaBuzzPlayer::where('position', '=', 1)->groupBy('team_id')->get());
            $secondPositionMbTeams = count(MediaBuzzPlayer::where('position', '=', 2)->groupBy('team_id')->get());
            $thirdPositionMbTeams = count(MediaBuzzPlayer::where('position', '=', 3)->groupBy('team_id')->get());
            $data['totalTeamWinners'] = $firstPositionMbTeams + $secondPositionMbTeams + $thirdPositionMbTeams;

            $data['leagues'] = League::get();
            $data['seasons'] = Season::whereNotNull('seasons.start_date')
                            ->whereNotNull('seasons.end_date')->groupBy('seasons.name')->get();
            $data['offSeasons'] = Season::whereNotNull('seasons.off_season_start_date')
                            ->whereNotNull('seasons.off_season_end_date')->groupBy('seasons.name')->get();
            return view('front.database.index')->with($data);
        } else {
            return redirect('/database/upgrade');
        }
    }

    public function getUpgrade() {
        if (\Auth::user()->isPremium()) {
            return redirect('database');
        }
        else
        {
            return view('front.database.upgrade');
        }
    }

    /**
     * Common query to fetch data.     
     */
    public function databaseCommonQuery() {
        $query = Player::where('players.football_index_common_name', '!=', "")
                ->join('teams', 'teams.id', 'players.team_id')
                ->select('players.id', 'players.team_id', 'players.common_name as player_name', 'players.football_index_common_name as football_index_common_name', 'players.football_index_position as position', 'players.local_image_path', 'players.is_local_image', 'players.image_path', 'teams.name as team');
        return $query;
    }

    /**
     * Datatables ajax call for fetching combined players data.     
     */
    public function getCombinedData(Request $request) {
        if (!\Auth::user()->isPremium()) {
            return false;
        }
        if (@$request->start_date != '' && @$request->end_date != '') {
            $endDate = @$request->end_date;
            $startDate = @$request->start_date;
        } else {
            $startDate = '';
            $endDate = '';
        }

        $query = $this->databaseCommonQuery();
        $data = $query
                ->when($startDate == '' && $endDate == '' && @$request->orderBy == "wins", function($query) {
                    $query->addSelect('players.mb_first_count as mb_first', 'players.mb_second_count as mb_second', 'players.mb_third_count as mb_third', 'players.positional_wins as positional', 'players.top_player_wins as top_player');
                    $query->addSelect('players.mb_first_dividend as div_mb_first', 'players.mb_second_dividend as div_mb_second', 'players.mb_third_dividend as div_mb_third', 'players.positional_dividend as div_positional', 'players.top_player_dividend as div_top_player');
                })
                ->when($startDate == '' && $endDate == '' && @$request->orderBy == "dividend", function($query) {
                    $query->addSelect('players.mb_first_dividend as mb_first', 'players.mb_second_dividend as mb_second', 'players.mb_third_dividend as mb_third', 'players.positional_dividend as positional', 'players.top_player_dividend as top_player');
                    $query->addSelect('players.mb_first_dividend as div_mb_first', 'players.mb_second_dividend as div_mb_second', 'players.mb_third_dividend as div_mb_third', 'players.positional_dividend as div_positional', 'players.top_player_dividend as div_top_player');
                })
                ->when($startDate != '' && $endDate != '' && @$request->orderBy == "wins", function($query)use($startDate, $endDate) {
                    $query->addSelect(\DB::raw("(SELECT COUNT(id) from media_buzz_players where media_buzz_players.player_id = players.id AND
                        media_buzz_players.position = 1 AND date(media_buzz_players.date) >= '" . $startDate . "' AND 
                        date(media_buzz_players.date) <= '" . $endDate . "') as mb_first"))
                    ->addSelect(\DB::raw("(SELECT COUNT(id) from media_buzz_players where media_buzz_players.player_id = players.id AND
                        media_buzz_players.position = 2 AND date(media_buzz_players.date) >= '" . $startDate . "' AND 
                        date(media_buzz_players.date) <= '" . $endDate . "') as mb_second"))
                    ->addSelect(\DB::raw("(SELECT COUNT(id) from media_buzz_players where media_buzz_players.player_id = players.id AND
                         media_buzz_players.position = 3 AND date(media_buzz_players.date) >= '" . $startDate . "' AND 
                        date(media_buzz_players.date) <= '" . $endDate . "') as mb_third"))
                    ->addSelect(\DB::raw("(SELECT COUNT(id) from performance_buzz where performance_buzz.player_id = players.id AND
                         performance_buzz.position IN (1,2,3,4) AND date(performance_buzz.date) BETWEEN '" . $startDate . "' AND  '" . $endDate . "') as positional"))
                    ->addSelect(\DB::raw("(SELECT COUNT(id) FROM `performance_buzz` 
                                WHERE player_id = players.id AND position = 4 AND
                                date(performance_buzz.date) BETWEEN '" . $startDate . "' AND '" . $endDate . "')
                                AS top_player"));

                    $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_first)),0.00) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 1 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_first"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_second),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 2 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_second"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_third),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 3 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_third"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_positional_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND
                        pb.position != 0 AND date(pb.date) >= '" . $startDate . "' AND 
                        date(pb.date) <= '" . $endDate . "') as div_positional"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND date(pb.date) >= '" . $startDate . "' AND pb.position = 4 AND
                        date(pb.date) <= '" . $endDate . "') as div_top_player"));

                })
                ->when($startDate != '' && $endDate != '' && @$request->orderBy == "dividend", function($query)use($startDate, $endDate) {
                    $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_first),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 1 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as mb_first"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_second),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 2 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as mb_second"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_third),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 3 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as mb_third"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_positional_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND
                        pb.position != 0 AND date(pb.date) >= '" . $startDate . "' AND 
                        date(pb.date) <= '" . $endDate . "') as positional"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND date(pb.date) >= '" . $startDate . "' AND pb.position = 4 AND
                        date(pb.date) <= '" . $endDate . "') as top_player"));

                    $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_first),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 1 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_first"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_second),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 2 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_second"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_third),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 3 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_third"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_positional_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND
                        pb.position != 0 AND date(pb.date) >= '" . $startDate . "' AND 
                        date(pb.date) <= '" . $endDate . "') as div_positional"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND date(pb.date) >= '" . $startDate . "' AND pb.position = 4 AND
                        date(pb.date) <= '" . $endDate . "') as div_top_player"));

                })
                ->when(@$request->leagueId != '', function($query)use($request) {
                    $query->where('players.league_id', $request->leagueId);
                })
                ->addSelect('players.media_dividend', 'players.performance_dividend', 'players.last_win_date', \DB::raw("(SELECT (IF(`div_top_player` IS null, 0, `div_top_player`) + IF(`div_mb_third` IS null, 0, `div_mb_third`) + IF(`div_positional` IS null, 0, `div_positional`) + IF(`div_mb_second` IS null, 0, `div_mb_second`) + IF(`div_mb_first` IS null, 0, `div_mb_first`))) AS combined_dividend"), \DB::raw("(SELECT date from `media_buzz_players` where `media_buzz_players`.`player_id` = `players`.`id`
                                order by date DESC LIMIT 1) AS last_mb_win_date"));
        $datatables = Datatables::of($data)
                ->filter(function ($query) use ($request) {
                    $position = @$request->position;
                    $search_token = @$request->search_token;
                    $orderBy = @$request->orderBy;
                    if (@$position != "") {
                        $query->where('players.football_index_position', '=', $position);
                    }
                    if ($search_token != "") {
                        $query->where(function($query)use($search_token) {
                            $query->where('players.football_index_common_name', 'like', '%' . $search_token . '%')
                            ->orWhere('teams.name', 'like', '%' . $search_token . '%');
                        });
                    }
                })
                ->editColumn('id', function($data) {
                    $ranks = \DB::select('SELECT @rank := (@rank+1) AS rank, sc.player_id as pid, sc.score
                             FROM(SELECT id as player_id, (media_dividend+performance_dividend) AS score FROM `players` 
                             WHERE players.football_index_common_name != "" ORDER BY score DESC ) 
                             AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                    $playerRank = '';

                    foreach ($ranks as $value) {
                        if ($value->pid == $data->id) {
                            $playerRank = $value->rank;
                        }
                    }
                    return '<span class="ser-mb">' . $playerRank . '</span>';
                })
                ->editColumn('football_index_common_name', function($data) {
                    if ($data->is_local_image == 0) {
                        $src = $data->image_path;
                    } elseif ($data->is_local_image == 1 && file_exists(public_path('uploads/player-images/' . $data->local_image_path))) {
                        $src = asset('uploads/player-images/' . $data->local_image_path);
                    } else {
                        $src = asset('assets/images/no-image.jpeg');
                    }
                    return '<div class="b-player-b view-player" data-id="' . $data->id . '" data-teamid="' . $data->team_id . '">
                                <div class="img-player-v">
                                    <img src="' . $src . '" class="home-mb-team-img">
                                </div>
                                <div class="player-name-c sec-us-play">
                                    <h2>' . $data->football_index_common_name . '</h2>
                                    <p>' . $data->team . '<span class="fd-pl">' . $data->position . '</span></p>
                                </div>
                                <p></p>
                            </div>';
                })
                ->editColumn('mb_first', function($data) {
                    return '<span class="ser-mb">' . $data->mb_first . '</span>';
                })
                ->editColumn('mb_second', function($data) {
                    return '<span class="ser-mb">' . $data->mb_second . '</span>';
                })
                ->editColumn('mb_third', function($data) {
                    return '<span class="ser-mb">' . $data->mb_third . '</span>';
                })
                ->editColumn('top_player', function($data) {
                    return '<span class="ser-mb">' . $data->top_player . '</span>';
                })
                ->editColumn('positional', function($data) {
                    return '<span class="ser-mb">' . $data->positional . '</span>';
                })
                ->editColumn('last_mb_win_date', function($data) {
                    // MB LAST DAY WIN CALCULATION
                    if ($data->last_mb_win_date == 0) {
                        $mbDays = "No Win";
                    } else {
                        $today = date_create(date('Y-m-d'));
                        $last = date_create(date('Y-m-d', strtotime($data->last_mb_win_date)));
                        $diff = date_diff($today, $last);

                        if (@$diff->format("%a") == 0) {
                            $mbDays = "Today";
                        } elseif (@$diff->format("%a") == 1) {
                            $mbDays = @$diff->format("%a") . ' ' . "day";
                        } else {
                            $mbDays = @$diff->format("%a") . ' ' . "days";
                        }
                    }
                    // PB LAST DAY WIN CALCULATION
                    if ($data->last_win_date == '0000-00-00') {
                        $pbDays = "No Win";
                    } else {
                        $today = date_create(date('Y-m-d'));
                        $last = date_create(date('Y-m-d', strtotime($data->last_win_date)));
                        $diff = date_diff($today, $last);

                        if (@$diff->format("%a") == 0) {
                            $pbDays = "Today";
                        } elseif (@$diff->format("%a") == 1) {
                            $pbDays = @$diff->format("%a") . ' ' . "day";
                        } else {
                            $pbDays = @$diff->format("%a") . ' ' . "days";
                        }
                    }
                    return '<span class="pb3days">MB ' . $mbDays . '<br>PB ' . $pbDays . '</span>';
                })
                ->editColumn('combined_dividend', function($data) {
                    return '<span class="ser-mb">£' . number_format($data->combined_dividend,2) . '</span>';
                })
                ->rawColumns(['id', 'football_index_common_name', 'mb_first', 'mb_second', 'mb_third', 'top_player', 'positional', 'last_mb_win_date', 'combined_dividend'])
                ->make(true);
        return $datatables;
    }

    /**
     * Datatables ajax call for fetching meida buzz players data.     
     */
    public function getMediaBuzzData(Request $request) {
        if (!\Auth::user()->isPremium()) {
            return false;
        }
        if (@$request->start_date != '' && @$request->end_date != '') {
            $endDate = @$request->end_date;
            $startDate = @$request->start_date;
        } else {
            $startDate = '';
            $endDate = '';
        }

        $query = $this->databaseCommonQuery();

        $data = $query
                ->when($startDate == '' && $endDate == '' && @$request->orderBy == "wins", function($query) {
                    $query->addSelect('players.mb_first_count as mb_first', 'players.mb_second_count as mb_second', 'players.mb_third_count as mb_third');
                    $query->addSelect('players.mb_first_dividend as div_mb_first', 'players.mb_second_dividend as div_mb_second', 'players.mb_third_dividend as div_mb_third');
                })
                ->when($startDate == '' && $endDate == '' && @$request->orderBy == "dividend", function($query) {
                    $query->addSelect('players.mb_first_dividend as mb_first', 'players.mb_second_dividend as mb_second', 'players.mb_third_dividend as mb_third');
                    $query->addSelect('players.mb_first_dividend as div_mb_first', 'players.mb_second_dividend as div_mb_second', 'players.mb_third_dividend as div_mb_third');
                })
                ->when($startDate != '' && $endDate != '' && @$request->orderBy == "wins", function($query)use($startDate, $endDate) {
                    $query->addSelect(\DB::raw("(SELECT COUNT(id) from media_buzz_players where media_buzz_players.player_id = players.id AND
                        media_buzz_players.position = 1 AND date(media_buzz_players.date) >= '" . $startDate . "' AND 
                        date(media_buzz_players.date) <= '" . $endDate . "') as mb_first"))
                    ->addSelect(\DB::raw("(SELECT COUNT(id) from media_buzz_players where media_buzz_players.player_id = players.id AND
                        media_buzz_players.position = 2 AND date(media_buzz_players.date) >= '" . $startDate . "' AND 
                        date(media_buzz_players.date) <= '" . $endDate . "') as mb_second"))
                    ->addSelect(\DB::raw("(SELECT COUNT(id) from media_buzz_players where media_buzz_players.player_id = players.id AND
                         media_buzz_players.position = 3 AND date(media_buzz_players.date) >= '" . $startDate . "' AND 
                        date(media_buzz_players.date) <= '" . $endDate . "') as mb_third"));

                    $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_first),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 1 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_first"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_second),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 2 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_second"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_third),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 3 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_third"));
                })
                ->when($startDate != '' && $endDate != '' && @$request->orderBy == "dividend", function($query)use($startDate, $endDate) {
                    $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_first),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 1 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as mb_first"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_second),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 2 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as mb_second"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_third),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 3 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as mb_third"));

                    $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_first),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 1 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_first"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_second),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 2 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_second"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_third),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.player_id = players.id AND
                        mbp.position = 3 AND date(mbp.date) >= '" . $startDate . "' AND 
                        date(mbp.date) <= '" . $endDate . "') as div_mb_third"));
                })
                ->when(@$request->leagueId != '', function($query)use($request) {
                    $query->where('players.league_id', $request->leagueId);
                })
                ->addSelect(\DB::raw("(SELECT(IF(`div_mb_first` IS null, 0, `div_mb_first`) + IF(`div_mb_second` IS null, 0, `div_mb_second`) + IF(`div_mb_third` IS null, 0, `div_mb_third`))) AS media_dividend"), \DB::raw("(SELECT date from `media_buzz_players` where `media_buzz_players`.`player_id` = `players`.`id`
                                order by date DESC LIMIT 1) AS last_mb_win_date"));
                

        $datatables = Datatables::of($data)
                ->filter(function ($query) use ($request) {
                    $position = @$request->position;
                    $search_token = @$request->search_token;
                    $orderBy = @$request->orderBy;
                    if (@$position != "") {
                        $query->where('players.football_index_position', '=', $position);
                    }
                    if ($search_token != "") {
                        $query->where(function($query)use($search_token) {
                            $query->where('players.football_index_common_name', 'like', '%' . $search_token . '%')
                            ->orWhere('teams.name', 'like', '%' . $search_token . '%');
                        });
                    }
                })
                ->editColumn('id', function($data) {
                    $ranks = \DB::select('SELECT @rank := (@rank+1) AS rank, sc.player_id as pid, sc.score
                             FROM(SELECT id as player_id, media_dividend AS score FROM `players` 
                             WHERE players.football_index_common_name != "" ORDER BY score DESC ) 
                             AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                    $playerRank = '';
                    foreach ($ranks as $value) {
                        if ($value->pid == $data->id) {
                            $playerRank = $value->rank;
                        }
                    }
                    return '<span class="ser-mb">' . $playerRank . '</span>';
                })
                ->editColumn('football_index_common_name', function($data) {
                    if ($data->is_local_image == 0) {
                        $src = $data->image_path;
                    } elseif ($data->is_local_image == 1 && file_exists(public_path('uploads/player-images/' . $data->local_image_path))) {
                        $src = asset('uploads/player-images/' . $data->local_image_path);
                    } else {
                        $src = asset('assets/images/no-image.jpeg');
                    }
                    return '<div class="b-player-b view-player" data-id="' . $data->id . '" data-teamid="' . $data->team_id . '">
                                <div class="img-player-v">
                                    <img src="' . $src . '" class="home-mb-team-img">
                                </div>
                                <div class="player-name-c sec-us-play">
                                    <h2>' . $data->football_index_common_name . '</h2>
                                    <p>' . $data->team . '<span class="fd-pl">' . $data->position . '</span></p>
                                </div>
                                <p></p>
                            </div>';
                })
                ->editColumn('mb_first', function($data) {
                    return '<span class="ser-mb">' . $data->mb_first . '</span>';
                })
                ->editColumn('mb_second', function($data) {
                    return '<span class="ser-mb">' . $data->mb_second . '</span>';
                })
                ->editColumn('mb_third', function($data) {
                    return '<span class="ser-mb">' . $data->mb_third . '</span>';
                })
                ->editColumn('last_mb_win_date', function($data) {
                    if ($data->last_mb_win_date == 0) {
                        $days = "<br>No Win";
                    } else {
                        $today = date_create(date('Y-m-d'));
                        $last = date_create(date('Y-m-d', strtotime($data->last_mb_win_date)));
                        $diff = date_diff($today, $last);

                        if (@$diff->format("%a") == 0) {
                            $days = "Today";
                        } elseif (@$diff->format("%a") == 1) {
                            $days = @$diff->format("%a") . ' ' . "day" . '<br>' . $data->last_mb_win_date;
                        } else {
                            $days = @$diff->format("%a") . ' ' . "days" . '<br>' . $data->last_mb_win_date;
                        }
                    }

                    return '<span class="pb3days">MB ' . $days . '</span>';
                })
                ->editColumn('media_dividend', function($data) {
                    return '<span class="ser-mb">£' . number_format($data->media_dividend, 2) . '</span>';
                })
                ->rawColumns(['id', 'football_index_common_name', 'mb_first', 'mb_second', 'mb_third', 'last_mb_win_date', 'media_dividend'])
                ->make(true);
        return $datatables;
    }

    /**
     * Datatables ajax call for fetching performance buzz players data.     
     */
    public function getPerformanceBuzzData(Request $request) {
        if (!\Auth::user()->isPremium()) {
            return false;
        }
        if (@$request->start_date != '' && @$request->end_date != '') {
            $endDate = @$request->end_date;
            $startDate = @$request->start_date;
        } else {
            $startDate = '';
            $endDate = '';
        }
        $query = $this->databaseCommonQuery();

        $data = $query->when($startDate == '' && $endDate == '' && @$request->orderBy == "wins", function($query) {
                    $query->addSelect('players.positional_wins as positional', 'players.top_player_wins as top_player')
                    ->addSelect('players.performance_dividend', 'players.last_win_date', 'players.id as pid', \DB::raw('(SELECT AVG(performance_buzz.score) as total_score FROM performance_buzz WHERE performance_buzz.player_id = `players`.`id`) as average_pb_score'), \DB::raw("(SELECT COUNT(id) from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id`) AS games_played"));
                    $query->addSelect('players.positional_dividend as div_positional', 'players.top_player_dividend as div_top_player');
                })
                ->when($startDate == '' && $endDate == '' && @$request->orderBy == "dividend", function($query) {
                    $query->addSelect('players.positional_dividend as positional', 'players.top_player_dividend as top_player')
                    ->addSelect('players.performance_dividend', 'players.last_win_date', 'players.id as pid', \DB::raw('(SELECT AVG(performance_buzz.score) as total_score FROM performance_buzz WHERE performance_buzz.player_id = `players`.`id`) as average_pb_score'), \DB::raw("(SELECT COUNT(id) from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id`) AS games_played"))->addSelect('players.positional_dividend as div_positional', 'players.top_player_dividend as div_top_player');
                })
                ->when($startDate != '' && $endDate != '' && @$request->orderBy == "wins", function($query)use($startDate, $endDate) {
                    $query->addSelect(\DB::raw("(SELECT COUNT(id) from performance_buzz where performance_buzz.player_id = players.id AND
                         performance_buzz.position IN (1,2,3,4) AND date(performance_buzz.date) BETWEEN '" . $startDate . "' AND  '" . $endDate . "') as positional"))
                    ->addSelect(\DB::raw("(SELECT COUNT(id) FROM `performance_buzz` 
                                WHERE player_id = players.id AND position = 4 AND
                                date(performance_buzz.date) BETWEEN '" . $startDate . "' AND '" . $endDate . "')
                                AS top_player"))
                    ->addSelect('players.performance_dividend', 'players.last_win_date', 'players.id as pid', \DB::raw('(SELECT AVG(performance_buzz.score) as total_score FROM performance_buzz WHERE performance_buzz.player_id = `players`.`id` AND date(performance_buzz.date) BETWEEN "' . $startDate . '" AND "' . $endDate . '") as average_pb_score'), \DB::raw("(SELECT COUNT(id) from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id` AND date(performance_buzz.date) BETWEEN '" . $startDate . "' AND '" . $endDate . "') AS games_played"));
                    $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_positional_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND
                        pb.position != 0 AND date(pb.date) >= '" . $startDate . "' AND 
                        date(pb.date) <= '" . $endDate . "') as div_positional"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND date(pb.date) >= '" . $startDate . "' AND pb.position = 4 AND
                        date(pb.date) <= '" . $endDate . "') as div_top_player"));
                })
                ->when($startDate != '' && $endDate != '' && @$request->orderBy == "dividend", function($query)use($startDate, $endDate) {
                    $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_positional_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND
                        pb.position != 0 AND date(pb.date) >= '" . $startDate . "' AND 
                        date(pb.date) <= '" . $endDate . "') as div_positional"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND date(pb.date) >= '" . $startDate . "' AND pb.position = 4 AND
                        date(pb.date) <= '" . $endDate . "') as div_top_player"));
                    $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_positional_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND
                        pb.position != 0 AND date(pb.date) >= '" . $startDate . "' AND 
                        date(pb.date) <= '" . $endDate . "') as positional"))
                    ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.player_id = players.id AND date(pb.date) >= '" . $startDate . "' AND pb.position = 4 AND
                        date(pb.date) <= '" . $endDate . "') as top_player"))
                    ->addSelect('players.performance_dividend as performances_dividend',\DB::raw("(SELECT(IF(`div_top_player` IS null, 0, `div_top_player`) + IF(`div_positional` IS null, 0, `div_positional`))) AS total_dividend"),\DB::raw("(SELECT(IF(`performances_dividend` IS null, 0, `total_dividend`))) AS performance_dividend"),'players.last_win_date', 'players.id as pid', \DB::raw('(SELECT AVG(performance_buzz.score) as total_score FROM performance_buzz WHERE performance_buzz.player_id = `players`.`id` AND date(performance_buzz.date) BETWEEN "' . $startDate . '" AND "' . $endDate . '") as average_pb_score'), \DB::raw("(SELECT COUNT(id) from `performance_buzz` where `performance_buzz`.`player_id` = `players`.`id` AND date(performance_buzz.date) BETWEEN '" . $startDate . "' AND '" . $endDate . "') AS games_played"));
                    
                })
                ->when(@$request->leagueId != '', function($query)use($request) {
                    $query->where('players.league_id', $request->leagueId);
                });
                dd($data);
        $datatables = Datatables::of($data)
                ->filter(function ($query) use ($request) {
                    $position = @$request->position;
                    $search_token = @$request->search_token;
                    $orderBy = @$request->orderBy;
                    if (@$position != "") {
                        $query->where('players.football_index_position', '=', $position);
                    }
                    if ($search_token != "") {
                        $query->where(function($query)use($search_token) {
                            $query->where('players.football_index_common_name', 'like', '%' . $search_token . '%')
                            ->orWhere('teams.name', 'like', '%' . $search_token . '%');
                        });
                    }
                })
                ->editColumn('id', function($data) {
                    $ranks = \DB::select('SELECT @rank := (@rank+1) AS rank, sc.player_id as pid, sc.score FROM(
                        SELECT player_id, avg(score) AS score FROM `performance_buzz` GROUP BY player_id ORDER BY score DESC )
                        AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                    $playerRank = '';
                    foreach ($ranks as $value) {
                        if ($value->pid == $data->id) {
                            $playerRank = $value->rank;
                        }
                    }
                    return '<span class="ser-mb">' . $playerRank . '</span>';
                })
                ->editColumn('football_index_common_name', function($data) {
                    if ($data->is_local_image == 0) {
                        $src = $data->image_path;
                    } elseif ($data->is_local_image == 1 && file_exists(public_path('uploads/player-images/' . $data->local_image_path))) {
                        $src = asset('uploads/player-images/' . $data->local_image_path);
                    } else {
                        $src = asset('assets/images/no-image.jpeg');
                    }
                    return '<div class="b-player-b view-player" data-id="' . $data->id . '" data-teamid="' . $data->team_id . '">
                                <div class="img-player-v">
                                    <img src="' . $src . '" class="home-mb-team-img">
                                </div>
                                <div class="player-name-c sec-us-play">
                                    <h2>' . $data->football_index_common_name . '</h2>
                                    <p>' . $data->team . '<span class="fd-pl">' . $data->position . '</span></p>
                                </div>
                                <p></p>
                            </div>';
                })
                ->editColumn('top_player', function($data) {
                    return '<span class="ser-mb">' . $data->top_player . '</span>';
                })
                ->editColumn('positional', function($data) {
                    return '<span class="ser-mb">' . $data->positional . '</span>';
                })
                ->editColumn('average_pb_score', function($data) {
                    return '<span class="ser-mb">' . round($data->average_pb_score) . '</span>';
                })
                ->editColumn('games_played', function($data) {
                    return '<span class="ser-mb">' . $data->games_played . '</span>';
                })
                ->editColumn('last_win_date', function($data) {
                    if ($data->last_win_date == '0000-00-00') {
                        $days = "<br>No Win";
                    } else {
                        $today = date_create(date('Y-m-d'));
                        $last = date_create(date('Y-m-d', strtotime($data->last_win_date)));
                        $diff = date_diff($today, $last);

                        if (@$diff->format("%a") == 0) {
                            $days = "Today" . '<br>' . $data->last_win_date;
                        } elseif (@$diff->format("%a") == 1) {
                            $days = @$diff->format("%a") . ' ' . "day" . '<br>' . $data->last_win_date;
                        } else {
                            $days = @$diff->format("%a") . ' ' . "days" . '<br>' . $data->last_win_date;
                        }
                    }
                    return '<span class="pb3days">PB ' . $days . '</span>';
                })
                ->editColumn('performance_dividend', function($data) {
                    if ($data->performance_dividend == '') {
                        $dividend = 0;
                    } else {
                        $dividend = number_format($data->div_top_player+$data->div_positional, 2);
                    }

                    return '<span class="ser-mb">£' . number_format($data->performance_dividend,2) . '</span>';
                })
                ->rawColumns(['id', 'football_index_common_name', 'top_player', 'positional', 'last_win_date', 'performance_dividend', 'average_pb_score', 'games_played'])
                ->make(true);
        return $datatables;
    }

    /**
     * Datatables ajax call for fetching combined teams data.     
     */
    public function getCombinedTeamData(Request $request) {
        if (!\Auth::user()->isPremium()) {
            return false;
        }
        if (@$request->start_date != '' && @$request->end_date != '') {
            $endDate = @$request->end_date;
            $startDate = @$request->start_date;
        } else {
            $startDate = '';
            $endDate = '';
        }
        $data = Team::select('teams.id', 'teams.name', 'teams.logo_path', 'teams.local_image_path', 'teams.is_local_image', 'teams.last_win_date', \DB::raw("(teams.media_dividend + teams.performance_dividend) AS combined_dividend"), \DB::raw("(SELECT date from `media_buzz_players` where `media_buzz_players`.`team_id` = `teams`.`id`
                                order by date DESC LIMIT 1) AS last_mb_win_date"))
                        ->when($startDate == '' && $endDate == '' && @$request->orderBy == "wins", function($query) {
                            $query->addSelect('teams.mb_first_count as mb_first', 'teams.mb_second_count as mb_second', 'teams.mb_third_count as mb_third', 'teams.positional_wins as positional', 'teams.top_player_wins as top_player');
                            $query->addSelect('teams.mb_first_dividend as div_mb_first', 'teams.mb_second_dividend as div_mb_second', 'teams.mb_third_dividend as div_mb_third', 'teams.positional_dividend as div_positional', 'teams.top_player_dividend as div_top_player');
                        })
                        ->when($startDate == '' && $endDate == '' && @$request->orderBy == "dividend", function($query) {
                            $query->addSelect('teams.mb_first_dividend as mb_first', 'teams.mb_second_dividend as mb_second', 'teams.mb_third_dividend as mb_third', 'teams.positional_dividend as positional', 'teams.top_player_dividend as top_player');
                            $query->addSelect('teams.mb_first_dividend as div_mb_first', 'teams.mb_second_dividend as div_mb_second', 'teams.mb_third_dividend as div_mb_third', 'teams.positional_dividend as div_positional', 'teams.top_player_dividend as div_top_player');
                        })
                        ->when($startDate != '' && $endDate != '' && @$request->orderBy == "wins", function($query)use($startDate, $endDate) {
                            $query->addSelect(\DB::raw("(SELECT COUNT(id) from media_buzz_players where media_buzz_players.team_id = teams.id AND
                        media_buzz_players.position = 1 AND date(media_buzz_players.date) >= '" . $startDate . "' AND 
                        date(media_buzz_players.date) <= '" . $endDate . "') as mb_first"))
                            ->addSelect(\DB::raw("(SELECT COUNT(id) from media_buzz_players where media_buzz_players.team_id = teams.id AND
                        media_buzz_players.position = 2 AND date(media_buzz_players.date) >= '" . $startDate . "' AND 
                        date(media_buzz_players.date) <= '" . $endDate . "') as mb_second"))
                            ->addSelect(\DB::raw("(SELECT COUNT(id) from media_buzz_players where media_buzz_players.team_id = teams.id AND
                         media_buzz_players.position = 3 AND date(media_buzz_players.date) >= '" . $startDate . "' AND 
                        date(media_buzz_players.date) <= '" . $endDate . "') as mb_third"))
                            ->addSelect(\DB::raw("(SELECT COUNT(id) from performance_buzz where performance_buzz.team_id = teams.id AND
                         performance_buzz.position IN (1,2,3,4) AND date(performance_buzz.date) BETWEEN '" . $startDate . "' AND  '" . $endDate . "') as positional"))
                            ->addSelect(\DB::raw("(SELECT COUNT(id) FROM `performance_buzz` 
                                WHERE team_id = teams.id AND position = 4 AND
                                date(performance_buzz.date) BETWEEN '" . $startDate . "' AND '" . $endDate . "')
                                AS top_player"));

                            $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_first),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.team_id = teams.id AND
                                mbp.position = 1 AND date(mbp.date) >= '" . $startDate . "' AND 
                                date(mbp.date) <= '" . $endDate . "') as div_mb_first"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_second),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.team_id = teams.id AND
                                mbp.position = 2 AND date(mbp.date) >= '" . $startDate . "' AND 
                                date(mbp.date) <= '" . $endDate . "') as div_mb_second"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_third),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.team_id = teams.id AND
                                mbp.position = 3 AND date(mbp.date) >= '" . $startDate . "' AND 
                                date(mbp.date) <= '" . $endDate . "') as div_mb_third"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_positional_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.team_id = teams.id AND
                                pb.position != 0 AND date(pb.date) >= '" . $startDate . "' AND 
                                date(pb.date) <= '" . $endDate . "') as div_positional"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.team_id = teams.id AND date(pb.date) >= '" . $startDate . "' AND pb.position = 4 AND
                                date(pb.date) <= '" . $endDate . "') as div_top_player"));
                        })
                        ->when($startDate != '' && $endDate != '' && @$request->orderBy == "dividend", function($query)use($startDate, $endDate) {
                            $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_first),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.team_id = teams.id AND
                                mbp.position = 1 AND date(mbp.date) >= '" . $startDate . "' AND 
                                date(mbp.date) <= '" . $endDate . "') as mb_first"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_second),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.team_id = teams.id AND
                                mbp.position = 2 AND date(mbp.date) >= '" . $startDate . "' AND 
                                date(mbp.date) <= '" . $endDate . "') as mb_second"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_third),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.team_id = teams.id AND
                                mbp.position = 3 AND date(mbp.date) >= '" . $startDate . "' AND 
                                date(mbp.date) <= '" . $endDate . "') as mb_third"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_positional_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.team_id = teams.id AND
                                pb.position != 0 AND date(pb.date) >= '" . $startDate . "' AND 
                                date(pb.date) <= '" . $endDate . "') as positional"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.team_id = teams.id AND date(pb.date) >= '" . $startDate . "' AND pb.position = 4 AND
                                date(pb.date) <= '" . $endDate . "') as top_player"));

                            $query->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_first),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.team_id = teams.id AND
                                mbp.position = 1 AND date(mbp.date) >= '" . $startDate . "' AND 
                                date(mbp.date) <= '" . $endDate . "') as div_mb_first"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_second),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.team_id = teams.id AND
                                mbp.position = 2 AND date(mbp.date) >= '" . $startDate . "' AND 
                                date(mbp.date) <= '" . $endDate . "') as div_mb_second"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(media_buzz_third),2) from day_and_dividends as dd LEFT JOIN media_buzz_players as mbp ON mbp.date = dd.date where mbp.team_id = teams.id AND
                                mbp.position = 3 AND date(mbp.date) >= '" . $startDate . "' AND 
                                date(mbp.date) <= '" . $endDate . "') as div_mb_third"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_positional_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.team_id = teams.id AND
                                pb.position != 0 AND date(pb.date) >= '" . $startDate . "' AND 
                                date(pb.date) <= '" . $endDate . "') as div_positional"))
                            ->addSelect(\DB::raw("(SELECT FORMAT(SUM(top_performance_player),2) from day_and_dividends as dd LEFT JOIN performance_buzz as pb ON pb.date = dd.date where pb.team_id = teams.id AND date(pb.date) >= '" . $startDate . "' AND pb.position = 4 AND
                                date(pb.date) <= '" . $endDate . "') as div_top_player"));

                        })
                        ->when(@$request->leagueId != '', function($query)use($request) {
            $query->where('teams.league_id', $request->leagueId);
        });

        $datatables = Datatables::of($data)
                ->filter(function ($query) use ($request, $startDate, $endDate) {
                    $search_token = @$request->search_token;
                    $orderBy = @$request->orderBy;
                    if ($search_token != "") {
                        $query->where('teams.name', 'like', '%' . $search_token . '%');
                    }
                })
                ->editColumn('id', function($data) {
                    $ranks = \DB::select('SELECT @rank := (@rank+1) AS rank, sc.team_id as pid, sc.score
                             FROM(SELECT id as team_id, (media_dividend+performance_dividend) AS score FROM `teams` 
                             ORDER BY score DESC ) 
                             AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                    $playerRank = '';
                    foreach ($ranks as $value) {
                        if ($value->pid == $data->id) {
                            $playerRank = $value->rank;
                        }
                    }
                    return '<span class="ser-mb">' . $playerRank . '</span>';
                })
                ->editColumn('name', function($data) {
                    if ($data->is_local_image == 0) {
                        $src = $data->logo_path;
                    } elseif ($data->is_local_image == 1 && file_exists(public_path('uploads/team-images/' . $data->local_image_path))) {
                        $src = asset('uploads/team-images/' . $data->local_image_path);
                    } else {
                        $src = asset('assets/images/no-image.jpeg');
                    }
                    return '<div class="b-player-b">
                                <div class="img-player-v">
                                    <img src="' . $src . '" class="home-mb-team-img">
                                </div>
                                <div class="player-name-c sec-us-play">
                                    <h2>' . $data->name . '</h2>                                    
                                </div>
                                <p></p>
                            </div>';
                })
                ->editColumn('mb_first', function($data) {
                    return '<span class="ser-mb">' . $data->mb_first . '</span>';
                })
                ->editColumn('mb_second', function($data) {
                    return '<span class="ser-mb">' . $data->mb_second . '</span>';
                })
                ->editColumn('mb_third', function($data) {
                    return '<span class="ser-mb">' . $data->mb_third . '</span>';
                })
                ->editColumn('top_player', function($data) {
                    return '<span class="ser-mb">' . $data->top_player . '</span>';
                })
                ->editColumn('positional', function($data) {
                    return '<span class="ser-mb">' . $data->positional . '</span>';
                })
                ->editColumn('last_mb_win_date', function($data) {
                    // MB LAST DAY WIN CALCULATION
                    if ($data->last_mb_win_date == 0) {
                        $mbDays = "No Win";
                    } else {
                        $today = date_create(date('Y-m-d'));
                        $last = date_create(date('Y-m-d', strtotime($data->last_mb_win_date)));
                        $diff = date_diff($today, $last);

                        if (@$diff->format("%a") == 0) {
                            $mbDays = "Today";
                        } elseif (@$diff->format("%a") == 1) {
                            $mbDays = @$diff->format("%a") . ' ' . "day";
                        } else {
                            $mbDays = @$diff->format("%a") . ' ' . "days";
                        }
                    }
                    // PB LAST DAY WIN CALCULATION
                    if ($data->last_win_date == '') {
                        $pbDays = "No Win";
                    } else {
                        $today = date_create(date('Y-m-d'));
                        $last = date_create(date('Y-m-d', strtotime($data->last_win_date)));
                        $diff = date_diff($today, $last);

                        if (@$diff->format("%a") == 0) {
                            $pbDays = "Today";
                        } elseif (@$diff->format("%a") == 1) {
                            $pbDays = @$diff->format("%a") . ' ' . "day";
                        } else {
                            $pbDays = @$diff->format("%a") . ' ' . "days";
                        }
                    }
                    return '<span class="pb3days">MB ' . $mbDays . '<br>PB ' . $pbDays . '</span>';
                })
                ->editColumn('combined_dividend', function($data) {
                    return '<span class="ser-mb">' . number_format($data->div_mb_first+$data->div_mb_second+$data->div_mb_third+$data->div_top_player+$data->div_positional, 2) . '</span>';
                })
                ->rawColumns(['id', 'name', 'mb_first', 'mb_second', 'mb_third', 'top_player', 'positional', 'last_mb_win_date', 'combined_dividend'])
                ->make(true);
        return $datatables;
    }

}
