<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;
use App\Player;
use App\Team;
use App\Playerdata;
use App\PerformanceBuzz;
use App\Fixture;
use App\Leagues;
use App\Season;
use Helper;
use DB;

class PlayerDataController extends Controller {

    public function index() {
        $playerdata = new Playerdata();
        $url = 'https://api-prod.footballindex.co.uk/football.all24hrchanges?page=1&per_page=20&sort=asc';
        $items = Helper::executeGet($url);
    }

    /**
     * This function is used to get player data for popup of player.
     *
     * @param Array $request - Laravels' Request Parameter, player's id is used from request parameter
     * @return View - Player Information
     */
    public function viewplayer(Request $request) {

        $data['pbplayerInfo'] = PerformanceBuzz::where('performance_buzz.player_id', $request->id)->first();

        $data['playerInfo'] = $playerInfo = PerformanceBuzz::selectRaw('COUNT(performance_buzz.player_id) as total_count, AVG(performance_buzz.score) as total_score')
                ->addSelect('players.common_name', 'players.injured', 'players.firstname', 'players.football_index_common_name', 'players.lastname', 'players.id as playerid', 'players.position', 'players.football_index_position', 'players.lineups', 'players.is_local_image', 'players.image_path', 'players.local_image_path', 'players.team_id', 'players.top_player_wins', 'players.positional_wins', 'players.mb_third_count', 'players.mb_second_count', 'players.mb_first_count')
                ->addSelect(\DB::raw("(players.mb_first_dividend + players.mb_second_dividend + players.mb_third_dividend + players.positional_dividend + players.top_player_dividend) AS dividend_yield"))
                ->leftJoin('players', 'performance_buzz.player_id', '=', 'players.id')
                ->where('performance_buzz.player_id', $request->id)
                ->groupBy('performance_buzz.player_id')
                ->first();

        if (!$data['playerInfo']) {

            $data['playerInfo'] = $playerInfo = Player::findOrFail($request->id);
        }

        $data['pbdemo'] = PerformanceBuzz::select('score', 'date')
                ->where('player_id', $request->id)
                ->orderBy('date', 'desc')
                ->limit(5)
                ->get();

        $data['pbhighestscore'] = PerformanceBuzz::selectRaw('performance_buzz.score as total_score,date')
                ->where('player_id', $request->id)
                ->orderby('score', 'DESC')
                ->limit(5)
                ->get();

        $data['fixture'] = Fixture::addSelect('fixtures.league_id', 'fixtures.localteam_id', 'fixtures.visitorteam_id', 'fixtures.starting_at', 'leagues.name as league_name')
                ->leftJoin('leagues', 'fixtures.league_id', '=', 'leagues.id')
                ->where(function($query) use($playerInfo) {
                    $query->orWhere(['fixtures.localteam_id' => $playerInfo->team_id, 'fixtures.visitorteam_id' => $playerInfo->team_id]);
                })
                ->whereRaw('fixtures.starting_at > NOW()')
                ->limit(1)
                ->first();

        $localteamid = @$data['fixture']->localteam_id;
        $visitorteamid = @$data['fixture']->visitorteam_id;

        $team = '';
        if ($playerInfo->team_id == $localteamid) {
            $team = $visitorteamid;
        } else {
            $team = $localteamid;
        }
        @$data['teamObj'] = Team::find($team);

        // $mytime = \Carbon\Carbon::parse(date('Y-m-d H:i:s'));
        // $endtime = \Carbon\Carbon::parse(@$data['fixture']->starting_at);

        $date1 = date_create(date('Y-m-d'));
        $date2 = date_create(date('Y-m-d',strtotime(@$data['fixture']->starting_at)));

        if(!empty($data['fixture'])){
            $diff = date_diff($date1,$date2); 

            $data['diff_in_days'] = $diff->format("%a");
        }
        else{
            $data['diff_in_days'] =-1;
        }
        

        $data['pbrank'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id, sc.score FROM(SELECT players.football_index_position AS sector ,performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON players.id = performance_buzz.player_id where players.football_index_position="' . $playerInfo->football_index_position . '" GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

        $playerRank = '';
        foreach ($data['pbrank'] as $value) {
            if (@$value->player_id == $request->id) {

                $playerRank = $value;
            }
        }
        $data['playerdetail'] = @$playerRank->rank;
        $data['playersector'] = @$playerRank->sector;

        if (isset($data["playerdetail"])) {
            $data['playerdetail'] = @$playerRank->rank;
            $data['playersector'] = @$playerRank->sector;
        } else {
            $data['playerdetail'] = @$playerRank->rank = '0';
            $data['playersector'] = @$playerRank->sector = $playerInfo->football_index_position;
        }

        $data['pballrank'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.player_id as pid, sc.score FROM(SELECT performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON performance_buzz.player_id = players.id GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

        $playerOverallRank = '';
        foreach ($data['pballrank'] as $value) {
            if (@$value->pid == $request->id) {

                $playerOverallRank = $value;
            }
        }
        $data['overallrankdetail'] = @$playerOverallRank->rank;

        if (isset($data["overallrankdetail"])) {
            $data['overallrankdetail'] = @$playerOverallRank->rank;
        } else {
            $data['overallrankdetail'] = @$playerOverallRank->rank = '0';
        }

        $data['forward_position'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id, sc.score FROM(SELECT performance_buzz.sector ,performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON performance_buzz.player_id = players.id WHERE performance_buzz.sector=players.football_index_position AND players.football_index_position="forward" GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

        $playerRank = '';
        foreach ($data['forward_position'] as $value) {
            if (@$value->player_id == $request->id) {

                $playerRank = $value;
            }
        }
        $data['forward_position'] = @$playerRank->rank;
       

        if (isset($data['forward_position'])) {
            $data['forward_position'] = @$playerRank->rank;   
        } else {
            $data['forward_position'] = @$playerRank->rank = '0';
        }

        $data['midfielder_position'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id, sc.score FROM(SELECT performance_buzz.sector ,performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON performance_buzz.player_id = players.id WHERE performance_buzz.sector=players.football_index_position AND players.football_index_position="midfielder" GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

        $playerRank = '';
        foreach ($data['midfielder_position'] as $value) {
            if (@$value->player_id == $request->id) {

                $playerRank = $value;
            }
        }
        $data['midfielder_position'] = @$playerRank->rank;
       

        if (isset($data["midfielder_position"])) {
            $data['midfielder_position'] = @$playerRank->rank;   
        } else {
            $data['midfielder_position'] = @$playerRank->rank = '0';
        }

        $data['Defender_position'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id, sc.score FROM(SELECT performance_buzz.sector ,performance_buzz.player_id, avg(performance_buzz.score) AS score FROM `performance_buzz` LEFT JOIN `players` ON performance_buzz.player_id = players.id WHERE performance_buzz.sector=players.football_index_position AND players.football_index_position="Defender" GROUP BY performance_buzz.player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

        $playerRank = '';
        foreach ($data['Defender_position'] as $value) {
            if (@$value->player_id == $request->id) {

                $playerRank = $value;
            }
        }
        $data['Defender_position'] = @$playerRank->rank;
       

        if (isset($data["Defender_position"])) {
            $data['Defender_position'] = @$playerRank->rank;   
        } else {
            $data['Defender_position'] = @$playerRank->rank = '0';
        }


        $data['pbzsearch'] = $pbzsearch = PerformanceBuzz::where('performance_buzz.player_id', $request->id)
                ->select('performance_buzz.team_id', DB::raw('YEAR(performance_buzz.date) year, MONTH(performance_buzz.date) month, DAY(performance_buzz.date) day'))
                ->get();

        $data['getfixture'] = Fixture::addSelect('teams.id', 'fixtures.localteam_id', 'fixtures.visitorteam_id', 'fixtures.starting_at', 'teams.name as teams_name')
                ->leftJoin('teams', 'fixtures.localteam_id', '=', 'teams.id')
                ->leftJoin('teams as team', 'fixtures.visitorteam_id', '=', 'teams.id')
                ->where(function($query) use($playerInfo) {
                    $query->orWhere(['fixtures.localteam_id' => $playerInfo->team_id, 'fixtures.visitorteam_id' => $playerInfo->team_id]);
                })
                ->when(@$pbzsearch[0]->date != '', function($query) {
                    return $query->whereRaw('fixtures.starting_at', $pbzsearch[0]->date);
                })
                ->get();


        //dd($data['pbrank']);
               
        $view = \View::make('front.fixtures.player-info', $data);

        return $view;
    }

    public function upgrade() {
        if (\Auth::user()->isPremium()) {
            return redirect('/');
        }
        else
        {
            return view('front.fixtures.upgrade-player-popup');
        }
    }

    public function getfinalplayerfixturedata(Request $request)
    {
        
        $months = PerformanceBuzz::where('performance_buzz.player_id', $request->player_id)->whereBetween('performance_buzz.date', [($request->year-1).'-08-09', $request->year.'-08-09'])
                ->select(DB::raw('MONTH(performance_buzz.date) month'))
                ->groupby('month')
                ->orderBy('performance_buzz.date')
                ->get();

        $playerInfo = Player::findOrFail($request->player_id);

        $html = "";
        $html .= '<div class="player_score_tables">';
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-3">';
        $html .= '<img ';
        if($playerInfo['is_local_image'] == 0):
        $html .= 'src="'.$playerInfo['image_path'].'"';
        elseif($playerInfo['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.$playerInfo['local_image_path']))):
        $html .= 'src="'.asset('uploads/player-images/'.$playerInfo['local_image_path']).'"';
        else:
        $html .= 'src ="'.asset('assets/images/no-image.jpeg').'" ';
        endif;
        $html .= '>';
        $html .= '</div>';
        $html .= '<div class="col-sm-9 text-left">';
        $html .= '<h2>'.$playerInfo['football_index_common_name'].'</h2>';
        if($request->year != "" && $request->year != 'Select'):
        $html .= '<h3>SEASON '.($request->year-1).'/'.$request->year.'</h3>';
        endif;
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<table class="table">';
        $html .= '<thead>';
        $html .= '<tr>';
        $html .= '<th>DATE</th>';
        $html .= '<th>FIXTURE</th>';
        $html .= '<th>SCORE</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<body>';

        foreach ($months as $month) {
            $sql_get_past_fixture_date = "SELECT fd.team_id FROM fixture_details fd JOIN fixtures f ON f.id=fd.fixture_id WHERE fd.player_id=" . $request->player_id . " AND (DATE(f.starting_at) BETWEEN '".($request->year-1)."-08-10' AND '".$request->year."-08-10') AND MONTH(f.starting_at)=" . $month->month . " GROUP BY fd.team_id";
            $past_teams = DB::select($sql_get_past_fixture_date);
            foreach ($past_teams as $key => $value) {
                $request->teamid = $value->team_id;
                $sql = "SELECT fixtures.id,DATE(starting_at) as starting_at,IF(localteam_id = " . $request->teamid . ", visitorteam_id, localteam_id) as teamid, IF(localteam_id = " . $request->teamid . ", 'H', 'A') as played_at, IF(localteam_id = " . $request->teamid . ", (SELECT name FROM teams WHERE id=visitorteam_id), (SELECT name FROM teams WHERE id=localteam_id)) as teamname FROM fixtures JOIN performance_buzz ON performance_buzz.date=DATE(starting_at) AND performance_buzz.player_id=" . $request->player_id . " WHERE MONTH(fixtures.starting_at)=" . $month->month . " AND (DATE(fixtures.starting_at) BETWEEN '".($request->year-1)."-08-10' AND '".$request->year."-08-10') AND (localteam_id=" . $request->teamid . " OR visitorteam_id=" . $request->teamid . ") order by starting_at";
                $fixtures_arr = DB::select($sql);
                foreach ($fixtures_arr as $fixture_arr) {
                    $html .= '<tr>';
                    $html .= '<td>' . date('M j', strtotime($fixture_arr->starting_at)) . '</td>';
                    $html .= '<td>' . $fixture_arr->teamname . ' (' . $fixture_arr->played_at . ') </td>';
                    $score = PerformanceBuzz::select('score')->where('player_id', $request->player_id)->whereDate('date', '=', $fixture_arr->starting_at)->get();
                    if ($score[0]->score) {

                        $colorArray = $this->getColor($score[0]->score);
                        $background = $colorArray['colorCode'];
                        $fontcolor = $colorArray['fontcolor'];
                        $html .= '<td class="text-center" style="background:'.$background.';color:'.$fontcolor.';"><strong>' . $score[0]->score . '</strong></td>';
                    } else {
                        $html .= '<td class="text-center"><strong>0</strong></td>';
                    }
                    $html .= '</tr>';
                }
            }
        }
        $html .= '</body>';
        $html .= '</table>';
        $html .= '</div>';
        return $html;
    }


    /**
     * This Function is used in players Fixture search to get Months of Fixture of a particular player in selected Year.
     *
     * @param Array $request - Laravel's Request parameter. player_id and selected year parameters are used from $request parameter.
     *
     * @return json - Months of fixtures in selcted year for player
     */
    public function getfixturemonthforyear(Request $request) {
        $months = PerformanceBuzz::where('performance_buzz.player_id', $request->player_id)->whereYear('performance_buzz.date', $request->year)
                ->select(DB::raw('MONTH(performance_buzz.date) month'))
                ->groupby('month')
                ->get();
        $data = array();
        foreach ($months as $month) {
            $data[] = $month->month;
        }
        return json_encode($data);
        exit;
    }

    /**
     * This Function is used in players Fixture search to get Fixtures of player of current and past teams based on selected year and month.
     *
     * @param Array $request - Laravel's Request parameter. player_id and selected year and selected month parameters are used from $request parameter.
     *
     * @return json - Fixtures of player for selected year and month
     */
    public function getfixtureoppositeteam(Request $request) {
        $sql_get_past_fixture_date = "SELECT fd.team_id FROM fixture_details fd JOIN fixtures f ON f.id=fd.fixture_id WHERE fd.player_id=" . $request->player_id . " AND MONTH(f.starting_at)=" . $request->month . " AND YEAR(f.starting_at)=" . $request->year . " GROUP BY fd.team_id";
        $past_teams = DB::select($sql_get_past_fixture_date);
        $fixtures = array();
        foreach ($past_teams as $key => $value) {
            $request->teamid = $value->team_id;
            $sql = "SELECT fixtures.id,DATE(starting_at) as starting_at,IF(localteam_id = " . $request->teamid . ", visitorteam_id, localteam_id) as teamid, IF(localteam_id = " . $request->teamid . ", 'H', 'A') as played_at, IF(localteam_id = " . $request->teamid . ", (SELECT name FROM teams WHERE id=visitorteam_id), (SELECT name FROM teams WHERE id=localteam_id)) as teamname FROM fixtures JOIN performance_buzz ON performance_buzz.date=DATE(starting_at) AND performance_buzz.player_id=" . $request->player_id . " WHERE MONTH(fixtures.starting_at)=" . $request->month . " AND YEAR(fixtures.starting_at)=" . $request->year . " AND (localteam_id=" . $request->teamid . " OR visitorteam_id=" . $request->teamid . ")";
            $fixtures_arr = DB::select($sql);
            foreach ($fixtures_arr as $fixture_arr) {
                $fixtures[] = $fixture_arr;
            }
        }
        return json_encode($fixtures);
        exit;
    }

    /**
     * This Function is used in players Fixture search to get Performance Buzz Score of the player for selected Fixture Date.
     *
     * @param Array $request - Laravel's Request parameter. player_id and date parameters are used from $request parameter.
     *
     * @return integer - Score of the player for selected Fixture
     */
    public function getfinalplayerfixturescore(Request $request) {
        $score = PerformanceBuzz::select('score')->where('player_id', $request->player_id)->whereDate('date', '=', $request->date)->get();
        if ($score[0]->score) {

            $colorArray = $this->getColor($score[0]->score);
            $background = $colorArray['colorCode'];
            $fontcolor = $colorArray['fontcolor'];

            return response()->json(['score' => $score[0]->score, 'color' => $background, 'fontcolor' => $fontcolor]);
        } else {
            return 0;
        }
        exit;
    }

    /**
     * This function is used to get backgrond and font color based on score
     * 
     * @param  integer $score - Score
     * @return Array
     */
    public function getColor($score) {

        $colorCode = "";
        $fontcolor = "";

        if (@$score <= 30) {
            $colorCode = "#FF0000";
            $fontcolor = "#ffffff ";
        } elseif (@$score >= 31 && @$score <= 70) {
            $colorCode = "#FF5733";
            $fontcolor = "#ffffff";
        } elseif (@$score >= 71 && @$score <= 110) {
            $colorCode = "#eacd0e";
            $fontcolor = "#000";
        } elseif (@$score >= 111 && @$score <= 150) {
            $colorCode = "#FFFF00";
            $fontcolor = "#000";
        } elseif (@$score >= 151 && @$score <= 180) {
            $colorCode = "#aad77d";
            $fontcolor = "#ffffff";
        } elseif (@$score >= 180) {
            $colorCode = "#39a02b";
            $fontcolor = "#ffffff";
        }

        return ['colorCode' => $colorCode, 'fontcolor' => $fontcolor];
    }

}

?>