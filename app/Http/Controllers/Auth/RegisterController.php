<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Mail;
use App\Helpers\Helper;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm() {
        return abort('404');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        $stripe_customer = $card = $stripe_subscription = array();
        if($request->account_type == 'premium')
        {
            $stripe = Stripe::make(env('STRIPE_SECRET'));
            try {
                $stripe_customer = $stripe->customers()->create([
                    'email' => $request->email,
                ]);
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $request->card_no,
                        'exp_month' => $request->ccExpiryMonth,
                        'cvc'       => $request->cvvNumber,
                        'exp_year'  => $request->ccExpiryYear,
                    ],
                ]);

                $card = $stripe->cards()->create($stripe_customer['id'], $token['id']);
                $stripe_plan_count = Setting::where('name', '=', 'stripe_subscription_plan')->count();
                if($stripe_plan_count > 0)
                {
                    $stripe_plan = Setting::where('name', '=', 'stripe_subscription_plan')->first();
                    $stripe_subscription = $stripe->subscriptions()->create($stripe_customer['id'], [
                        'plan' => $stripe_plan->value,
                    ]);
                }                
            } catch (\Exception $e) {
                $message['status'] = 'error';
                $message['message'] = $e->getMessage();

                return response()->json($message);
            } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
                $message['status'] = 'error';
                $message['message'] = $e->getMessage();

                return response()->json($message);
            } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                $message['status'] = 'error';
                $message['message'] = $e->getMessage();

                return response()->json($message);
            }
        }
        $data = $request->all();
        if($request->account_type == 'premium' && !empty($stripe_customer))
        {
            $data['stripe_id'] = $stripe_customer['id'];
        }
        else
        {
            $data['stripe_id'] = '';
        }
        if($request->account_type == 'premium' && !empty($card))
        {
            $data['stripe_card_id'] = $card['id'];
            $data['card_brand'] = $card['brand'];
            $data['card_last_four'] = $card['last4'];
            $data['account_type'] = 'tier_3';
        }
        else
        {
            $data['stripe_card_id'] = '';
            $data['card_brand'] = '';
            $data['card_last_four'] = '';
            $data['account_type'] = 'tier_2';
        }
        if($request->account_type == 'premium' && !empty($stripe_subscription))
        {
            $data['stripe_subscription_id'] = $stripe_subscription['id'];
        }
        else
        {
            $data['stripe_subscription_id'] = '';
        }
        event(new Registered($user = $this->create($data)));
        // SEND EMAIL TO USER
        Mail::send('emails.register-email', array('user' => $user), function($message)use ($request) {
            $message->to($request->email, $request->first_name . ' ' . $request->last_name)->subject('Welcome to Footy Index Scout');
            $message->from(config('mail.from.address'), config('mail.from.name'));
        });

//        $this->guard()->login($user);$company
        $message['status'] = 'success';
        $message['message'] = "Thanks for registering for a Free/Premium account. We have sent an email to ".$request->email." to verify and complete the process.";

        return response()->json($message);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        $rule = [
                    'firstname' => 'required|string|max:50',
                    'lastname' => 'required|string|max:50',
                    'phone' => 'required|digits_between:1,11',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'confirmed|required|min:6'
                ];
        if($data['account_type'] == 'premium')
        {
            $rule['card_no'] = 'required|numeric';
            $rule['ccExpiryMonth'] = 'required|numeric|between:1,12';
            $rule['ccExpiryYear'] = 'required|numeric|digits_between:2,2';
            $rule['cvvNumber'] = 'required|numeric|digits_between:3,3';
            $rule['cardToken'] = 'required';
        }
        return Validator::make($data, $rule, [
                    'cardToken.required' => 'Card token not generated. Please try again.',
                    'phone.digits_between' => 'The phone number should be valid.',
                    'ccExpiryYear.digits_between' => 'The expiry year must be a valid year.',
                    'cvvNumber.digits_between' => 'The cvv number must be of 3 digits only.',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {
        return User::create([
                    'firstname' => $data['firstname'],
                    'lastname' => $data['lastname'],
                    'phone' => $data['phone'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'stripe_id' => $data['stripe_id'],
                    'stripe_card_id' => $data['stripe_card_id'],
                    'card_brand' => $data['card_brand'],
                    'card_last_four' => $data['card_last_four'],
                    'account_type' => $data['account_type'],
                    'stripe_subscription_id' => $data['stripe_subscription_id'],
        ]);
    }

    /**
     * Activate user      
     */
    public function activateUserAccount(Request $request, $email) {
        $user = User::where('email', '=', Helper::decrypt($email))->first();
        
        if ($user) {
            if ($user['status'] === 'inactive') {

                User::where('email', '=', Helper::decrypt($email))->update(['status' => 'active']);
                $alert['status'] = 'success';
                $alert['message'] = 'User account activated successfully.';
            } else {

                $alert['status'] = 'success';
                $alert['message'] = 'User account already activated.';
            }
            $this->guard()->login($user);
            return redirect('/home')->with($alert);
        } else {
            return abort('404');
        }
    }

}
