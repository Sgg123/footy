<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm() {
        return abort('404');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) {

        $rules = ['email' => 'required|string', 'password' => 'required|string'];
        $validator = Validator::make($request->only(['email', 'password']), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        } else {

            $user = User::where('email', '=', $request->email)->first();
            if ($user) {
                if ($user['status'] === 'inactive') {
                    $response = ['status' => 'danger', 'message' => 'Activate your account first from mail sent to you.'];
                } else {
                    if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => 'active'], $request->remember)) {

                        $response = ['status' => 'success', 'message' => 'Login successfull!'];
                    } else {

                        $response = ['status' => 'danger', 'message' => 'Invalid login credentials!'];
                    }
                }
            } else {
                $response = ['status' => 'danger', 'message' => 'User not found!'];
            }


            return response()->json($response);
        }
    }

    /**
     * This function is used to send failed login response
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    protected function sendFailedLoginResponse(Request $request) {
        if ($request->wantsJson()) {
            return response([
                'success' => false,
                'message' => $this->getFailedLoginMessage()
                    ], 422);
        };

        return redirect()->back()
                        ->withInput($request->only($this->loginUsername(), 'remember'))
                        ->withErrors([
                            $this->loginUsername() => $this->getFailedLoginMessage(),
        ]);
    }

}
