<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;
use App\Player;
use App\Team;
use App\Fixture;
use App\Page;
use App\MediaBuzzPlayer;
use App\PerformanceBuzz;
use Helper;
use DB;

class PerformanceBuzzPlayerController extends Controller {

    protected $perPage = 20;
    protected $positions = ['Forward', 'Defender', 'Midfielder'];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'performance-buzz', 'activeSubMenu' => 'performance-buzz-player']);
    }

    /**
     * This function is used to get performance buzz view
     *
     * @param Array $request - Laravel Request variable which contains all request variable of that page
     *
     * @return View - Performance Buzz Data View.
     *
     */
    public function index(Request $request) {

        $data['page'] = Page::find(6);
        $performance = Helper::performanceBuzzPlayers_new($request);
        $data['PBWinners'] = Helper::dispatchObject($performance);
        $data['links'] = $performance;
        return view('front.performance-buzz.player.index')->with($data);
    }

    /**
     * This function is used to get performance buzz players data
     *
     * @param Array $request - Laravel Request variable which contains all request variable of that page. Variables team_name and player_name are used optionally.
     *
     * @return Array - Performance Buzz Data Array.
     *
     */
    public function performanceBuzzPlayers(Request $request) {
        $isTeamName = false;
        $team_name = '';
        if ($request->has('team_name') && $request->team_name != '') {
            $isTeamName = true;
            $team_name = $request->team_name;
        }
        $isPlayerName = false;
        $player_name = '';
        if ($request->has('player_name') && $request->player_name != '') {
            $isPlayerName = true;
            $player_name = $request->player_name;
        }
        $isDate = false;
        $startDate = '';
        $endDate = '';
        if ($request->has('from') && $request->from != '') {
            $isDate = true;
            $startDate = $request->from;
            $endDate = $request->to;
        }
        $performance = PerformanceBuzz::leftJoin('players', 'players.id', 'performance_buzz.player_id')
                ->leftJoin('teams', 'teams.id', 'performance_buzz.team_id')
                ->when((!\Auth::user()->isPremium()), function($query) {
                    $endDate = date('Y-m-d');
                    $startDate = date('Y-m-d', strtotime('-30 day', strtotime($endDate)));
                    $query->whereBetween(\DB::raw('DATE(date)'), [$startDate, $endDate]);
                })
                ->whereIn('performance_buzz.score', function($q) {
                    $q->select(DB::raw('MAX(score) as max_score'))
                    ->from('performance_buzz as p2')
                    ->whereRaw("`p2`.`date` = `performance_buzz`.`date` AND 
                                    `p2`.`sector` != 'Goalkeeper' AND  `p2`.`sector` = `performance_buzz`.`sector`")
                    ->groupBy('p2.sector')
                    ->groupBy('p2.date');
                })
                ->whereIn('performance_buzz.sector', $this->positions)
//                ->where('performance_buzz.sector', '!=', 'Goalkeeper')
                ->select('performance_buzz.date AS date', DB::raw('group_concat(IFNULL(performance_buzz.id,"")) as ids'), DB::raw('group_concat(IFNULL(performance_buzz.score,"")) as scores'), DB::raw('group_concat(IFNULL(players.id,"")) as player_ids'), DB::raw('group_concat(IFNULL(players.team_id,"")) as team_ids'), DB::raw('group_concat(IFNULL(players.common_name,"")) as player_names'), DB::raw('group_concat(IFNULL(performance_buzz.sector,"")) as player_positions'), DB::raw('group_concat(IFNULL(players.football_index_common_name,"")) as player_football_index_common_names'), DB::raw('group_concat(IFNULL(players.image_path,"")) as player_images'), DB::raw('group_concat(IFNULL(players.is_local_image,"")) as is_local_images'), DB::raw('group_concat(IFNULL(players.local_image_path,"")) as player_local_images'), DB::raw('group_concat(IFNULL(teams.name,"")) as player_team_names'))
                ->when($isTeamName, function($query) use ($team_name) {
                    $query->havingRaw('LOCATE(\'' . $team_name . '\',player_team_names)>0');
                })
                ->when($isPlayerName, function($query) use ($player_name) {
                    $query->havingRaw('LOCATE(\'' . $player_name . '\',player_football_index_common_names)>0');
                })
                ->when($isDate, function($query) use ($startDate, $endDate) {
                    $query->whereRaw(DB::raw('DATE(date) BETWEEN \'' . $startDate . '\' AND \'' . $endDate . '\''));
                })
                ->orderBy('performance_buzz.date', 'DESC')
                ->groupBy('performance_buzz.date')
                // ->simplePaginate($this->perPage);
                ->simplePaginate($this->perPage);

        return $performance;
    }

    /**
     * Function to  get Performance Buzz team records based on team logic.
     * @param Array $request - Laravel Request variable which contains all request variable of that page
     * @return Array - Performance Buzz Data Array.
     */
    public function pbTeamHistory(Request $request) {
        View::share(['activeMenu' => 'performance-buzz', 'activeSubMenu' => 'performance-buzz-team']);
        $data['page'] = Page::find(6);
        $performance = Helper::performanceBuzzPlayers_new($request);
        $data['PBWinners'] = Helper::dispatchObject($performance);
        $data['links'] = $performance;
        return view('front.performance-buzz.team.index')->with($data);
    }

    /**
     * This function is used to fetch MB Player data from Football Index Site
     */


        /**
     * Function to  get Performance Buzz team records based on team logic.
     * @param Array $request - Laravel Request variable which contains all request variable of that page
     * @return Array - Performance Buzz Data Array.
     */
    public function pbStatsHistory(Request $request) {
        View::share(['activeMenu' => 'performance-buzz', 'activeSubMenu' => 'performance-buzz-stats']);
        $data['page'] = Page::find(6);
        $performance = Helper::performanceBuzzPlayers_new($request);
        $data['PBWinners'] = Helper::dispatchObject($performance);
        $data['links'] = $performance;
        return view('front.performance-buzz.stats.index')->with($data);
    }

    /**
     * This function is used to fetch MB Player data from Football Index Site
     */    

    public function getPbPlayerData() {
        $date = date('Y-m-d');
        $url = 'https://api-prod.footballindex.co.uk/football.perfomancebuzz.all?page=1&per_page=1500&sort=asc';

        $items = Helper::executeGet($url);
        $this->createJsonResponseFile($items);
        $items = json_decode(json_encode($items), true);

        if (!empty($items)) {

            usort($items['items'], 'self::usort_callback');
            $array = [];
            foreach ($items['items'] as $a) {
                array_push($array, $a);
            }

            $pbcount = $fwd = $mid = $def = 0;
            foreach ($array as $val) {
                $player = Player::where('football_index_common_name', '=', $val['name'])->first();
                if ($player != '') {

                    $pbData = new PerformanceBuzz();
                    $pbData->player_id = $player->id;
                    $pbData->team_id = $player->team_id;
                    $pbData->score = $val['score'];
                    $pbData->sector = $player->football_index_position;
                    $pbData->date = $date;
                    $pbData->position = 0;
                    $pbData->save();

//                   UPDATE PLAYER WINS COUNT HERE & FORM GUIDE SCORE
                    $form_guide = PerformanceBuzz::where(['player_id' => $player->id])->select('score')->orderBy('date', 'DESC')->limit(5)->get();
                    $player->form_guide = $form_guide->sum('score');
                    $player->save();
                }
            }
            Helper::updatePBPosition($date);
            Helper::updateCounts($date);
            Helper::updateDividends($date);
        }
    }

    /**
     * This function is used to sort array
     */
    public function usort_callback($a, $b) {
        if ($a['score'] == $b['score'])
            return 0;

        return ( $a['score'] > $b['score']) ? -1 : 1;
    }

    /**
     * This function is used to update PB wins count
     */
    public function updateCounts() {
        \DB::table('teams')->update(['top_player_wins' => 0, 'positional_wins' => 0]);
        \DB::table('players')->update(['top_player_wins' => 0, 'positional_wins' => 0]);
        $dates = PerformanceBuzz::select('date')->groupBy('date')->get();

        foreach ($dates as $date) {
            $query = PerformanceBuzz::where('date', $date->date)->orderBy('score', 'DESC')
                    ->whereIn('performance_buzz.sector', $this->positions)
                    ->select('player_id', 'team_id')
                    ->groupBy('sector');

            // SAVE TOP PLAYER COUNT
            if ($query->first()->player_id != NULL) {
                $player = Player::where('id', '=', $query->first()->player_id)->first();
                $player->top_player_wins = $player->top_player_wins + 1;
                $player->save();
            }

            // SAVE TOP TEAM COUNT
            if ($query->first()->team_id != NULL) {
                $team = Team::where('id', '=', $query->first()->team_id)->first();
                $team->top_player_wins = $team->top_player_wins + 1;
                $team->save();
            }

            $player_ids = $query->limit(3)->get();

            foreach ($player_ids as $id) {

                if ($id->player_id != NULL) {
                    // SAVE PLAYERS COUNT
                    $player = Player::where('id', '=', $id->player_id)->first();
                    $player->positional_wins = $player->positional_wins + 1;
                    $player->save();
                }

                // SAVE TEAMS COUNT
                if ($id->team_id != NULL) {
                    $team = Team::where('id', '=', $id->team_id)->first();
                    $team->positional_wins = $team->positional_wins + 1;
                    $team->save();
                }
            }
        }
    }

    /**
     * This function is used to fetch PB data from CSV file  
     */
    public function getCsvData() {

        $filename = public_path('assets/New-PB-Manual-Import-Data.csv');

        $file_handle = fopen($filename, 'r');

        while (!feof($file_handle)) {
            $array[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);

        $pbcount = $fwd = $mid = $def = 0;
        foreach ($array as $arr) {
            $date = $arr[0];
            $name = $arr[1];
            $score = $arr[2];
            $player = Player::where('football_index_common_name', '=', $name)->first();
            if ($player != '' && $date != '' && $score != '') {

                $query = PerformanceBuzz::where(['date' => $date, 'player_id' => $player->id, 'score' => $score])->count();
                if ($query == 0) {
                    $pbData = new PerformanceBuzz();
                    $pbData->player_id = $player->id;
                    $pbData->team_id = $player->team_id;
                    $pbData->score = $score;
                    $pbData->sector = $player->position;
                    $pbData->date = $date;
                    $pbData->save();

//                   UPDATE PLAYER FORM GUIDE SCORE
                    $form_guide = PerformanceBuzz::where(['player_id' => $player->id])->select('score')->orderBy('date', 'DESC')->limit(5)->get();
                    $player->form_guide = $form_guide->sum('score');
                    $player->save();
                }
            }
        }
    }

    /**
     * This function is used to create json file for response received from Football index site
     */
    public function createJsonResponseFile($data) {

        $file = date('Y-m-d') . '.txt';
        $destinationPath = public_path() . '/uploads/json-response/';
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        \File::put($destinationPath . $file, json_encode($data, TRUE));
        return response()->download($destinationPath . $file);
    }

    /**
     * This function is used to update positions
     */
    public function updatepositions() {
        $performance_dates = PerformanceBuzz::select('date')->groupBy('date')->orderBy('date')->get();
        // echo '<pre>'; print_r($performance_dates); exit;
        foreach ($performance_dates as $performance_date) {
            Helper::updatePBPosition($performance_date->date);
        }
    }

}
