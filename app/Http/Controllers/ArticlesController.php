<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use App\ArticleCategories;
use App\Article;
use App\Helpers\Helper;

class ArticlesController extends Controller {

    protected $perPage = 20;

    public function __construct() {

        View::share(['activeMenu' => 'articles', 'activeSubMenu' => 'articles', 'articleCategories' => $this->getArticleCategories()]);
    }

    public function index(Request $request) {

        if (isset($request->keyword) && $request->keyword != '') {

            $data['articles'] = Article::where('title', 'like', '%' . $request->keyword . '%')->get();
        } else {

            $data['articles'] = Article::take(6)->get();
        }
        $data['articlePage'] = "articles";
        return view('front.articles.index')->with($data);
    }

    public function getArticleCategories() {

        return ArticleCategories::get();
    }

    public function articlesByCategory($slug = '') {

        $data['categoryDetail'] = ArticleCategories::where('slug', $slug)->firstOrFail();

        $data['articles'] = $data['categoryDetail']->getArticles()->paginate($this->perPage);

        return view('front.articles.index', $data);
    }

    public function articleDetail($slug) {

        $data['article'] = Article::where('slug', '=', $slug)->with('getArticlesCategory')->firstOrFail();
        $data['articlePage'] = "articles";
        return view('front.articles.article-detail', $data);
    }

    public function loadAllArticles(Request $request) {

        if (isset($request->keyword) && $request->keyword != '') {

            $data['articles'] = Article::where('title', 'like', '%' . $request->keyword . '%')->paginate($this->perPage);
        } else {

            $data['articles'] = Article::paginate($this->perPage);
        }
        $data['articlePage'] = "all_article";
        return view('front.articles.all-articles')->with($data);
    }

}
