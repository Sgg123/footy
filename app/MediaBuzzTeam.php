<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\MediaBuzzTeamScope;

class MediaBuzzTeam extends Model
{
    protected $fillable = ['mb_first', 'mb_second', 'mb_third', 'pb_top_player', 'pb_top_defender', 'pb_top_midfielder', 'pb_top_forwarder', 'status','date'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new MediaBuzzTeamScope);
    }
}
