<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayAndDividends extends Model
{
    protected $table = "day_and_dividends";
    protected $fillable = ['date', 'type_of_day', 'top_performance_player','top_positional_performance_player','media_buzz_first','media_buzz_second','media_buzz_third','first_p','second_p','fifth_p','goalkeeper'];
}