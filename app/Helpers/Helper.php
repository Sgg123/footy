<?php

namespace App\Helpers;

use App\ArticleCategories;
use App\Article;
use App\Player;
use App\Fixture;
use App\Season;
use App\League;
use App\Banner;
use Carbon\Carbon;
use App\Team;
use DB;
use App\DayAndDividends;
use App\MediaBuzzPlayer;
use App\PerformanceBuzz;

class Helper {

    static $secretKey = 'footyindex@iih';
    static $secretIv = 'footyindex@iih';
    static $apiToken = 'P3RvBQG8M11B0b0enUzNEUb5HQRfTIrtRoRvniUNCV5mA3tSFKw3bG6fImFm';
    static $perPage = 20;

    /**
     * Returns encrypted original string
     *
     * @param  $string - Enctrypted string
     *
     * @return string
     */
    public static function encrypt($string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        // hash
        $key = hash('sha256', self::$secretKey);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', self::$secretIv), 0, 16);

        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }

    /**
     * Returns decrypted original string
     *
     * @param  $string - Enctrypted string
     *
     * @return string
     */
    public static function decrypt($string) {

        $output = false;
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = 'iih';
        $secret_iv = 'iih';

        // hash
        $key = hash('sha256', self::$secretKey);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', self::$secretIv), 0, 16);

        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

        return $output;
    }

    public static function getArticleCategories() {
        return ArticleCategories::get();
    }

    /**
     * Returns one latest article
     * */
    public static function getLatestArticle() {

        $article = Article::first();

        return $article;
    }

    /**
     * Returns one random players data for sidebar
     * */
    public static function getRandomPlayerData() {

        $data['player_details'] = Player::inRandomOrder()
                ->join('teams', 'teams.id', 'players.team_id')
                ->select('players.*', 'teams.name', 'teams.logo_path', 'teams.local_image_path AS team_local_logo', 'teams.is_local_image as team_is_local', DB::raw('(SELECT AVG(performance_buzz.score) as total_score FROM performance_buzz WHERE performance_buzz.player_id = players.id) as avgscore'))
                ->where('players.football_index_common_name', '!=', NULL)
                ->where('players.is_local_image', '=', 1)
                ->first();


        $teamId = $data['player_details']['team_id'];
        $data['fixture_details'] = Fixture::where('starting_at', '>=', Carbon::now())
                ->where(function($query)use ($teamId) {
                    $query->where('localteam_id', $teamId)
                    ->orWhere('visitorteam_id', $teamId);
                })
                ->select('fixtures.starting_at', 'fixtures.visitorteam_id', 'fixtures.localteam_id')
                ->addSelect(\DB::raw("(CASE WHEN (fixtures.visitorteam_id = '$teamId') THEN (select name from teams where id=fixtures.localteam_id)
                                         WHEN (fixtures.localteam_id = '$teamId') THEN (select name from teams where id=fixtures.visitorteam_id)
                                          END) AS oppTeamName"))
                ->orderBy('starting_at', 'ASC')
                ->first();

//      STANDINGS TABLE DATA
        $seasonIds = Season::where('standing_active', 1)->pluck('id')->toArray();
        $id = $seasonIds[array_rand($seasonIds)];

        $url = "https://soccer.sportmonks.com/api/v2.0/standings/season/" . $id . "?api_token=" . self::$apiToken;
        $standings = Helper::executeGet($url);

        $data['league_name'] = [];
        if ($standings->data != "") {
            $data['standings'] = @$standings->data[0]->standings->data;
            $data['league_name'] = '';
            $leagName = League::where('id', '=', @$standings->data[0]->league_id)->first();

            if ($leagName) {
                $data['league_name'] = $leagName->name;
            }
        }

        /* For get Overall and Postion player rank code Start */
        $position = $data['player_details']['football_index_position'];
        $plyerID = $data['player_details']['id'];
        $data['pbrank'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id as pid, sc.score FROM(SELECT sector ,player_id, avg(score) AS score FROM `performance_buzz` where sector="' . $position . '" GROUP BY player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

        $playerRank = '';
        foreach ($data['pbrank'] as $value) {
            if (@$value->pid == $plyerID) {

                $playerRank = $value;
            }
        }
        $data['playerdetail'] = @$playerRank->rank;
        $data['playersector'] = @$playerRank->sector;

        if (isset($data["playerdetail"])) {
            $data['playerdetail'] = @$playerRank->rank;
            $data['playersector'] = @$playerRank->sector;
        } else {
            $data['playerdetail'] = @$playerRank->rank = '-';
            $data['playersector'] = @$playerRank->sector = $position;
        }

        $data['pballrank'] = DB::select('SELECT @rank := (@rank+1) AS rank, sc.player_id as pid, sc.score FROM(SELECT player_id, avg(score) AS score FROM `performance_buzz` GROUP BY player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');

        $playerOverallRank = '';
        foreach ($data['pballrank'] as $value) {
            if (@$value->pid == $plyerID) {

                $playerOverallRank = $value;
            }
        }
        $data['overallrankdetail'] = @$playerOverallRank->rank;
        if (isset($data["overallrankdetail"])) {
            $data['overallrankdetail'] = @$playerOverallRank->rank;
        } else {
            $data['overallrankdetail'] = @$playerOverallRank->rank = '-';
        }
        /* For get Overall and Postion player rank code End */


        return $data;
    }

    /**
     * This function is used to execure cURL get request
     * 
     * @param  string $url - URL
     * 
     * @return Array of Object
     */
    public static function executeGet($url = '') {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'FootyIndex'
        ));
        $result = curl_exec($curl);
        curl_close($curl);

        return json_decode($result);
    }

    /**
     * This functin is used to get upcoming metch detail for team
     * 
     * @param  integer $teamId - Team ID
     * @return Object
     */
    public static function getUpcomingFixtureForTeam($teamId) {

        $fixture = Fixture::whereRaw('starting_at > CURDATE()')->where(function($query) use($teamId) {
                    return $query->orWhere(['localteam_id' => $teamId, 'visitorteam_id' => $teamId]);
                })->first();

        $matchDetail = [];

        if ($fixture) {

            if ($fixture->localteam_id == $teamId) {
                $matchDetail['teamType'] = '(H)';
                $matchDetail['team'] = Team::where('id', $fixture->visitorteam_id)->select('name', 'logo_path', 'local_image_path', 'is_local_image')->first();
            } else {
                $matchDetail['teamType'] = '(A)';
                $matchDetail['team'] = Team::where('id', $fixture->localteam_id)->select('name', 'logo_path', 'local_image_path', 'is_local_image')->first();
            }

            $today = date("Y-m-d");
            $next_date = date("Y-m-d", strtotime($fixture->starting_at));

            $date1 = new \DateTime($today);
            $date2 = new \DateTime($next_date);

            $diff = $date2->diff($date1)->format("%a");

            $matchDetail['datediff'] = $diff;

            $matchDetail['starting_at'] = $fixture->starting_at;
        }

        return $matchDetail;
    }

    /**
     * This function is used to get player top wins count and positional wins count
     * 
     * @param  integer $playerId  - Player ID
     * @param  string $position   - Player Position
     * @param  boolean $isDay     - Whether to check date filter or not
     * @param  string $startDate  - Start date
     * @param  string $endDate    - End date
     * 
     * @return Array
     */
    public static function getTopAndPositionalWinsCount($playerId, $position, $isDay = '', $startDate = '', $endDate = '', $isLeague = '', $league_dates = '') {

        $dateFilter = '';
        if ($isDay === true) {

            $dateFilter = ' AND date BETWEEN "' . $startDate . '" AND "' . $endDate . '" ';
        }
        if ($isLeague === true) {
            $dateFilter = ' AND date IN (' . $league_dates . ')';
        }
        $topWins = DB::select('SELECT COUNT(*) as MAX_SCORE_COUNT FROM performance_buzz
            WHERE performance_buzz.position IN (1,2,3,4) AND performance_buzz.player_id = ' . $playerId . $dateFilter . ' GROUP BY PLAYER_ID');

        if ($topWins) {

            $topWins = $topWins[0]->MAX_SCORE_COUNT;
        } else {
            $topWins = 0;
        }

        $positionalWins = DB::select('SELECT COUNT(*) as MAX_SCORE_COUNT FROM performance_buzz          
        WHERE performance_buzz.position = 4 AND performance_buzz.player_id = ' . $playerId . $dateFilter . ' GROUP BY PLAYER_ID');

        if ($positionalWins) {

            $positionalWins = $positionalWins[0]->MAX_SCORE_COUNT;
        } else {
            $positionalWins = 0;
        }

        return ['topWins' => $topWins, 'positionalWins' => $positionalWins];
    }

    /**
     * This function is used to get backgrond and font color based on score
     * 
     * @param  integer $score - Score
     * @return Array
     */
    public static function getColor($score) {

        $score = number_format($score);
        $colorCode = "";
        $fontcolor = "";

        if (@$score <= 30) {

            $colorCode = "#FF0000";
            $fontcolor = "#FFFFFF";
        } elseif (@$score >= 31 && @$score <= 70) {

            $colorCode = "#FF5733";
            $fontcolor = "#FFFFFF";
        } elseif (@$score >= 71 && @$score <= 110) {

            $colorCode = "#eacd0e";
            $fontcolor = "#000";
        } elseif (@$score >= 111 && @$score <= 150) {

            $colorCode = "#FFFF00";
            $fontcolor = "#000";
        } elseif (@$score >= 151 && @$score <= 180) {

            $colorCode = "#aad77d";
            $fontcolor = "#FFFFFF";
        } elseif (@$score >= 180) {

            $colorCode = "#39a02b";
            $fontcolor = "#FFFFFF";
        }

        return ['colorCode' => $colorCode, 'fontcolor' => $fontcolor];
    }

    /**
     * This function is used to calculte dividend of the player. Dividend of the Player will be calculated based on Data from both Media Buzz and Performance Buzz Winning Entries of particular player. Amount of divided per win will be taken from Days and Dividends as per the date of either Media or Performance Buzzs' record.
     *
     * @param - integer $player_id - Player Id
     * @return - Array
     */
    public static function calculatePlayerDividend($player_id) {
        ini_set('max_execution_time', 420); //420 seconds = 7 minutes    

        $allmediabuzz = MediaBuzzPlayer::where('player_id', $player_id)->get();
        $mb_first_count = 0;
        $mb_second_count = 0;
        $mb_third_count = 0;
        $mb_first_dividend = 0;
        $mb_second_dividend = 0;
        $mb_third_dividend = 0;
        $player_media_dividend = 0;

        $top_player_wins = 0;
        $positional_wins = 0;
        $top_player_dividend = 0;
        $positional_dividend = 0;
        $player_performance_dividend = 0;

        foreach ($allmediabuzz as $mediabuzz) {
            $daydividend = DayAndDividends::whereDate('date', '=', $mediabuzz->date)->first();
            // print_r($mediabuzz);
            // print_r($daydividend['type_of_day']);
            // print_r('=============================================');
            if ($mediabuzz->position == 1) {
                $mb_first_count++;
                $mb_first_dividend = $mb_first_dividend + $daydividend['media_buzz_first'];
                $player_media_dividend = $player_media_dividend + $daydividend['media_buzz_first'];
            } elseif ($mediabuzz->position == 2) {
                $mb_second_count++;
                $mb_second_dividend = $mb_second_dividend + $daydividend['media_buzz_second'];
                $player_media_dividend = $player_media_dividend + $daydividend['media_buzz_second'];
            } elseif ($mediabuzz->position == 3) {
                $mb_third_count++;
                $mb_third_dividend = $mb_third_dividend + $daydividend['media_buzz_third'];
                $player_media_dividend = $player_media_dividend + $daydividend['media_buzz_third'];
            }
        }
        $allperformancebuzz = PerformanceBuzz::where('player_id', $player_id)->where('position', '!=', 0)->get();
        foreach ($allperformancebuzz as $performancebuzz) {
            $daydividend = DayAndDividends::whereDate('date', '=', $performancebuzz->date)->first();
            // print_r($performancebuzz);
            // print_r($daydividend);
            // print_r('==============================================');
            if ($performancebuzz->position == 4) {
                $top_player_wins++;
                $positional_wins++;
                $top_player_dividend = $top_player_dividend + $daydividend['top_performance_player'];
                $positional_dividend = $positional_dividend + $daydividend['top_positional_performance_player'];

                $player_performance_dividend = $player_performance_dividend + $daydividend['top_performance_player'] + $daydividend['top_positional_performance_player'];
            } else {
                $positional_wins++;
                $positional_dividend = $positional_dividend + $daydividend['top_positional_performance_player'];
                $player_performance_dividend = $player_performance_dividend + $daydividend['top_positional_performance_player'];
            }
        }
        $player = Player::find($player_id);

        // UPDATE PLAYER FORM GUIDE SCORE
        $form_guide = PerformanceBuzz::where(['player_id' => $player_id])->select('score')->orderBy('date', 'DESC')->limit(5)->get();
        $player->form_guide = $form_guide->sum('score');

        $data['Player Name'] = $player->football_index_common_name;
        $player->mb_first_count = $mb_first_count;
        $player->mb_second_count = $mb_second_count;
        $player->mb_third_count = $mb_third_count;
        $player->mb_first_dividend = $mb_first_dividend;
        $player->mb_second_dividend = $mb_second_dividend;
        $player->mb_third_dividend = $mb_third_dividend;
        $player->top_player_wins = $top_player_wins;
        $player->positional_wins = $positional_wins;
        $player->top_player_dividend = $top_player_dividend;
        $player->positional_dividend = $positional_dividend;
        $player->media_dividend = $player_media_dividend;
        $player->performance_dividend = $player_performance_dividend;
        $player->save();

        $data['MB 1st Count'] = $mb_first_count;
        $data['MB 2nd Count'] = $mb_second_count;
        $data['MB 3rd Count'] = $mb_third_count;
        $data['MB 1st Dividend'] = $mb_first_dividend;
        $data['MB 2nd Dividend'] = $mb_second_dividend;
        $data['MB 3rd Dividend'] = $mb_third_dividend;
        $data['MEDIA BUZZ DIVIDEND'] = $player_media_dividend;
        $data['PB Top Count'] = $top_player_wins;
        $data['PB Positional Count'] = $positional_wins;
        $data['PB Top Dividend'] = $top_player_dividend;
        $data['PB Positional Dividend'] = $positional_dividend;
        $data['PERFORMANCE BUZZ DIVIDEND'] = $player_performance_dividend;

        return $data;
    }

    /**
     * This function is used to calculte dividend of the team. Dividend of the Team will be calculated based on Data from both Media Buzz and Performance Buzz Winning Entries of particular team of players. Amount of divided per win will be taken from Days and Dividends as per the date of either Media or Performance Buzzs' record.
     *
     * @param - integer $team_id - Team Id
     * @return - Array
     */
    public static function calculateTeamDividend($team_id) {
        $allmediabuzz = MediaBuzzPlayer::where('team_id', $team_id)->get();
        $mb_first_count = 0;
        $mb_second_count = 0;
        $mb_third_count = 0;
        $mb_first_dividend = 0;
        $mb_second_dividend = 0;
        $mb_third_dividend = 0;
        $player_media_dividend = 0;

        $top_player_wins = 0;
        $positional_wins = 0;
        $top_player_dividend = 0;
        $positional_dividend = 0;
        $player_performance_dividend = 0;

        foreach ($allmediabuzz as $mediabuzz) {
            $daydividend = DayAndDividends::whereDate('date', '=', $mediabuzz->date)->first();
            // print_r($mediabuzz);
            // print_r($daydividend['type_of_day']);
            // print_r('=============================================');
            if ($mediabuzz->position == 1) {
                $mb_first_count++;
                $mb_first_dividend = $mb_first_dividend + $daydividend['media_buzz_first'];
                $player_media_dividend = $player_media_dividend + $daydividend['media_buzz_first'];
            } elseif ($mediabuzz->position == 2) {
                $mb_second_count++;
                $mb_second_dividend = $mb_second_dividend + $daydividend['media_buzz_second'];
                $player_media_dividend = $player_media_dividend + $daydividend['media_buzz_second'];
            } else {
                $mb_third_count++;
                $mb_third_dividend = $mb_third_dividend + $daydividend['media_buzz_third'];
                $player_media_dividend = $player_media_dividend + $daydividend['media_buzz_third'];
            }
        }
        $allperformancebuzz = PerformanceBuzz::where('team_id', $team_id)->where('position', '!=', 0)->get();
        foreach ($allperformancebuzz as $performancebuzz) {
            $daydividend = DayAndDividends::whereDate('date', '=', $performancebuzz->date)->first();
            // print_r($performancebuzz);
            // print_r($daydividend);
            // print_r('==============================================');
            if ($performancebuzz->position == 4) {
                $top_player_wins++;
                $positional_wins++;
                $top_player_dividend = $top_player_dividend + $daydividend['top_performance_player'];
                $positional_dividend = $positional_dividend + $daydividend['top_positional_performance_player'];

                $player_performance_dividend = $player_performance_dividend + $daydividend['top_performance_player'] + $daydividend['top_positional_performance_player'];
            } else {
                $positional_wins++;
                $positional_dividend = $positional_dividend + $daydividend['top_positional_performance_player'];
                $player_performance_dividend = $player_performance_dividend + $daydividend['top_positional_performance_player'];
            }
        }

        $team = Team::find($team_id);
        $data['Team Name'] = $team->name;
        $team->mb_first_count = $mb_first_count;
        $team->mb_second_count = $mb_second_count;
        $team->mb_third_count = $mb_third_count;
        $team->mb_first_dividend = $mb_first_dividend;
        $team->mb_second_dividend = $mb_second_dividend;
        $team->mb_third_dividend = $mb_third_dividend;
        $team->top_player_wins = $top_player_wins;
        $team->positional_wins = $positional_wins;
        $team->top_player_dividend = $top_player_dividend;
        $team->positional_dividend = $positional_dividend;
        $team->media_dividend = $player_media_dividend;
        $team->performance_dividend = $player_performance_dividend;
        $team->save();

        $data['MB 1st Count'] = $mb_first_count;
        $data['MB 2nd Count'] = $mb_second_count;
        $data['MB 3rd Count'] = $mb_third_count;
        $data['MB 1st Dividend'] = $mb_first_dividend;
        $data['MB 2nd Dividend'] = $mb_second_dividend;
        $data['MB 3rd Dividend'] = $mb_third_dividend;
        $data['MEDIA BUZZ DIVIDEND'] = $player_media_dividend;
        $data['PB Top Count'] = $top_player_wins;
        $data['PB Positional Count'] = $positional_wins;
        $data['PB Top Dividend'] = $top_player_dividend;
        $data['PB Positional Dividend'] = $positional_dividend;
        $data['PERFORMANCE BUZZ DIVIDEND'] = $player_performance_dividend;

        return $data;
    }

    /**
     * Function to update the positions in Perfoamnce Buzz. This function will reassign OR refresh the positions in Performance Buzz Table. Positions in performance_buzz are as below.
     * 1: Top Forward, 2: Top Midfielder, 3: Top Defender | Goalkeeper, 4: Top Player
     * 
     * @param Date - date - Date for which Positions in Performance Buzz are needed to be reassigned/ refreshed.
     * @return 1
     */
    public static function updatePBPosition($parent_date) {
        PerformanceBuzz::whereDate('date', $parent_date)->update(['position' => 0]);
        // $performance = PerformanceBuzz::whereDate('date', $parent_date)->orderBy('score')->groupBy('sector')->get();
        $performance = PerformanceBuzz::whereDate('date', $parent_date)->select(DB::raw('MAX(score) score,id,sector,date'))->groupBy('sector')->get();
        foreach ($performance as $key => $value) {
            $pb = PerformanceBuzz::whereDate('date', $parent_date)->where('score', $value->score)->where('sector', $value->sector)->where('date', $value->date)->get();
            $pbata[] = $pb[0];
        }

        $c = 0;
        $top_player_arr = array();
        if (!empty($pbata)) {
            foreach ($pbata as $key => $value) {
                $top_player_arr[$value->id] = $value->score;
                switch ($value->sector) {
                    case 'Goalkeeper':
                        $pb2 = PerformanceBuzz::find($value->id);
                        $pb2->position = 4;
                        $pb2->save();

                        $pl = Player::find($value->player_id);
                        $pl->last_win_date = $value->date;
                        $pl->save();

                        if ($value->team_id != NULL) {
                            $team = Team::find($value->team_id);
                            $team->last_win_date = $value->date;
                            $team->save();
                        }
                        break;

                    case 'Defender':
                        $pb2 = PerformanceBuzz::find($value->id);
                        $pb2->position = 3;
                        $pb2->save();

                        $pl = Player::find($value->player_id);
                        $pl->last_win_date = $value->date;
                        $pl->save();

                        if ($value->team_id != NULL) {
                            $team = Team::find($value->team_id);
                            $team->last_win_date = $value->date;
                            $team->save();
                        }
                        break;

                    case 'Forward':
                        $pb2 = PerformanceBuzz::find($value->id);
                        $pb2->position = 1;
                        $pb2->save();

                        $pl = Player::find($value->player_id);
                        $pl->last_win_date = $value->date;
                        $pl->save();

                        if ($value->team_id != NULL) {
                            $team = Team::find($value->team_id);
                            $team->last_win_date = $value->date;
                            $team->save();
                        }

                        break;

                    case 'Midfielder':
                        $pb2 = PerformanceBuzz::find($value->id);
                        $pb2->position = 2;
                        $pb2->save();

                        $pl = Player::find($value->player_id);
                        $pl->last_win_date = $value->date;
                        $pl->save();

                        if ($value->team_id != NULL) {
                            $team = Team::find($value->team_id);
                            $team->last_win_date = $value->date;
                            $team->save();
                        }

                        break;

                    default:
                        # code...
                        break;
                }
            }

            $top_player_id = array_search(max($top_player_arr), $top_player_arr);
            $pbt = PerformanceBuzz::find($top_player_id);
            $pbt->position = 4;
            $pbt->save();
        }

        return 1;
    }

    /**
     * Function to update the positions in Perfoamnce Buzz. This function will update wins count of players & teams with particular date.
     * 
     * @param Date - date - Date for which Positions are needed to be updated.
     */
    public static function updateCounts($date) {
        $players = PerformanceBuzz::whereDate('date', $date)->orderBy('score')->pluck('player_id')->toArray();
        $teams = PerformanceBuzz::whereDate('date', $date)->orderBy('score')->pluck('team_id')->toArray();

        \DB::table('teams')->whereIn('id', $players)->update(['top_player_wins' => 0, 'positional_wins' => 0]);
        \DB::table('players')->whereIn('id', $teams)->update(['top_player_wins' => 0, 'positional_wins' => 0]);

        foreach ($players as $player) {
            $topWins = PerformanceBuzz::where('player_id', '=', $player)->where('position', '=', 4)->count();
            $positionalWins = PerformanceBuzz::where('player_id', '=', $player)->where('position', '!=', 0)->count();
            Player::where('id', '=', $player)->update(['top_player_wins' => $topWins, 'positional_wins' => $positionalWins]);
        }

        foreach ($teams as $team) {
            $topWins = PerformanceBuzz::where('team_id', '=', $team)->where('position', '=', 4)->count();
            $positionalWins = PerformanceBuzz::where('team_id', '=', $team)->where('position', '!=', 0)->count();
            Team::where('id', '=', $team)->update(['top_player_wins' => $topWins, 'positional_wins' => $positionalWins]);
        }
    }

    /**
     * Function to update the dividend of players & teams Perfoamnce Buzz. 
     * 
     * @param Date - date - Date for which Positions are needed to be updated.
     */
    public static function updateDividends($date) {
        $players = PerformanceBuzz::whereDate('date', $date)->orderBy('score')->pluck('player_id')->toArray();
        $teams = PerformanceBuzz::whereDate('date', $date)->orderBy('score')->pluck('team_id')->toArray();

        foreach ($players as $player) {
            Helper::calculatePlayerDividend($player);
        }

        foreach ($teams as $team) {
            if ($team != '') {
                Helper::calculateTeamDividend($team);
            }
        }
    }

    /**
     * Function to dispatch object of Perfoamnce Buzz Data. 
     * 
     * @param PBWinners - Pb winners object.     
     */
    public static function dispatchObject($PBWinners) {

        $array = [];
        $tempArray = [];
        foreach ($PBWinners as $key => $value) {

            $tempArray['date'] = $value['date'];
            $player_name = explode(',', @$value['player_names']);
            $player_position = explode(',', @$value['player_positions']);
            $player_common_name = explode(',', @$value['player_football_index_common_names']);
            $player_image = explode(',', @$value['player_images']);
            $player_is_local_image = explode(',', @$value['is_local_images']);
            $player_local_images = explode(',', @$value['player_local_images']);
            $player_team_name = explode(',', @$value['player_team_names']);
            $score = explode(',', @$value['scores']);
            $player_ids = explode(',', @$value['player_ids']);
            $team_ids = explode(',', @$value['team_ids']);

            $forward = Helper::findValue($player_position, 'Forward');
            $midfielder = Helper::findValue($player_position, 'Midfielder');
            $defender = Helper::findValue($player_position, 'Defender');
            $goalkeeper = Helper::findValue($player_position, 'Goalkeeper');
            // TOP PLAYER 
            if (@$score[@$midfielder] >= @$score[@$defender] && @$score[@$midfielder] >= @$score[@$forward] && @$score[@$midfielder] >= @$score[@$goalkeeper]) {

                $tempArray['topPlayerName'] = @$player_name[@$midfielder];
                $tempArray['topPlayerFCommonName'] = @$player_common_name[@$midfielder];
                $tempArray['topPlayerPosition'] = @$player_position[@$midfielder];
                $tempArray['topPlayerScore'] = @$score[@$midfielder];
                $tempArray['topPlayerTeamName'] = @$player_team_name[@$midfielder];
                $tempArray['topPlayerImagePath'] = @$player_image[@$midfielder];
                $tempArray['topPlayerIsLocalImage'] = @$player_is_local_image[@$midfielder];
                $tempArray['topPlayerLocalImagePath'] = @$player_local_images[@$midfielder];
                $tempArray['topPlayerPlayerId'] = @$player_ids[@$midfielder];
                $tempArray['topPlayerTeamId'] = @$team_ids[@$midfielder];
            } else if (@$score[@$defender] >= @$score[@$midfielder] && @$score[@$defender] >= @$score[@$forward] && @$score[@$defender] >= @$score[@$goalkeeper]) {

                $tempArray['topPlayerName'] = @$player_name[@$defender];
                $tempArray['topPlayerFCommonName'] = @$player_common_name[@$defender];
                $tempArray['topPlayerPosition'] = @$player_position[@$defender];
                $tempArray['topPlayerScore'] = @$score[@$defender];
                $tempArray['topPlayerTeamName'] = @$player_team_name[@$defender];
                $tempArray['topPlayerImagePath'] = @$player_image[@$defender];
                $tempArray['topPlayerIsLocalImage'] = @$player_is_local_image[@$defender];
                $tempArray['topPlayerLocalImagePath'] = @$player_local_images[@$defender];
                $tempArray['topPlayerPlayerId'] = @$player_ids[@$defender];
                $tempArray['topPlayerTeamId'] = @$team_ids[@$defender];
            } else if (@$score[@$forward] >= @$score[@$midfielder] && @$score[@$forward] >= @$score[@$defender] && @$score[@$forward] >= @$score[@$goalkeeper]) {

                $tempArray['topPlayerName'] = @$player_name[@$forward];
                $tempArray['topPlayerFCommonName'] = @$player_common_name[@$forward];
                $tempArray['topPlayerPosition'] = @$player_position[@$forward];
                $tempArray['topPlayerScore'] = @$score[@$forward];
                $tempArray['topPlayerTeamName'] = @$player_team_name[@$forward];
                $tempArray['topPlayerImagePath'] = @$player_image[@$forward];
                $tempArray['topPlayerIsLocalImage'] = @$player_is_local_image[@$forward];
                $tempArray['topPlayerLocalImagePath'] = @$player_local_images[@$forward];
                $tempArray['topPlayerPlayerId'] = @$player_ids[@$forward];
                $tempArray['topPlayerTeamId'] = @$team_ids[@$forward];
            }else if (@$score[@$goalkeeper] >= @$score[@$midfielder] && @$score[@$goalkeeper] >= @$score[@$defender] && @$score[@$goalkeeper] >= @$score[@$forward]) {
                $tempArray['topPlayerName'] = @$player_name[@$goalkeeper];
                $tempArray['topPlayerFCommonName'] = @$player_common_name[@$goalkeeper];
                $tempArray['topPlayerPosition'] = @$player_position[@$goalkeeper];
                $tempArray['topPlayerScore'] = @$score[@$goalkeeper];
                $tempArray['topPlayerTeamName'] = @$player_team_name[@$goalkeeper];
                $tempArray['topPlayerImagePath'] = @$player_image[@$goalkeeper];
                $tempArray['topPlayerIsLocalImage'] = @$player_is_local_image[@$goalkeeper];
                $tempArray['topPlayerLocalImagePath'] = @$player_local_images[@$goalkeeper];
                $tempArray['topPlayerPlayerId'] = @$player_ids[@$goalkeeper];
                $tempArray['topPlayerTeamId'] = @$team_ids[@$goalkeeper];
            }
            // TOP PLAYER ENDS
//
            $tempArray['forwardName'] = @$player_name[@$forward];
            $tempArray['forwardFCommonName'] = @$player_common_name[@$forward];
            $tempArray['forwardPosition'] = @$player_position[@$forward];
            $tempArray['forwardScore'] = @$score[@$forward];
            $tempArray['forwardTeamName'] = @$player_team_name[@$forward];
            $tempArray['forwardImagePath'] = @$player_image[@$forward];
            $tempArray['forwardIsLocalImage'] = @$player_is_local_image[@$forward];
            $tempArray['forwardLocalImagePath'] = @$player_local_images[@$forward];
            $tempArray['forwardPlayerPlayerId'] = @$player_ids[@$forward];
            $tempArray['forwardPlayerTeamId'] = @$team_ids[@$forward];
            //
            $tempArray['midfielderName'] = @$player_name[@$midfielder];
            $tempArray['midfielderFCommonName'] = @$player_common_name[@$midfielder];
            $tempArray['midfielderPosition'] = @$player_position[@$midfielder];
            $tempArray['midfielderScore'] = @$score[@$midfielder];
            $tempArray['midfielderTeamName'] = @$player_team_name[@$midfielder];
            $tempArray['midfielderImagePath'] = @$player_image[@$midfielder];
            $tempArray['midfielderIsLocalImage'] = @$player_is_local_image[@$midfielder];
            $tempArray['midfielderLocalImagePath'] = @$player_local_images[@$midfielder];
            $tempArray['midfielderPlayerPlayerId'] = @$player_ids[@$midfielder];
            $tempArray['midfielderPlayerTeamId'] = @$team_ids[@$midfielder];
//
            $tempArray['defenderName'] = @$player_name[@$defender];
            $tempArray['defenderFCommonName'] = @$player_common_name[@$defender];
            $tempArray['defenderPosition'] = @$player_position[@$defender];
            $tempArray['defenderScore'] = @$score[@$defender];
            $tempArray['defenderTeamName'] = @$player_team_name[@$defender];
            $tempArray['defenderImagePath'] = @$player_image[@$defender];
            $tempArray['defenderIsLocalImage'] = @$player_is_local_image[@$defender];
            $tempArray['defenderLocalImagePath'] = @$player_local_images[@$defender];
            $tempArray['defenderPlayerPlayerId'] = @$player_ids[@$defender];
            $tempArray['defenderPlayerTeamId'] = @$team_ids[@$defender];

            $tempArray['goalkeeperName'] = @$player_name[@$goalkeeper];
            $tempArray['goalkeeperFCommonName'] = @$player_common_name[@$goalkeeper];
            $tempArray['goalkeeperPosition'] = @$player_position[@$goalkeeper];
            $tempArray['goalkeeperScore'] = @$score[@$goalkeeper];
            $tempArray['goalkeeperTeamName'] = @$player_team_name[@$goalkeeper];
            $tempArray['goalkeeperImagePath'] = @$player_image[@$goalkeeper];
            $tempArray['goalkeeperIsLocalImage'] = @$player_is_local_image[@$goalkeeper];
            $tempArray['goalkeeperLocalImagePath'] = @$player_local_images[@$goalkeeper];
            $tempArray['goalkeeperPlayerPlayerId'] = @$player_ids[@$goalkeeper];
            $tempArray['goalkeeperPlayerTeamId'] = @$team_ids[@$goalkeeper];

            array_push($array, $tempArray);
            unset($tempArray);
        }
        return $array;
    }

    /**
     * Function to find position from Perfoamnce Buzz Data Object.      
     */
     public static function findValue($collectionArray, $sector) {

        foreach ($collectionArray as $key => $value) {
            if ($value == $sector) {
                return $key;
            }
        }
        /*foreach ($collectionArray as $key => $value) {
            if ($sector === 'Defender') {
                if ($value == 'Goalkeeper' || $value == 'Defender') {
                    return $key;
                }
            } else {
                if ($value == $sector) {
                    return $key;
                }
            }
        }*/
    }

    /**
     * Function to  get Performance Buzz records based on new positions logic. Older version of this funtion => performanceBuzzPlayers used to get performance buzz data based on calculations.
     * @param Array $request - Laravel Request variable which contains all request variable of that page
     * @param String $token - To verify if the User Requesting the data is a tier_2 user of tier_3 user.
     * @return Array - Performance Buzz Data Array.
     */
    public static function performanceBuzzPlayers_new($request, $token = null) {
        $isTeamName = false;
        $team_name = '';
        if ($request->has('team_name') && $request->team_name != '') {
            $isTeamName = true;
            $team_name = $request->team_name;
        }
        $isPlayerName = false;
        $player_name = '';
        if ($request->has('player_name') && $request->player_name != '') {
            $isPlayerName = true;
            $player_name = $request->player_name;
        }
        $isDate = false;
        $startDate = '';
        $endDate = '';
        if ($request->has('from') && $request->from != '') {
            $isDate = true;
            $startDate = $request->from;
            $endDate = $request->to;
        }

        $performance = PerformanceBuzz::leftJoin('players', 'players.id', 'performance_buzz.player_id')
                ->leftJoin('teams', 'teams.id', 'performance_buzz.team_id')
                // ->whereDate('date','2018-01-24')
                ->where('performance_buzz.position', '!=', 0)
                ->when($token != 'admin', function($query) {
                    $query->when((!\Auth::user()->isPremium()), function($query) {
                        $endDate = date('Y-m-d');
                        $startDate = date('Y-m-d', strtotime('-30 day', strtotime($endDate)));
                        $query->whereBetween(\DB::raw('DATE(date)'), [$startDate, $endDate]);
                    });
                })
                ->select('performance_buzz.date AS date', DB::raw('group_concat(IFNULL(performance_buzz.id,"")) as ids'), DB::raw('group_concat(IFNULL(performance_buzz.score,"")) as scores'), DB::raw('group_concat(IFNULL(players.id,"")) as player_ids'), DB::raw('group_concat(IFNULL(players.team_id,"")) as team_ids'), DB::raw('group_concat(IFNULL(players.common_name,"")) as player_names'), DB::raw('group_concat(IFNULL(performance_buzz.sector,""))as player_positions'), DB::raw('group_concat(IFNULL(players.football_index_common_name,"")) as player_football_index_common_names'), DB::raw('group_concat(IFNULL(players.image_path,"")) as player_images'), DB::raw('group_concat(IFNULL(players.is_local_image,"")) as is_local_images'), DB::raw('group_concat(IFNULL(players.local_image_path,"")) as player_local_images'), DB::raw('group_concat(IFNULL(teams.name,"")) as player_team_names'))
                ->when($isTeamName, function($query) use ($team_name) {
                    $query->havingRaw('LOCATE(\'' . $team_name . '\',player_team_names)>0');
                })
                ->when($isPlayerName, function($query) use ($player_name) {
                    $query->havingRaw('LOCATE(\'' . $player_name . '\',player_football_index_common_names)>0');
                })
                ->when($isDate, function($query) use ($startDate, $endDate) {
                    $query->whereRaw(DB::raw('DATE(date) BETWEEN \'' . $startDate . '\' AND \'' . $endDate . '\''));
                })
                ->orderBy('performance_buzz.date', 'DESC')
                ->groupBy('performance_buzz.date')
                ->simplePaginate(self::$perPage);
        //->paginate($this->perPage);
        // echo '<pre>'; print_r($performance); exit;
        return $performance;
    }

    /**
     * Function to  update the player league
     * @param Array $player_id - Player Id whose position is needed to be updated
     * @return Bool - 1 if player league updated, 0 if player league is not updated
     */
    public static function updatePlayerLeague($player_id) {
        $league_id = '';
        $player = Player::find($player_id);
        $future_fixture = Fixture::whereRaw('fixtures.starting_at > NOW()')
                ->whereRaw('(localteam_id=' . $player->team_id . ' OR visitorteam_id=' . $player->team_id . ')')
                ->orderBy('starting_at', 'asc')
                ->first();

        if ($future_fixture) {
            $league_id = $future_fixture->league_id;
        } else {
            $past_fixture = Fixture::whereRaw('fixtures.starting_at > NOW()')
                    ->whereRaw('(localteam_id=' . $player->team_id . ' OR visitorteam_id=' . $player->team_id . ')')
                    ->orderBy('starting_at', 'desc')
                    ->first();
            if ($future_fixture) {
                $league_id = $past_fixture->league_id;
            }
        }
        $player->league_id = $league_id;
        if ($player->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Function to  update the team league
     * @param Array $player_id - Player Id whose position is needed to be updated
     * @return Bool - 1 if player league updated, 0 if player league is not updated
     */
    public static function updateTeamLeague($team_id) {
        $league_id = '';
        $team = Team::find($team_id);
        $future_fixture = Fixture::whereRaw('fixtures.starting_at > NOW()')
                ->whereRaw('(localteam_id=' . $team->id . ' OR visitorteam_id=' . $team->id . ')')
                ->orderBy('starting_at', 'asc')
                ->first();

        if ($future_fixture) {
            $league_id = $future_fixture->league_id;
        } else {
            $past_fixture = Fixture::whereRaw('fixtures.starting_at > NOW()')
                    ->whereRaw('(localteam_id=' . $team->id . ' OR visitorteam_id=' . $team->id . ')')
                    ->orderBy('starting_at', 'desc')
                    ->first();
            if ($past_fixture) {
                $league_id = $past_fixture->league_id;
            }
        }
        $team->league_id = $league_id;
        if ($team->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function benner($type){ 
        $benners =Banner::where('type','=',$type)->get();
        return $benners;
    }

    public static function bennerCount($type){ 
        $benners = Banner::where('type','=',$type)->get()->count();
        return $benners;
    }
}
