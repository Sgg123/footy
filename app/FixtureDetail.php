<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FixtureDetail extends Model
{
    protected $fillable = [
    	'fixture_id',
    	'team_id', 
    	'player_id',
    	'number',
    	'position',
    	'additional_position',
    	'formation_position',
    	'posx',
    	'posy',
		'shots_total',
		'shots_on_goal',
		'goals_scored',
		'goals_conceded',
		'fouls_drawn',
		'fouls_committed',
		'yellowcards',
		'redcards',
		'total_crosses',
		'crosses_accuracy',
		'passes',
		'passes_accuracy',
		'assists',
		'offsides',
		'saves',
		'pen_scored',
		'pen_missed',
		'pen_saved',
		'tackles',
		'blocks',
		'interceptions',
		'clearances',
		'minutes_played'
    ];
}
