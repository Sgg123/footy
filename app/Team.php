<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Player;

class Team extends Model
{
    public $incrementing = false;
    
    protected $fillable = [
        'id',
    	'name', 
    	'twitter', 
    	'country_id',
    	'venue_id',
    	'national_team',
    	'founded',
    	'logo_path',
    	'local_image_path',
        'top_player_wins',
        'positional_wins',
        'top_player_dividend',
        'positional_dividend',
        'mb_first_count',
        'mb_second_count',
        'mb_third_count',        
        'mb_first_dividend',
        'mb_second_dividend',
        'mb_third_dividend',        
        'media_dividend',
        'performance_dividend',
    	'is_local_image',
        'last_win_date'
    ];
    public function getTeamPlayers() {

        return $this->hasMany(Player::class);
    }
}
