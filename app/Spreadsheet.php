<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spreadsheet extends Model
{
    protected $fillable = ['title', 'slug', 'file_name', 'file_path'];
}
