<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\League;

class Season extends Model {

    public $incrementing = false;
    protected $fillable = [
        'id',
        'league_id',
        'name',
        'type',
        'standing_active',
        'start_date',
        'end_date',
        'off_season_start_date',
        'off_season_end_date'
    ];

    public function getLeagueData() {

        return $this->belongsTo(League::class,'league_id','id');
    }

}
