<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\MediaBuzzPlayerScope;

class MediaBuzzPlayer extends Model
{
    protected $fillable = ['player_id', 'position', 'score', 'date', 'status','team_id'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new MediaBuzzPlayerScope);
    }
}
