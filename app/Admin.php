<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\AdminResetPasswordNotification;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;

class Admin extends Authenticatable implements CanResetPasswordContract
{
  
    use CanResetPassword, Notifiable;

    //Mass assignable attributes
  	protected $fillable = [
      'firstname', 'lastname', 'email', 'password', 'phone', 'status', 'image_path'
  	];

  	//hidden attributes
   	protected $hidden = [
       'password', 'remember_token',
   	];

   	//Send password reset notification
	public function sendPasswordResetNotification($token)
	{    
	    $this->notify(new AdminResetPasswordNotification($token));
	}
}
