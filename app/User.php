<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

class User extends Authenticatable
{
    use Notifiable;

    public $incrementing = false;
    
    //Mass assignable attributes
    protected $fillable = [
    'id', 'firstname', 'lastname', 'email', 'password', 'phone', 'status', 'image_path', 'stripe_card_id', 'stripe_id', 'card_brand', 'card_last_four', 'account_type', 'stripe_subscription_id', 'create_by_admin'
    ];

    //hidden attributes
    protected $hidden = [
       'password', 'remember_token',
    ];

    public function getSubscription()
    {
        if(empty($this->stripe_id) || empty($this->stripe_subscription_id))
        {
            return false;
        }
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        try {
            return $stripe->subscriptions()->find($this->stripe_id, $this->stripe_subscription_id);
        } catch (\Exception $e) {
            return false;
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            return false;
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            return false;
        }
    }
    
    public function isPremium()
    {
	// if($this->account_type == 'tier_3' && $this->create_by_admin == true)
 //        {
 //            return true;
 //        }
 //        if(empty($this->stripe_id) || empty($this->stripe_subscription_id))
 //        {
 //            return false;
 //        }
 //        $stripe = Stripe::make(env('STRIPE_SECRET'));
 //        try {
 //            $subscription = $stripe->subscriptions()->find($this->stripe_id, $this->stripe_subscription_id);
 //            if($subscription['status'] == 'trialing' || $subscription['status'] == 'active')
 //            {
 //                return true;
 //            }
 //            else
 //            {
 //                return false;
 //            }
 //        } catch (\Exception $e) {
 //            return false;
 //        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
 //            return false;
 //        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
 //            return false;
 //        }
        return true;
    }

    public function getSubscriptionStatus()
    {
        if(empty($this->stripe_id) || empty($this->stripe_subscription_id))
        {
            return false;
        }
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        try {
            $subscription = $stripe->subscriptions()->find($this->stripe_id, $this->stripe_subscription_id);
            return $subscription['status'];
        } catch (\Exception $e) {
            return false;
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            return false;
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            return false;
        }
    }
}
