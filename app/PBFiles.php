<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PBFiles extends Model
{
    protected $table = "performance_buzz_files";
    protected $fillable = ['file_name', 'date'];
}