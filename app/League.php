<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Season;

class League extends Model
{
	public $incrementing = false;
	
    protected $fillable = [ 
    	'id',
    	'legacy_id', 
    	'name', 
    	'is_cup', 
    	'country_id'
    ];

    public function getSeasons() {

        return $this->hasMany(Season::class);
    }

}
