<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Page;

class ShareWithViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share([
            'pages' => Page::get()->toArray()
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
