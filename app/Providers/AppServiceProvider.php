<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Spreadsheet;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $spreadsheets = Spreadsheet::get();
        View::share('spreadsheets', $spreadsheets);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
