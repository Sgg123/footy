<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ArticleCategoryScope;
use App\Article;

class ArticleCategories extends Model {

    protected $table = 'article_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'status','slug'
    ];

    protected static function boot() {
        parent::boot();

        static::addGlobalScope(new ArticleCategoryScope);
    }

    public function getArticles() {

        return $this->hasMany(Article::class, 'article_category_id');
    }

}
