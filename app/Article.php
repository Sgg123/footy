<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ArticleCategories;
use App\Scopes\ArticleScope;

class Article extends Model {

    protected $table = 'articles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_category_id', 'title', 'image_path', 'content', 'status', 'slug', 'thumbnail_image', 'youtube_video', 'meta_keywords', 'meta_description', 'date'
    ];

    protected static function boot() {
        parent::boot();

        static::addGlobalScope(new ArticleScope);
    }

    public function getArticlesCategory() {

        return $this->hasOne(ArticleCategories::class, 'id', 'article_category_id')->where('status', 'active');
    }

}
