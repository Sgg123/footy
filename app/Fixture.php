<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FixtureDetail;

class Fixture extends Model
{   
    public $incrementing = false;

    protected $fillable = [
        'id',
    	'league_id', 
    	'season_id', 
    	'stage_id',
    	'round_id',
    	'aggregate_id',
    	'venue_id',
    	'referee_id',
    	'localteam_id',
    	'visitorteam_id',
    	'pitch',
    	'status',
    	'starting_at',
    	'minute',
    ];
     public function getFixtureDetails() {

        return $this->hasMany(FixtureDetail::class);
    }

}
