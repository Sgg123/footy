<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashbackEmail extends Model
{
    protected $fillable = ['email', 'source', 'reference_code'];
}
