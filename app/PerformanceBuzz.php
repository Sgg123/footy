<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerformanceBuzz extends Model
{
    protected $table = "performance_buzz";
    protected $fillable = ['player_id', 'rank', 'score','sector','position','sector_rank','date','name'];
}
