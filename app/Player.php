<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Team;
use DB;

class Player extends Model {

    public $incrementing = false;
    protected $fillable = [
        'id',
        'team_id',
        'country_id',
        'position',
        'football_index_position',
        'common_name',
        'football_index_common_name',
        'firstname',
        'middlename',
        'lastname',
        'nationality',
        'birthdate',
        'birthcountry',
        'birthplace',
        'height',
        'weight',
        'image_path',
        'local_image_path',
    	'is_local_image',
        'form_guide',
        'top_player_wins',
        'positional_wins',
        'top_player_dividend',
        'positional_dividend',
        'mb_first_count',
        'mb_second_count',
        'mb_third_count',        
        'mb_first_dividend',
        'mb_second_dividend',
        'mb_third_dividend',        
        'media_dividend',
        'performance_dividend',
        'injured',
        'minutes',
        'appearences',
        'lineups',
        'substitute_in',
        'substitute_out',
        'substitutes_on_bench',
        'goals',
        'assists',
        'yellowcards',
        'yellowred',
        'redcards',
        'last_win_date'
        
    ];

    public function getTeam() {

        return $this->belongsTo(Team::class, 'team_id');
    }

    public function getTop5Avg() {
        $top_5_average = $this->select(DB::raw("(SELECT AVG(plr.`score`) FROM (SELECT perbuzz.score FROM `performance_buzz` as perbuzz WHERE perbuzz.player_id=".$this->player_id." ORDER BY perbuzz.score DESC LIMIT 5 ) AS plr ) AS top_5_average"))->first();
        return round($top_5_average->top_5_average);
    }
}