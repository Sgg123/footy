<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ArticleCategories;
use App\Scopes\ArticleScope;

class Banner extends Model {

    protected $table = 'banner';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'image', 'link'
    ];

}