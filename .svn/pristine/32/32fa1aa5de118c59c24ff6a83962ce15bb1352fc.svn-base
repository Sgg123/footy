<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Page;
use Helper;
use Auth;
use Youtube;

class PagesController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        View::share(['activeMenu' => 'pages']);
    }

    /**
     * This function is used to show pages list
     *
     * @return View
     */
    public function index() {
        $data['pages'] = Page::get();
        return view('admin.pages.list', $data);
    }

    public function create() {
        return abort('404');
    }

    public function store(Request $request) {
        return abort('404');
    }

    /**
     * This function is used to get youtube videos for specific channer
     * 
     * @param  string $pageId - next/previous page id
     * 
     * @return Object
     */
    public function getYouTubeVideos($pageId = null, $limit = null) {

        $params = [
            'channelId' => 'UCPa0_uGMI-cKV0pJ3sO1QBw',
            'type' => 'video',
            'part' => 'id, snippet',
            'maxResults' => $limit
        ];

        // Make inital search
        $data['vidoes'] = Youtube::paginateResults($params, $pageId);

        // Store token
        $data['nextPageToken'] = @$data['vidoes']['info']['nextPageToken'];
        $data['previousPageToken'] = @$data['vidoes']['info']['prevPageToken'];

        return $data;
    }

    public function show($id) {

        $page = Page::where('slug', $id)->firstOrFail();

        if ($page->id == 8) {

            return view('front.pages.cashback-page')->with('page', $page);
        } else if ($page->id == 7) {

            if (Auth::check()) {

                $data['page'] = $page;
                $ypage = \Illuminate\Support\Facades\Input::get('ypage') ?? null;
                $limit = 6;
                $data['youtube'] = $this->getYouTubeVideos($ypage, $limit);

                $data['googleTransferFeed'] = $this->getFeed("https://www.google.com/alerts/feeds/10415485569351908531/1864104971025935395");

                $transferRumours = $this->getFeed("http://talksport.com/rss/sports-news/transfer-rumours/feed");
                $data['transferRumours'] = (array) $transferRumours->channel;
                $data['transferRumours'] = (array) $data['transferRumours']['item'];
                return view('front.pages.media-page', $data);
            } else {
                return redirect('/home');
            }
        } else if ($page->id == 5) {

            if (Auth::check()) {
                View::share(['activeMenu' => 'media-buzz', 'activeSubMenu' => 'what-is-media-buzz']);

                $data['page'] = $page;

                return view('front.pages.media-buzz', $data);
            } else {
                return redirect('/home');
            }
        } else if ($page->id == 6) {

            if (Auth::check()) {
                View::share(['activeMenu' => 'performance-buzz', 'activeSubMenu' => 'what-is-performance-buzz']);
                return view('front.pages.media-buzz')->with('page', $page);
            } else {
                return redirect('/home');
            }
        } else if ($page->id == 1) {

            return view('front.pages.about')->with('page', $page);
        } else if ($page->id == 2) {

            return view('front.pages.contact-us')->with('page', $page);
        } else if ($page->id == 3) {

            return view('front.pages.privacy-policy')->with('page', $page);
        } else if ($page->id == 4) {

            return view('front.pages.faq')->with('page', $page);
        } else if($page->id == 11){
            return view('front.pages.others')->with('page', $page);
        }else {

            return abort('404');
        }
    }

    /**
     * This function is used to edit page content
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['page'] = Page::findOrFail(Helper::decrypt($id));

        return view('admin.pages.form', $data);
    }

    /**
     * This function is used to update page content
     *
     * @param  Request  $request - Request Object
     * @param  int  $id - Page ID
     * 
     * @return Response
     */
    public function update(Request $request, $id) {
        //upload image of size 1920*1280 for better experience.
        $this->validate($request, $this->validationRules());

        $page = Page::findOrFail(Helper::decrypt($id));
        $page->fill($request->only('title', 'slug', 'content','terms_conditions','meta_keywords','meta_description'));

        if ($request->hasFile('image')) {

            $this->removeImage($page->image_path);
            $imageName = time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('/uploads/pages/'), $imageName);
            $page->image_path = $imageName;
        }

        if ($page->save()) {

            $alert['status'] = 'success';
            $alert['message'] = 'Page updated successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'Something went wrong!';
        }

        return redirect()->route('pages.index')->with($alert);
    }

    /**
     * Remove old image
     * 
     * @param  string $image - Image name to delete
     */
    public function removeImage($image) {

        @unlink(public_path('/uploads/pages/') . $image);
    }

    public function destroy($id) {
        return abort('404');
    }

    /**
     * This function is used to get validation rules
     * 
     * @return Array
     */
    public function validationRules() {

        return [
            'title' => 'required|max:255',
            'slug' => 'required|max:255',
            'image' => 'mimes:jpeg,jpg,png|max:2048',
                //'content' => 'required',
        ];
    }

    public function getFeed($feedUrl) {

        $content = file_get_contents($feedUrl);
        $obj = new \SimpleXmlElement($content);

        return $obj;

        /* foreach($x->channel->item as $entry) {
          echo "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
          } */
    }

    /**
     * This function is used to get all youtube videos
     */
    public function loadAllYoutubeVideos() {

        View::share(['activeMenu' => 'media']);
        $ypage = \Illuminate\Support\Facades\Input::get('ypage') ?? null;
        $limit = 20;
        $data['youtube'] = $this->getYouTubeVideos($ypage, $limit);

        return view('front.pages.all-youtube-videos', $data);
    }

}
