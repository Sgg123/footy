<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\League;
use App\Season;
use App\Fixture;
use App\Team;
use App\Player;
use App\FixtureDetail;
use Helper;

class SportMonkFetcherController extends Controller {

    protected $apiToken = '';

    public function __construct() {

        $this->apiToken = 'P3RvBQG8M11B0b0enUzNEUb5HQRfTIrtRoRvniUNCV5mA3tSFKw3bG6fImFm';
    }

    public function index() {
        View::share(['activeMenu' => 'games', 'activeSubMenu' => 'fetch-api-data']);
        return view('admin.fixtures.sportsmonk-data');
    }

    /**
     * This function is used to get data from API from Admin Panel with specific dates
     * 
     * @return Response
     */
    public function getSportMonkApiData(Request $request) {
        $this->validate($request, ['start_date' => 'required', 'end_date' => 'required']);
        $result = 0;
        $begin = new \DateTime($request->start_date);
        $end = new \DateTime($request->end_date);
        $end = $end->modify('+1 day');

        $daterange = new \DatePeriod($begin, new \DateInterval('P1D'), $end);

        foreach ($daterange as $date) {
            $matches = $this->getUpcomingMatches($date->format("Y-m-d"));
            $this->dispatchMatcheObject($matches, $date->format("Y-m-d"));
            $result = 1;
        }

        if ($result == 1) {

            $alert['status'] = 'success';
            $alert['message'] = 'Fixture data updated successfully!';
        } else {

            $alert['status'] = 'danger';
            $alert['message'] = 'No data found!';
        }
        return redirect()->route('sportmonk.fetch.api.data')->with($alert);
    }

    /**
     * This function is used to get data from API
     * 
     * @return Response
     */
    public function getDataFromAPI($token) {

        if ($token == "upcoming") {
            $date = date('Y-m-d', strtotime(' +7 day'));
        } else {
            $date = date('Y-m-d', strtotime(' -2 day'));
        }

        $matches = $this->getUpcomingMatches($date);

        $this->dispatchMatcheObject($matches, $date);
    }

    public function getUpcomingMatches($date) {

        $url = "https://soccer.sportmonks.com/api/v2.0/fixtures/between/" . $date . "/" . $date . "?include=league,season,localTeam,visitorTeam,lineup&api_token=" . $this->apiToken;

        $matches = Helper::executeGet($url);

        return $matches;
    }

    public function dispatchMatcheObject($matches, $date) {

        if (@count(@$matches->data) > 0) {

            foreach ($matches->data as $match) {

                $league_ids = [2, 5, 564, 82, 384, 301, 8];
                if (in_array(@$match->league->data->id, $league_ids)) {

                    $this->storeLeague($match->league);
                    $this->storeSeason($match->season);
                    $this->storeFixture($match);
                    $this->storeTeam($match->localTeam, $date);
                    $this->storeTeam($match->visitorTeam, $date);
                    $this->addNewPlayer($match->lineup);
                    $this->storeFixtureDetail($match->lineup);
                }
            }
        }
    }

    /**
     * This function is used to store league detail.
     * 
     * @param Object $league - League Detail
     * 
     * @return Object
     */
    public function storeLeague($league) {

        if (@$league->data->id) {

            $league = League::updateOrCreate([
                        'id' => @$league->data->id
                            ], [
                        'id' => @$league->data->id,
                        'legacy_id' => @$league->data->legacy_id,
                        'name' => @$league->data->name,
                        'is_cup' => @$league->data->is_cup,
                        'country_id' => @$league->data->country_id
                            ]
            );

            return $league;
        }
    }

    /**
     * This function is used to store season detail.
     * 
     * @param Object $season - Season Detail
     * 
     * @return Object
     */
    public function storeSeason($season) {

        if (@$season->data->id) {

            $season = Season::updateOrCreate([
                        'id' => @$season->data->id
                            ], [
                        'id' => @$season->data->id,
                        'league_id' => @$season->data->league_id,
                        'name' => @$season->data->name,
                        'type' => @$season->data->type
                            ]
            );

            return $season;
        }
    }

    /**
     * This function is used to store fixture detail.
     * 
     * @param Object $fixture - Fixture Detail
     * 
     * @return Object
     */
    public function storeFixture($fixture) {

        if (@$fixture) {

            $fixture = Fixture::updateOrCreate([
                        'id' => @$fixture->id
                            ], [
                        'id' => @$fixture->id,
                        'league_id' => @$fixture->league_id,
                        'season_id' => @$fixture->season_id,
                        'stage_id' => @$fixture->stage_id,
                        'round_id' => @$fixture->round_id,
                        'group_id' => @$fixture->group_id,
                        'aggregate_id' => @$fixture->aggregate_id,
                        'venue_id' => @$fixture->venue_id,
                        'localteam_id' => @$fixture->localteam_id,
                        'visitorteam_id' => @$fixture->visitorteam_id,
                        'pitch' => @$fixture->pitch,
                        'status' => @$fixture->time->status,
                        'starting_at' => @$fixture->time->starting_at->date_time,
                        'minute' => @$fixture->time->minute
                            ]
            );

            return $fixture;
        }
    }

    /**
     * This function is used to store team detail.
     * 
     * @param Object $team - Team Detail
     * 
     * @return Object
     */
    public function storeTeam($team, $date) {
        $curdate = strtotime(date('Y-m-d'));
        $mydate = strtotime($date);

        if (@$team->data->id) {

            if ($mydate > $curdate) {

                //upcoming matches teams
                $teamObj = Team::updateOrCreate([
                            'id' => @$team->data->id
                                ], [
                            'id' => @$team->data->id,
                            'country_id' => @$team->data->country_id,
                            'name' => @$team->data->name,
                            'twitter' => @$team->data->twitter,
                            'venue_id' => @$team->data->venue_id,
                            'national_team' => @$team->data->national_team,
                            'founded' => @$team->data->founded,
                            'logo_path' => @$team->data->logo_path,
                                ]
                );

                //$this->getTeamSquad($team->data->id);
                //return $teamObj;
            } else {

                //Remove this once you have get exact data
                $teamObj = Team::updateOrCreate([
                            'id' => @$team->data->id
                                ], [
                            'id' => @$team->data->id,
                            'country_id' => @$team->data->country_id,
                            'name' => @$team->data->name,
                            'twitter' => @$team->data->twitter,
                            'venue_id' => @$team->data->venue_id,
                            'national_team' => @$team->data->national_team,
                            'founded' => @$team->data->founded,
                            'logo_path' => @$team->data->logo_path,
                                ]
                );

                //return $teamObj;
            }

            $this->getTeamSquad($team->data->id);
        }
    }

    /**
     * This function is used to check player detail exists or not.
     * 
     * @param Object $player - Player Detail
     * 
     */
    public function addNewPlayer($player) {


        if (count(@$player->data) > 0) {

            foreach (@$player->data as $player) {

                $playerObj = Player::find(@$player->player_id);

                if (!$playerObj) {

//                    dump(@$player);
                    $this->storePlayerData(@$player, 1, $player->team_id);
                } else {


                    if (@$player->position == "A") {
                        $position = "Forward";
                    } elseif (@$player->position == "D") {
                        $position = "Defender";
                    } elseif (@$player->position == "F") {
                        $position = "Forward";
                    } elseif (@$player->position == "G") {
                        $position = "Goalkeeper";
                    } elseif (@$player->position == "M") {
                        $position = "Midfielder";
                    } else {
                        $position = @$player->position;
                    }
                    $playerObj->position = @$position;
                    $playerObj->team_id = @$player->team_id;
                    $playerObj->save();
                }
            }
        }
    }

    /**
     * This function is used to store missing player detail.
     * 
     * @param $player_id - Player Id
     * 
     */
    public function storePlayerData($playerObj = null, $isNew = null, $teamId = null) {
        $injuredStatus = "false";
        if ($isNew == 1) {

            $url = "https://soccer.sportmonks.com/api/v2.0/players/" . @$playerObj->player_id . "?api_token=" . $this->apiToken . "&include=position,sidelined";
            $playerDetail = Helper::executeGet($url);
            $playerId = @$playerDetail->data->player_id;
            $team = @$playerDetail->data->team_id;
            $countryId = @$playerDetail->data->country_id;
            $commonName = @$playerDetail->data->common_name;
            $firstName = @$playerDetail->data->firstname;
            $lastName = @$playerDetail->data->lastname;
            $nationality = @$playerDetail->data->nationality;
            $birthCountry = @$playerDetail->data->birthcountry;
            $birthPlace = @$playerDetail->data->birthplace;
            $height = @$playerDetail->data->height;
            $weight = @$playerDetail->data->weight;
            $imagePath = @$playerDetail->data->image_path;
            $birthDate = date('Y-m-d', strtotime(@$playerDetail->data->birthdate));
            // LOGIC FOR INJURED STATUS
            $sidelined = @$playerDetail->data->sidelined->data;
            if (!empty(@$sidelined)) {
                foreach (@$sidelined as $val) {
                    if ($val->end_date != '') {
                        if ($val->end_date < date('Y-m-d')) {
                            $injuredStatus = "false";
                        } else {
                            $injuredStatus = "true";
                            break;
                        }
                    }
                }
            }
        } else {

            $playerDetail = $playerObj;
            $playerId = @$playerDetail->player_id;
            $team = $teamId;
            $commonName = @$playerDetail->player->data->common_name;
            $countryId = @$playerDetail->player->data->country_id;
            $firstName = @$playerDetail->player->data->firstname;
            $lastName = @$playerDetail->player->data->lastname;
            $nationality = @$playerDetail->player->data->nationality;
            $birthCountry = @$playerDetail->player->data->birthcountry;
            $birthPlace = @$playerDetail->player->data->birthplace;
            $height = @$playerDetail->player->data->height;
            $weight = @$playerDetail->player->data->weight;
            $imagePath = @$playerDetail->player->data->image_path;
            $birthDate = date('Y-m-d', strtotime(@$playerDetail->player->data->birthdate));
            // LOGIC FOR INJURED STATUS
            $sidelined = @$playerDetail->player->data->sidelined->data;
            if (!empty(@$sidelined)) {
                foreach (@$sidelined as $val) {
                    if ($val->end_date != '') {
                        if ($val->end_date < date('Y-m-d')) {
                            $injuredStatus = "false";
                        } else {
                            $injuredStatus = "true";
                            break;
                        }
                    }
                }
            }
        }

        if ($playerDetail) {

            if ($playerId != '') {
                if (@$playerDetail->player->data->position->data->name == 'Forwarder') {
                    @$position = 'Forward';
                } else if (@$playerDetail->player->data->position->data->name == 'Attacker') {
                    @$position = 'Forward';
                } else {
                    @$position = @$playerDetail->player->data->position->data->name;
                }
                $array = [
                    'id' => @$playerId,
                    'team_id' => @$team,
                    'country_id' => @$countryId,
                    'common_name' => @$commonName,
                    'firstname' => @$firstName,
                    'lastname' => @$lastName,
                    'nationality' => @$nationality,
                    'birthdate' => @$birthDate,
                    'birthcountry' => @$birthCountry,
                    'birthplace' => @$birthPlace,
                    'height' => @$height,
                    'weight' => @$weight,
                    'image_path' => @$imagePath,
                    'injured' => $injuredStatus,
                    'minutes' => @$playerDetail->minutes,
                    'appearences' => @$playerDetail->appearences,
                    'lineups' => @$playerDetail->lineups,
                    'substitute_in' => @$playerDetail->substitute_in,
                    'substitute_out' => @$playerDetail->substitute_out,
                    'substitutes_on_bench' => @$playerDetail->substitutes_on_bench,
                    'goals' => @$playerDetail->goals,
                    'assists' => @$playerDetail->assists,
                    'yellowcards' => @$playerDetail->yellowcards,
                    'yellowred' => @$playerDetail->yellowred,
                    'redcards' => @$playerDetail->redcards,
                ];
                $player = Player::find(@$playerId);
                if (empty($player)) {
                    $array['position'] = @$position;
                }
                $player = Player::updateOrCreate([
                            'id' => @$playerId
                                ], $array
                );
                unset($player);
            }
        }
    }

    /**
     * This function is used to store fixture lineups
     * 
     * @param Object $lineUp - Player object
     */
    public function storeFixtureDetail($lineUp) {

        if (count(@$lineUp->data) > 0) {

            foreach (@$lineUp->data as $playerDetail) {
                if (@$playerDetail->player_id != NULL) {
                    $player = FixtureDetail::updateOrCreate([
                                'fixture_id' => @$playerDetail->fixture_id,
                                'team_id' => @$playerDetail->team_id,
                                'player_id' => @$playerDetail->player_id
                                    ], [
                                'fixture_id' => @$playerDetail->fixture_id,
                                'team_id' => @$playerDetail->team_id,
                                'player_id' => @$playerDetail->player_id,
                                'number' => @$playerDetail->number,
                                'position' => @$playerDetail->position,
                                'additional_position' => @$playerDetail->additional_position,
                                'formation_position' => @$playerDetail->formation_position,
                                'posx' => @$playerDetail->posx,
                                'posy' => @$playerDetail->posy,
                                'shots_total' => @$playerDetail->stats->shots->shots_total,
                                'shots_on_goal' => @$playerDetail->stats->shots->shots_on_goal,
                                'goals_scored' => @$playerDetail->stats->goals->scored,
                                'goals_conceded' => @$playerDetail->stats->goals->conceded,
                                'fouls_drawn' => @$playerDetail->stats->fouls->fouls_drawn,
                                'fouls_committed' => @$playerDetail->stats->fouls->fouls_committed,
                                'yellowcards' => @$playerDetail->stats->cards->yellowcards,
                                'redcards' => @$playerDetail->stats->cards->redcards,
                                'total_crosses' => @$playerDetail->stats->passing->total_crosses,
                                'crosses_accuracy' => @$playerDetail->stats->passing->crosses_accuracy,
                                'passes' => @$playerDetail->stats->passing->passes,
                                'passes_accuracy' => @$playerDetail->stats->passing->passes_accuracy,
                                'assists' => @$playerDetail->stats->other->assists,
                                'offsides' => @$playerDetail->stats->other->offsides,
                                'saves' => @$playerDetail->stats->other->saves,
                                'pen_scored' => @$playerDetail->stats->other->pen_scored,
                                'pen_missed' => @$playerDetail->stats->other->pen_missed,
                                'pen_saved' => @$playerDetail->stats->other->pen_saved,
                                'tackles' => @$playerDetail->stats->other->tackles,
                                'blocks' => @$playerDetail->stats->other->blocks,
                                'interceptions' => @$playerDetail->stats->other->interceptions,
                                'clearances' => @$playerDetail->stats->other->clearances,
                                'minutes_played' => @$playerDetail->stats->other->minutes_played,
                                    ]
                    );
                }
            }
        }
    }

    /**
     * This function is used to execure cURL get request
     * 
     * @param  string $url - URL
     * 
     * @return Array of Object
     */
    public function getTeamSquad($teamId) {

        $url = "https://soccer.sportmonks.com/api/v2.0/teams/" . $teamId . "?api_token=" . $this->apiToken . "&include=squad.player.position,squad.player.sidelined";

        $teamSquad = Helper::executeGet($url);
        if (isset($teamSquad)) {
            foreach ($teamSquad->data->squad->data as $data) {

                $this->storePlayerData(@$data, null, $teamId);
            }
        }
    }

    /*
     * This function is used to get all data from Sports Monks Every Hour
     */

    public function getDataFromAPIAll() {
        error_log('getDataFromAPIAll');
        $date_future = date('Y-m-d', strtotime(' +15 day'));
        $date_back = date('Y-m-d', strtotime(' -5 day'));

        $begin = new \DateTime($date_back);
        $end = new \DateTime($date_future);
        $end = $end->modify('+1 day');

        $daterange = new \DatePeriod($begin, new \DateInterval('P1D'), $end);
        foreach ($daterange as $date) {
            $matches = $this->getUpcomingMatches($date->format("Y-m-d"));
            $this->dispatchMatcheObject($matches, $date->format("Y-m-d"));
        }
    }

}
