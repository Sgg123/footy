<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('players', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('team_id')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->string('position')->nullable();

            $table->string('common_name')->nullable();
            $table->string('football_index_common_name')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('nationality')->nullable();

            $table->date('birthdate')->nullable();
            $table->string('birthcountry')->nullable();
            $table->string('birthplace')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('image_path')->nullable();
            $table->text('local_image_path')->nullable();
            $table->boolean('is_local_image')->default(0);

            $table->integer('form_guide')->default(0);
            $table->integer('top_player_wins')->default(0);
            $table->integer('positional_wins')->default(0);
            $table->integer('mb_first_count')->default(0);
            $table->integer('mb_second_count')->default(0);
            $table->integer('mb_third_count')->default(0);
            $table->decimal('mb_first_dividend',11,2)->default(0);
            $table->decimal('mb_second_dividend',11,2)->default(0);
            $table->decimal('mb_third_dividend',11,2)->default(0);            
            $table->decimal('top_player_dividend',11,2)->default(0);
            $table->decimal('positional_dividend',11,2)->default(0);
            $table->decimal('media_dividend',11,2)->default(0);
            $table->decimal('performance_dividend',11,2)->default(0);

            $table->string('injured')->nullable();
            $table->string('minutes')->nullable();
            $table->string('appearences')->nullable();
            $table->string('lineups')->nullable();
            $table->string('substitute_in')->nullable();
            $table->string('substitute_out')->nullable();
            $table->string('substitutes_on_bench')->nullable();
            $table->string('goals')->nullable();
            $table->string('assists')->nullable();
            $table->string('goals')->nullable();
            $table->string('yellowcards')->nullable();
            $table->string('yellowred')->nullable();
            $table->string('redcards')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('players');
    }

}
