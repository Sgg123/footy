@extends('admin.layouts.app')

@section('page_css')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('backoffice-fis') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">
            <a href="{{ url()->route('pages.index') }}">Pages</a>
        </li>
        <li class="breadcrumb-item">{{ @$page->title }}</li>
    </ol>
    @include ('alerts.alert')
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Pages
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <form method="POST" action="{{ route('pages.update', ['id' => Helper::encrypt(@$page->id)]) }}" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}  
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="exampleInputName">Title <span class= "error">*</span></label>
                            <input class="form-control" id="title" name="title" type="text" placeholder="Title" maxlength="255" value="{{ old('title', @$page->title) }}">
                            <label id="title-error" class="error" for="title">{{ @$errors->first('title') }}</label>
                        </div>
                        <div class="col-md-6">
                            <label for="exampleInputLastName">Slug <span class= "error">*</span></label>
                            <input class="form-control" id="slug" name="slug" type="text" placeholder="Slug" maxlength="255" value="{{ old('slug', @$page->slug) }}">
                            <label id="slug-error" class="error" for="slug">{{ @$errors->first('slug') }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="exampleInputName">Banner Image <span class= "error">*</span></label> 
                            <small class="text-muted">( Use 1920 X 1280 size for better result.)</small>
                            <input class="form-control" id="image" name="image" type="file">
                            <label id="image-error" class="error" for="image">{{ @$errors->first('image') }}</label>
                        </div>
                        <div class="col-md-6">
                            @if(@$page->image_path != '' && file_exists(public_path('uploads/pages/'.@$page->image_path)))
                            <img src="{{ asset('uploads/pages/'.@$page->image_path) }}" class="img-thumbnail" alt="Cinque Terre">
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="">SEO Keywords</label> 
                        </div>
                        <div class="col-md-12">
                            <input class="form-control" id="meta_keywords" name="meta_keywords" type="text" placeholder="Slug" maxlength="255" value="{{ old('meta_keywords', @$page->meta_keywords) }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="">SEO Description</label> 
                        </div>
                        <div class="col-md-12">
                            <input class="form-control" id="meta_description" name="meta_description" type="text" placeholder="Slug" maxlength="255" value="{{ old('meta_description', @$page->meta_description) }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="exampleInputName">Content</label>
                            <textarea name="content" id="summernote" class="summernote">{{ old('content', @$page->content)}}</textarea>
                            <label id="content-error" class="error" for="content">{{ @$errors->first('content') }}</label>
                        </div>
                    </div>
                </div>

                @if($page->terms_conditions != "")
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="exampleInputName">Terms & Conditions</label>
                            <textarea name="terms_conditions"  class="summernote">{{ old('terms_conditions', @$page->terms_conditions)}}</textarea>
                            <label id="terms_conditions-error" class="error" for="terms_conditions">{{ @$errors->first('terms_conditions') }}</label>
                        </div>
                    </div>
                </div> 
                @endif

                <button class="btn btn-primary" type="submit">Update</button>
                <a class="btn btn-danger" href="{{ route('pages.index') }}">Cancel</a> 
            </form>

        </div>
    </div>
</div>

@endsection

@section('page_js')
<script src="{{ asset('assets/admin/js/jquery.seourl.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script src="{{ asset('assets/front/js/summernote-image-attributes.js') }}"></script>

<script type="text/javascript">
    $(function () {
        $('.summernote').summernote({
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ],
            },            
            imageAttributes:{
                icon:'<i class="note-icon-pencil"/>',                
            },
            height: 250,
            toolbar: [
                ['style', ['style']],
                ['group2', ['fontsize']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture', 'hr', 'link', 'video']],
                ['view', ['fullscreen', 'codeview']],
                ['table', ['table']]
            ],
            fontNames: ['Segoe UI', 'Arial', 'Helvetica', 'Comic Sans MS', 'Calibri', 'Consolas', 'Courier New', 'Garamond', 'Georgia', 'Lucida Console', 'Lucida Sans', 'Segoe UI', 'Tahoma',
                'Tempus Sans ITC', 'Times New Roman', 'Trebuchet MS', 'Verdana'],
        });
    });
    $(document).on("focusout", "#title", function () {
        var pageTitle = $(this).val().toLowerCase();
        var pageSlug = pageTitle.seoURL();
        $("#slug").val(pageSlug);
    });
</script>
@endsection