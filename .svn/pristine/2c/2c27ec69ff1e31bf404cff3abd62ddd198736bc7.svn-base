<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CashbackEmail;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Helper;
use Mail;

class CashbackEmailController extends Controller {

    protected $perPage = 20;

    public function __construct() {
        View::share(['activeMenu' => 'cahsback-emails']);
    }

    /**
     * This function is used to show email listing
     *
     * @return View
     */
    public function index() {
        $data['emails'] = CashbackEmail::paginate($this->perPage);
        return view('admin.cashback-emails.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return abort('404');
    }

    /**
     * Store paypal emails for cashback offer.
     *
     * @param  Request  $request - Request Object
     * 
     * @return Response
     */
    public function store(Request $request) {
        $cashback = CashbackEmail::firstOrNew(['email' => $request->email]);
        $cashback->source = $request->source;
        $cashback->reference_code = $request->reference_code != 'null' ? $request->reference_code : '';
        $cashback->save();

        $cashback_data['email'] = $request->email;
        $cashback_data['source'] = $request->source;
        $cashback_data['reference_code'] = $request->reference_code;

        // SEND EMAIL TO USER
        Mail::send('emails.cashback-email-to-user', array('cashback' => $cashback_data), function($message)use ($request) {
            $message->to($request->email)->subject('Footy Index Scout - Cashback Update');
            $message->from(config('mail.from.address'), config('mail.from.name'));
        });

        // SEND EMAIL TO ADMIN
        Mail::send('emails.cashback-email-to-admin', array('cashback' => $cashback), function($message)use ($request) {
            $message->to('info@footyindexscout.co.uk')->subject('A New Cashback Sign Up');
            $message->from(config('mail.from.address'), config('mail.from.name'));
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return abort('404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        return abort('404');
    }

    /**
     * This function is used to delete cashback offer email.
     *
     * @param  int  $id - ID
     * 
     * @return Response
     */
    public function destroy($id) {

        $email = CashbackEmail::findOrFail(Helper::decrypt($id));
        $email->delete();

        $alert['status'] = 'success';
        $alert['message'] = 'Email deleted successfully!';

        return redirect()->route('cashback-offer.index')->with($alert);
    }

}
