<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaBuzzPlayersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('media_buzz_players', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('player_id')->nullable();            
            $table->unsignedInteger('team_id')->nullable();            
            $table->enum('position', ['1', '2', '3']);
            $table->date('date');
            $table->string('score')->nullable();
            $table->enum('status', ['active', 'deleted'])->default('active');
            $table->timestamps();
            
            $table->foreign('player_id')->references('id')->on('players');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('media_buzz_players');
    }

}
