<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFirstpFieldToDayAndDividendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('day_and_dividends', function (Blueprint $table) {
            $table->string('first_p')->nullable();
            $table->string('second_p')->nullable();
            $table->string('fifth_p')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        $table->dropColumn('first_p');
        $table->dropColumn('second_p');
        $table->dropColumn('fifth_p');
    }
}
