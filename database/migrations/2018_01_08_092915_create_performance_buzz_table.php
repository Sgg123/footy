<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceBuzzTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance_buzz', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('player_id')->nullable();
            $table->integer('team_id')->nullable();
            $table->integer('score')->nullable();
            $table->string('sector')->nullable();
            $table->date('date');
            $table->integer('position');
            $table->enum('status', ['active', 'deleted'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance_buzz');
    }
}
