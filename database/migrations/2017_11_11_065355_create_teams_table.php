<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('teams', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('twitter')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('venue_id')->unsigned()->nullable();
            $table->boolean('national_team')->default(0);

            $table->string('founded')->nullable();
            $table->string('logo_path')->nullable();
            $table->text('local_image_path')->nullable();
            $table->boolean('is_local_image')->default(0);
            $table->integer('top_player_wins')->default(0);
            $table->integer('positional_wins')->default(0);
            $table->integer('mb_first_count')->default(0);
            $table->integer('mb_second_count')->default(0);
            $table->integer('mb_third_count')->default(0);
            $table->decimal('mb_first_dividend',11,2)->default(0);
            $table->decimal('mb_second_dividend',11,2)->default(0);
            $table->decimal('mb_third_dividend',11,2)->default(0);            
            $table->decimal('top_player_dividend',11,2)->default(0);
            $table->decimal('positional_dividend',11,2)->default(0);
            $table->decimal('media_dividend',11,2)->nullable()->default(0);
            $table->decimal('performance_dividend',11,2)->nullable()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('teams');
    }

}
