<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_category_id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->text('image_path');
            $table->text('thumbnail_image');
            $table->text('youtube_video');
            $table->longText('content');
            $table->enum('status', ['active', 'deleted']);
            $table->timestamps();
        });
        Schema::table('articles', function($table) {
            $table->foreign('article_category_id')->references('id')->on('article_categories');            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
