<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixtures', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('league_id')->unsigned();
            $table->foreign('league_id')->references('id')->on('leagues');

            $table->integer('season_id')->unsigned();
            $table->foreign('season_id')->references('id')->on('seasons');

            $table->integer('stage_id')->unsigned()->nullable();
            $table->integer('round_id')->unsigned()->nullable();
            $table->integer('aggregate_id')->unsigned()->nullable();
            $table->integer('venue_id')->unsigned()->nullable();
            $table->integer('referee_id')->unsigned()->nullable();

            $table->integer('localteam_id')->unsigned()->nullable();
            $table->integer('visitorteam_id')->unsigned()->nullable();

            $table->string('pitch')->nullable();
            $table->string('status')->nullable();

            $table->timestamp('starting_at')->nullable();
            $table->string('minute')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixtures');
    }
}
