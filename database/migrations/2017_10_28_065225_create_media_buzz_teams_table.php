<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaBuzzTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_buzz_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mb_first')->nullable();
            $table->integer('mb_second')->nullable();
            $table->integer('mb_third')->nullable();
            $table->integer('pb_top_team')->nullable();
            $table->integer('pb_top_defender')->nullable();
            $table->integer('pb_top_midfielder')->nullable();
            $table->integer('pb_top_forwarder')->nullable();
            $table->date('date');
            $table->enum('status', ['active', 'deleted'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_buzz_teams');
    }
}
