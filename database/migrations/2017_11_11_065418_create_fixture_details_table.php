<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixtureDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixture_details', function (Blueprint $table) {
            
            $table->increments('id');
         
            $table->unsignedInteger('fixture_id')->nullable();
            $table->foreign('fixture_id')->references('id')->on('fixtures');

            $table->integer('team_id')->unsigned()->nullable();
            $table->integer('player_id')->unsigned();

            $table->string('number')->nullable();
            $table->string('position')->nullable();
            $table->string('additional_position')->nullable();
            $table->string('formation_position')->nullable();
            $table->string('posx')->nullable();
            $table->string('posy')->nullable();
            $table->string('shots_total')->nullable();
            $table->string('shots_on_goal')->nullable();
            $table->string('goals_scored')->nullable();
            $table->string('goals_conceded')->nullable();
            $table->string('fouls_drawn')->nullable();
            $table->string('fouls_committed')->nullable();

            $table->string('yellowcards')->nullable();
            $table->string('redcards')->nullable();

            $table->string('total_crosses')->nullable();
            $table->string('crosses_accuracy')->nullable();
            $table->string('passes')->nullable();
            $table->string('passes_accuracy')->nullable();

            $table->string('assists')->nullable();
            $table->string('offsides')->nullable();
            $table->string('saves')->nullable();
            $table->string('pen_scored')->nullable();
            $table->string('pen_missed')->nullable();
            $table->string('pen_saved')->nullable();
            $table->string('tackles')->nullable();
            $table->string('blocks')->nullable();
            $table->string('interceptions')->nullable();
            $table->string('clearances')->nullable();
            $table->string('minutes_played')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixture_details');
    }
}
