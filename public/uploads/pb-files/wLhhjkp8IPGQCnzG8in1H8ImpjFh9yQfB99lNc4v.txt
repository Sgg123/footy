Player Name,Score,Position (Optional),Team (Optional)
Pierre Lees-Melou,216,,
Nicolas Pepe,194,,
Pablo Fornals,190,,
Florian Thauvin,183,,
Martin Hinteregger,182,,
Son Heung-min,182,,
Jean Seri,177,,
Goncalo Guedes,176,,
Jan Vertonghen,175,,
Toni Kroos,171,,
Malang Sarr,170,,
Manuel Lanzini,167,,
Sergio Asenjo,166,,
Gaetan Laborde,159,,
Rodrigo Moreno,159,,
Marwin Hitz,159,,
Timothy Fosu-Mensah,157,,
Timo Baumgartl,155,,
Philipp Max,155,,
Jonny Evans,155,,
Christian Eriksen,155,,
Benjamin Hubner,155,,
Mike Maignan,153,,
Kasper Schmeichel,153,,
Lucas Hernandez,152,,
Timo Werner,149,,
Diego Demme,149,,
James Ward-Prowse,148,,
Maxime Le Marchand,148,,
Marcelo,147,,
Wilfred Ndidi,143,,
Edgar Ie,140,,
Bruma,139,,
Morgan Sanson,138,,
Ben Davies,137,,
Antonio Rudiger,135,,
Harry Kane,135,,
Alvaro Gonzalez,135,,
Valere Germain,133,,
Benjamin Lecomte,132,,
Bernardo Espinosa,131,,
Marko Arnautovic,131,,
Marcel Sabitzer,130,,
Walter Benitez,129,,
Paul Baysse,129,,
Lucas Ocampos,128,,
Fabinho,128,,
Mario Gaspar,128,,
Mousa Dembele,128,,
Serge Aurier,127,,
Benoit Costil,127,,
Mark Noble,127,,
Daryl Janmaat,126,,
Jose Angel,124,,
Jorge Moraes,123,,
Matt Phillips,122,,
Zlatko Junuzovic,122,,
Abdoulaye Doucoure,121,,
Daniel Parejo,121,,
Thomas Partey,119,,
N'Golo Kante,119,,
Jerome Roussillon,117,,
Ahmed Hegazi,117,,
Kevin Kampl,117,,
Cesar Azpilicueta,117,,
Yassine Benzia,115,,
Davinson Sanchez,113,,
Rodrigo Hernandez,111,,
Eric Dier,111,,
Danijel Subasic,110,,
Jose Gaya,108,,
Ibrahim Amadou,108,,
Daniel Opare,108,,
Naldo,108,,
Craig Dawson,105,,
Junior Alonso,104,,
Adil Rami,104,,
Jordan Ayew,103,,
Patrick van Aanholt,103,,
Benjamin Stambouli,102,,
Jan Oblak,101,,
Daniel Carvajal,101,,
Marcos Alonso,101,,
Kevin Gameiro,101,,
Nordi Mukiele,100,,
Alexander Hack,100,,
Jaume Costa,100,,
Wayne Hennessey,100,,
Naby Keita,98,,
Nemanja Maksimovic,98,,
Francis Coquelin,98,,
Martin Kelly,98,,
Omar Mascarell,97,,
Ezequiel Garay,97,,
Florian Grillitsch,96,,
Luiz Gustavo,96,,
Isco,94,,
Thiago Mendes,94,,
Kevin Vogt,94,,
Kamil Glik,94,,
Waldemar Anton,93,,
Bernardo,92,,
Casemiro,92,,
Florian Kainz,91,,
Arthur Masuaku,90,,
Luka Modric,90,,
Oliver Sorg,89,,
Kieran Gibbs,89,,
Per Skjelbred,89,,
Rachid Ghezzal,88,,
Felix Klaus,87,,
Mehdi Abeid,87,,
Ronny Rodelin,86,,
Manu Trigueros,85,,
Nacho Fernandez,85,,
Victor Moses,85,,
Ralf Fahrmann,85,,
Diego Godin,85,,
Peter Gulacsi,84,,
Ron-Robert Zieler,84,,
Lucas Perez,84,,
Allan Saint-Maximin,83,,
Wesley Hoedt,83,,
Pedro Obiang,83,,
Jeffrey Gouweleeuw,83,,
Steven Zuber,83,,
Angelo Ogbonna,83,,
Luka Milivojevic,82,,
Christopher Schindler,82,,
Gabriel Paulista,82,,
Hugo Lloris,82,,
Dele Alli,81,,
Adama Soumaoro,81,,
Lewis Dunk,81,,
Baptiste Reynet,81,,
James Tomkins,81,,
Theodor Gebre Selassie,81,,
Harry Maguire,80,,
Riyad Mahrez,80,,
Hiroki Sakai,80,,
Dayot Upamecano,80,,
Dimitri Payet,80,,
Max Meyer,78,,
Sam Clucas,78,,
Maximilian Eggestein,77,,
Jose Gimenez,77,,
Marvin Plattenhardt,77,,
Juanfran,77,,
Cristiano Ronaldo,77,,
Gareth Barry,77,,
Matt Ritchie,76,,
Shane Duffy,75,,
Benjamin Pavard,74,,
Thomas Lemar,74,,
Ihlas Bebou,74,,
Tiemoue Bakayoko,74,,
Wilfried Zaha,74,,
Ermin Bicakcic,74,,
Matthias Ostrzolek,74,,
Vincent Bessat,74,,
Sime Vrsaljko,73,,
Alassane Plea,71,,
Lukasz Fabianski,71,,
Michael Gregoritsch,70,,
Luisinho,70,,
Malcom,69,,
Jack Stephens,69,,
Max Kruse,68,,
Willian,68,,
Almamy Toure,67,,
Salomon Rondon,67,,
Michael Esser,67,,
Mijat Gacinovic,66,,
Ayoze Perez,66,,
Thiago Maia,65,,
Samu Castillejo,65,,
Alfie Mawson,65,,
Frederic Guilbert,65,,
Steve Mandanda,65,,
James McCarthy,64,,
Ellyes Skhiri,64,,
Aaron Cresswell,64,,
Mason Holgate,63,,
Thibaut Courtois,63,,
Ximo Navarro,63,,
Christian Gunter,62,,
Ander Capa,62,,
Grzegorz Krychowiak,62,,
Takashi Inui,62,,
Cesc Fabregas,62,,
Renaud Cohade,62,,
Jamaal Lascelles,61,,
Andre Gray,61,,
Timothy Chandler,61,,
Bastian Oczipka,61,,
Victor Wanyama,60,,
Milos Veljkovic,59,,
Tom Carroll,59,,
Denis Cheryshev,59,,
Roberto Pereyra,58,,
DeAndre Yedlin,58,,
Anthony Knockaert,58,,
Christian Kabasele,58,,
Jay Rodriguez,58,,
Dennis Diekmeier,57,,
Richarlison,56,,
Youssef Ait Bennasser,56,,
Willi Orban,56,,
Antoine Griezmann,56,,
Jemerson,55,,
Aaron Mooy,55,,
Dani Garcia,55,,
Mamadou Samassa,55,,
Ashley Barnes,54,,
Nils Petersen,54,,
Kyriakos Papadopoulos,53,,
Koke,53,,
Dale Stephens,53,,
Daniel Caligiuri,53,,
Saul Niguez,52,,
Dennis Geiger,52,,
Mathew Leckie,52,,
Johann Berg Gudmundsson,52,,
Jordan Pickford,51,,
Oliver Baumann,51,,
Idrissa Gueye,51,,
Younousse Sankhare,51,,
Steven Defour,51,,
Benjamin Bourigeaud,50,,
Christian Atsu,50,,
Amine Harit,49,,
Christian Benteke,49,,
Mario Balotelli,49,,
Ben Mee,49,,
Marc Albrighton,49,,
Pedro,49,,
James Tarkowski,48,,
Damien Da Silva,48,,
Serge Gnabry,47,,
Ramy Bensebaini,45,,
Andreas Christensen,45,,
Mitchell Weiser,45,,
Joselu,45,,
Jean-Kevin Augustin,44,,
Santi Mina,44,,
Yussuf Poulsen,44,,
Raphael Varane,44,,
Martin Olsson,44,,
Anastasios Donis,43,,
Rony Lopes,43,,
Brice Samba,43,,
Oriol Romeu,43,,
Jamie Vardy,43,,
Filip Kostic,42,,
Oussama Haddadi,41,,
Jack Cork,41,,
Chadrac Akolo,40,,
Nicolas de Preville,40,,
Cheikhou Kouyate,40,,
Carlos Bacca,40,,
Wylan Cyprien,39,,
Jiri Pavlenka,39,,
Marco Asensio,38,,
Florin Andone,38,,
Pere Pons,38,,
Yannick Carrasco,38,,
Dwight Gayle,38,,
Jake Livermore,38,,
Keylor Navas,38,,
Ryan Bertrand,37,,
Dusan Tadic,36,,
Angel Correa,35,,
Georges-Kevin Nkoudou,35,,
Allan Nyom,35,,
Tom Ince,34,,
Matija Nastasic,33,,
Tomas Koubek,33,,
Emre Colak,33,,
Martin Montoya,32,,
Moussa Sissoko,32,,
Keita Balde Diao,32,,
Luciano Vietto,32,,
Cedric Soares,32,,
Giovanni Sio,32,,
Daniel Brosinski,32,,
Erik Lamela,31,,
Yoshinori Muto,31,,
Holger Badstuber,31,,
Niklas Stark,30,,
Oualid El Hajjam,30,,
Simone Zaza,30,,
Giulio Donati,30,,
Tom Cleverley,30,,
Ben Chilwell,29,,
Yves Bissouma,29,,
Pierre-Emile Hojbjerg,29,,
Stefan Ilsanker,28,,
Gary Cahill,27,,
Heurelho Gomes,27,,
Lukas Hradecky,26,,
Ivan Santini,26,,
Fernando Torres,26,,
Gylfi Sigurdsson,25,,
Celso Borges,25,,
Pablo De Blasis,25,,
Declan Rice,24,,
Bobby Wood,24,,
Jonjo Shelvey,24,,
Eden Hazard,24,,
Enes Unal,23,,
Isaac Mbenza,23,,
Samuel Grandsir,23,,
Alessandro Schopf,23,,
Davy Propper,23,,
Neto,23,,
Mikel Merino,22,,
Solly March,22,,
Ismaila Sarr,21,,
Clinton N'Jie,21,,
Andrej Kramaric,20,,
Alexander Djiku,19,,
Gideon Jung,19,,
Janik Haberer,19,,
Christophe Jallet,19,,
Orel Mangala,18,,
Kyle Walker-Peters,18,,
Maxime Lopez,18,,
Toni Lato,18,,
Luiz Araujo,18,,
Lukas Klostermann,18,,
Valentin Vada,18,,
Remi Walter,18,,
Vincent Koziello,18,,
Theo Pellenard,18,,
Yoan Cardinale,18,,
Yvon Mvogo,18,,
Kieran Trippier,18,,
Vitolo,18,,
Kostas Mitroglou,18,,
Yohan Cabaye,18,,
Ibrahima Konate,18,,
Francois Kamano,18,,
Anwar El Ghazi,18,,
Jerome Prior,18,,
Aymen Abdennour,18,,
David Timor,18,,
Guido Burgstaller,18,,
Hal Robson-Kanu,18,,
James McClean,18,,
Jonathan Viera,18,,
Javier Hernandez,18,,
Joe Hart,18,,
Fernando Llorente,18,,
Dante,18,,
Adama Diakhaby,16,,
Alexander Schwolow,16,,
Marko Dmitrovic,16,,
Douglas Luiz,15,,
Mat Ryan,15,,
Jonas Lossl,15,,
Douglas Santos,14,,
Pascal Gross,14,,
Mario Lemina,13,,
Robert Bauer,12,,
Terence Kongolo,11,,
Julian Korb,11,,
Tommy Smith,11,,
Troy Deeney,10,,
Wesley Said,9,,
Andre Ayew,9,,
Moussa Konate,8,,
Wilfried Bony,8,,
Stefano Okaka,7,,
Andre Hahn,5,,
Stevan Jovetic,5,,
Benjamin Andre,4,,
Salomon Kalou,4,,
Demarai Gray,3,,
Jeff Hendrick,3,,
Paul Lasne,3,,
Mark Uth,2,,
Sergi Enrich,2,,
Gareth Bale,2,,
Sofiane Boufal,1,,
Nick Pope,1,,
Wayne Rooney,1,,
Johannes Eggestein,0,,
Josh Sims,0,,
Luka Jovic,0,,
Stefan Posch,0,,
Kelechi Iheanacho,0,,
Pascal Stenzel,0,,
Marko Pjaca,0,,
Javier Manquillo,0,,
Michy Batshuayi,0,,
Nico Schulz,0,,
Manolo Gabbiadini,0,,
Marco Terrazzino,0,,
Leroy Fer,0,,
Roque Mesa,0,,
Bartosz Kapustka,0,,
Isaac Hayden,0,,
Chris Philipps,0,,
Davide Zappacosta,0,,
Djibril Sidibe,0,,
Guido Carrillo,0,,
Ivan Balliu,0,,
Federico Fernandez,0,,
Fraser Forster,0,,
Islam Slimani,0,,
David Luiz,0,,
Raul Lizoain,-1,,
Jonjoe Kenny,-3,,
Herve Bazile,-3,,
Morgan Schneiderlin,-3,,
Vicente Iborra,-4,,
Yevhen Konoplyanka,-5,,
Luciano Narsingh,-6,,
Jose Izquierdo,-7,,
Breel Embolo,-10,,
Alen Halilovic,-13,,
A�lvaro Morata,-13,,
Lucas Vazquez,-13,,
Ondrej Duda,-14,,
Theo Hernandez,-15,,
Joris Gnagnon,-15,,
Thilo Kehrer,-15,,
Mauricio Lemos,-15,,
Mateo Kovacic,-15,,
Nabil Bentaleb,-15,,
Steve Mounie,-15,,
Viktor Fischer,-15,,
Joel Robles,-15,,
Leandro Chichizola,-15,,
Oumar Niasse,-15,,
Vladimir Darida,-15,,
Matthew Lowton,-15,,
Sebastian Langkamp,-15,,
Thomas Kraft,-15,,
Kiko Casilla,-15,,
Faitout Maouassa,-15,,
Ademola Lookman,-15,,
Borja Mayoral,-15,,
Jesus Vallejo,-15,,
Davie Selke,-15,,
Jannik Huth,-15,,
Gonzalo Escalante,-15,,
Carles Gil,-15,,
Christian Mathenia,-15,,
Fabian Schar,-15,,
Gotoku Sakai,-15,,
Ashley Williams,-15,,
Stefan Bell,-19,,
Vedad Ibisevic,-19,,
Dominic Calvert-Lewin,-22,,
Sam Vokes,-29,,
Cenk Tosun,-46,,
Jonathan Calleri,-53,,
