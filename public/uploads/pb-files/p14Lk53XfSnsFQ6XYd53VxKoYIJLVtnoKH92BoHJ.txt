Player Name,Score,Position (Optional),Team (Optional)
James Rodriguez,201,,
Yannis Salibur,201,,
Franck Ribery,179,,
Arturo Vidal,143,,
Javi Martinez,121,,
Karl-Johan Johnsson,101,,
Niklas Sule,99,,
David Alaba,99,,
Adrian Gonzalez,84,,
Christophe Kerbrat,83,,
Rafael Ferreira,82,,
Lucas Deaux,78,,
Emiliano Martinez,77,,
Jonathan Tah,73,,
Arjen Robben,67,,
Luis Hernandez,64,,
Jerome Boateng,57,,
Kevin Volland,52,,
Federico Ricca,51,,
Sven Bender,49,,
Leon Bailey,49,,
Roberto Rosales,48,,
Ludovic Blas,42,,
Amath Ndiaye,40,,
Sven Ulreich,40,,
Thomas Muller,39,,
Lars Bender,37,,
Gonzalo Castro,37,,
Jonas Martin,36,,
Kai Havertz,35,,
Wendell,26,,
Benjamin Henrichs,25,,
Julian Brandt,20,,
Kingsley Coman,20,,
Joshua Kimmich,18,,
Corentin Tolisso,18,,
Juan Bernat,18,,
Bernd Leno,18,,
Sebastian Rudy,18,,
Sandro Wagner,15,,
Karim Bellarabi,8,,
Dominik Kohr,6,,
Adalberto Penaranda,5,,
Javier Ontiveros,-15,,
Yoann Salmier,-15,,
Charles Aranguiz,-15,,
Julian Baumgartlinger,-15,,
Admir Mehmedi,-16,,
