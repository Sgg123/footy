Player Name,Score,Position (Optional),Team (Optional)
Riyad Mahrez,278,,
Jorginho,264,,
Miralem Pjanic,260,,
Islam Slimani,249,,
Luka Milivojevic,228,,
Tiemoue Bakayoko,224,,
Neymar,224,,
Stefano Sensi,219,,
Alejandro Gomez,218,,
Daniel Parejo,212,,
Matt Doherty,208,,
Trent Alexander-Arnold,206,,
Amine Harit,206,,
Toni Kroos,203,,
Marco Verratti,202,,
Kai Havertz,199,,
Georginio Wijnaldum,199,,
Paulo Dybala,196,,
Alassane Plea,192,,
Juan Cuadrado,191,,
Willian,185,,
Thiago Silva,184,,
Mitchell Weiser,184,,
Philippe Coutinho,183,,
Serge Gnabry,181,,
Joel Matip,181,,
Sven Bender,180,,
Cristiano Ronaldo,180,,
Virgil van Dijk,177,,
Fabinho,177,,
Tanguy NDombele,176,,
Achraf Hakimi,176,,
Marcelo Brozovic,175,,
Angel Montoro,172,,
Yann Sommer,171,,
Wilfried Zaha,171,,
Thomas Meunier,171,,
Robin Knoche,168,,
Layvin Kurzawa,168,,
Gelson Martins,168,,
Aleksandar Dragovic,167,,
Marc-Andre ter Stegen,166,,
Arthur,164,,
Fernandinho,163,,
Wissam Ben Yedder,162,,
Alban Lafont,162,,
Xavier Chavalerin,161,,
Willy Boly,161,,
Nacho Fernandez,161,,
Andrew Robertson,159,,
Nadiem Amiri,158,,
Harry Kane,157,,
Matthijs de Ligt,156,,
Marquinhos,155,,
Abdou Diallo,155,,
Nico Elvedi,154,,
Ross Barkley,153,,
Mehdi Abeid,153,,
Lukasz Piszczek,153,,
Jerome Boateng,153,,
Habib Maiga,153,,
Fode Ballo-Toure,152,,
Kevin Volland,151,,
Andriy Yarmolenko,151,,
Stefan Lainer,150,,
Stanley N'Soki,150,,
Declan Rice,149,,
Cesc Fabregas,149,,
Mario Pasalic,146,,
Joshua Kimmich,146,,
Christian Eriksen,145,,
Idrissa Gueye,143,,
Rui Silva,142,,
Emiliano Buendia,142,,
Thorgan Hazard,141,,
Robin Gosens,141,,
Oscar Wendt,141,,
Oleksandr Zinchenko,140,,
Thomas Partey,139,,
Sada Thioub,139,,
Marcus Thuram,139,,
Denis Cheryshev,139,,
Yunis Abdelhamid,137,,
Blaise Matuidi,137,,
Andrei Girotto,137,,
Benoit Badiashile,136,,
Wylan Cyprien,135,,
Julian Baumgartlinger,135,,
Hugo Lloris,135,,
Raheem Sterling,134,,
Jadon Sancho,134,,
Niklas Sule,133,,
John McGinn,133,,
Alphonso Davies,133,,
Kwadwo Asamoah,132,,
Inigo Martinez,132,,
Gary Cahill,132,,
Aaron Cresswell,131,,
Salif Sane,130,,
Rui Patricio,130,,
Mario Gotze,130,,
Ederson,130,,
Daniel Carvajal,130,,
Kingsley Coman,129,,
Junior Firpo,129,,
Jakub Jankto,129,,
Duvan Zapata,129,,
Antonio Puertas,129,,
Matthias Ginter,128,,
Leonardo Bonucci,128,,
Charles Aranguiz,128,,
Thomas Mangani,127,,
Pablo Fornals,127,,
Marcos Alonso,127,,
Luis Suarez,127,,
Julio Tavares,127,,
Jeremiah St. Juste,127,,
Jaume Costa,126,,
Benoit Costil,126,,
Conor Coady,125,,
Kieran Trippier,124,,
Jack Grealish,124,,
Ryan Bertrand,123,,
Nayef Aguerd,123,,
Jamilu Collins,123,,
Cesar Azpilicueta,123,,
Sergio Busquets,122,,
Gerard Pique,122,,
Berat Djimsiti,122,,
Patrick van Aanholt,121,,
Joao Moutinho,121,,
Yangel Herrera,120,,
Willi Orban,120,,
Victor Diaz,120,,
Ryan Fredericks,120,,
Robert Lewandowski,120,,
Omar Mascarell,120,,
Kevin De Bruyne,120,,
Diego Rico,120,,
Nicolas Otamendi,119,,
Nathan Ake,119,,
Manuel Akanji,119,,
Lukas Hradecky,119,,
Luiz Araujo,119,,
Baptiste Santamaria,119,,
Rafael Toloi,118,,
Mat Ryan,118,,
Jonathan Silva,118,,
Anwar El Ghazi,118,,
Abdoulaye Doucoure,118,,
Rodrigo Hernandez,117,,
Roberto Gagliardini,117,,
Marco Sportiello,117,,
Jerome Roussillon,117,,
Jose Gimenez,117,,
Jasper Cillessen,117,,
Felipe Anderson,116,,
Suat Serdar,115,,
Remi Oudin,115,,
Walter Benitez,114,,
Vicente Guaita,114,,
Joel Ward,114,,
Gabriel Jesus,114,,
Domingos Duarte,114,,
Seamus Coleman,113,,
Max Gradel,113,,
Ilkay Gundogan,113,,
Rachid Alioui,112,,
Francis Coquelin,111,,
Etrit Berisha,110,,
Kepa Arrizabalaga,109,,
Josuha Guilavogui,109,,
James McArthur,109,,
Andros Townsend,109,,
Son Heung-min,108,,
Sergio Ramos,108,,
Rabbi Matondo,108,,
Mathieu Dossevi,108,,
Jonny Castro,108,,
Andreas Christensen,108,,
Jefferson Lerma,107,,
Gylfi Sigurdsson,107,,
Danny Rose,107,,
Marco Reus,106,,
Stefan Savic,105,,
Romain Saiss,105,,
Mohamed Drager,105,,
Manuel Neuer,105,,
Florian Neuhaus,105,,
William,104,,
Jan Vertonghen,104,,
Fernando Marcal,104,,
Darwin Machis,104,,
Christoph Kramer,104,,
Milot Rashica,103,,
Maximilian Arnold,103,,
Davy Klaassen,103,,
Bruno Ecuele Manga,103,,
Moritz Leitner,102,,
German Sanchez,102,,
Saul Niguez,101,,
Remo Freuler,101,,
Pierre Kunde,101,,
Frenkie de Jong,101,,
Daniel Brosinski,101,,
Regis Gurtner,100,,
Romain Thomas,100,,
Mahmoud Dahoud,100,,
Harry Winks,100,,
Hans Hateboer,100,,
Enda Stevens,100,,
Dennis Geiger,100,,
Benjamin Pavard,100,,
Thiago Mendes,99,,
Jeffrey Bruma,99,,
Alessandro Bastoni,99,,
Jemerson,98,,
Gianluigi Buffon,98,,
Abdoulaye Toure,98,,
Quentin Cornette,97,,
Nuri Sahin,97,,
Julian Weigl,97,,
Josh King,97,,
Jeffrey Schlupp,97,,
Dale Stephens,97,,
Pierre Lees-Melou,96,,
Kasper Dolberg,96,,
Axel Witsel,96,,
Kyle Walker,95,,
Jonathan Bamba,95,,
Bastian Oczipka,95,,
Alexis Sanchez,95,,
Alexander Nubel,95,,
Yeray Alvarez,94,,
Theodor Gebre Selassie,94,,
Memphis Depay,94,,
Adrien Rabiot,93,,
Pedro,92,,
Pedro Neto,92,,
Leander Dendoncker,92,,
Ferran Torres,92,,
Raphael Varane,91,,
Martin Montoya,91,,
Ludovic Butelle,91,,
Keylor Navas,91,,
Adama Traore,91,,
Robert Skov,90,,
Alfred Gomis,90,,
Marcelo Guedes,89,,
Leo Dubois,89,,
Nicola Murru,88,,
Jordan Pickford,88,,
Jan Oblak,88,,
Emil Forsberg,88,,
Denis Zakaria,88,,
Peter Gulacsi,87,,
Kylian Mbappe,87,,
Jordan Henderson,87,,
Dayot Upamecano,87,,
Benjamin Andre,87,,
Thibaut Courtois,86,,
Sergi Roberto,86,,
Callum Wilson,86,,
Angel di Maria,86,,
Rodrigo Moreno,85,,
Oscar Rodriguez,85,,
Mason Mount,85,,
Etienne Capoue,85,,
Moussa Doumbia,84,,
Kamil Glik,84,,
Josip Ilicic,84,,
Boubakary Soumare,84,,
Ben Godfrey,84,,
Yuri Berchiche,83,,
Stefan de Vrij,83,,
Nathan Redmond,83,,
Matthew Lowton,83,,
Keita Balde,83,,
Moussa Niakhate,82,,
Milan Skriniar,82,,
Marcel Halstenberg,82,,
Koke,82,,
Gareth Bale,81,,
Erik Pieters,81,,
Daniel Wass,81,,
Pierre-Emile Hojbjerg,80,,
Moussa Sissoko,80,,
Marcel Sabitzer,80,,
Javi Martinez,80,,
Frederic Guilbert,80,,
Ander Capa,80,,
Maya Yoshida,79,,
Kevin Akpoguma,79,,
Conor Hourihane,79,,
Ridle Baku,79,,
Youcef Atal,78,,
Matt Targett,78,,
Ghislain Konan,78,,
Casemiro,78,,
Callum Hudson-Odoi,78,,
Axel Disasi,78,,
Adama,78,,
Martin Kelly,77,,
Federico Valverde,77,,
Sami Khedira,76,,
Roque Mesa,76,,
Roberto Firmino,76,,
James Ward-Prowse,76,,
Felix Uduokhai,76,,
Toby Alderweireld,75,,
Tammy Abraham,75,,
Steve Cook,75,,
Jose Holebas,75,,
Vitolo,74,,
Karol Linetty,74,,
Florian Grillitsch,74,,
Danny Ings,74,,
Bartosz Bereszynski,74,,
Andrea Masiello,74,,
Alexandre Oukidja,74,,
Pedro Obiang,73,,
Thiago Alcantara,72,,
Samir Handanovic,72,,
Oliver Baumann,72,,
Jason Denayer,72,,
Ibrahim Amadou,72,,
Tyrone Mings,71,,
Oriol Romeu,71,,
Mohamed Salah,71,,
Kenny McLean,71,,
Jean-Paul Boetius,71,,
Lucas Hernandez,70,,
Jose Fonte,70,,
Craig Dawson,70,,
Aurelien Tchouameni,70,,
Alfred Duncan,70,,
Aaron Ramsey,70,,
Mouctar Diakhaby,69,,
Michael Lang,69,,
Lewis Dunk,69,,
Jordan Ayew,69,,
Jannik Huth,69,,
Gregoire Defrel,69,,
Dwight McNeil,69,,
Chris Wood,69,,
Jiri Pavlenka,68,,
Cheikhou Kouyate,68,,
Ibrahima Niane,67,,
Emiliano Rigoni,67,,
Vlad Chiriches,66,,
Robin Zentner,66,,
Jean-Victor Makengo,66,,
Ihlas Bebou,66,,
Antoine Griezmann,66,,
Richarlison,65,,
Otavio Santos,65,,
Oliver Norwood,65,,
Jamal Lewis,65,,
Eden Hazard,65,,
Divock Origi,65,,
Wesley Said,64,,
Uwe Hunemeier,64,,
Roberto Pereyra,64,,
Omar Colley,64,,
Maximilian Eggestein,64,,
Guilherme Arana,64,,
Gianmarco Ferrari,64,,
Gael Kakuta,64,,
Emil Audero,64,,
Albin Ekdal,64,,
Sebastien Haller,63,,
Lautaro Martinez,63,,
Jeremy Toljan,63,,
Habib Diallo,63,,
Federico Bernardeschi,63,,
Benjamin Stambouli,63,,
Philip Billing,62,,
Moses Simon,62,,
Fabian Delph,62,,
Erik Lamela,62,,
Romelu Lukaku,61,,
Laurent Koscielny,61,,
Josh Sargent,61,,
Jan Bednarek,61,,
Christopher Antwi-Adjei,61,,
Arturo Calabresi,61,,
Mateo Kovacic,60,,
Jonathan Ikone,60,,
Domenico Berardi,60,,
Craig Cathcart,60,,
Benjamin Lecomte,60,,
William Vainqueur,59,,
Ezequiel Garay,59,,
Renan Lodi,58,,
John Fleck,58,,
Antonio Candreva,58,,
Angelo Ogbonna,58,,
Rayan Ait Nouri,57,,
Nenad Tomovic,57,,
Jacques-Alaixys Romao,57,,
Alexandru Maxim,57,,
Unai Simon,56,,
Pascal Gross,56,,
Ibrahim Sangare,56,,
Bjorn Engels,56,,
Wendell,55,,
Mike Maignan,55,,
Lucas Digne,55,,
Kai Proger,55,,
Aaron Mooy,55,,
Tom Cleverley,54,,
Roman Burki,54,,
Martin Terrier,54,,
David Remeseiro,54,,
Issa Diop,54,,
Harry Wilson,54,,
Danny Welbeck,54,,
Adam Webster,54,,
Hamed Junior Traore,53,,
Timo Werner,52,,
Ronael Pierre Gabriel,52,,
Paul Lasne,52,,
Mathieu Cafaro,52,,
Julian Brandt,52,,
Jannik Vestergaard,52,,
Houssem Aouar,52,,
Dani Garcia,52,,
Angelo Fulgini,52,,
Roberto Soldado,51,,
Hamza Mendyl,51,,
Carles Perez,51,,
Benat Etxebarria,51,,
Ronaldo Vieira,50,,
Mahmoud Hassan,50,,
Chidozie Awaziem,50,,
Sadio Mane,49,,
Jonjoe Kenny,49,,
Dimitrios Siovas,49,,
Clement Lenglet,49,,
Yerry Mina,48,,
Samuel Kalu,48,,
Pavel Kaderabek,48,,
Max Aarons,48,,
Gabriel Paulista,48,,
Bakaye Dibassy,48,,
James Tarkowski,47,,
Bertrand Traore,47,,
Arkadiusz Reca,47,,
Weston McKennie,46,,
Karim Benzema,46,,
Guido Burgstaller,46,,
Wout Weghorst,45,,
Jay Rodriguez,45,,
Casimir Ninga,45,,
Aleksandr Golovin,45,,
Sofiane Boufal,43,,
Luka Modric,43,,
Christopher Nkunku,43,,
Pablo Sarabia,42,,
Nelson Semedo,42,,
Thomas Lemar,41,,
Sergio Aguero,41,,
Raul Jimenez,41,,
Oliver McBurnie,41,,
Amadou Haidara,41,,
Maxi Gomez,40,,
Jeff Hendrick,40,,
Iker Muniain,40,,
Victor Osimhen,39,,
Dominic Calvert-Lewin,39,,
Breel Embolo,39,,
Baptiste Reynet,39,,
Admir Mehmedi,39,,
Nordi Mukiele,38,,
Gerard Deulofeu,38,,
Diego Costa,38,,
Cauly Oliveira Souza,38,,
Ruben Sobrino,37,,
Lucas Alario,37,,
Stefan Posch,36,,
Sebastian Rudy,36,,
Dominic Solanke,36,,
Rodrigo Bentancur,35,,
Ibai Gomez,35,,
Emre Can,35,,
Arnaut Danjuma Groeneveld,35,,
Manuel Lanzini,34,,
Josip Brekalo,34,,
Anthony Lopes,34,,
Alexis Blin,34,,
Ruben Neves,33,,
Philipp Max,33,,
Musa Barrow,33,,
Morgan Gibbs-White,33,,
Ben Mee,33,,
Andrea Consigli,33,,
Predrag Rajkovic,32,,
Nicolo Barella,32,,
Martin Braithwaite,32,,
Leonardo Bittencourt,32,,
Bernardo Silva,32,,
Jonathan Tah,31,,
Adrien Silva,31,,
Paulinho,30,,
James McCarthy,30,,
Danilo D'Ambrosio,30,,
Alex Iwobi,30,,
Kenneth Omeruo,29,,
Jeremie Boga,29,,
James Rodriguez,29,,
Jack O'Connell,29,,
Felix Klaus,29,,
David Soria,29,,
Benjamin Hubner,29,,
Alvaro Vadillo,28,,
Thomas Muller,28,,
Mark Noble,28,,
Edimilson Fernandes,28,,
Damian Suarez,28,,
Alessandro Schopf,28,,
Yannick Gerhardt,26,,
Teemu Pukki,26,,
Ruben Aguilar,26,,
Raul Garcia,26,,
Ludovic Blas,26,,
Kenedy,26,,
Carlos Fernandez,26,,
Andre Gray,26,,
Daryl Janmaat,25,,
Daniel Caligiuri,25,,
Nick Pope,24,,
Max Meyer,24,,
Juan Bernat,24,,
Alessandro Murgia,24,,
Tomas Koubek,23,,
Patrick Herrmann,23,,
Jean-Clair Todibo,23,,
Ivan Rakitic,23,,
Enock Kwateng,23,,
Alex Oxlade-Chamberlain,23,,
Tony Jantschke,22,,
Sven Michel,22,,
Patrick Cutrone,22,,
George Baldock,22,,
Morgan Schneiderlin,21,,
David Silva,21,,
Jeff Reine-Adelaide,20,,
Gianluca Caprari,20,,
Dean Henderson,20,,
Michael Keane,19,,
Klaus Gjasula,19,,
Juan Ferney Otero,19,,
Francesco Vicari,19,,
Yunus Malli,18,,
Wojciech Szczesny,18,,
Willy Caballero,18,,
Wayne Hennessey,18,,
Victor Wanyama,18,,
Valentino Lazaro,18,,
Timothy Castagne,18,,
Sven Ulreich,18,,
Simon Kjaer,18,,
Sergio Rico,18,,
Sebastian Vasiliadis,18,,
Scott Dann,18,,
Ryan Bennett,18,,
Runar Runarsson,18,,
Ruben Vinagre,18,,
Romain Amalfitano,18,,
Rene Krhin,18,,
Renato Steffen,18,,
Reece James,18,,
Ramy Bensebaini,18,,
Presnel Kimpembe,18,,
Pierluigi Gollini,18,,
Phil Foden,18,,
Paulo Gazzaniga,18,,
Panagiotis Retsos,18,,
Neto,18,,
Moussa Wague,18,,
Moussa Diaby,18,,
Michy Batshuayi,18,,
Michael Cuisance,18,,
Merih Demiral,18,,
Maxwel Cornet,18,,
Maxime Gonalons,18,,
Mauro Icardi,18,,
Matteo Politano,18,,
Matija Nastasic,18,,
Matias Vecino,18,,
Matheus Cunha,18,,
Marten de Roon,18,,
Mark Uth,18,,
Laszlo Benes,18,,
Lucas Moura,18,,
Lee Kang-In,18,,
Leandro Paredes,18,,
Lars Bender,18,,
Kevin Mbabu,18,,
Kerem Demirbay,18,,
Juan Miranda,18,,
Jose Palomino,18,,
John Ruddy,18,,
Joel Pohjanpalo,18,,
Joe Gomez,18,,
Joao Cancelo,18,,
Jean-Kevin Augustin,18,,
James Tomkins,18,,
James Milner,18,,
Jaime Mata,18,,
Henry Onyekuru,18,,
Guillermo Maripan,18,,
Goncalo Guedes,18,,
Gonzalo Higuain,18,,
Francesco Rossi,18,,
Federico Dimarco,18,,
Elvis Rexhbecaj,18,,
Diego Godin,18,,
Diego Benaglio,18,,
Dele Alli,18,,
Dejan Lovren,18,,
David Alaba,18,,
Daniele Rugani,18,,
Cristiano Biraghi,18,,
Corentin Tolisso,18,,
Claudio Bravo,18,,
Christian Pulisic,18,,
Christian Benteke,18,,
Chris Basham,18,,
Carles Alena,18,,
Borja Valero,18,,
Benjamin Mendy,18,,
Benito Raman,18,,
Ben Davies,18,,
Arturo Vidal,18,,
Andrea Ranocchia,18,,
Ander Herrera,18,,
Adam Lallana,18,,
Marc Cucurella,17,,
Inaki Williams,17,,
Angus Gunn,17,,
Jack Cork,16,,
Gaetan Bong,16,,
Ben Foster,16,,
Andrea Petagna,16,,
Alexandre Mendy,16,,
Ashley Westwood,15,,
Yacine Adli,14,,
Joao Felix,14,,
Jacopo Sala,14,,
Ashley Barnes,14,,
Xeka,13,,
Ozan Kabak,13,,
Serge Aurier,12,,
Marco Stiepermann,12,,
Kelvin Amian,12,,
Guido Carrillo,12,,
Eric Dier,12,,
Callum Robinson,12,,
Inigo Lekue,11,,
Johannes Eggestein,11,,
Diego Demme,11,,
Angel Correa,10,,
Moussa Dembele,10,,
Marcos Llorente,10,,
Ignatius Ganago,10,,
Chadrac Akolo,10,,
Simon Francis,9,,
Todd Cantwell,8,,
Yussuf Poulsen,7,,
Samuel Grandsir,7,,
Neil Taylor,6,,
Luka Jovic,6,,
Djene Dakonam,6,,
Tin Jedvaj,5,,
Nemanja Maksimovic,5,,
Ruben Perez,4,,
Ismaila Sarr,4,,
Ademola Lookman,4,,
Ralf Fahrmann,3,,
Lewis Cook,3,,
Jose Arnaiz,3,,
Allan Nyom,3,,
Yves Bissouma,2,,
Tom Heaton,2,,
Tom Davies,2,,
Moise Kean,2,,
Robbie Brady,1,,
Loic Remy,1,,
Vinicius Junior,0,,
Thomas Monconduit,0,,
Thomas Delaney,0,,
Stuart Armstrong,0,,
Santiago Arias,0,,
Robert Snodgrass,0,,
Renaud Cohade,0,,
Renato Sanches,0,,
Pablo Zabaleta,0,,
Myziane Maolida,0,,
Milos Veljkovic,0,,
Mehmet Zeki Celik,0,,
Mateo Pavlovic,0,,
Marwin Hitz,0,,
Malang Sarr,0,,
Lucas Vazquez,0,,
Leonardo Balerdi,0,,
Jordon Ibe,0,,
Johann Berg Gudmundsson,0,,
Joe Hart,0,,
Jed Steer,0,,
Jacob Bruun Larsen,0,,
Hector Herrera,0,,
Fabian Balbuena,0,,
Ezri Konsa,0,,
Eder Militao,0,,
Douglas Luiz,0,,
Dan-Axel Zagadou,0,,
Chris Mepham,0,,
Cedric Soares,0,,
Ben Gibson,0,,
Bassem Srarfi,0,,
Artur Boruc,0,,
Antonio Adan,0,,
Antonin Bobichon,0,,
Andrew Surman,0,,
Alphonse Areola,0,,
Albian Ajeti,0,,
Adama Soumaoro,0,,
Raphael Guerreiro,-1,,
Lukasz Fabianski,-1,,
Jack Wilshere,-1,,
Marc Navarro,-2,,
Josip Drmic,-2,,
Bongani Zungu,-3,,
Anastasios Donis,-3,,
Danilo Barbosa,-6,,
Paco Alcacer,-7,,
Jurgen Locadia,-7,,
Francisco Portillo,-7,,
Patrick Roberts,-8,,
Glenn Murray,-8,,
Fabio Quagliarella,-8,,
Youssef Ait Bennasser,-9,,
Nicolas de Preville,-9,,
Jimmy Briand,-10,,
Youssef En-Nesyri,-11,,
Sergio Cordova,-11,,
Alfred Finnbogason,-11,,
Adam Szalai,-12,,
Reece Oxford,-14,,
Yvon Mvogo,-15,,
Yan Valery,-15,,
Xabier Etxeita,-15,,
Vukasin Jovanovic,-15,,
Vasco Regini,-15,,
Unai Nunez,-15,,
Thiago Cionek,-15,,
Theo Walcott,-15,,
Streli Mamba,-15,,
Stefan Ilsanker,-15,,
Simon Moore,-15,,
Sam Byram,-15,,
Rafael,-15,,
Nicolas Lemaitre,-15,,
Nathaniel Chalobah,-15,,
Muhamed Besic,-15,,
Mikel Vesga,-15,,
Michael Obafemi,-15,,
Michael Gregoritsch,-15,,
Mason Holgate,-15,,
Marlon Santos,-15,,
Marcelo Saracchi,-15,,
Manuel Locatelli,-15,,
Luke Freeman,-15,,
Lukas Klostermann,-15,,
Lucas Tousart,-15,,
Lucas Ribeiro,-15,,
Levin Oztunali,-15,,
Leandro Chichizola,-15,,
Laurent Jans,-15,,
Kevin Rodrigues,-15,,
Konstantinos Stafylidis,-15,,
Kiko Femenia,-15,,
Kevin Danso,-15,,
Karlo Letica,-15,,
Josh Maja,-15,,
Jonathan Burkardt,-15,,
Jonas Lossl,-15,,
Joachim Andersen,-15,,
Ibrahima Konate,-15,,
Iago Herrerin,-15,,
Havard Nordtveit,-15,,
Heurelho Gomes,-15,,
Hassane Kamara,-15,,
Grant Hanley,-15,,
Gaston Ramirez,-15,,
Francois Kamano,-15,,
Francesco Caputo,-15,,
Florian Muller,-15,,
Filippo Romagna,-15,,
Felipe Dal Bello,-15,,
Faycal Fajr,-15,,
Ethan Ampadu,-15,,
Ermin Bicakcic,-15,,
Domingos Quina,-15,,
Djibril Sidibe,-15,,
David Timor,-15,,
Cristian Battocchio,-15,,
Ciprian Tatarusanu,-15,,
Christian Strohdiek,-15,,
Christian Kabasele,-15,,
Cenk Tosun,-15,,
Andre Hahn,-15,,
Alireza Jahanbakhsh,-15,,
Alexander Hack,-15,,
Abdelhamid Sabiri,-15,,
Wesley,-17,,
Shane Long,-17,,
Neal Maupay,-17,,
Mauro Arambarri,-18,,
Stephan Lichtsteiner,-20,,
Lys Mousset,-22,,
Aritz Aduriz,-24,,
Angel Rodriguez,-30,,
Florian Niederlechner,-33,,
