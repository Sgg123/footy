Player Name,Score,Position (Optional),Team (Optional)
Jorginho,211,,
Jose Callejon,199,,
Aleksandar Kolarov,199,,
Daniele Baselli,196,,
Patrick Cutrone,190,,
Jordi Alba,187,,
Hakan Calhanoglu,184,,
Goncalo Guedes,175,,
Maxime Gonalons,172,,
Matias Vecino,165,,
David Lopez,163,,
Seko Fofana,161,,
Philippe Coutinho,158,,
Jose Gaya,154,,
Federico Viviani,150,,
Piotr Zielinski,145,,
Nelson Semedo,144,,
Luca Ceppitelli,144,,
Gaetano Letizia,143,,
Kostas Manolas,141,,
Kalidou Koulibaly,136,,
Giacomo Bonaventura,132,,
Benat Etxebarria,132,,
Rodrigo de Paul,131,,
Yerry Mina,130,,
Danilo D'Ambrosio,129,,
Alin Tosca,128,,
Mario Rui,128,,
Daniel Parejo,128,,
Diego Perotti,126,,
Elseid Hysaj,125,,
Davide Calabria,124,,
Nacho Vidal,123,,
Samir Santos,122,,
Bram Nuytinck,119,,
Pablo Piatti,119,,
Miranda,118,,
Gerard Pique,117,,
Raul Albiol,117,,
Berat Djimsiti,116,,
Alessio Cragno,115,,
Arkadiusz Milik,114,,
Marcelo Brozovic,110,,
Marc-Andre ter Stegen,110,,
Ezequiel Garay,110,,
Danilo Larangeira,110,,
Carlos Soler,109,,
Allan,109,,
Josip Ilicic,109,,
Gianluigi Donnarumma,108,,
Nemanja Maksimovic,108,,
Jeison Murillo,107,,
Leonardo Bonucci,107,,
Lorenzo Pellegrini,106,,
Lorenzo Insigne,104,,
Sergio Busquets,104,,
Alejandro Gomez,104,,
Gonzalo Escalante,102,,
Federico Fazio,102,,
Nicolas Nkoulou,101,,
Joao Cancelo,100,,
Iago Falque,100,,
Nikola Kalinic,99,,
Felipe Anderson,97,,
Simone Zaza,95,,
Samir Handanovic,95,,
Andres Iniesta,95,,
Andrea Barberis,93,,
Perparim Hetemaj,93,,
Emil Hallfredsson,92,,
Roberto Inglese,91,,
Luisinho,91,,
Pepe Reina,91,,
Marc Roca,90,,
Milan Skriniar,89,,
Diego Farias,89,,
Iker Muniain,88,,
Alessandro Florenzi,87,,
Kevin Strootman,87,,
Oscar de Marcos,87,,
Ricardo Rodriguez,84,,
Andrea Poli,84,,
Lucas Castro,84,,
Fernando Torres,84,,
Gabi,84,,
Manuel Locatelli,81,,
Alessio Romagnoli,81,,
Lucas Perez,81,,
Ivan Rakitic,81,,
Federico Mattiello,80,,
Danilo Cataldi,80,,
Antonin Barak,79,,
Rodrigo Moreno,79,,
Pedro Mosquera,79,,
Ousmane Dembele,78,,
Nenad Tomovic,77,,
Luis Suarez,77,,
Juanfran,76,,
Mauro Icardi,75,,
Jasmin Kurtic,75,,
Inigo Lekue,74,,
Andrea Bertolacci,73,,
David Zurutuza,72,,
Franck Kessie,71,,
Paolo Farago,71,,
Valter Birsa,71,,
Alex Berenguer,70,,
Adam Masina,69,,
Ivan Perisic,69,,
Kevin Lasagna,68,,
Rafinha,68,,
Kike Garcia,68,,
Ivan Radovanovic,68,,
Matteo Politano,67,,
Artur Ionita,66,,
Asier Illarramendi,66,,
Edin Dzeko,66,,
Saul Niguez,65,,
Nicolo Barella,64,,
Felipe Dal Bello,64,,
Leo Baptistao,63,,
Geoffrey Kondogbia,62,,
Tomas Rincon,61,,
Yeray Alvarez,60,,
Remo Freuler,58,,
Jakub Jankto,57,,
Luciano Vietto,57,,
Lorenzo Crisetig,57,,
Mikel Oyarzabal,56,,
Denis Suarez,56,,
Filipe Luis,56,,
Diego Laxalt,55,,
Gerard Moreno,55,,
Jose Palomino,55,,
Francesco Acerbi,55,,
Cristian Ansaldi,54,,
Alessandro Murgia,53,,
Unai Nunez,53,,
Lorenzo De Silvestri,53,,
Francesco Vicari,51,,
Vanja Milinkovic-Savic,50,,
Karol Linetty,50,,
Paulo Oliveira,50,,
Dani Garcia,50,,
Pierluigi Gollini,49,,
Bryan Dabo,49,,
Emre Colak,49,,
Marco Sau,49,,
Luca Cigarini,49,,
Patrik Schick,48,,
Zakaria Bakkali,48,,
Sergej Milinkovic-Savic,48,,
Alfred Duncan,48,,
Federico Ceccherini,48,,
Antonio Candreva,48,,
Giovanni Simeone,47,,
Alfred Gomis,47,,
Stephan El Shaarawy,47,,
Mattia Caldara,46,,
Koke,45,,
Dennis Praet,45,,
Diego Llorente,45,,
Jose Angel,45,,
Ivan Alejo,44,,
Manuel Lazzari,44,,
Angel Correa,43,,
Simone Verdi,43,,
Afriyie Acquah,43,,
Oscar Melendo,42,,
Joan Jordan,42,,
Mikel Vesga,41,,
Andrea Belotti,40,,
Adam Marusic,40,,
Marten de Roon,40,,
Lucas Leiva,40,,
Alessandro Bastoni,39,,
Yann Karamoh,39,,
Marko Dmitrovic,39,,
Marek Hamsik,39,,
Marko Rog,38,,
Stefan Savic,37,,
Raul Navas,37,,
Stefan Radu,37,,
Marco Sportiello,36,,
Lionel Messi,36,,
Samuel Bastien,35,,
Mariusz Stepinski,35,,
Bartosz Bereszynski,35,,
Aritz Elustondo,34,,
Bruno Martella,34,,
Dawid Kownacki,33,,
Federico Chiesa,32,,
Aaron Martin,32,,
Nicola Murru,32,,
Timo Letschert,32,,
Jan Oblak,32,,
Gerson,30,,
Vasco Regini,30,,
Andrea Ranocchia,30,,
Alberto de la Bella,30,,
Andre Silva,29,,
Kepa Arrizabalaga,28,,
Dries Mertens,28,,
Nikola Milenkovic,26,,
Inigo Cordoba,26,,
Stipe Perica,26,,
Silvan Widmer,26,,
Adrian Stoian,26,,
Alvaro Odriozola,24,,
Hans Hateboer,24,,
Leonardo Capezzi,23,,
Paco Alcacer,23,,
Maximiliano Olivera,23,,
Rolando Mandragora,22,,
Santi Mina,22,,
Adnan Januzaj,22,,
Sergi Darder,22,,
Juan Lopez,22,,
Mauricio Lemos,21,,
Daniel Bessa,21,,
Adrian Lopez,21,,
Ladislav Krejci,19,,
Sergio Canales,19,,
Vid Belec,19,,
Simone Scuffet,18,,
Marc Navarro,18,,
Mario Hermoso,18,,
Zinedine Machach,18,,
Pau Lopez,18,,
Pawel Jaroszynski,18,,
Roberto Gagliardini,18,,
Manuel Pucciarelli,18,,
Salvador Ichazo,18,,
Willian Jose,18,,
Martin Montoya,18,,
Mateo Musacchio,18,,
Neto,18,,
Rafael Cabral,18,,
Salvatore Sirigu,18,,
Tommaso Berni,18,,
Ferran Torres,18,,
Luca Pellegrini,18,,
Filippo Romagna,18,,
Giuseppe Pezzella,18,,
Jonathan Silva,18,,
M'Baye Niang,18,,
Dalbert,18,,
Francesco Zampano,18,,
Lucas Digne,18,,
Gustavo Gomez,18,,
Joel Obi,18,,
Juan Jesus,18,,
Davide Santon,18,,
Faouzi Ghoulam,18,,
Luigi Sepe,18,,
Lorenzo Tonelli,18,,
Jasper Cillessen,18,,
Fabrizio Cacciatore,18,,
Lucas Biglia,18,,
Daniele de Rossi,18,,
Andrea Pinamonti,18,,
Amadou Diawara,18,,
Cengiz Under,18,,
Antonio Barreca,18,,
Ali Adnan,18,,
Andre Gomes,18,,
Aleix Vidal,18,,
Anaitz Arbilla,18,,
Borja Valero,18,,
Thomas Strakosha,17,,
Maxim Koval,16,,
Inaki Williams,15,,
Marcello Trotta,15,,
Vitolo,15,,
Miguel Veloso,15,,
Armando Izzo,14,,
Luiz Felipe,13,,
Diego Costa,13,,
Timothy Castagne,12,,
Domenico Berardi,9,,
Jose Gimenez,7,,
Khouma Babacar,4,,
Mattia Perin,3,,
Cristiano Biraghi,2,,
Antoine Griezmann,1,,
Bryan Cristante,0,,
Marco Benassi,0,,
Thomas Partey,0,,
Ander Capa,0,,
Sergi Enrich,0,,
Ander Iturraspe,0,,
Yoel Rodriguez,0,,
Kevin Gameiro,0,,
Diego Godin,0,,
Valerio Verre,-1,,
Stefan de Vrij,-1,,
Gaston Ramirez,-1,,
Jordan Lukaku,-3,,
Francesco Rossi,-3,,
Senad Lulic,-3,,
Bruno Gaspar,-4,,
Aritz Aduriz,-4,,
Ciro Immobile,-5,,
Leonardo Pavoletti,-5,,
Nani,-5,,
Fabio Quagliarella,-7,,
Bartlomiej Dragowski,-11,,
Federico Di Francesco,-12,,
German Pezzella,-12,,
Massimo Coda,-14,,
Adam Nagy,-15,,
Lucas Torreira,-15,,
Wallace,-15,,
Mario Sampirisi,-15,,
Ruben Pardo,-15,,
Valentin Eysseric,-15,,
Nicolas Viola,-15,,
Milan Badelj,-15,,
Markel Susaeta,-15,,
Martin Caceres,-15,,
Gil Dias,-15,,
Godfred Donsah,-15,,
Igor Zubeldia,-15,,
Guido Guerrieri,-15,,
Jon Bautista,-15,,
Lucas Hernandez,-15,,
Emil Krafth,-15,,
Arlind Ajeti,-15,,
Andrea Tozzo,-15,,
Alberto Brignoli,-15,,
Diego Falcinelli,-15,,
Geronimo Rulli,-15,,
Gianmarco Ferrari,-15,,
Eugenio Lamanna,-15,,
Davide Di Gennaro,-15,,
Etrit Berisha,-15,,
Felipe Caicedo,-15,,
Iago Herrerin,-15,,
Ivan Strinic,-15,,
Andrea Consigli,-15,,
Marcus Rohden,-20,,
Riccardo Orsolini,-22,,
Gianluca Caprari,-59,,
