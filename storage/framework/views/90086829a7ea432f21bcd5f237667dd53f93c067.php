<?php if(@session('message')): ?>
<div class="alert alert-<?php echo e(session('status')); ?>">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
    <?php echo e(@session('message')); ?>

</div>
<?php endif; ?>