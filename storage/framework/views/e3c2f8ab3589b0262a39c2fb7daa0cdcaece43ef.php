<?php $__env->startSection('page_css'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Performance Buzz History</li>
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Performance Buzz History
            <a href="<?php echo e(route('performance-buzz.create')); ?>" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <div class="card-body">

            <form action="" class="row">
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="player_name" placeholder="Player name" value="<?php echo e(@Request::get('player_name')); ?>">
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="date" name="date" placeholder="Date" value="<?php echo e(@Request::get('date')); ?>">
                </div>
                <div class="col-sm-3">
                    <button type="submit" class="btn red-btn">Search</button>
                    <a href="<?php echo e(route('performance-buzz.index')); ?>" class="btn btn-danger">Reset</a>
                </div>
            </form>
            <br>

            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Date</th>   
                            <th>PLAYER NAME</th>
                            <th>TEAM NAME</th>
                            <th>SCORE</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!$pbData->isEmpty()): ?>
                        <?php $__currentLoopData = $pbData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td nowrap=""><?php echo e($history->date); ?></td>  
                            <td>
                                <?php echo e(@$history->football_index_common_name != '' ?  @$history->football_index_common_name : @$history->common_name); ?>

                            </td>
                            <td><?php echo e(@$history->name); ?></td>                                    
                            <td><?php echo e(@$history->score); ?></td>                                    
                            <td>
                                <a href="<?php echo e(route('performance-buzz.edit', ['id' => Helper::encrypt($history->id)])); ?>" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                <a href="javascript:destroy('<?php echo e(route('performance_buzz_data_delete', ['id' => Helper::encrypt($history->id)])); ?>')" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                        <tr>
                            <td colspan="8" class="text-center">No performance buzz history found!</td>
                        <tr>
                            <?php endif; ?>
                    </tbody>
                </table>
                <?php if(@Request::get('player_name') != '' || @Request::get('date') != ''): ?>
                <?php echo $pbData->appends(['player_name' => @Request::get('player_name'), 'date' => @Request::get('date')])->links(); ?>

                <?php else: ?>
                <?php echo $pbData->links(); ?>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>

<?php echo $__env->make('alerts.delete-confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $('.select2').select2({dropdownCssClass: 'bigdrop'});

    $("#date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });

    function destroy(url){
        var conf=confirm('Do you want to delete this performance buzz record?');
        if(conf==true){
            $.ajax({
                url: url,
                data: {},
                success: function(data){
                    alert('deleted');
                    location.reload();
                }
            })
        }
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>