<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <!--[if gte mso 9]><xml>
     <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
     </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <!--[if !mso]><!--><!--<![endif]-->
    <style type="text/css" id="media-query">
      body {
  margin: 0;
  padding: 0; }

table, tr, td {
  vertical-align: top;
  border-collapse: collapse; }

.ie-browser table, .mso-container table {
  table-layout: fixed; }

* {
  line-height: inherit; }

a[x-apple-data-detectors=true] {
  color: inherit !important;
  text-decoration: none !important; }

[owa] .img-container div, [owa] .img-container button {
  display: block !important; }

[owa] .fullwidth button {
  width: 100% !important; }

[owa] .block-grid .col {
  display: table-cell;
  float: none !important;
  vertical-align: top; }

.ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
  width: 540px !important; }

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
  line-height: 100%; }

.ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
  width: 180px !important; }

.ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
  width: 360px !important; }

.ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
  width: 270px !important; }

.ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
  width: 180px !important; }

.ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
  width: 135px !important; }

.ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
  width: 108px !important; }

.ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
  width: 90px !important; }

.ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
  width: 77px !important; }

.ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
  width: 67px !important; }

.ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
  width: 60px !important; }

.ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
  width: 54px !important; }

.ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
  width: 49px !important; }

.ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
  width: 45px !important; }

@media  only screen and (min-width: 560px) {
  .block-grid {
    width: 540px !important; }
  .block-grid .col {
    vertical-align: top; }
    .block-grid .col.num12 {
      width: 540px !important; }
  .block-grid.mixed-two-up .col.num4 {
    width: 180px !important; }
  .block-grid.mixed-two-up .col.num8 {
    width: 360px !important; }
  .block-grid.two-up .col {
    width: 270px !important; }
  .block-grid.three-up .col {
    width: 180px !important; }
  .block-grid.four-up .col {
    width: 135px !important; }
  .block-grid.five-up .col {
    width: 108px !important; }
  .block-grid.six-up .col {
    width: 90px !important; }
  .block-grid.seven-up .col {
    width: 77px !important; }
  .block-grid.eight-up .col {
    width: 67px !important; }
  .block-grid.nine-up .col {
    width: 60px !important; }
  .block-grid.ten-up .col {
    width: 54px !important; }
  .block-grid.eleven-up .col {
    width: 49px !important; }
  .block-grid.twelve-up .col {
    width: 45px !important; } }

@media (max-width: 560px) {
  .block-grid, .col {
    min-width: 320px !important;
    max-width: 100% !important;
    display: block !important; }
  .block-grid {
    width: calc(100% - 40px) !important; }
  .col {
    width: 100% !important; }
    .col > div {
      margin: 0 auto; }
  img.fullwidth, img.fullwidthOnMobile {
    max-width: 100% !important; }
  .no-stack .col {
    min-width: 0 !important;
    display: table-cell !important; }
  .no-stack.two-up .col {
    width: 50% !important; }
  .no-stack.mixed-two-up .col.num4 {
    width: 33% !important; }
  .no-stack.mixed-two-up .col.num8 {
    width: 66% !important; }
  .no-stack.three-up .col.num4 {
    width: 33% !important; }
  .no-stack.four-up .col.num3 {
    width: 25% !important; }
  .mobile_hide {
    min-height: 0px;
    max-height: 0px;
    max-width: 0px;
    display: none;
    overflow: hidden;
    font-size: 0px; } }

    </style>
    <title>Welcome to Footy Index Scout</title>
  </head>
  <body class="clean-body" style="margin: 0px; padding: 0px;"
    text="#000000" bgcolor="#FFFFFF">
    <style type="text/css" id="media-query-bodytag">
    @media (max-width: 520px) {
      .block-grid {
        min-width: 320px!important;
        max-width: 100%!important;
        width: 100%!important;
        display: block!important;
      }

      .col {
        min-width: 320px!important;
        max-width: 100%!important;
        width: 100%!important;
        display: block!important;
      }

        .col > div {
          margin: 0 auto;
        }

      img.fullwidth {
        max-width: 100%!important;
      }
            img.fullwidthOnMobile {
        max-width: 100%!important;
      }
      .no-stack .col {
                min-width: 0!important;
                display: table-cell!important;
            }
            .no-stack.two-up .col {
                width: 50%!important;
            }
            .no-stack.mixed-two-up .col.num4 {
                width: 33%!important;
            }
            .no-stack.mixed-two-up .col.num8 {
                width: 66%!important;
            }
            .no-stack.three-up .col.num4 {
                width: 33%!important;
            }
            .no-stack.four-up .col.num3 {
                width: 25%!important;
            }
      .mobile_hide {
        min-height: 0px!important;
        max-height: 0px!important;
        max-width: 0px!important;
        display: none!important;
        overflow: hidden!important;
        font-size: 0px!important;
      }
    }
  </style>
    <!--[if IE]><div class="ie-browser"><![endif]-->
    <!--[if mso]><div class="mso-container"><![endif]-->
    <table class="nl-container" style="border-collapse:
      collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace:
      0pt;mso-table-rspace: 0pt;vertical-align: top;min-width:
      320px;Margin: 0 auto;background-color: #FFFFFF;width: 100%"
      cellspacing="0" cellpadding="0">
      <tbody>
        <tr style="vertical-align: top">
          <td style="word-break: break-word;border-collapse: collapse
            !important;vertical-align: top">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #FFFFFF;"><![endif]-->
            <div style="background-color:transparent;">
              <div style="Margin: 0 auto;min-width: 320px;max-width:
                540px;overflow-wrap: break-word;word-wrap:
                break-word;word-break: break-word;background-color:
                transparent;" class="block-grid ">
                <div style="border-collapse: collapse;display:
                  table;width: 100%;background-color:transparent;">
                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 540px;"><tr class="layout-full-width" style="background-color:transparent;"><![endif]-->
                  <!--[if (mso)|(IE)]><td align="center" width="540" style=" width:540px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                  <div class="col num12" style="min-width:
                    320px;max-width: 540px;display:
                    table-cell;vertical-align: top;">
                    <div style="background-color: transparent; width:
                      100% !important;">
                      <!--[if (!mso)&(!IE)]><!-->
                      <div style="border-top: 0px solid transparent;
                        border-left: 0px solid transparent;
                        border-bottom: 0px solid transparent;
                        border-right: 0px solid transparent;
                        padding-top:5px; padding-bottom:5px;
                        padding-right: 0px; padding-left: 0px;"><!--<![endif]-->
                        <div class="img-container center fixedwidth "
                          style="padding-right: 0px; padding-left: 0px;"
                          align="center">
                          <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px;line-height:0px;"><td style="padding-right: 0px; padding-left: 0px;" align="center"><![endif]-->
                          <img class="center fixedwidth"
src="<?php echo e(asset('assets/front/images/FIS-Club-Badge-new.png')); ?>"
                            alt="Image" title="Image" style="outline:
                            medium none; text-decoration: none; clear:
                            both; display: block ! important; border:
                            0px none; float: none; max-width: 324px;"
                            width="222" height="291" border="0"
                            align="middle">
                          <!--[if mso]></td></tr></table><![endif]--> </div>
                        <div class="">
                          <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                          <div
                            style="color:#282F50;line-height:120%;font-family:Arial,
                            'Helvetica Neue', Helvetica, sans-serif;
                            padding-right: 10px; padding-left: 10px;
                            padding-top: 10px; padding-bottom: 10px;">
                            <div
style="font-size:12px;line-height:14px;font-family:inherit;color:#282F50;text-align:left;">
                              <p style="margin: 0;font-size:
                                14px;line-height: 17px;text-align:
                                center"><span style="font-size: 20px;
                                  line-height: 24px;">FOOTY INDEX SCOUT</span></p>
                            </div>
                          </div>
                          <!--[if mso]></td></tr></table><![endif]--> </div>
                        <div class="img-container right autowidth
                          fullwidth " style="padding-right: 5px;
                          padding-left: 5px;" align="right">
                          <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                          <div align="left"><font color="#282f50">Dear
                              <?php echo e(@$user['firstname']); ?> <?php echo e(@$user['lastname']); ?>,<br>
                              <br>
                              Thank you for signing up to Footy Index
                              Scout!<br>
                              <br>
                              To confirm your email address and activate
                              your account, please click on the link
                              below:<br>
                              <br>
                              <a href="<?php echo e(route('activate-user',[Helper::encrypt(@$user['email'])])); ?>"><b>Click here to confirm your
                                account</b></a><br>
                              <br>
                              Once completed, you are free to enjoy full
                              and unlimited access to all of our
                              dedicated Football INDEX tools and data!<br>
                              <br>
                              Many thanks,<br>
                              <br>
                              Footy Index Scout</font></div>
                          <div style="line-height:20px;font-size:1px"
                            align="left"><font color="#282f50">&nbsp;</font></div>
                          <!--[if mso]></td></tr></table><![endif]--> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div style="background-color:transparent;">
              <div style="Margin: 0 auto;min-width: 320px;max-width:
                540px;overflow-wrap: break-word;word-wrap:
                break-word;word-break: break-word;background-color:
                transparent;" class="block-grid two-up ">
                <div style="border-collapse: collapse;display:
                  table;width: 100%;background-color:transparent;">
                  <div class="col num6" style="max-width:
                    320px;min-width: 270px;display:
                    table-cell;vertical-align: top;">
                    <div style="background-color: transparent; width:
                      100% !important;">
                      <div style="border-top: 0px solid transparent;
                        border-left: 0px solid transparent;
                        border-bottom: 0px solid transparent;
                        border-right: 0px solid transparent;
                        padding-top:5px; padding-bottom:5px;
                        padding-right: 0px; padding-left: 0px;">
                        <!--[if (!mso)&(!IE)]><!--></div>
                      <!--<![endif]--> </div>
                  </div>
                  <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                </div>
              </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table><![endif]--> </td>
        </tr>
      </tbody>
    </table>
    <!--[if (mso)|(IE)]></div><![endif]-->
  </body>
</html>
