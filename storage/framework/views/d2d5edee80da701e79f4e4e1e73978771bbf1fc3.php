<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Cashback Offer Emails</li>
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Cashback Offer Emails
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Source</th>
                            <th>Referral Code</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count(@$emails) <= 0): ?>
                        <tr>
                            <td colspan="4" class="text-center">Sorry no cashback emails found!</td>
                        </tr>
                        <?php else: ?>  
                        <?php $__currentLoopData = @$emails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $email): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php 
                        	$id = Helper::encrypt($email->id);
                        ?>
                        <tr>
                            <td><?php echo e($email->email); ?></td>
                            <td><?php echo e($email->source); ?></td>
                            <td><?php echo e($email->reference_code); ?></td>
                            <td nowrap="">
                              <a href="javascript:void(0)" data-url="<?php echo e(route('cashback-offer.destroy', $id)); ?>" class="btn btn-sm btn-danger delete-list-record"><i class="fa fa-fw fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?> 
                    </tbody> 
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <?php echo e(@$emails->links()); ?>

                </div> 
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<?php echo $__env->make('alerts.delete-confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>