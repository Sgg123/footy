<!-- Delete confirm Modal-->
<div class="modal fade" id="deleteConfirmModal" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="POST" action="" id="delete-list-form">
    <?php echo e(method_field('DELETE')); ?>

    <?php echo e(csrf_field()); ?>

      <div class="modal-header">
        <h5 class="modal-title">Are you sure you want to delete this record?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">You can not recover deleted item.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
        <button class="btn btn-danger">Yes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function() {
    $(".delete-list-record").on("click", function(){
    	$deleteUrl = $(this).attr('data-url');
    	$("#delete-list-form").attr('action', $deleteUrl);
    	$("#deleteConfirmModal").modal();
    });
});
</script>