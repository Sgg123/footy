<div class="row m-0">
    <div class="col-sm-12 p-0">
            <!--<h2><a href="https://www.youtube.com/channel/UCPa0_uGMI-cKV0pJ3sO1QBw/videos/maxResults=6" target="_blank">WATCH US ON YOUTUBE <i class="fa fa-youtube-play"></i></a></h2>-->
        <h2><a href="http://gdata.youtube.com/feeds/base/users/UCPa0_uGMI-cKV0pJ3sO1QBw/uploads?max-results=1&start-index=1" target="_blank">WATCH US ON YOUTUBE <i class="fa fa-youtube-play"></i></a></h2>
    </div>
    <?php if(count(@$youtube['vidoes']['results']) > 0): ?>

    <?php $__currentLoopData = @$youtube['vidoes']['results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-sm-4 col-xs-6 r-full-width p-0">
        <figure class="gallery-figure">
            <img src="<?php echo e(@$video->snippet->thumbnails->high->url); ?>" alt="">
            <figcaption class="overlay">
                <div class="position-center-center">
                    <ul class="btn-list">
                        <li>
                            <a href="https://www.youtube.com/watch?v=<?php echo e(@$video->id->videoId); ?>" data-rel="prettyPhoto[video]" rel="prettyPhoto[video]"><i class="fa fa-video-camera"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </figcaption>
        </figure>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-sm-12 p-0 text-center">
        <a class="btn red-btn load_all_articles_btn" href="<?php echo e(route('youtube')); ?>">See More Videos</a>
    </div>
</div>
