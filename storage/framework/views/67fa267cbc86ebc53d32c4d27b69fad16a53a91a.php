 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<footer class="main-footer style-2">
     <div class="cookies-part" id="cookiesHolder">
    <div class="cookies-part-row">
      <div class="modal-body">
        <p>By continuing to browse this site you permit us and our partners to place identification cookies on your browser. Review our <a href="<?php echo url('/'); ?>/public/page/privacy-policy">privacy policy</a> for details.</p>
        <a id="cookiesAgree" onclick="checkCookie();">OK</a>
      </div>
    </div>
  </div>
        <!-- Footer Columns -->
        <div class="container">

            <!-- Footer columns -->
            <div class="footer-column border-0">
                <div class="row">
                    
                    <!-- Footer Column -->
                    <div class="col-sm-4 col-xs-6 r-full-width-2 r-full-width">
                        <div class="column-widget h-white">
                          <h5>Contact us</h5>
                            <div class="logo-column p-white">
                                <img style="width: 50%;" class="footer-logo" src="<?php echo e(asset('assets/front/images/Footy-Index-Scout-Logo-Main.png')); ?>" alt="">
                                <ul class="address-list style-2">
                                    <li><i class="fa fa-map-marker"></i><span>Address:</span>Summit House, 170 Finchley Road, London, United Kingdom, NW3 6BP</li>
                                    <li><i class="fa fa-envelope"></i><span>Help and Support:</span>info@footyindexscout.co.uk</li>
                                </ul>
                                <span class="follow-us">follow us </span>
                                <ul class="social-icons">
                                    <li><a class="facebook" target="_blank" href="https://www.facebook.com/footyindexscout/"><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="twitter" target="_blank" href="https://twitter.com/FootyIndexScout"><i class="fa fa-twitter"></i></a></li>
                                    <li><a class="youtube" target="_blank" href="https://www.youtube.com/c/footyindexscout"><i class="fa fa-youtube-play"></i></a></li>
                                    <li><a class="instagram" target="_blank" href="https://www.instagram.com/footyindexscout"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Footer Column -->

                    <!-- Footer Column -->
                    <div class="col-sm-4 col-xs-6 r-full-width-2 r-full-width">
                        <div class="column-widget h-white">
                         <h5>Navigation</h5>
                            <ul >
                                <li class="footer-menu-links"><a href="<?php echo e(route('page', [@$pages[0]['slug']])); ?>">Terms & Conditions</a></li>
                                <li class="footer-menu-links"><a href="<?php echo e(route('page', [@$pages[1]['slug']])); ?>">Contact</a></li>
                                <li class="footer-menu-links"><a href="<?php echo e(route('page', [@$pages[2]['slug']])); ?>">Privacy Policy</a></li>
                                <li class="footer-menu-links"><a href="<?php echo e(route('page', [@$pages[3]['slug']])); ?>">FAQ</a></li>
                            </ul> 
                        </div>
                    </div>
                    <!-- Footer Column -->

                    <!-- Footer Column -->
                    <div class="col-sm-4 col-xs-6 r-full-width-2 r-full-width">
                        <div class="column-widget h-white">
                            <!-- <h5>Sponsor</h5>
                            <ul id="brand-icons-slider-2" class="brand-icons-slider-2">
                                <li>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-1.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-2.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-3.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-4.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-5.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-6.png')); ?>" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-1.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-2.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-3.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-4.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-5.png')); ?>" alt=""></a>
                                    <a href="#"><img src="<?php echo e(asset('assets/front/images/brand-icons/img-1-6.png')); ?>" alt=""></a>
                                </li>
                            </ul> -->
                        </div>
                    </div>
                    <!-- Footer Column -->

                </div>
            </div>
            <!-- Footer columns -->

        </div>
        <!-- Footer Columns -->

        <!-- Copy Rights -->
        <div class="copy-rights">
            <div class="container">
                <p>© Copyright by <i class="red-color">Footy Index Scout</i> All rights reserved.</p>
                <br/>
                <p>Disclaimer: Footy Index Scout are committed to responsible gambling, all users must be 18+. For more information please visit gambleaware.co.uk. Gamble Responsibly.</p>
                <a class="back-to-top scrollup" href="#"><i class="fa fa-angle-up"></i></a>
            </div>
        </div>
        <!-- Copy Rights -->

    </footer> 
       <style type="text/css">
  
.cookies-part {
  background: #fff;
  bottom: 0;
  color: #fff;
  font-size: 16px;
  height: auto;
  left: 0;
  padding: 10px;
  position: fixed;
  text-align: center;
  width: 100%;
}
.modal-body p {
  display: inline;
  line-height: 18px;
}
.modal-body > form {
  display: inline-block;
}
.modal-body #cookiesAgree {
  background: #000000 none repeat scroll 0 0;
  color: #fff;
  display: inline-block;
  margin-left: 10px;
  text-align: center;
  width: 40px;
}
.modal-body a {
  color: #000;
  font-weight: bold;
  display: inline;
  cursor: pointer;
}

    </style>

    <script type="text/javascript">
    $(document).ready(function(){
      var x = getCookie('footyindex');
      console.log(x);
      if(x){
        $('#cookiesHolder').hide();
      }
    });
        function checkCookie() {
            document.cookie = "footyindex=true";
            $('#cookiesHolder').hide();
        }
        function getCookie(name) {
          var pair = document.cookie.match(new RegExp(name + '=([^;]+)'));
        return !!pair ? pair[1] : null;
}
    </script>