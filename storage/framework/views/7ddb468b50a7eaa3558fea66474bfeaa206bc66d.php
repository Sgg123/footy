<?php $__env->startSection('page_css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('assets/front/css/twitterfeed.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2><?php echo e(@$page->title); ?></h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Media Buzz</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg')); ?>">
</div>


<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                <?php echo $__env->make('front.pages.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-8">

                    <div class="team-detail-content theme-padding-bottom">
                        <div class="row m-0">
                            <div class="col-sm-12 p-0">
                                    <!--<h2><a href="https://www.youtube.com/channel/UCPa0_uGMI-cKV0pJ3sO1QBw/videos/maxResults=6" target="_blank">WATCH US ON YOUTUBE <i class="fa fa-youtube-play"></i></a></h2>-->
                                <h2><a href="http://gdata.youtube.com/feeds/base/users/UCPa0_uGMI-cKV0pJ3sO1QBw/uploads?max-results=1&start-index=1" target="_blank">WATCH US ON YOUTUBE <i class="fa fa-youtube-play"></i></a></h2>
                            </div>
                            <?php if(count(@$youtube['vidoes']['results']) > 0): ?>

                            <?php $__currentLoopData = @$youtube['vidoes']['results']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-sm-3 col-xs-6 r-full-width p-0">
                                <figure class="gallery-figure">
                                    <img src="<?php echo e(@$video->snippet->thumbnails->high->url); ?>" alt="">
                                    <figcaption class="overlay">
                                        <div class="position-center-center">
                                            <ul class="btn-list">
                                                <li>
                                                    <a href="https://www.youtube.com/watch?v=<?php echo e(@$video->id->videoId); ?>" data-rel="prettyPhoto[video]" rel="prettyPhoto[video]"><i class="fa fa-video-camera"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 p-0 text-center">
                                <div class="pagination-holder">
                                    <ul class="pagination">
                                        <li <?php echo e(@$youtube['previousPageToken'] ? '' : 'class=disabled'); ?>>
                                            <?php if(@$youtube['previousPageToken'] != ""): ?>
                                            <a href="<?php echo e(url()->current() . '?' . http_build_query(['ypage' => @$youtube['previousPageToken']])); ?>">
                                                <i class="fa fa-angle-double-left"></i>Previous
                                            </a>
                                            <?php else: ?>
                                            <a>
                                                <i class="fa fa-angle-double-left"></i>Previous
                                            </a>
                                            <?php endif; ?>

                                        </li>	

                                        <li <?php echo e(@$youtube['nextPageToken'] ? '' : 'class=disabled'); ?> >
                                            <?php if(@$youtube['nextPageToken'] != ""): ?>
                                            <a href="<?php echo e(url()->current() . '?' . http_build_query(['ypage' => @$youtube['nextPageToken']])); ?>">Next
                                                <i class="fa fa-angle-double-right"></i>
                                            </a>
                                            <?php else: ?>
                                            <a>Next
                                                <i class="fa fa-angle-double-right"></i>
                                            </a>
                                            <?php endif; ?>

                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<script src="<?php echo e(asset('assets/front/js/prettyPhoto.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('assets/front/js/twitterFetcher_min.js')); ?>"></script>

<script type="text/javascript">
    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
    animation_speed:'normal',
            theme:'dark_square',
            slideshow:3000,
            autoplay_slideshow:false,
            social_tools:false
    });
    jQuery("a[rel^='prettyPhoto']").prettyPhoto();</script>

<script>
    (function() {
    var config_footyindexscout = {
    "profile": {"screenName": 'footyindexscout'},
            "domId":'twitter-feed-footyindexscout',
            "maxTweets":3,
            "showUser": true,
            "showTime": false,
            "showRetweet": true,
            "showInteraction": false,
            "showImages": false,
            "linksInNewWindow": true,
    };
    function defer() {
    if (typeof twitterFetcher === 'object') {
    twitterFetcher.fetch(config_footyindexscout);
    } else {
    setTimeout(function() { defer(); }, 50);
    }
    }

    defer();
    })();
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>