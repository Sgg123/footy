
<nav id="menu" class="responive-nav">
    <a class="r-nav-logo" href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('assets/front/images/Footy-Index-Scout-Logo-Main.png')); ?>" alt=""></a>
    <?php if(auth()->guard()->guest()): ?>
  
    <ul class="respoinve-nav-list">
        <li class="<?php echo e(@$activeMenu == 'home' ? 'active' : ''); ?>">
            <a href="<?php echo e(url('')); ?>">Home</a>
        </li>
        <li>
            <a href="<?php echo e(route('page', [@$pages[7]['slug']])); ?>">Cashback Offer</a>
        </li>
        <li>
            <a href="<?php echo e(route('page', [@$pages[6]['slug']])); ?>" >Media</a>
        </li>  
        <li class="<?php echo e(@$activeSubMenu == 'articles' ? 'active' : ''); ?>">
            <a href="<?php echo e(route('articles')); ?>">Article</a>
        </li> 

        <li>
            <a data-toggle="collapse" href="#list-1"><i class="pull-right fa fa-angle-down"></i>Media Buzz</a>
            <ul class="collapse" id="list-1">
                <li>
                    <a href="<?php echo e(route('page', [@$pages[4]['slug']])); ?>">What is Media Buzz?</a></li>
                <li>
                    <a href="#" class="show-login-frm">Media Buzz Winner Player History</a>
                </li>
                <li>
                    <a href="#" class="show-login-frm">Media Buzz Winner Team History</a>
                </li>
            </ul>
        </li>
        <li>
            <?php @$todayDate = Helper::encrypt(date('Y-m-d')); ?>
            <a data-toggle="collapse" href="#list-2"><i class="pull-right fa fa-angle-down"></i>Performance Buzz</a>
            <ul class="collapse" id="list-2">
                <li class="<?php echo e(@$activeSubMenu == 'what-is-performance-buzz' ? 'active' : ''); ?>"><a href="<?php echo e(route('page', [@$pages[5]['slug']])); ?>">What is Performance Buzz?</a></li>
                <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-player' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Winner Player History</a></li>
                <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-team' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Winner Team History</a></li>
                <li class="<?php echo e(@$activeSubMenu == 'pb-planner' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Planner</a></li>
                <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-statistics' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Statistics</a></li>
                 <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-stats-history' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Scores History</a></li>
            </ul>
        </li>
        <li class="<?php echo e(@$activeMenu == 'database' ? 'active' : ''); ?>">
            <a href="#" class="show-login-frm">Database</a>
        </li>       
        <li class="<?php echo e(@$activeMenu == 'spreadsheet' ? 'active' : ''); ?>">
                        <a data-toggle="collapse" href="#list-3" class="show-login-frm"><i class="pull-right fa fa-angle-down"></i>Spreadsheet</a>

                        <ul class="collapse" id="list-3">
                            <?php $__currentLoopData = $spreadsheets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spreadsheet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="<?php echo e(@$activeSubMenu == 'spreadsheet' ? 'active' : ''); ?>">
                               <a href="#" class="show-login-frm"><?php echo e(@$spreadsheet->title); ?></a>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>

                    </li>                                                              
    </ul>
    <?php else: ?>
    <ul class="respoinve-nav-list">
        <li class="<?php echo e(@$activeMenu == 'home' ? 'active' : ''); ?>">
            <a href="<?php echo e(url('')); ?>">Home</a>
        </li>
        <li>
            <a href="<?php echo e(route('page', [@$pages[7]['slug']])); ?>">Cashback Offer</a>
        </li>
        <li>
            <a href="<?php echo e(route('page', [@$pages[6]['slug']])); ?>">Media</a>
        </li>  
        <li class="<?php echo e(@$activeSubMenu == 'articles' ? 'active' : ''); ?>">
            <a href="<?php echo e(route('articles')); ?>">Article</a>
        </li> 

        <li>
            <a data-toggle="collapse" href="#list-1"><i class="pull-right fa fa-angle-down"></i>Media Buzz</a>
            <ul class="collapse" id="list-1">
                <li>
                    <a href="<?php echo e(route('page', [@$pages[4]['slug']])); ?>">What is Media Buzz?</a></li>
                <li>
                    <a href="<?php echo e(route('media.buzz.player')); ?>">Media Buzz Winner Player History</a>
                </li>
                <li>
                    <a href="<?php echo e(route('media.buzz.team')); ?>">Media Buzz Winner Team History</a>
                </li>
            </ul>
        </li>
        <li>
            <?php @$todayDate = Helper::encrypt(date('Y-m-d')); ?>
            <a data-toggle="collapse" href="#list-2"><i class="pull-right fa fa-angle-down"></i>Performance Buzz</a>
            <ul class="collapse" id="list-2">
                <li class="<?php echo e(@$activeSubMenu == 'what-is-performance-buzz' ? 'active' : ''); ?>"><a href="<?php echo e(route('page', [@$pages[5]['slug']])); ?>">What is Performance Buzz?</a></li>
                <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-player' ? 'active' : ''); ?>"><a href="<?php echo e(route('performance.buzz.player')); ?>">Performance Buzz Winner Player History</a></li>
                <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-team' ? 'active' : ''); ?>"><a href="<?php echo e(route('performance.buzz.team')); ?>">Performance Buzz Winner Team History</a></li>
                <li class="<?php echo e(@$activeSubMenu == 'pb-planner' ? 'active' : ''); ?>"><a href="<?php echo e((\Auth::user()->isPremium()) ? route('pb.planner', @$todayDate) : route('pb.planner.upgrade')); ?>">Performance Buzz Planner</a></li>
                <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-statistics' ? 'active' : ''); ?>"><a href="<?php echo e((\Auth::user()->isPremium()) ? route('performance.buzz.statistics') : route('performance.buzz.statistics.upgrade')); ?>">Performance Buzz Statistics</a></li>
                 <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-stats-history' ? 'active' : ''); ?>"><a href="<?php echo e((\Auth::user()->isPremium()) ? route('performance.buzz.statshistory') : route('performance.buzz.statshistory.upgrade')); ?>">Performance Buzz Scores History</a></li>
            </ul>
        </li>
        <li class="<?php echo e(@$activeMenu == 'database' ? 'active' : ''); ?>">
            <a href="<?php echo e((\Auth::user()->isPremium()) ? route('database') : route('database.upgrade')); ?>">Database</a>
        </li>   

                    <li class="<?php echo e(@$activeMenu == 'spreadsheet' ? 'active' : ''); ?>">
                        <a data-toggle="collapse" href="#list-3" class="show-login-frm"><i class="pull-right fa fa-angle-down"></i>Spreadsheet</a>

                        <ul class="collapse" id="list-3">
                            <?php $__currentLoopData = $spreadsheets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spreadsheet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="<?php echo e(@$activeSubMenu == 'spreadsheet' ? 'active' : ''); ?>">
                               <a href="<?php echo e(url('spreadsheets/'.@$spreadsheet->file_path)); ?>"><?php echo e(@$spreadsheet->title); ?></a>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>

                    </li>                                                                  
    </ul>
    <?php endif; ?>
</nav>
