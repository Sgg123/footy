<?php $__env->startSection('content'); ?>
<meta name="keywords" content="<?php echo e(@$article['meta_keywords']); ?>" />
<meta name="description" content="<?php echo e(@$article['meta_description']); ?>" />
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Article Detail</h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li><a href="<?php echo e(url()->route('articles')); ?>">Articles</a></li>
            <li>Article Detail</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<!-- Page Heading banner -->
<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg')); ?>">
</div>
<!-- Page Heading banner -->

<!-- Main Content -->
<main class="main-content">

    <!-- Blog Detail -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Blog Content -->
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

                    <!-- Blog detail -->
                    <div class="blog-detail-holder">
                        <div class="author-header">
                            <h2><?php echo e($article['title']); ?></h2>
                            <div class="pull-left">
<!--                                <img src="images/aurthor-img.jpg" alt="">-->
                                <strong> <i class="red-color article-category-breadcrumb">Category / <a href="<?php echo e(route('articles.category',[@$article['getArticlesCategory']['slug']])); ?>"><?php echo e(@$article['getArticlesCategory']['title']); ?></a></i></strong>
                                <br>
                                <span>Posted on <?php echo e(date('d F Y',strtotime($article['date']))); ?>  </span>
                            </div>                           
                            <!--                            <div class="share-option pull-right">
                                                            <span id="share-btn1"><i class="fa fa-share-alt"></i>Share</span>
                                                            <div id="show-social-icon1" class="on-hover-share">
                                                                <ul class="social-icons">
                                                                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                                                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                                                    <li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>
                                                                    <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>-->
                        </div>

                        <div class="blog-detail articles_image">
                            <figure>
                                <img
                                    <?php if(file_exists('uploads/articles/' . @$article['image_path']) &&  @$article['image_path'] != ''): ?>
                                    src="<?php echo e(asset('uploads/articles/'.@$article['image_path'])); ?>"
                                    <?php else: ?>
                                    src="<?php echo e(asset('assets/images/no-image.jpeg')); ?>"
                                    <?php endif; ?>  alt="">
                            </figure>
                            <div>
                                <?php echo $article['content']; ?>

                            </div>
                            <?php if(@$article['youtube_video'] != ""): ?>

                            <?php
                            $url = @$article['youtube_video'];
                            preg_match(
                            '/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches );                        
                            $id = $matches[1];                        
                            ?>

                            <div class="container-fluid extra-v-set">
                                <object class="video-set-d" data="http://www.youtube.com/v/<?php echo e(@$id); ?>" type="application/x-shockwave-flash"><param name="src" value="http://www.youtube.com/v/<?php echo e(@$article['youtube_video']); ?>" /></object>
                            </div>
                            <?php endif; ?>
                        </div>                       
                    </div>
                    <!-- Blog Detail -->

                </div>
                <!-- Blog Content -->

                <!-- Aside -->
                <?php echo $__env->make('front.articles.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Aside -->

            </div>
        </div>
    </div>
    <!-- Blog Detail -->

</main>
<!-- Main Content -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<style type="text/css">
   .extra-v-set{padding-left:0} 
.video-set-d {width: 100%; min-height: 462px;}
</style>
<script type="text/javascript" src="<?php echo e(asset('assets/front/js/embed.js')); ?>"></script>
<script type="text/javascript">
    function storeData(payload) {

        var paypalEmail = payload.data.entry.YourPayPalEmail;
        var source = payload.data.entry.HowDidYouHearAboutUs;
        var referenceCode = payload.data.entry.FootyIndexScoutReferAFriendCode;

        $formData = new FormData();

        $formData.append('email', paypalEmail);
        $formData.append('source', source);
        $formData.append('reference_code', referenceCode);
        $URL = "<?php echo e(route('cashback-offer.store')); ?>";

        $.ajax({
            url: $URL,
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken
            },
            data: $formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {

            }
        });
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>