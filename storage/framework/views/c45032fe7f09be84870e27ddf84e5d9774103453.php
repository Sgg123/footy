<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?php echo e(route('fixtures')); ?>">Fixtures</a>
        </li>
        <li class="breadcrumb-item active">Fixture Details</li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-tags"></i> <?php echo e(@$localTeam[0]['name']); ?>  <b>vs</b>  <?php echo e(@$visitorTeam[0]['name']); ?>      
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <div class="row">
                    <div class="col-sm-6">
                        <table class="table table-bordered table-hover ">
                            <h3><span><?php echo e($fixture_details['local_team']); ?></span></h3>
                            <?php if(@$localTeam[0]['get_team_players']): ?>
                            <?php $__currentLoopData = @$localTeam[0]['get_team_players']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $player): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($player['firstname']); ?> <?php echo e($player['lastname']); ?></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <tr>
                                <td>No Players found.</td>
                            </tr>
                            <?php endif; ?>
                        </table>
                    </div>

                    <div class="col-sm-6">
                        <table class="table table-bordered table-hover">
                            <h3><span><?php echo e($fixture_details['visitor_team']); ?></span></h3>
                            <?php if(@$visitorTeam[0]['get_team_players']): ?>
                            <?php $__currentLoopData = @$visitorTeam[0]['get_team_players']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $player): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($player['firstname']); ?> <?php echo e($player['lastname']); ?></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <tr>
                                <td>No Players found.</td>
                            </tr>
                            <?php endif; ?>
                        </table>
                    </div>

                </div>
            </div>       
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>