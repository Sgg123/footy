<?php $__env->startSection('page_css'); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?php echo e(route('media-buzz-winner-team.index')); ?>">
                Media Buzz Winner Team History
            </a>
        </li>
        <li class="breadcrumb-item active">
            <?php echo e(@$teamHistory === null ? 'Add' : 'Edit'); ?>

        </li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> <?php echo e(@$teamHistory === null ? 'Add' : 'Edit'); ?> Media Buzz Winner Team History
        </div>
        <div class="card-body">
            <?php if(@$teamHistory): ?>
            <form method="POST" action="<?php echo e(route('media-buzz-winner-team.update', ['id' => Helper::encrypt(@$teamHistory->date)])); ?>">
                <?php echo e(method_field('PUT')); ?>

                <?php else: ?>
                <form method="POST" action="<?php echo e(route('media-buzz-winner-team.store')); ?>">
                    <?php endif; ?>
                    <input type="hidden" name="ids" value="<?php echo e(@$teamHistory->mb_ids); ?>">
                    <?php echo e(csrf_field()); ?>  
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-3  col-md-offset-1">
                                <label for="exampleInputName">MB 1st Place <span class= "error">*</span></label>
                                <select class="form-control select2" name="mb_first">
                                    <option value="">Select Team</option>
                                    <?php $__currentLoopData = @$teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($team['id']); ?>" <?php if($team['id'] == old('mb_first',@$teamIds[0])): ?> selected <?php endif; ?>><?php echo e($team['name']); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <label id="mb_first-error" class="error" for="mb_first"><?php echo e(@$errors->first('mb_first')); ?></label>
                            </div>
                            <div class="col-md-3 col-md-offset-2">
                                <label for="exampleInputPassword1">MB 2nd Place</label>
                                <select class="form-control select2" name="mb_second">
                                    <option value="">Select Team</option>
                                    <?php $__currentLoopData = @$teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($team['id']); ?>" <?php if($team['id'] == old('mb_second', @$teamIds[1])): ?> selected <?php endif; ?>><?php echo e($team['name']); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>

                                <label id="mb_second-error" class="error" for="mb_second"><?php echo e(@$errors->first('mb_second')); ?></label>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <label for="exampleInputPassword1">MB 3rd Place </label>
                                <select class="form-control select2" name="mb_third">
                                    <option value="">Select Team</option>
                                    <?php $__currentLoopData = @$teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($team['id']); ?>" <?php if($team['id'] == old('mb_third', @$teamIds[2])): ?> selected <?php endif; ?>><?php echo e($team['name']); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <label id="mb_third-error" class="error" for="mb_third"><?php echo e(@$errors->first('mb_third')); ?></label>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <label for="exampleInputPassword1">MB 4th Place </label>
                                <select class="form-control select2" name="mb_fourth">
                                    <option value="">Select Team</option>
                                    <?php $__currentLoopData = @$teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($team['id']); ?>" <?php if($team['id'] == old('mb_fourth', @$teamIds[3])): ?> selected <?php endif; ?>><?php echo e($team['name']); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <label id="mb_fourth-error" class="error" for="mb_fourth"><?php echo e(@$errors->first('mb_fourth')); ?></label>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <label for="exampleInputPassword1">MB 5th Place </label>
                                <select class="form-control select2" name="mb_fifth">
                                    <option value="">Select Team</option>
                                    <?php $__currentLoopData = @$teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($team['id']); ?>" <?php if($team['id'] == old('mb_fifth', @$teamIds[4])): ?> selected <?php endif; ?>><?php echo e($team['name']); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <label id="mb_fifth-error" class="error" for="mb_fifth"><?php echo e(@$errors->first('mb_fifth')); ?></label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit"><?php echo e(@$teamHistory === null ? 'Save' : 'Update'); ?></button>
                    <a class="btn btn-danger" href="<?php echo e(route('media-buzz-winner-team.index')); ?>">Cancel</a> 
                </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $("#mb_date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });
    $('.select2').select2({dropdownCssClass: 'bigdrop'});

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>