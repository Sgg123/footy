<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?php echo e(route('article-category.index')); ?>">
                Article Categories
            </a>
        </li>
        <li class="breadcrumb-item active"><?php echo e(@$categories === null ? 'Add' : 'Edit'); ?></li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> <?php echo e(@$categories === null ? 'Add' : 'Edit'); ?> Article Categories
        </div>
        <div class="card-body">
            <?php if(@$categories): ?>
            <form method="POST" action="<?php echo e(route('article-category.update', ['id' => Helper::encrypt(@$categories->id)])); ?>">
                <?php echo e(method_field('PUT')); ?>

                <?php else: ?>
                <form method="POST" action="<?php echo e(route('article-category.store')); ?>">
                    <?php endif; ?>
                    <?php echo e(csrf_field()); ?>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Title <span class= "error">*</span></label>
                                <input class="form-control" id="title" name="title" type="text" aria-describedby="nameHelp" placeholder="Enter title" value ="<?php echo e(@$categories === null ? old('title') : @$categories->title); ?>">
                                <label id="title-error" class="error" for="title"><?php echo e(@$errors->first('title')); ?></label>
                            </div>
                        </div>
                    </div>         
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Slug <span class= "error">*</span></label>
                                <input class="form-control pointer-disabled" name="slug" type="text" aria-describedby="nameHelp" placeholder="" value ="<?php echo e(@$categories === null ? old('slug') : @$categories->slug); ?>" id="slug" readonly>
                                <label id="slug-error" class="error" for="slug"><?php echo e(@$errors->first('slug')); ?></label>
                            </div>
                        </div>
                    </div>         
                    <input type="submit" value="<?php echo e(@$categories === null ? 'Save' : 'Update'); ?>" class="btn btn-primary"/>
                    <a class="btn btn-danger" href="<?php echo e(route('article-category.index')); ?>">Cancel</a>	
                </form>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<script src="<?php echo e(asset('assets/admin/js/jquery.seourl.min.js')); ?>"></script>
<script type="text/javascript">
$(document).on("focusout", "#title", function () {
var productTitle = $(this).val().toLowerCase();
var productSlug = productTitle.seoURL();
$("#slug").val(productSlug);
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>