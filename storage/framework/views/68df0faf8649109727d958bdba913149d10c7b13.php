<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 r-full-width articles-sidebar">

    <!-- Search Bar -->
    <div class="aside-search-bar">
        <?php $route = ""; ?>
        <?php if(@$articlePage == "all_article"): ?>
        <?php $route = route('all.articles'); ?>
        <?php else: ?>
        <?php $route = route('articles'); ?>
        <?php endif; ?>
        <form action="<?php echo e($route); ?>" method="GET" id="articles_search_form">
            <input class="form-control" name="keyword" type="text" placeholder="Search..." value="<?php echo e(@Request::get('keyword')); ?>">
            <button><i class="fa fa-search articles_search"></i></button>
        </form>
    </div>
    <!-- Search Bar -->

    <!-- Aside Widget -->
    <div class="aside-widget">
        <h3><span>Top Categories</span></h3>
        <div class="top-categories">
            <?php if(!$articleCategories->isEmpty()): ?>
            <ul>                             
                <?php $__currentLoopData = $articleCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <a href="<?php echo e(route('articles.category',[$category->slug])); ?>" class="<?php echo e(@$categoryDetail->id == $category->id ? 'active' : ''); ?> categories"><?php echo e($category->title); ?></a>
                </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
            <?php endif; ?>
        </div>
    </div>
    <!--                    <div class="aside-widget">
                            <a href="#"><img src="images/adds.jpg" alt=""></a>
                        </div>-->
    <!-- Aside Widget -->

</div>