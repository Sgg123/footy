<script type="text/javascript">
    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>
<script type="text/javascript">

    $(function () {

        oTable = $('#mediabuzz_player').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 25,
            searching: false,
            "sDom": "tpr",
            "dom": 'difrtp',
            ordering: false,
            "bFilter": true,
            ajax: {
                url: '<?php echo route("media.buzz.player.data"); ?>',
                data: function (d) {
                    d.start_date_filter = $("#start_date").val();
                    d.end_date_filter = $("#end_date").val();
                },
            },
            columns: [
                {data: 'mb_first', name: 'mb_first'},
                {data: 'mb_first_score', name: 'mb_first_score'},
                {data: 'mb_second', name: 'mb_second'},
                {data: 'mb_second_score', name: 'mb_second_score'},
                {data: 'mb_third', name: 'mb_third'},
                {data: 'mb_third_score', name: 'mb_third_score'},
                {data: 'created_at', name: 'created_at'},
            ]
        });
    });
    $('#search-form').on('submit', function (e) {
        oTable.draw();
        e.preventDefault();
    });
</script>