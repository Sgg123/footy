<?php $__env->startSection('page_css'); ?>
<link href="<?php echo e(asset('assets/admin/css/selectize.default.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?php echo e(route('teams')); ?>">Teams</a>
        </li>
        <li class="breadcrumb-item active">Players</li>
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-users"></i> Players   

            <a href="<?php echo e(route('teams.players.create', ['id' => @$teamID])); ?>" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>        
        </div>
        <div class="clearfix"></div>

        <div class="card-body">            
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="players-table">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Common Name</th>
                            <th>Team</th>
                            <th>Position</th>
                            <th>Nationality</th>
                            <th>Birthdate</th>                            
                            <th>Height</th>
                            <th>Weight</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>                        
                    </tbody> 
                </table>
            </div>            
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" charset="utf-8"></script>\
<script src="<?php echo e(asset('assets/admin/js/selectize.min.js')); ?>"></script>
<?php echo $__env->make('admin.players.common-script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>