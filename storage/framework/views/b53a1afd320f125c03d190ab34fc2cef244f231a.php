<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Media Buzz Winner Teams History</li>
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
<!--        <div class="card-header">
            <i class="fa fa-table"></i> Media Buzz Winner Teams History
            <a href="<?php echo e(route('media-buzz-winner-team.create')); ?>" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>-->
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>MB 1st Place</th>
                            <th>MB 2nd Place</th>
                            <th>MB 3rd Place</th>
                            <th>MB 4th Place</th>
                            <th>MB 5th Place</th> 
                            <th>Actions</th>                                  
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($winnerTeamHistory)): ?>
                        <?php $__currentLoopData = $winnerTeamHistory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $player_team_name = explode(',',@$history['player_team_names']); ?>
                        <tr>
                            <td><?php echo e(@$history['date']); ?></td>
                            <td><?php echo e(@$player_team_name[0]); ?></td>
                            <td><?php echo e(@$player_team_name[1]); ?></td>
                            <td><?php echo e(@$player_team_name[2]); ?></td>
                            <td><?php echo e(@$player_team_name[3]); ?></td>
                            <td><?php echo e(@$player_team_name[4]); ?></td>
                            <td>
                                <a href="<?php echo e(route('media-buzz-winner-team.edit', ['id' => Helper::encrypt($history->date)])); ?>" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                
                            </td>
                        </tr>  
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                        <tr>
                            <td colspan="3">No data found</td>                                         
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <?php echo e($winnerTeamHistory->links()); ?>

            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>

<?php echo $__env->make('alerts.delete-confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>