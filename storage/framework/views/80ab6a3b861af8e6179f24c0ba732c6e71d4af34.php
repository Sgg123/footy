<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2><?php echo e(@$page->title); ?></h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Pages</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('uploads/pages/'.@$page->image_path)); ?>">
</div>


<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                <?php echo $__env->make('front.pages.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-8">

                    <div class="team-detail-content theme-padding-bottom">
                        <?php if($message = Session::get('success')): ?>
                        <div class="custom-alerts alert alert-success fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php echo $message; ?>

                        </div>
                        <?php Session::forget('success'); ?>
                        <?php endif; ?>
                        <?php if($message = Session::get('error')): ?>
                        <div class="custom-alerts alert alert-danger fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php echo $message; ?>

                        </div>
                        <?php Session::forget('error'); ?>
                        <?php endif; ?>
                        <?php if(\Auth::user()->isPremium()): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Payment Method</div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="POST" id="payment-form" role="form" action="<?php echo route('updatepaymentmethod'); ?>" >
                                    <?php echo e(csrf_field()); ?>

                                    <div class="form-group<?php echo e($errors->has('card_no') ? ' has-error' : ''); ?>">
                                        <label for="card_no" class="col-md-4 control-label">Card No</label>
                                        <div class="col-md-6">
                                            <input id="card_no" type="text" class="form-control" name="card_no" value="<?php echo e(old('card_no')); ?>" autofocus>
                                            <?php if($errors->has('card_no')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('card_no')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group<?php echo e($errors->has('ccExpiryMonth') ? ' has-error' : ''); ?>">
                                        <label for="ccExpiryMonth" class="col-md-4 control-label">Expiry Month</label>
                                        <div class="col-md-6">
                                            <input id="ccExpiryMonth" type="text" class="form-control" name="ccExpiryMonth" value="<?php echo e(old('ccExpiryMonth')); ?>" autofocus>
                                            <?php if($errors->has('ccExpiryMonth')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('ccExpiryMonth')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group<?php echo e($errors->has('ccExpiryYear') ? ' has-error' : ''); ?>">
                                        <label for="ccExpiryYear" class="col-md-4 control-label">Expiry Year</label>
                                        <div class="col-md-6">
                                            <input id="ccExpiryYear" type="text" class="form-control" name="ccExpiryYear" value="<?php echo e(old('ccExpiryYear')); ?>" autofocus>
                                            <?php if($errors->has('ccExpiryYear')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('ccExpiryYear')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group<?php echo e($errors->has('cvvNumber') ? ' has-error' : ''); ?>">
                                        <label for="cvvNumber" class="col-md-4 control-label">CVV No.</label>
                                        <div class="col-md-6">
                                            <input id="cvvNumber" type="text" class="form-control" name="cvvNumber" value="<?php echo e(old('cvvNumber')); ?>" autofocus>
                                            <?php if($errors->has('cvvNumber')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('cvvNumber')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>                                    

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary red-btn">
                                                Update Payment Method
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> 
                        <?php endif; ?>
                    </div>
                </div>
                <!-- Match Result Contenet -->
            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>