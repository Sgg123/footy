<div class="slider-holder">

    <!-- Banner slider -->
    <ul id="main-slides" class="main-slides">

        <!-- Itme -->
        <li>
            <div id="animated-slider" class="carousel slide carousel-fade">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <!-- Item -->
                    <div class="item active">
                        <img src="<?php echo e(asset('assets/front/images/banner-slider/FIS-Splash-Screen-1.jpg')); ?>" alt="">
                    </div> 
                    <!-- Item -->

                    <!-- Item -->
                    <div class="item">
                        <img src="<?php echo e(asset('assets/front/images/banner-slider/FIS-Splash-Screen-4a.jpg')); ?>" alt="">
                    </div> 
                    <!-- Item -->

                    <!-- Item -->
                    <div class="item">
                        <img src="<?php echo e(asset('assets/front/images/banner-slider/FIS-Splash-Screen-3.jpg')); ?>" alt="">
                    </div> 
                    <!-- Item -->       

                </div>
                <!-- Wrapper for slides -->

                <!-- Nan Control -->
                <!-- <a class="slider-nav next" href="#animated-slider" data-slide="prev"><i class="fa fa-long-arrow-right"></i></a>
                <a class="slider-nav prev" href="#animated-slider" data-slide="next"><i class="fa fa-long-arrow-left"></i></a> -->
                <!-- Nan Control -->

                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#animated-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#animated-slider" data-slide-to="1"></li>
                    <li data-target="#animated-slider" data-slide-to="2"></li>
                </ul>
                <!-- Indicators -->

            </div>
        </li>
        <!-- Itme -->

    </ul>
    <!-- Banner slider -->

</div>