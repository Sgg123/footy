<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?php echo e(route('settings')); ?>">
                Settings
            </a>
        </li>        
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Manage Settings
        </div>
        <div class="card-body">
            
            <form method="POST" action="<?php echo e(route('settings')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Title </label>
                                <input class="form-control pointer-disabled" name="title" type="text" aria-describedby="nameHelp" placeholder="Enter title" value ="<?php echo e(@$settings->title); ?>" readonly>
                            </div>
                        </div>
                    </div>   
                    
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Value <span class= "error">*</span></label>
                                <input class="form-control" name="value" type="text" aria-describedby="nameHelp" placeholder="Enter subscription charge" value ="<?php echo e(@$settings->value); ?>" autofocus>
                                <label id="value-error" class="error" for="value"><?php echo e(@$errors->first('value')); ?></label>
                            </div>
                        </div>
                    </div>         
                    <input type="submit" value="Update" class="btn btn-primary"/>
                    <a class="btn btn-danger" href="<?php echo e(url('backoffice-fis')); ?>">Cancel</a>	
                </form>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>