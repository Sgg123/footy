
<div class="row">
  <div class="col-sm-3">
    <div class="player-v">

      <img 
      <?php if(@$playerInfo['is_local_image'] == 0): ?>
      src="<?php echo e(@$playerInfo['image_path']); ?>"
      <?php elseif(@$playerInfo['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$playerInfo['local_image_path']))): ?>
      src="<?php echo e(asset('uploads/player-images/'.@$playerInfo['local_image_path'])); ?>"
      <?php else: ?>
      src ="<?php echo e(asset('assets/images/no-image.jpeg')); ?>" 
      <?php endif; ?>
      >
    </div>
    <div class="set-player-btn">
      <?php
      $colorCode = "";
      $title ="";
      ?>
      <?php if(@$playerInfo->football_index_position == "Attacker" || @$playerInfo->football_index_position == "Forward"): ?>

      <?php
      $colorCode = "#d52cbb";
      $title ="Forward";
      ?>

      <?php elseif(@$playerInfo->football_index_position == "Defender"): ?>

      <?php
      $colorCode = "#19AEF4";
      $title ="Defender";
      ?>

      <?php elseif(@$playerInfo->football_index_position == "Midfielder"): ?>

      <?php
      $colorCode = "#F89300";
      $title ="Midfielder";
      ?>

      <?php elseif(@$playerInfo->football_index_position == "Goalkeeper"): ?>

      <?php
      $colorCode = "#19AEF4";
      $title ="Goalkeeper";
      ?>
      <?php endif; ?>
      <div class="fd-btn-py" style="background:<?php echo e($colorCode); ?>">
        <?php echo e($title); ?>

      </div>
     
        <?php
        $colorCodes = "";
        $fontcolor ="";
        $playerScore = @number_format(@$playerInfo->total_score); 
        ?>
        <?php if(@$playerScore <= 30): ?>
        <?php
        $colorCodes = "#FF0000";
        $fontcolor = "#FFFFFF";
        ?>
        <?php elseif(@$playerScore >= 31 && @$playerScore <= 70): ?>
        <?php
        $colorCodes = "#FF5733";
        $fontcolor = "#FFFFFF";
        ?>
        <?php elseif(@$playerScore >= 71 && @$playerScore <= 110): ?>
        <?php
        $colorCodes = "#eacd0e";
        $fontcolor = "#000";
        ?>
        <?php elseif(@$playerScore >= 111 && @$playerScore <= 150): ?>
        <?php
        $colorCodes = "#FFFF00";
        $fontcolor = "#000";
        ?>
        <?php elseif(@$playerScore >= 151 && @$playerScore <= 180): ?>
        <?php
        $colorCodes = "#aad77d";
        $fontcolor = "#FFFFFF";
        ?>
        <?php elseif(@$playerScore >= 180): ?>
        <?php
        $colorCodes = "#39a02b";
        $fontcolor = "#FFFFFF";
        ?>
        <?php endif; ?>
         <div class="avrage-btn" style="background:<?php echo e($colorCodes); ?>;color:<?php echo e($fontcolor); ?>">
        <div class="av-pb">
          Average<br>PB Score
        </div>
        <div class="sc-wo">
         <?php echo e(@number_format($playerInfo->total_score ,0.5)); ?>

       </div>
     </div>
     <div class="game-btn-v">
      <div class="game-text">
       Games<br>Played 
     </div>
     <div class="game-con">
       <?php echo e(@$playerInfo->total_count); ?>

     </div>
   </div>


 </div>
</div>
<?php
$playerObj = \App\Player::find(@$playerInfo->playerid);
?>
<div class="col-sm-9 up-c-set">
  <div class="player-v-text">
   <div class="col-sm-9 hedading-s"> <h2><?php echo e(@$playerInfo->football_index_common_name); ?></h2>
    <div class="fiture-b">
      <h3><?php echo e(@$playerObj->getTeam->name); ?> <br/> <?php echo e(@$fixture->league_name); ?></h3>
    </div>
  </div>
  <div class="col-sm-3 image-sv-set">
   <div class="harry-img-v">
     <img src="<?php echo e(asset('assets/front/images/FIS-Club-Badge-new.png')); ?>" alt="">
   </div>
 </div>
<?php $days=''; ?>
<?php if($diff_in_days == -1): ?>
    <?php $days = "No Data"; ?>
<?php elseif($diff_in_days == 0): ?>
    <?php $days = "Today"; ?>
<?php elseif($diff_in_days == 1): ?>
    <?php $days = $diff_in_days." Day"; ?>
<?php elseif($diff_in_days > 1): ?>
    <?php $days = $diff_in_days." Days"; ?>
<?php endif; ?>
 <div class="next-f">
  <h3>Next Fixture: <?php echo e(@$teamObj->name); ?> (<?php echo e($days); ?>) </h3>          
  </div>
</div>
<div class="rank-box row"> 
  <div class="col-sm-3 p-frd-set">
    <div class="frd-rank" style="background:<?php echo e($colorCode); ?>">
      <?php
      $colorCode = "";
      $title ="";
      ?>
      <?php if(@$playersector == "Attacker" ||@$playersector == 'Forward'): ?>
      <?php
      $colorCode = "#d52cbb";
      $title ="FWD";
      ?>
      <?php elseif(@$playersector == 'Midfielder'): ?>
      <?php
      $colorCode = "#F89300";
      $title ="MID";
      ?>
      <?php elseif(@$playersector == 'Defender'): ?>
      <?php
      $colorCode = "#19AEF4";
      $title ="DEF";
      ?>
      <?php elseif(@$playersector == 'Goalkeeper'): ?>
      <?php
      $colorCode = "#19AEF4";
      $title ="GK";
      ?>
      <?php endif; ?>
      <?php echo e($title); ?><br/>Rank
      <span><?php echo e($playerdetail); ?></span>
    </div>
  </div>
  <div class="col-sm-9">
    <div class="row">
      <div class="col-sm-4 wins-p">
        <div class="overl-1">
          <div class="sep-overrall">
            OVERALL <br>RANK
          </div>
          <span><?php echo e($overallrankdetail); ?></span>
        </div>
      </div>
      <div class="col-sm-4 wins-p">
        <div class="overl-1">
          <div class="sep-overrall">
            <?php
            $title ="";
            ?>
            <?php if(@$playerInfo->football_index_position == "Attacker" ||@$playerInfo->football_index_position == 'Forward'): ?>
            <?php
            $title ="FWD";
            ?>
            <?php elseif(@$playerInfo->football_index_position == 'Midfielder'): ?>
            <?php
            $title ="MID";
            ?>
            <?php elseif(@$playerInfo->football_index_position == 'Defender'): ?>
            <?php
            $title ="DEF";
            ?>
            <?php elseif(@$playerInfo->football_index_position == 'Goalkeeper'): ?>
            <?php
            $title ="GK";
            ?>
            <?php endif; ?>
            TOP <?php echo e($title); ?> <br> WINS
          </div>
          <span><?php echo e(@$playerInfo->positional_wins); ?></span>
        </div>
      </div>
      <div class="col-sm-4 wins-p">
        <div class="overl-1">
          <div class="sep-overrall">
            TOP PLAYER <br> WINS
          </div>
          <span><?php echo e(@$playerInfo->top_player_wins); ?></span>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="row media-wind-v">
          <div class="col-sm-3 ex-media">
            <div class="u-media-f">MEDIA PLACINGS</div>
          </div>
          <div class="col-sm-3">
            <div class="onest-p">
             1st Place<br>
             <span><?php echo e(@$playerInfo->mb_first_count); ?></span>
            </div>
          </div>
          <div class="col-sm-3">
             <div class="onest-p">
               2nd Place<br>
               <span><?php echo e(@$playerInfo->mb_second_count); ?></span>
             </div>
          </div>
          <div class="col-sm-3">
            <div class="onest-p">
              3rd Place<br>
              <span><?php echo e(@$playerInfo->mb_third_count); ?></span>
            </div>
          </div>
        </div>
      </div>
    </div>    
  </div>
</div>
</div>
</div>
<div class="row">
  <div class="last-f-s last-b">
    <div class="lst-s lst-s-sec pb_top" id="idlast">
      <a onclick="show('id1');">
        Last 5 Scores
      </a>
    </div>
    <div class="top-s top-s-sec" id="idtop">
      <a onclick="show('id2');">
        Top 5 Scores
      </a>
    </div>    
    <div class="top-s top-s-sec div_yield_pop">
      <span>
        <?php if(@$playerInfo->dividend_yield > 0): ?>
        Dividend Yield : £<?php echo e(@$playerInfo->dividend_yield); ?>

        <?php else: ?>
        Dividend Yield : £0.00
        <?php endif; ?>
      </span>
    </div>    
  </div>
</div>
<div id="id1">
  <div class="row">
    <div class="different-b">                          
     <?php if(!$pbdemo->isEmpty()): ?>
     <?php $__currentLoopData = $pbdemo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $object): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
     <?php
     $colorCode = "";
     $fontcolor ="";
     ?>
     <?php if(@$object->score <= 30): ?>
     <?php
     $colorCode = "#FF0000";
     ?>
     <?php elseif(@$object->score >= 31 && @$object->score <= 70): ?>
     <?php
     $colorCode = "#FF5733";
     ?>
     <?php elseif(@$object->score >= 71 && @$object->score <= 110): ?>
     <?php
     $colorCode = "#eacd0e";
     $fontcolor = "#000";
     ?>
     <?php elseif(@$object->score >= 111 && @$object->score <= 150): ?>
     <?php
     $colorCode = "#FFFF00";
     $fontcolor = "#000";
     ?>
     <?php elseif(@$object->score >= 151 && @$object->score <= 180): ?>
     <?php
     $colorCode = "#aad77d";
     ?>
     <?php elseif(@$object->score >= 180): ?>
     <?php
     $colorCode = "#39a02b";
     ?>
     <?php endif; ?>
     <div class="main-d-v-set" >
      <div class="first-d-v" style="background:<?php echo e($colorCode); ?>; color:<?php echo e($fontcolor); ?>">
        <span><?php echo e($object->score); ?></span>
      </div>
      <p><?php echo e(date('d M y', strtotime($object->date))); ?></p>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
    <div class="data-not">No Records Found</div>
    <?php endif; ?>
  </div>
</div>
</div>
<div id="id2">
  <div class="row">
    <div class="different-b">
     <?php if(!$pbhighestscore->isEmpty()): ?>
     <?php $__currentLoopData = $pbhighestscore; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $object): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

     <?php
     $colorCode = "";
     $fontcolor = "";
     ?>
     <?php if(@$object->total_score <= 30): ?>
     <?php
     $colorCode = "#FF0000";
     ?>
     <?php elseif(@$object->total_score >= 31 && @$object->total_score <= 70): ?>
     <?php
     $colorCode = "#FF5733";
     ?>
     <?php elseif(@$object->total_score >= 71 && @$object->total_score <= 110): ?>
     <?php
     $colorCode = "#eacd0e";
     $fontcolor = "#000";
     ?>
     <?php elseif(@$object->total_score >= 111 && @$object->total_score <= 150): ?>
     <?php
     $colorCode = "#FFFF00";
     $fontcolor = "#000";
     ?>
     <?php elseif(@$object->total_score >= 151 && @$object->total_score <= 180): ?>
     <?php
     $colorCode = "#aad77d";
     ?>
     <?php elseif(@$object->total_score >= 180): ?>
     <?php
     $colorCode = "#39a02b";
     ?>
     <?php endif; ?>
     <div class="main-d-v-set">
       <div class="first-d-v" style="background:<?php echo e($colorCode); ?>; color:<?php echo e($fontcolor); ?>">
        <span><?php echo e($object->total_score); ?></span>
      </div>
      <p><?php echo e(date('d M y', strtotime($object->date))); ?></p>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
    <div class="data-not">No Records Found</div>
    <?php endif; ?>
  </div>
</div>
</div>
<div class="row search-f-box">
  <div class="col-sm-4">
    <div class="f-search-f show-filter" style="cursor: pointer;">
      Search Fixtures
    </div>
  </div>
  <div class="col-sm-4">
    
  </div>
  <div class="col-sm-4 ft-tf-set">
    <div class="ft-play">
      <?php if($playerInfo->injured == "true"): ?>
      <?php $pl_status="Injured"; ?>
      <img src="<?php echo e(asset('assets/front/images/plus.png')); ?>">
      <?php else: ?>
      <?php $pl_status="Fit to play"; ?>
      <img src="<?php echo e(asset('assets/front/images/minus.png')); ?>">
      <?php endif; ?>
    </div>
    <div class="ft-play-con">
      <span><?php echo e($pl_status); ?></span>    
    </div>
  </div>
</div>
<div class="row">
<div class="col">
<div id="div1" class="col-sm-12" style="display: none; background-color: white;">
  <div class="row">
    <div class="col-md-5">
      <div class="row">
        <div class="form-group row season_dropdown">
          <label for="sort" class="col-sm-5 control-label"> SEASON </label>
          <div class="col-sm-7">
          <?php
            $year_array = array();
            foreach(@$pbzsearch as $searchfix){
              if($searchfix->month < 8 || ($searchfix->month == 8 && $searchfix->day < 10))
              {
                $year_array[] = $searchfix->year;
              }
              if($searchfix->month > 8 || ($searchfix->month == 8 && $searchfix->day > 9))
              {
                $year_array[] = ($searchfix->year + 1);
              }
            }
            $year_array = array_unique($year_array);
          ?>
            <select class="form-control" id="season_year" onchange="getfinalplayerfixturedata(this.value);">
              <option>Select</option>

              <?php $__currentLoopData = @$year_array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e(@$year); ?>"><?php echo e(@$year-1); ?>/<?php echo e(@$year); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-7">
      <div class="selected-f" id="player_fixture_score">
        Selected Fixture Score
      </div>
    </div>
  </div>
</div>
</div>

