<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content=""/>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <?php echo $__env->yieldContent('no_follow'); ?>

        <title><?php echo e(config('app.name', 'FOOTY INDEX SCOUT')); ?> <?php echo $__env->yieldContent('title'); ?></title>
        <!-- StyleSheets -->
        <link rel="icon" href="<?php echo asset('favicon.ico'); ?>" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo asset('favicon.ico'); ?>" type="image/x-icon" />
        <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/bootstrap/bootstrap.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/main.css')); ?>"> 
        <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/icomoon.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/main.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/transition.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/font-awesome.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/style.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/color.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('assets/front/css/responsive.css')); ?>">
        <!-- FontsOnline -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800|Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <!-- JavaScripts -->
        <script src="<?php echo e(asset('assets/front/js/vendor/modernizr.js')); ?>"></script>
        <!-- Stripe JS -->
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

        <script>
            Stripe.setPublishableKey("<?php echo e(env('STRIPE_KEY')); ?>");
            window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
    'APP_URL' => url('/'),
]);
?>
        </script>      

        <?php if(auth()->guard()->guest()): ?>
        <style>
            @media (max-width: 991px) {
                .header.style-3 .nav-list{ display: none;}
            }
        </style>
        <?php else: ?>
        <style>
            @media (max-width: 991px) {
                .nav-list{ display: none;}
                .header.style-3 .nav-list{ display: none;}
            }
        </style>
        <?php endif; ?> 
        <?php echo $__env->yieldContent('meta_desc'); ?>
        <?php echo $__env->yieldContent('page_css'); ?>
    </head>
    <body>

        <!-- Wrapper -->
        <div class="wrap push">

            <!-- Header -->
            <?php echo $__env->make('front.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- Header -->
            <?php if(Route::getCurrentRoute()->uri() == '/'): ?>
            <?php echo $__env->make('front.common.home-slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
            <!-- Slider Holder -->

            <!-- Slider Holder -->

            <!-- Main Content -->
            <main class="main-content">
                <?php echo $__env->yieldContent('content'); ?>
            </main>
            <!-- Main Content -->

            <!-- Footer -->
            <?php echo $__env->make('front.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- Footer -->

        </div>
        <!-- Wrapper -->

        <!-- Responsive Menu -->
        <?php if(auth()->guard()->guest()): ?>
        <?php echo $__env->make('front.common.responsive-nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php else: ?>
        <?php echo $__env->make('front.common.responsive-nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
        <!-- Responsive Menu -->

        <!-- Java Script -->
        <script src="<?php echo e(asset('assets/front/js/vendor/jquery.js')); ?>"></script>        
        <script src="<?php echo e(asset('assets/front/js/vendor/bootstrap.min.js')); ?>"></script>  
        <script src="<?php echo e(asset('assets/front/js/bigslide.js')); ?>"></script>      
        <script src="<?php echo e(asset('assets/front/js/slick.js')); ?>"></script> 
        <!-- <script src="<?php echo e(asset('assets/front/js/waterwheelCarousel.js')); ?>"></script> -->
        <!-- <script src="<?php echo e(asset('assets/front/js/contact-form.js')); ?>"></script>  
        <script src="<?php echo e(asset('assets/front/js/countTo.js')); ?>"></script>        -->
        <script src="<?php echo e(asset('assets/front/js/datepicker.js')); ?>"></script>        
        <!-- <script src="<?php echo e(asset('assets/front/js/rating-star.js')); ?>"></script> -->                           
        <!-- <script src="<?php echo e(asset('assets/front/js/range-slider.js')); ?>"></script>              
        <script src="<?php echo e(asset('assets/front/js/spinner.js')); ?>"></script>            -->
        <script src="<?php echo e(asset('assets/front/js/parallax.js')); ?>"></script>               <!-- 
        <script src="<?php echo e(asset('assets/front/js/countdown.js')); ?>"></script>  -->
        <!-- <script src="<?php echo e(asset('assets/front/js/appear.js')); ?>"></script>                
        <script src="<?php echo e(asset('assets/front/js/prettyPhoto.js')); ?>"></script>   -->         

        <script src="<?php echo e(asset('assets/front/js/wow-min.js')); ?>"></script>                       
        <script src="<?php echo e(asset('assets/front/js/main.js')); ?>"></script> 

        <script>
            (function(i, s, o, g, r, a, m){i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function(){
            (i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date(); a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-92395841-1', 'auto');
            ga('send', 'pageview');</script>

        <!-- TradeDoubler site verification 2942702 -->

        <!-- Facebook Pixel Code -->
        <script>
            !function(f, b, e, v, n, t, s){if (f.fbq)return; n = f.fbq = function(){n.callMethod?
                    n.callMethod.apply(n, arguments):n.queue.push(arguments)}; if (!f._fbq)f._fbq = n;
            n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)}(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1066561756810184'); // Insert your pixel ID here.
            fbq('track', 'PageView');</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1066561756810184&ev=PageView&noscript=1"
                       /></noscript>
        <!-- DO NOT MODIFY -->
        <!-- End Facebook Pixel Code -->

        <?php echo $__env->yieldContent('page_js'); ?>
        <?php echo $__env->yieldContent('sidebar_js'); ?>
        <script type="text/javascript">
            $(document).ready(function() {
            $(document).on("contextmenu", function(){
            return false;
            });
            });</script>
        <script type="text/javascript">
            $(document).ready(function() {
            $(document).bind('cut copy', function (e) {
            e.preventDefault();
            });
            });
        </script>

        <?php echo $__env->make('front.common.authentication-script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </body>
</html>