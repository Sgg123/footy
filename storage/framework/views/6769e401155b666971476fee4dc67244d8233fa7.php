<?php $__env->startSection('no_follow'); ?>
<meta name="robots" content="noindex, nofollow">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>


<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Performance Buzz Statistics</h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Performance Buzz Statistics</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg')); ?>">
</div>

<!-- Main Content -->
<main class="main-content">

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                <?php echo $__env->make('front.pages.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-12 sidebarres">

                    <div class="row">
                        <div class="col-md-12 player-set-m">
                            <form action="" id="buzz_statistics_form">
                                <div class="form-group col-md-3">

                                    <input type="text" class="form-control" name="keyword" placeholder="Player name" value="<?php echo e(@Request::get('keyword')); ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <!-- <input type="text" class="form-control" name="min_games" placeholder="Minimum number of games" value="<?php echo e(@$minGame); ?>"> -->
                                    <select class="form-control fg-py-drop" name="min_games">
                                        <option value="select">Minimum games</option>
                                        <option value="1" <?php if(@Request::get('min_games') == 1): ?> selected <?php endif; ?>>1</option>
                                        <option value="2" <?php if(@Request::get('min_games') == 2): ?> selected <?php endif; ?>>2</option>
                                        <option value="3" <?php if(@Request::get('min_games') == 3): ?> selected <?php endif; ?>>3</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 py-drop">
                                    <select class="form-control fg-py-drop" name="last" onchange="return reset_dates()">
                                        <option value=''>Select Period</option>
                                        <option value="30" <?php if(@Request::get('last') == 30): ?> selected <?php endif; ?>>Last 30 days</option>
                                        <option value="100" <?php if(@Request::get('last') == 100): ?> selected <?php endif; ?>>Last 100 days</option>
                                        <option value="20152016" <?php if(@Request::get('last') == 20152016): ?> selected <?php endif; ?>>2015/2016</option>
                                        <option value="20162017" <?php if(@Request::get('last') == 20162017): ?> selected <?php endif; ?>>2016/2017</option>
                                        <option value="20172018" <?php if(@Request::get('last') == 20172018): ?> selected <?php endif; ?>>2017/2018</option>
                                        <option value="20182019" <?php if(@Request::get('last') == 20182019): ?> selected <?php endif; ?>>2018/2019</option>
                                        <option value="20192020" <?php if(@Request::get('last') == 20192020): ?> selected <?php endif; ?>>2019/2020</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 py-drop">
                                    <select class="form-control fg-py-drop" name="position">
                                        <option value="">Select Position</option>
                                        <option value="Forward" <?php if(@Request::get('position') == 'Forward'): ?> selected <?php endif; ?>>Forward</option>
                                        <option value="Midfielder" <?php if(@Request::get('position') == 'Midfielder'): ?> selected <?php endif; ?>>Midfielder</option>
                                        <option value="Defender" <?php if(@Request::get('position') == 'Defender'): ?> selected <?php endif; ?>>Defender</option>
                                        <option value="Goalkeeper" <?php if(@Request::get('position') == 'Goalkeeper'): ?> selected <?php endif; ?>>Goalkeeper</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="from" id="start_date" placeholder="From" value="<?php echo e(@$from); ?>" readonly>
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" id="end_date" name="to" placeholder="To" value="<?php echo e(@$to); ?>" readonly>
                                </div>
                                <div class="form-group col-md-4 py-drop">
                                    <select class="form-control fg-py-drop" name="league_id">
                                        <option value="">Select League</option>
                                        <?php $__currentLoopData = @$leagues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $league): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($league->id); ?>" <?php if(@Request::get('league_id') == $league->id): ?> selected <?php endif; ?> ><?php echo e($league->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <div style="display: none;">
                                    <select name="sort" id="buzz_statistics_sort">
                                        <option>select</option>
                                        <option>form_guide</option>
                                        <option>last_score</option>
                                        <option>lineups</option>
                                        <option>top_player_wins</option>
                                        <option>positional_wins</option>
                                        <option>player_name</option>
                                    </select>
                                    <select name="order" id="buzz_statistics_order">
                                        <option>select</option>
                                        <option>DESC</option>
                                        <option>ASC</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2 pull-right">
                                    <button type="submit" class="btn red-btn">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Piont Table -->
                    <div class="macth-fixture">

                        <div class="last-matches styel-3">

                            <!-- Table Style 3 -->
                            <div class="table-responsive padding-top-70">



                                <!-- <img src="<?php echo e(asset('assets/front/images/Performance-Stats-Table-Main-View-Top-10.jpg')); ?>" class="performance-statistics-img">      -->
                                <div class="ad-table table-responsive">
                                    <table class="us-table-v">
                                        <tr class="player-hd-b">
                                            <th class="r-set r-set-2">R</th>
                                            <th class="r-set"><a href="javascript:sort_tab('player_name')" style="color: #ffffff;">Player</a></th>
                                            <th><a href="javascript:sort_tab('form_guide')" style="color: #ffffff;">Form<br>Guide</a></th>
                                            <th><a href="javascript:sort_tab('last_score')" style="color: #ffffff;">last<br>Score</a></th>
                                            <th><a href="javascript:sort_tab('lineups')" style="color: #ffffff;">Games<br>Played</a></th>
                                            <th><a href="javascript:sort_tab('top_player_wins')" style="color: #ffffff;">Top Player<br>Wins</a></th>
                                            <th><a href="javascript:sort_tab('positional_wins')" style="color: #ffffff;">Positional<br>Wins</a></th>
                                            <th>Next Game <br>Versus</th>
                                            <th>Top 5 <br>Average</th>
                                            <th>Playing<br>Status</th>
                                        </tr>
                                        <?php
                                        if(@Request::get('position') != ""){
                                        $ranks = \DB::select('SELECT @rank := (@rank+1) AS rank, sc.sector ,sc.player_id as pid, sc.score FROM(SELECT sector ,player_id, avg(score) AS score FROM `performance_buzz` where sector="' . @Request::get('position') . '" GROUP BY player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                                        }else{
                                        $ranks = \DB::select('SELECT @rank := (@rank+1) AS rank, sc.player_id as pid, sc.score FROM(SELECT player_id, avg(score) AS score FROM `performance_buzz` GROUP BY player_id ORDER BY score DESC ) AS sc CROSS JOIN ( SELECT @rank := 0) AS param');
                                        }

                                        ?>
                                        <?php $__currentLoopData = @$performanceBuzzStates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $states): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <?php
                                        $fixtureDetail = Helper::getUpcomingFixtureForTeam($states->team_id);

                                        $matchMonth = '';
                                        $matchDay = '';
                                        $teamImage = '';
                                        $teamType = '';
                                        $teamName = '';
                                        if(!empty($fixtureDetail)) {
                                        $teamType = $fixtureDetail['teamType'];
                                        $matchMonth = date('M', strtotime($fixtureDetail['starting_at']));
                                        $matchDay = date('d', strtotime($fixtureDetail['starting_at']));
                                        $teamName = $fixtureDetail['team']->name;
                                        $datediff = $fixtureDetail['datediff'];
                                        if($datediff==0){
                                        $team_title = $teamName.", today will be next game";
                                        }
                                        else{
                                        $team_title = $teamName.", ".$datediff." Day(s) till the next game";
                                        }
                                        if($fixtureDetail['team']->is_local_image == 1) {
                                        $teamImage = asset('uploads/team-images/'.$fixtureDetail['team']->local_image_path);
                                        }
                                        else {

                                        $teamImage = $fixtureDetail['team']->logo_path;
                                        }

                                        }

                                        $color = Helper::getColor($states->form_guide);
                                        ?>

                                        <tr <?php if($loop->iteration % 2 == 0): ?>  style="background:#e8e8e8;" <?php else: ?> style="background:#a4d6e494;" <?php endif; ?> >
                                             <td class="pd-set-one"><span>
                                                    <?php
                                                    $playerRank = '';
                                                    foreach ($ranks as $value) {
                                                    if ($value->pid == @$states->player_id) {
                                                    $playerRank = $value->rank;
                                                    }
                                                    }
                                                    ?>
                                                    <?php echo e(@$playerRank); ?>

                                                </span></td>
                                            <td class="mb-player-details view-player" data-id="<?php echo e(@$states->player_id); ?>" data-teamid="<?php echo e(@$states->team_id); ?>">
                                                <div class="b-player-b">
                                                    <div class="img-player-v">
                                                        <?php if($states->is_local_image == 1): ?>

                                                        <img src="<?php echo e(asset('uploads/player-images/'.@$states->local_image_path)); ?>" class="home-mb-team-img">

                                                        <?php else: ?>
                                                        <img src="<?php echo e($states->image_path); ?>" class="home-mb-team-img">
                                                        <?php endif; ?>

                                                    </div>
                                                    <div class="player-name-c">
                                                        <h2>
                                                            <?php if($states->football_index_common_name != ''): ?>
                                                            <?php echo e($states->football_index_common_name); ?>

                                                            <?php else: ?>
                                                            <?php echo e($states->player_name); ?>


                                                            <?php endif; ?>
                                                        </h2><p><?php echo e($states->team); ?>

                                                            <span class="fd-pl"><?php echo e($states->football_index_position); ?></div>
                                                    </p>
                                                </div>
                                                </div>
                                            </td>
                                            <td class="pd-set-one">
                                                <span class="bg-using-py" style="background: <?php echo e($color['colorCode']); ?>; color: <?php echo e($color['fontcolor']); ?>" title="Average score from last 5 fixtures">
                                                    <?php echo e($states->form_guide); ?>

                                                </span>
                                            </td>
                                            <td class="pd-set-one">
                                                <span><?php echo e($states->last_score); ?></span>
                                            </td>
                                            <td class="pd-set-one">
                                                <span><?php echo e($states->lineups); ?></span>
                                            </td>
                                            <td class="pd-set-one">
                                                <span><?php echo e(@$states->top_player_wins); ?></span>
                                            </td>
                                            <td class="pd-set-one">
                                                <span><?php echo e(@$states->positional_wins); ?></span>
                                            </td>
                                            <td class="three-set-b">
                                                <?php if(!empty($fixtureDetail)): ?>
                                                <div class="f-set-b"><img src="<?php echo e(@$teamImage); ?>" title="<?php echo e($team_title); ?>">
                                                </div>
                                                <div class="hv-set">
                                                    <span><?php echo e(@$teamType); ?></span>
                                                </div>
                                                <div class="s-set-b"><?php echo e($matchMonth); ?><br><?php echo e($matchDay); ?></div>
                                                <!--                                                    <div class="t-set-b">
                                                                                                       <div class="fdr-1">FDR<span>1</span>
                                                                                                       </div>
                                                                                                    </div>-->
                                                <?php endif; ?>
                                            </td>
                                            <td class="pd-set-one">
                                                <span><?php echo e(@$states->getTop5Avg()); ?></span>
                                            </td>
                                            <td class="last-l-pr">

                                                <div class="latest-l-img define-new-s">
                                                    <?php if($states->injured == "true"): ?>
                                                    <img src="<?php echo e(asset('assets/front/images/plus.png')); ?>">
                                                    <?php else: ?>
                                                    <img src="<?php echo e(asset('assets/front/images/minus.png')); ?>">
                                                    <?php endif; ?>
                                                </div>
                                            </td>

                                        </tr>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </table>
                                    <div class="mb-player-pagination">
                                        <?php echo e(@$performanceBuzzStates->appends(Request::capture()->except('page'))->links()); ?>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Piont Table -->

                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection("page_js"); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
                                        $(function () {
                                            $("#start_date").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                //maxDate: 0,
                                                onClose: function (selectedDate) {
                                                    $("#end_date").datepicker("option", "minDate", selectedDate);
                                                }
                                            });
                                            $("#end_date").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                //maxDate: 0,
                                                onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
                                                }
                                            });
                                        });
                                        function sort_tab(value) {
                                            var url_string = window.location.href; //window.location.href
                                            var url = new URL(url_string);
                                            var sort = url.searchParams.get("sort");
                                            var order = url.searchParams.get("order");
                                            if (sort == value) {
                                                if (order == 'DESC') {
                                                    order = 'ASC';
                                                } else {
                                                    order = 'DESC';
                                                }
                                            } else {
                                                order = 'DESC';
                                            }
                                            $('form#buzz_statistics_form')[0].reset();
                                            $('#buzz_statistics_sort').val(value);
                                            $('#buzz_statistics_order').val(order);
                                            $('form#buzz_statistics_form').submit();
                                        }
                                        function reset_dates() {
                                            $('#start_date').val('');
                                            $('#end_date').val('');
                                        }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>