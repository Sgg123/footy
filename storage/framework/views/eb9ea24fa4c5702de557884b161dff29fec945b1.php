<script type="text/javascript">
    $(function () {

        oTable = $('#team-table').DataTable({
            "processing": true,
            serverSide: true,
            "pageLength": 25,
            searching: true,
            "dom": 'difrtp',
            ordering: false,
            "bInfo": false,
            ajax: {
                url: '<?php echo route("teams.data"); ?>',
                data: function (d) {
                   
                },
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'twitter', name: 'twitter'},
                {data: 'founded', name: 'founded'},
                {data: 'national_team', name: 'national_team'},
                {data: 'action', name: 'action'},
            ]
        });
        
    });
</script>