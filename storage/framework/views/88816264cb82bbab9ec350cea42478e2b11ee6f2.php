<header class="header style-3">

    <!-- Top bar -->
    <div class="topbar-and-logobar">
        <div class="container">
            <!-- Responsive Button -->
            <ul class="user-login-option licence">
                <li class="login-modal">
                    <span>DATA LICENSED BY          </span><img src="<?php echo e(asset('long_logo.png')); ?>" width="155px">
                </li>
            </ul>
            <?php if(auth()->guard()->guest()): ?>
            <div class="responsive-btn pull-right">
                <a href="#menu" class="menu-link"><i class="fa fa-bars"></i></a>
            </div>
            <?php else: ?>
            <div class="responsive-btn pull-right">
                <a href="#menu" class="menu-link"><i class="fa fa-bars"></i></a>
            </div>
            <?php endif; ?>
            <!-- Responsive Button -->
               
            <!-- User Login Option -->
            <ul class="user-login-option pull-right">
                <li class="social-icon">
                    <a href="<?php echo e(route('page', [@$pages[10]['slug']])); ?>">Why Footy Index Scout</a>
                </li>
                <li class="login-modal">

                    <?php if(auth()->guard()->guest()): ?>
                    <a href="javascript:void(0)" class="login show-login-frm"><i class="fa fa-user"></i>Login / Registration</a>
                    <div class="modal fade" id="login-modal">
                        <div class="login-form position-center-center">
                            <h2>Login<button class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button></h2>
                            <div class="login-response">

                            </div>
                            <form id="modal-login-form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="login-email" name="email" placeholder="domain@live.com">
                                    <i class=" fa fa-envelope"></i> 
                                    <label id="login-email-error" class="error" for="login-email"></label>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="login-password" name="password" placeholder="**********">
                                    <i class=" fa fa-lock"></i> 
                                    <label id="login-password-error" class="error" for="login-password"></label>
                                </div>
                                <div class="form-group custom-checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Stay login
                                    </label>
                                    <a class="pull-right forgot-password" href="#"></a>
                                    <a href="#" class="pull-right forgot-password-modal" data-toggle="modal" data-target="#forgot-password-modal">Forgot password?</a>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="btn full-width red-btn" id="do-login">Login</a>
                                </div>
                            </form>
                            <span class="or-reprater"></span>
                            <div class="form-group">
                                <a href="javascript:void(0)" class="btn full-width btn-success show-register-frm">Click here for new registration</a>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="forgot-password-modal">
                        <div class="login-form position-center-center">
                            <h2>Forgot password<button class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button></h2>
                            <div class="forgot-response">

                            </div>
                            <form id="modal-forgot-password-form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="forgot-email" name="forgot-email" placeholder="domain@live.com">
                                    <i class="fa fa-envelope"></i> 
                                    <label id="forgot-email-error" class="error" for="forgot-email"></label>
                                </div>
                                <div class="form-group">
                                    <a  href="javascript:void(0)" class="btn full-width red-btn" id="do-reset">Send Password Reset Link</a>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="modal fade" id="registration-modal">
                        <div class="login-form position-center-center">
                            <h2>Registration<button class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button></h2>
                            <div class="register-response">

                            </div>
                            <form id="modal-register-form">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="register-firstname" name="register-firstname" placeholder="First Name">
                                            <i class=" fa fa-user"></i> 
                                            <label id="register-firstname-error" class="error" for="register-firstname"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="register-lastname" name="register-lastname" placeholder="Last Name">
                                            <i class=" fa fa-user"></i> 
                                            <label id="register-lastname-error" class="error" for="register-lastname"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="register-phone" name="register-phone" placeholder="Phone">
                                            <i class=" fa fa-phone"></i> 
                                            <label id="register-phone-error" class="error" for="register-phone"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="register-email" name="register-email" placeholder="domain@live.com">
                                            <i class=" fa fa-envelope"></i> 
                                            <label id="register-email-error" class="error" for="register-email"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="register-password" name="register-password" placeholder="Password">
                                            <i class=" fa fa-lock"></i> 
                                            <label id="register-password-error" class="error" for="register-password"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="register-confirm-password" name="register-confirm-password" placeholder="Password Confirmation">
                                            <i class=" fa fa-lock"></i> 
                                            <label id="register-confirm-password-error" class="error" for="register-confirm-password"></label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="free-register">
                                                <input type="radio" id="free-register" name="register_type" value="free" checked="checked"> Free Membership
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;">
                                        <div class="form-group">
                                            <label for="premium-register">
                                                <input type="radio" id="premium-register" name="register_type" value="premium"> Premium Membership
                                            </label>
                                        </div>
                                    </div>
                                    <div id="registerCreditCard" class="clearfix">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <h4>Credit Card Details</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="register-cardnumber" name="register_card_no" placeholder="Card Number">
                                                <i class=" fa fa-credit-card"></i> 
                                                <label id="register-cardnumber-error" class="error" for="register-cardnumber"></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="register-expiry-month" name="register_expiry_month" placeholder="Expiry Month">
                                                <i class=" fa fa-calendar"></i> 
                                                <label id="register-expiry-month-error" class="error" for="register-expiry-month"></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="register-expiry-year" name="register_expiry_year" placeholder="Expiry Year">
                                                <i class=" fa fa-calendar"></i> 
                                                <label id="register-expiry-year-error" class="error" for="register-expiry-year"></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="register-cvv-number" name="register_cvv_number" placeholder="CVV Number">
                                                <i class=" fa fa-user-secret"></i> 
                                                <label id="register-cvv-number-error" class="error" for="register-cvv-number"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" id="do-register" class="btn full-width red-btn">Register</a>
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php if(Route::is('password.reset')): ?>
                    <div class="modal fade" id="reset-password-modal">
                        <div class="login-form position-center-center">
                            <h2>Reset password</h2>
                            <div class="reset-response">

                            </div>
                            <form id="modal-reset-password-form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="reset-email" name="reset-email" placeholder="domain@live.com">
                                    <i class=" fa fa-envelope"></i> 
                                    <label id="reset-email-error" class="error" for="reset-email"></label>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="reset-password" name="reset-password" placeholder="Password">
                                    <i class=" fa fa-lock"></i> 
                                    <label id="reset-password-error" class="error" for="reset-password"></label>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="reset-confirm-password" name="reset-confirm-password" placeholder="Password Confirmation">
                                    <i class=" fa fa-lock"></i> 
                                    <label id="reset-confirm-password-error" class="error" for="reset-confirm-password"></label>
                                </div>
                                <div class="form-group">
                                    <a  href="javascript:void(0)" class="btn full-width red-btn" id="do-reset-password">Reset Password</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php else: ?>
                    <a href="javascript:void(0)" class="u_name"><i class="fa fa-user"></i><?php echo e(Auth::user()->firstname); ?></a>
                    <ul>
                    <li>
                        <a href="<?php echo e(route('myaccount')); ?>" class="login">My Account</a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('reset-password')); ?>" class="login">Change Password</a>
                    </li>
                    <!-- <?php if(\Auth::user()->isPremium()): ?>
                    <li>
                        <a href="<?php echo e(route('updatepaymentmethod')); ?>" class="login">Update payment method</a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('cancel-premium-subcription')); ?>" class="login">Cancel Subscription</a>
                    </li>
                    <?php else: ?>
                    <li>
                        <a href="<?php echo e(route('upgrade-account')); ?>" class="login">Upgrade Account</a>
                    </li>
                    <?php endif; ?>    -->                 
                    <li>
                        <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="login">Logout</a>
                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;"><?php echo e(csrf_field()); ?>

                        </form>
                    </li>
                    </ul>                  
                    <?php endif; ?>

                </li>
                <?php if(auth()->guard()->guest()): ?>
                <?php else: ?>                
                
                <?php endif; ?>
            </ul>
            <!-- User Login Option -->

        </div>  
    </div>
    <!-- Top bar -->

    <!-- Nav -->
    <div class="nav-holder">
        <div class="container">
            <div class="maga-drop-wrap">

                <!-- Logo -->
                <div class="logo">
                    <a href="<?php echo e(url('')); ?>"><img style="width: 100px; height: auto;" src="<?php echo e(asset('assets/front/images/FIS-Club-Badge-new.png')); ?>" alt=""></a>
                </div>
                <!-- Logo -->

                <!-- Search Bar -->
                <!-- <div class="search-bar-holder pull-right">
                    <div class="search-bar">
                        <input type="text" class="form-control" placeholder="search enter please...">
                        <i class="fa fa-search"></i>
                    </div>
                </div> -->
                <!-- Search Bar -->

                <!-- Nav List -->
                 <?php if(auth()->guard()->guest()): ?>                
                <ul class="nav-list pull-right">                     
                    <li class="<?php echo e(@$activeMenu == 'home' ? 'active' : ''); ?>">
                        <a href="<?php echo e(url('')); ?>">Home</a>
                    </li>

                    <li <?php if(@Route::input('slug') == @$pages[7]['slug']): ?> class="active"   <?php endif; ?>>
                         <a href="<?php echo e(route('page', [@$pages[7]['slug']])); ?>">Cashback Offer</a>
                    </li>
                     <li <?php if(@Route::input('slug') == @$pages[6]['slug']): ?> class="active"   <?php endif; ?>>
                        <a href="<?php echo e(route('page', [@$pages[6]['slug']])); ?>" class="">Media</a>
                    </li>                        

                    <li class="<?php echo e(@$activeSubMenu == 'articles' ? 'active' : ''); ?>"><a href="<?php echo e(route('articles')); ?>" class="">Articles</a>
                    </li>
                    <li class="<?php echo e(@$activeMenu == 'media-buzz' ? 'active' : ''); ?>">
                        <a href="javascript:void(0)" class="">Media Buzz</a>
                        <ul>
                            <li class="<?php echo e(@$activeSubMenu == 'what-is-media-buzz' ? 'active' : ''); ?>">
                                <a href="<?php echo e(route('page', [@$pages[4]['slug']])); ?>" class="">What is Media Buzz?</a>
                            </li>
                            <li class="<?php echo e(@$activeSubMenu == 'media-buzz-player-history' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Media Buzz Winner player history</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'media-buzz-team-history' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Media Buzz Winner team history</a></li>
                        </ul>
                    </li>

                    <li class="<?php echo e(@$activeMenu == 'performance-buzz' ? 'active' : ''); ?>">
                        <?php @$todayDate = Helper::encrypt(date('Y-m-d')); ?>
                        <a href="#" class="show-login-frm">Performance Buzz</a>
                        <ul>
                            <li class="<?php echo e(@$activeSubMenu == 'what-is-performance-buzz' ? 'active' : ''); ?>"><a href="<?php echo e(route('page', [@$pages[5]['slug']])); ?>" class="">What is Performance Buzz?</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-player' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Winner Player History</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-team' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Winner Team History</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'pb-planner' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Planner</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-statistics' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Statistics</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-stats-history' ? 'active' : ''); ?>"><a href="#" class="show-login-frm">Performance Buzz Scores History</a></li>
                        </ul>
                    </li>

                    <li class="<?php echo e(@$activeMenu == 'database' ? 'active' : ''); ?>">
                        <a href="#" class="show-login-frm">Database</a>
                    </li>

                    
                    <li class="<?php echo e(@$activeMenu == 'spreadsheet' ? 'active' : ''); ?>">
                        <a href="#" class="show-login-frm">Spreadsheet</a>

                        <ul>
                            <?php $__currentLoopData = $spreadsheets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spreadsheet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="<?php echo e(@$activeSubMenu == 'spreadsheet' ? 'active' : ''); ?>">
                                <a href="#" class="show-login-frm"><?php echo e(@$spreadsheet->file_name); ?></a>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>

                    </li>
                              
                </ul>
                <?php else: ?>                
                <ul class="nav-list pull-right">                     
                    <li class="<?php echo e(@$activeMenu == 'home' ? 'active' : ''); ?>">
                        <a href="<?php echo e(url('')); ?>">Home</a>
                    </li>

                    <li <?php if(@Route::input('slug') == @$pages[7]['slug']): ?> class="active"   <?php endif; ?>>
                         <a href="<?php echo e(route('page', [@$pages[7]['slug']])); ?>">Cashback Offer</a>
                    </li>

                    <li class="<?php if(@Route::input('slug') == @$pages[6]['slug']): ?> active  <?php endif; ?> <?php echo e(@$activeMenu == 'media' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('page', [@$pages[6]['slug']])); ?>">Media</a>
                    </li>                        

                    <li class="<?php echo e(@$activeSubMenu == 'articles' ? 'active' : ''); ?>"><a href="<?php echo e(route('articles')); ?>">Articles</a>
                    </li>

                    <li class="<?php echo e(@$activeMenu == 'media-buzz' ? 'active' : ''); ?>">
                        <a href="javascript:void(0)">Media Buzz</a>
                        <ul>
                            <li class="<?php echo e(@$activeSubMenu == 'what-is-media-buzz' ? 'active' : ''); ?>">
                                <a href="<?php echo e(route('page', [@$pages[4]['slug']])); ?>">What is Media Buzz?</a>
                            </li>
                            <li class="<?php echo e(@$activeSubMenu == 'media-buzz-player-history' ? 'active' : ''); ?>"><a href="<?php echo e(route('media.buzz.player')); ?>">Media Buzz Winner player history</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'media-buzz-team-history' ? 'active' : ''); ?>"><a href="<?php echo e(route('media.buzz.team')); ?>">Media Buzz Winner team history</a></li>
                        </ul>
                    </li>

                    <li class="<?php echo e(@$activeMenu == 'performance-buzz' ? 'active' : ''); ?>">
                        <?php @$todayDate = Helper::encrypt(date('Y-m-d')); ?>
                        <a href="javascript:void(0)">Performance Buzz</a>
                        <ul>
                            <li class="<?php echo e(@$activeSubMenu == 'what-is-performance-buzz' ? 'active' : ''); ?>"><a href="<?php echo e(route('page', [@$pages[5]['slug']])); ?>">What is Performance Buzz?</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-player' ? 'active' : ''); ?>"><a href="<?php echo e(route('performance.buzz.player')); ?>">Performance Buzz Winner Player History</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-team' ? 'active' : ''); ?>"><a href="<?php echo e(route('performance.buzz.team')); ?>">Performance Buzz Winner Team History</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'pb-planner' ? 'active' : ''); ?>"><a href="<?php echo e((\Auth::user()->isPremium()) ? route('pb.planner', @$todayDate) : route('pb.planner.upgrade')); ?>">Performance Buzz Planner</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-statistics' ? 'active' : ''); ?>"><a href="<?php echo e((\Auth::user()->isPremium()) ? route('performance.buzz.statistics') : route('performance.buzz.statistics.upgrade')); ?>">Performance Buzz Statistics</a></li>
                            <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-stats-history' ? 'active' : ''); ?>"><a href="<?php echo e((\Auth::user()->isPremium()) ? route('performance.buzz.statshistory') : route('performance.buzz.statshistory.upgrade')); ?>">Performance Buzz Scores History</a></li>
                        </ul>
                    </li>

                    <li class="<?php echo e(@$activeMenu == 'database' ? 'active' : ''); ?>">
                        <a href="<?php echo e((\Auth::user()->isPremium()) ? route('database') : route('database.upgrade')); ?>">Database</a>
                    </li>

                    <?php if(isset($spreadsheets) && count($spreadsheets) > 0): ?>
                    <li>
                        <a href="#" class="show-login-frm">Spreadsheet</a>
                        <ul>
                            <?php $__currentLoopData = $spreadsheets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spreadsheet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li>
                                <a href="<?php echo e(url('spreadsheets/'.@$spreadsheet->file_path)); ?>"><?php echo e(@$spreadsheet->title); ?></a>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </li>
                    <?php endif; ?>                                                                     
                    
                </ul>
                <!-- Nav List -->
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- Nav -->

</header>
<style type="text/css">
    .licence{
        float: left;
        margin-left: 24%;
        font-weight: bold;
        
    }
    .licence li span{
        color: #fff;
        font-size: 16px;
    }
    .licence li img{
        width: auto;
        height: 30px;
        top: -4px;
        position: relative;
    }
    @media  screen and  (max-width: 1058px) and (min-width: 1000px) {
        .licence{
            float: left;
            margin-left: 17%;    
        }
        .licence .login-modal{
            padding: 18px 0px !important;
        }
    }
   @media  screen and  (max-width: 1000px) and (min-width: 768px) {
        .licence{
            float: left;
            margin-left: 14%;    
        }
        .licence .login-modal{
            padding: 18px 0px !important;
        }
    }
    @media  screen and  (max-width: 767px) and (min-width: 375px) {
        .licence{
            float: left;
            margin-left: 14%;    
        }
        .licence li img{
             width: auto;
            height: 28px;
        }
        .licence li span{
            font-size: 15px;   
        }
        .header.style-3 .user-login-option{
            padding: 10px 0px !important;
        }
         .licence .login-modal{
            padding: 18px 20px !important;
        }
       
    }
    @media  screen and  (max-width: 414px) and (min-width: 320px) {
        .licence{
            float: right;
            margin-left: 0%;    
        }
        .licence li img{
            width: auto;
            height: 28px;
        }
        .licence li span{
            font-size: 13px;   
        }
        .header.style-3 .user-login-option{
            padding: 0px !important;
        }
        .licence .login-modal{
            padding: 18px 20px !important;
        }
    }
    @media  screen and (max-width: 320px){
        .licence .login-modal{
            padding: 18px 0px !important;
        }
    }

</style>