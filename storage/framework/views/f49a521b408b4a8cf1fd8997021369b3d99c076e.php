<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2><?php echo e(@$page->title); ?></h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Pages</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('uploads/pages/'.@$page->image_path)); ?>">
</div>


<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                <?php echo $__env->make('front.pages.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-8">

                    <div class="team-detail-content theme-padding-bottom">
                        <?php if($message = Session::get('success')): ?>
                        <div class="custom-alerts alert alert-success fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php echo $message; ?>

                        </div>
                        <?php Session::forget('success'); ?>
                        <?php endif; ?>
                        <?php if($message = Session::get('error')): ?>
                        <div class="custom-alerts alert alert-danger fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <?php echo $message; ?>

                        </div>
                        <?php Session::forget('error'); ?>
                        <?php endif; ?>
                        <?php if($cancel_at_period_end): ?>
                        <div class="custom-alerts alert alert-info fade in">
                            Your account is premium until <?php echo date('d/m/Y', $billing_cycle_anchor); ?>

                        </div>
                        <?php endif; ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">Cancel Subscription</div>
                            <div class="panel-body">
                                <div class="text-center">
                                    <?php if($cancel_at_period_end): ?>
                                    <a href="javascript:;" class="btn btn-primary disabled">Cancel Subscription</a>
                                    <?php else: ?>
                                    <a href="<?php echo e(route('cancel-subcription')); ?>" class="btn btn-primary">Cancel Subscription</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Match Result Contenet -->
            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>