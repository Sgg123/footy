<?php $__env->startSection('title', '- Contact Us'); ?>
<?php $__env->startSection('meta_desc'); ?>
<meta name="keywords" content="<?php echo e(@$page->meta_keywords); ?>" />
<meta name="description" content="<?php echo e(@$page->meta_description); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2><?php echo e(@$page->title); ?></h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Pages</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('uploads/pages/'.@$page->image_path)); ?>">
</div>

<!-- Main Content -->
<main class="main-content"> 
 <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
   
                <h1><?php echo e(@$page->title); ?></h1>
                <?php echo @$page->content; ?>



<div class="container">
<div class="panel panel-primary">
  <div class="panel-body"> 
    <form method="POST" action="<?php echo e(route('postcontact')); ?>" />
            <?php echo e(csrf_field()); ?>

               <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        <input class="form-control" type="text" name="name" placeholder="name">
                        <?php echo $errors->first('name', '<p class="alert alert-danger">:message</p>'); ?>

                    </div>
                </div>
                 <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email:</strong>
                        <input class="form-control" type="text" name="email" placeholder="Email">
                        <?php echo $errors->first('email', '<p class="alert alert-danger">:message</p>'); ?>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Message:</strong>
                        <textarea class="form-control" name="message" placeholder="Message" style="height:100px;"> 
                        </textarea>
                        <?php echo $errors->first('message', '<p class="alert alert-danger">:message</p>'); ?>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <input class="btn btn-success" type="submit" name="save" id="success-btn">
                </div>
      </form>
    </div>
</div>
</div>
            
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<!--<script src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<script src="<?php echo e(asset('assets/front/js/gmap3.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/front/js/contact-form.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<script>
  <?php if(Session::has('message')): ?>
    var type = "<?php echo e(Session::get('alert-type', 'info')); ?>";
    switch(type){
        case 'info':
            toastr.info("<?php echo e(Session::get('message')); ?>");
            break;
        
        case 'warning':
            toastr.warning("<?php echo e(Session::get('message')); ?>");
            break;
        case 'success':
            toastr.success("<?php echo e(Session::get('message')); ?>");
            break;
        case 'error':
            toastr.error("<?php echo e(Session::get('message')); ?>");
            break;
    }
  <?php endif; ?>
</script>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>