<div class="row" id="view_team_players">
    <div class="col-sm-6 detail-player">
        <table class="table table-bordered firstplayer-list">
            <tbody>
            <!--<h3><span><img src="<?php echo e(@$localTeam[0]['logo_path']); ?>" class="team_logo"></span></h3>-->
            <h3><span><?php echo e(@$localTeam[0]['name']); ?></span></h3>
            <?php if(@$localTeam[0]['get_team_players']): ?>
            <?php $__currentLoopData = @$localTeam[0]['get_team_players']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $player): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td class="players-image-logo"><img class="player_image view-player" data-id="<?php echo e(@$player['pid']); ?>"
                    <?php if(@$player['is_local_image'] == 0): ?>
                       src="<?php echo e(@$player['image_path']); ?>"
                    <?php elseif(@$player['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$player['local_image_path']))): ?>
                       src="<?php echo e(asset('uploads/player-images/'.@$player['local_image_path'])); ?>"
                    <?php else: ?>
                      src ="<?php echo e(asset('assets/images/no-image.jpeg')); ?>" 
                    <?php endif; ?>
                    >

                   

                  </td>
          
                <td class="view-player" data-id="<?php echo e(@$player['pid']); ?>" data-teamid="<?php echo e(@$localTeam[0]['id']); ?>"><?php echo e(@$player['football_index_common_name'] != "" ? @$player['football_index_common_name'] : @$player['common_name']); ?> - <?php echo e($player['football_index_position']); ?>

                    <br/>
                <?php
                  $colorCode = "";
                  $fontcolor = "";
                  $playerScore = @number_format(@$player['total_score']); 
                ?>
                <?php if(@$playerScore <= 30): ?>
                  <?php
                  $colorCode = "#FF0000";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php elseif(@$playerScore >= 31 && @$playerScore <= 70): ?>
                  <?php
                  $colorCode = "#FF5733";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php elseif(@$playerScore >= 71 && @$playerScore <= 110): ?>
                  <?php
                  $colorCode = "#eacd0e";
                  $fontcolor = "#000";
                  ?>
                <?php elseif(@$playerScore >= 111 && @$playerScore <= 150): ?>
                  <?php
                  
                  $colorCode = "#FFFF00";
                  $fontcolor = "#000";
                  ?>
                <?php elseif(@$playerScore >= 151 && @$playerScore <= 180): ?>
                  <?php
                  $colorCode = "#aad77d";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php elseif(@$playerScore >= 181): ?>
                  <?php
                  $colorCode = "#39a02b";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php endif; ?>
                <div style="background:<?php echo e($colorCode); ?>; color:<?php echo e($fontcolor); ?>">
                  <span class="pb_player_score"><?php echo e(@number_format($player['total_score'],0.5)); ?></span>
                </div>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
            <tr>
                <td>No Api Data found.</td>
            </tr>
            <?php endif; ?>
            </tbody> 
        </table>
    </div>

    <div class="col-sm-6 detail-player ">
        <table class="table table-bordered secondplayer-list">
            <!--<h3><span><img src="<?php echo e(@$visitorTeam[0]['logo_path']); ?>" class="team_logo"></span></h3>-->
            <h3><span><?php echo e(@$visitorTeam[0]['name']); ?></span></h3>
            <?php if(@$visitorTeam[0]['get_team_players']): ?>
            <?php $__currentLoopData = @$visitorTeam[0]['get_team_players']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $player): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td class="players-image-logo"><img class="player_image view-player" data-id="<?php echo e(@$player['pid']); ?>"
                  <?php if(@$player['is_local_image'] == 0): ?>
                    src="<?php echo e(@$player['image_path']); ?>"
                  <?php elseif(@$player['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$player['local_image_path']))): ?>
                    src="<?php echo e(asset('uploads/player-images/'.@$player['local_image_path'])); ?>"
                  <?php else: ?>
                    src ="<?php echo e(asset('assets/images/no-image.jpeg')); ?>" 
                  <?php endif; ?>
                ></td>

                <td class="view-player" data-id="<?php echo e(@$player['pid']); ?>" data-teamid="<?php echo e(@$visitorTeam[0]['id']); ?>"><?php echo e(@$player['football_index_common_name'] != "" ? @$player['football_index_common_name'] : @$player['common_name']); ?> - <?php echo e($player['football_index_position']); ?>

            <br/>
                <?php
                  $colorCode = "";
                  $fontcolor = "";
                  $playerScore = @number_format(@$player['total_score']); 
                ?>
                <?php if(@$playerScore <= 30): ?>
                  <?php
                  $colorCode = "#FF0000";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php elseif(@$playerScore >= 31 && @$playerScore <= 70): ?>
                  <?php
                  $colorCode = "#FF5733";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php elseif(@$playerScore >= 71 && @$playerScore <= 110): ?>
                  <?php
                  $colorCode = "#eacd0e";
                  $fontcolor = "#000";
                  ?>
                <?php elseif(@$playerScore >= 111 && @$playerScore <= 150): ?>
                  <?php
                  $colorCode = "#FFFF00";
                  $fontcolor = "#000";
                  ?>
                <?php elseif(@$playerScore >= 151 && @$playerScore <= 180): ?>
                  <?php
                  $colorCode = "#aad77d";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php elseif(@$playerScore >= 180): ?>
                  <?php
                  $colorCode = "#39a02b";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php endif; ?>
                <div style="background:<?php echo e($colorCode); ?>; color:<?php echo e($fontcolor); ?>;">
                  <span class="pb_player_score"><?php echo e(@number_format($player['total_score'],0.5)); ?></span>
                </div>
           </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
            <tr>
                <td>No Api Data found.</td>
            </tr>
            <?php endif; ?>

        </table>

    </div>

</div>

<div class="row" id="view_form_players" style="display:none;">
    <div class="col-sm-6 detail-player">
        <table class="table table-bordered firstplayer-list">
            <tbody>
            <!--<h3><span><img src="<?php echo e(@$localTeam[0]['logo_path']); ?>" class="team_logo"></span></h3>-->
            <!-- <h3><span><?php echo e(@$localTeam[0]['name']); ?></span></h3> -->
            <?php if(@$localTeam[0]['get_team_players']): ?>
            <?php $__currentLoopData = @$all_players; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $player): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td class="players-image-logo"><img class="player_image view-player" data-id="<?php echo e(@$player['pid']); ?>"
                  <?php if(@$player['is_local_image'] == 0): ?>
                    src="<?php echo e(@$player['image_path']); ?>"
                  <?php elseif(@$player['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$player['local_image_path']))): ?>
                    src="<?php echo e(asset('uploads/player-images/'.@$player['local_image_path'])); ?>"
                  <?php else: ?>
                    src ="<?php echo e(asset('assets/images/no-image.jpeg')); ?>" 
                  <?php endif; ?>
                  ></td>
      
                <td class="view-player" data-id="<?php echo e(@$player['pid']); ?>" data-teamid="<?php echo e(@$player['team_id']); ?>">
                  <?php echo e(@$player['football_index_common_name'] != "" ? @$player['football_index_common_name'] : @$player['common_name']); ?> - <?php echo e($player['football_index_position']); ?>

                <br/>
                <?php
                  $colorCode = "";
                  $fontcolor = "";
                  $playerScore = @number_format(@$player['total_score']); 
                ?>
                <?php if(@$playerScore <= 30): ?>
                  <?php
                  $colorCode = "#FF0000";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php elseif(@$playerScore >= 31 && @$playerScore <= 70): ?>
                  <?php
                  $colorCode = "#FF5733";
                  $fontcolor = "#FFFFFF"; 
                  ?>
                <?php elseif(@$playerScore >= 71 && @$playerScore <= 110): ?>
                  <?php
                  $colorCode = "#eacd0e";
                  $fontcolor = "#000";
                  ?>
                <?php elseif(@$playerScore >= 111 && @$playerScore <= 150): ?>
                  <?php
                  $colorCode = "#FFFF00";
                  $fontcolor = "#000";
                  ?>
                <?php elseif(@$playerScore >= 151 && @$playerScore <= 180): ?>
                  <?php
                  $colorCode = "#aad77d";
                  $fontcolor = "#FFFFFF"; 
                  ?>
                <?php elseif(@$playerScore >= 180): ?>
                  <?php
                  $colorCode = "#39a02b";
                  $fontcolor = "#FFFFFF";
                  ?>
                <?php endif; ?>
                <div style="background:<?php echo e($colorCode); ?>;color:<?php echo e($fontcolor); ?>">
                  <span class="pb_player_score"><?php echo e(@number_format($player['total_score'],0.5)); ?></span>
                </div>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
            <tr>
                <td>No Api Data found.</td>
            </tr>
            <?php endif; ?>
            </tbody> 
        </table>
    </div>


</div>