<h2>Welcome to Footyindex Scout!</h2>

<div>
    <p>Hi <?php echo e(@$user['firstname']); ?> <?php echo e(@$user['lastname']); ?>,</p>
    <p>
        Your user created successfully. Following is your credential.
        <br>
        Username: <?php echo e(@$user['email']); ?>

        <br>
        Password: <?php echo e(@$password); ?>

        <br>
        Please click on below link to get started & activate your account.
    </p>
    <b>
        <a href="<?php echo e(route('activate-user',[Helper::encrypt(@$user['email'])])); ?>">Click here</a>
    </b>
</div>