<?php $__env->startSection('content'); ?>


<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Portfolio Assistant</h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Portfolio Assistant</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg')); ?>">
</div>

<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                <?php echo $__env->make('front.pages.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-12">

                    <!-- Piont Table -->
                    <div class="macth-fixture">
                        
                        <div class="last-matches styel-3">
                            <!-- Table Style 3 -->
                            <div class="table-responsive padding-top-70">
                                <img src="<?php echo e(asset('assets/front/images/Portfolio-Integration-6-Players.jpg')); ?>" class="portfolio-assistant-img">                                                              
                            </div>
                        </div>
                    </div>
                    <!-- Piont Table -->

                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>