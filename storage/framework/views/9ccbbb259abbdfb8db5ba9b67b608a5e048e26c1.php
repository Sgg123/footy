<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Performance Buzz Data Files</li>
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-tags"></i> Performance Buzz Data Files           
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <a href="<?php echo e(route('performance_buzz_files_new')); ?>" class="btn btn-primary pull-right" style="margin-left: 10px;">Add New Data File</a>
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="seasons-table">
                    <thead>
                        <tr>
                            <th>File</th>
                            <th>Date</th>                            
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $pbfiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pbfile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($pbfile->file_name); ?></td>
                            <td><?php echo e($pbfile->date); ?></td>
                            <td><a href="javascript:void(0)" onclick="delete_pb_files(<?php echo e($pbfile->id); ?>)">Delete</a></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                     
                    </tbody> 
                </table>
            </div>            
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_js'); ?>
<script type="text/javascript">
    function delete_pb_files(id){
        var conf = confirm("Are you sure you want to delete all records of file as well as data?");
        if(conf==true){
            $.ajax({
                url: '<?php echo e(route('performance_buzz_files_del')); ?>',
                data: {id:id},
                success: function(data){
                    alert('File and data deleted');
                    location.reload();
                }
            });
        }
        return false;
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>