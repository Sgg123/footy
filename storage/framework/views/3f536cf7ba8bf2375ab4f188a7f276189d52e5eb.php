<?php $__env->startSection('page_css'); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?php echo e(route('day_and_dividends_list')); ?>">
                Day And Dividends
            </a>
        </li>
        <li class="breadcrumb-item active">
            Edit
        </li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Edit Day and Dividends
        </div>
        <div class="card-body">
            <form method="POST" action="<?php echo e(route('day_and_dividends_post', ['id' => Helper::encrypt(@$id)])); ?>">
  
                    <?php echo e(csrf_field()); ?>                   

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Date <span class= "error">*</span></label>
                                <input type="text" class="form-control" name="date" value="<?php echo e($date); ?>">
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputName">Day <span class= "error">*</span></label>
                                <select name="type_of_day" id="type_of_day" class="form-control" onchange="getdividends(this)">
                                    <option <?php if(@$type_of_day == 1): ?> selected <?php endif; ?> value="1">Single Performance</option>
                                    <option <?php if(@$type_of_day == 2): ?> selected <?php endif; ?> value="2">Double Performance</option>
                                    <option <?php if(@$type_of_day == 3): ?> selected <?php endif; ?> value="3">Triple Performance</option>
                                    <option <?php if(@$type_of_day == 4): ?> selected <?php endif; ?> value="4">Triple Media</option>
                                    <option <?php if(@$type_of_day == 5): ?> selected <?php endif; ?> value="5">Media Madness Top 5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-2">
                                <label for="">Top Player PB</label>
                                <input type="text" class="form-control" name="top_performance_player" value="<?php echo e($top_performance_player); ?>" id="top_performance_player">
                                
                            </div>
                            <div class="col-md-2">
                                <label for="">Top PB Positional</label>
                                <input type="text" class="form-control" name="top_positional_performance_player" value="<?php echo e($top_positional_performance_player); ?>" id="top_positional_performance_player">
                                
                            </div>
                            <div class="col-md-2">
                                <label for="">MB 1st</label>
                                <input type="text" class="form-control" name="media_buzz_first" value="<?php echo e($media_buzz_first); ?>" id="media_buzz_first">
                                
                            </div>
                            <div class="col-md-2">
                                <label for="">MB 2nd</label>
                                <input type="text" class="form-control" name="media_buzz_second" value="<?php echo e($media_buzz_second); ?>" id="media_buzz_second">
                            </div>
                            <div class="col-md-2">
                                <label for="">MB 3rd</label>
                                <input type="text" class="form-control" name="media_buzz_third" value="<?php echo e($media_buzz_third); ?>" id="media_buzz_third">
                            </div>
                            <div class="col-md-2">
                                <label for="">MB 4th</label>
                                <input type="text" class="form-control" name="media_buzz_fourth" value="<?php echo e($media_buzz_fourth); ?>" id="media_buzz_fourth">
                            </div>
                            <div class="col-md-2">
                                <label for="">MB 5th</label>
                                <input type="text" class="form-control" name="media_buzz_fifth" value="<?php echo e($media_buzz_fifth); ?>" id="media_buzz_fifth">
                            </div>
                        </div>
                    </div>
                    
                    <button class="btn btn-primary" type="submit">Update</button>
                    <a class="btn btn-danger" href="<?php echo e(route('day_and_dividends_list')); ?>">Cancel</a>   
                </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $("#mb_date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });
    $('.select2').select2({dropdownCssClass: 'bigdrop'});
    function getdividends(obj){
        var day_type = $('#type_of_day').val();
        if(day_type==1){
            $('#top_performance_player').val(0.01);
            $('#top_positional_performance_player').val(0.02);
            $('#media_buzz_first').val(0.02);
            $('#media_buzz_second').val(0.00);
            $('#media_buzz_third').val(0.00);
        }
        if(day_type==2){
            $('#top_performance_player').val(0.02);
            $('#top_positional_performance_player').val(0.03);
            $('#media_buzz_first').val(0.02);
            $('#media_buzz_second').val(0.00);
            $('#media_buzz_third').val(0.00);
        }
        if(day_type==3){
            $('#top_performance_player').val(0.02);
            $('#top_positional_performance_player').val(0.05);
            $('#media_buzz_first').val(0.02);
            $('#media_buzz_second').val(0.00);
            $('#media_buzz_third').val(0.00);
        }
        if(day_type==4){
            $('#top_performance_player').val(0.00);
            $('#top_positional_performance_player').val(0.00);
            $('#media_buzz_first').val(0.03);
            $('#media_buzz_second').val(0.02);
            $('#media_buzz_third').val(0.01);
        }
        if(day_type==5){
            $('#top_performance_player').val(0.00);
            $('#top_positional_performance_player').val(0.00);
            $('#media_buzz_first').val(0.03);
            $('#media_buzz_second').val(0.02);
            $('#media_buzz_third').val(0.01);
            $('#media_buzz_fourth').val(0.01);
            $('#media_buzz_fifth').val(0.01);
        }
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>