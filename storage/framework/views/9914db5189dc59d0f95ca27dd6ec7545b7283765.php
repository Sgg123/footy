<?php $__env->startSection('page_css'); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?php echo e(route('performance-buzz.index')); ?>">
                Performance Buzz History
            </a>
        </li>
        <li class="breadcrumb-item active">
            <?php echo e(@$pbData === null ? 'Add' : 'Edit'); ?>

        </li>
    </ol>

    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> <?php echo e(@$pbData === null ? 'Add' : 'Edit'); ?> Performance Buzz History
        </div>
        <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-body">
            <?php if(@$pbData): ?>
            <form method="POST" action="<?php echo e(route('performance-buzz.update', ['id' => Helper::encrypt(@$pbData->id)])); ?>">
                <?php echo e(method_field('PUT')); ?>                
                <?php else: ?>
                <form method="POST" action="<?php echo e(route('performance-buzz.store')); ?>">
                    <?php endif; ?>
                    <?php echo e(csrf_field()); ?>     

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">PLAYER NAME</label>
                                <select class="form-control player_id select2" name="player_id" id="player_id">
                                    <option value="">Select Player</option>
                                    <?php $__currentLoopData = @$players; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $player): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($player->id); ?>" <?php if(@$pbData->player_id == $player->id): ?> selected <?php endif; ?>><?php echo e($player->football_index_common_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>                               
                                <label id="player_id-error" class="error" for="player_id"><?php echo e(@$errors->first('player_id')); ?></label>
                            </div>                            
                        </div>
                    </div>                    

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">TEAM NAME </label>
                                <select class="form-control player_id select2" name="team_id" id="team_id">
                                    <option value="">Select Team</option>
                                    <?php $__currentLoopData = @$teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($team->id); ?>" <?php if(@$pbData->team_id == $team->id): ?> selected <?php endif; ?>><?php echo e($team->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>                               
                                <label id="team_id-error" class="error" for="team_id"><?php echo e(@$errors->first('team_id')); ?></label>
                            </div>                            
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputLastName">Score </label>
                                <input class="form-control number" id="score" name="score" type="text" placeholder="Score" maxlength="10" value="<?php echo e(old('score', @$pbData->score)); ?>">
                                <label id="score-error" class="error" for="score"><?php echo e(@$errors->first('score')); ?></label>
                            </div>                           
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputLastName">DATE </label>
                                <input class="form-control number" id="date" name="date" type="text" placeholder="Date" maxlength="10" value="<?php echo e(old('date', @$pbData->date)); ?>" <?php if(@$pbData != ""): ?> readonly <?php endif; ?>>
                                       <label id="date-error" class="error" for="date"><?php echo e(@$errors->first('date')); ?></label>
                            </div>                       
                        </div>
                    </div>

                    <button class="btn btn-primary" type="submit"><?php echo e(@$playerHistory === null ? 'Save' : 'Update'); ?></button>
                    <a class="btn btn-danger" href="<?php echo e(route('performance-buzz-winner-player.index')); ?>">Cancel</a>	
                </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $('.select2').select2({dropdownCssClass: 'bigdrop'});

    $("#date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>