<?php $__env->startSection('page_css'); ?>
<link href="<?php echo e(asset('assets/admin/css/selectize.default.css')); ?>" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Fixtures</li>
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-tags"></i> Fixtures           
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <form action="<?php echo route('media.buzz.player.data'); ?>" id="search-form" method="GET">
                <?php echo e(csrf_field()); ?>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-4">
                            <select class="form-control selectize-dropdowns-category" name="league_id" id="league_id">
                                <option value="">Select League</option>
                                <?php $__currentLoopData = $leagues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $league): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($league['id']); ?>"><?php echo e($league['name']); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>                            
                        </div>
                        <div class="col-md-4">
                            <select class="form-control selectize-dropdowns-category" name="season_id" id="season_id">
                                <option value="">Select Season</option>

                            </select> 
                        </div>
                        <div class="col-md-3">
                            <input type="submit" value="Filter" class="btn btn-primary"/>
                        </div>
                    </div>
                </div>

            </form>
            <div class="table-responsive">
             <table class="table table-bordered" width="100%" cellspacing="0" id="seasons-table">
                    <thead>
                        <tr>
                            <th>League</th>
                            <th>Season</th>
                            <th>Local Team</th>
                            <th>Visitor Team</th>
                            <th>Pitch</th>
                            <th>Status</th>                            
                            <th>Starting At</th>
                            <th>Minute</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>                        
                    </tbody> 
                </table>
            </div>            
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" charset="utf-8"></script>
<script src="<?php echo e(asset('assets/admin/js/selectize.min.js')); ?>"></script>
<?php echo $__env->make('admin.fixtures.common-script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>