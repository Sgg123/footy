<?php $__env->startSection('title', '- Free Tools and Premium Features'); ?>
<?php $__env->startSection('meta_desc'); ?>
<meta name="keywords" content="<?php echo e(@$pages[8]['meta_keywords']); ?> <?php echo e(@$pages[9]['meta_keywords']); ?>" />
<meta name="description" content="<?php echo e(@$pages[8]['meta_description']); ?> <?php echo e(@$pages[9]['meta_keywords']); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Match Detail -->

<section class="theme-padding-bottom bg-fixed">

    <?php if(auth()->guard()->guest()): ?> 
    <?php else: ?>
    <div class="activate-alert">
        <!-- alert -->
        <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Upcoming match slider -->
    </div>    
    <!-- Upcoming match slider -->
    <?php ($page = "homepage") ?>

    <?php echo $__env->make('front.common.upcoming-match-slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Upcoming match slider -->
    <?php endif; ?>
    <div class="container">

        <?php if(auth()->guard()->guest()): ?>
        <div class="logout-section">
            <!-- Latest Article -->
            <?php echo $__env->make('front.common.latest-article', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- Latest Article -->
        </div>
        <?php else: ?>



        <!-- Match Detail Content -->
        <div class="match-detail-content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">

                        <!-- Latest Article -->
                        <?php echo $__env->make('front.common.latest-article', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <!-- Latest Article -->

                        <!-- Upcoming Fixture -->
                        <div class="col-sm-12 col-xs-12 r-full-width p-full-width">
                            <h2><span><i class="red-color">Media Buzz Last Week Winner Player</i></span><!-- <a class="view-all pull-right" href="<?php echo e(route('media.buzz.player')); ?>">view all<i class="fa fa-angle-double-right"></i></a> --></h2>
                            <div class="last-matches styel-3">
                                <!-- Table Style 3 -->
                                <div class="table-responsive padding-top-70">
                                    <table class="table table-bordered table-striped table-dark-header">
                                        <thead class="player_heading">
                                            <tr>
                                                <th>DATE</th>
                                                <th >MB 1st Place</th>
                                                <th  >SCORE</th>
                                                <th >MB 2nd Place</th>
                                                <th >SCORE</th>
                                                <th >MB 3rd Place</th>
                                                <th >SCORE</th>
                                            </tr>
                                        </thead>
                                        <tbody class="player_alldetails">
                                            <?php if(!empty($data)): ?>
                                            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $player_name = explode(',',@$history['player_names']); ?>
                                            <?php $player_common_name = explode(',',@$history['player_football_index_common_names']); ?>
                                            <?php $player_image = explode(',',@$history['player_images']); ?>
                                            <?php $player_is_local_image = explode(',',@$history['is_local_images']); ?>
                                            <?php $player_local_images = explode(',',@$history['player_local_images']); ?>
                                            <?php $player_team_name = explode(',',@$history['player_team_names']); ?>
                                            <?php $score = explode(',',@$history['scores']); ?>
                                            <?php $team_ids = explode(',',@$history['team_ids']); ?>
                                            <?php $player_ids = explode(',',@$history['player_ids']); ?>
                                            <tr>
                                                <td><?php echo e(date('d M',strtotime(@$history['date']))); ?><br/><?php echo e(date('Y',strtotime(@$history['date']))); ?></td>
                                                <td class="mb-player-details view-player" data-id="<?php echo e(@$player_ids[0]); ?>" data-teamid="<?php echo e(@$team_ids[0]); ?>">
                                                    <div class="col-md-4 mb-player-image">
                                                        <?php if(@$player_image[0] != "" || @$player_local_images[0] != ""): ?>
                                                        <img 
                                                            <?php if(@$player_is_local_image[0] == 0): ?>
                                                            src="<?php echo e(@$player_image[0]); ?>"
                                                            <?php else: ?>
                                                            src="<?php echo e(asset('uploads/player-images/'.@$player_local_images[0])); ?>"
                                                            <?php endif; ?>
                                                            >
                                                            <?php endif; ?>
                                                    </div>
                                                    <div class="player_heading_title">
                                                        <div class="col-md-8 remove_player_space">
                                                            <div class="mb-player-name">
                                                                <?php echo e(@$player_common_name[0] != "" ? @$player_common_name[0] : @$player_name[0]); ?>

                                                            </div>
                                                            <div class="mb-player-team-name"><?php echo e(@$player_team_name[0]); ?></div>
                                                        </div> 

                                                </td>
                                                </div> 
                                                <td class="mb-player-score"><?php echo e(@$score[0]); ?></td>
                                                <td class="mb-player-details view-player" data-id="<?php echo e(@$player_ids[1]); ?>" data-teamid="<?php echo e(@$team_ids[1]); ?>">
                                                    <div class="col-md-4 mb-player-image">
                                                        <?php if(@$player_image[1] != "" || @$player_local_images[1] != ""): ?>
                                                        <img 
                                                            <?php if(@$player_is_local_image[1] == 0): ?>
                                                            src="<?php echo e(@$player_image[1]); ?>"
                                                            <?php else: ?>
                                                            src="<?php echo e(asset('uploads/player-images/'.@$player_local_images[1])); ?>"
                                                            <?php endif; ?>
                                                            >
                                                            <?php endif; ?>
                                                    </div>
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            <?php echo e(@$player_common_name[1] != "" ? @$player_common_name[1] : @$player_name[1]); ?>

                                                        </div>
                                                        <div class="mb-player-team-name"><?php echo e(@$player_team_name[1]); ?></div>
                                                    </div>                                                
                                                </td>
                                                <td class="mb-player-score"><?php echo e(@$score[1]); ?></td>
                                                <td class="mb-player-details view-player" data-id="<?php echo e(@$player_ids[2]); ?>" data-teamid="<?php echo e(@$team_ids[2]); ?>">
                                                    <div class="col-md-4 mb-player-image">
                                                        <?php if(@$player_image[2] != "" || @$player_local_images[2] != ""): ?>
                                                        <img 
                                                            <?php if(@$player_is_local_image[2] == 0): ?>
                                                            src="<?php echo e(@$player_image[2]); ?>"
                                                            <?php else: ?>
                                                            src="<?php echo e(asset('uploads/player-images/'.@$player_local_images[2])); ?>"
                                                            <?php endif; ?>
                                                            >
                                                            <?php endif; ?>
                                                    </div>
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            <?php echo e(@$player_common_name[2] != "" ? @$player_common_name[2] : @$player_name[2]); ?>

                                                        </div>
                                                        <div class="mb-player-team-name"><?php echo e(@$player_team_name[2]); ?></div>
                                                    </div>                                                
                                                </td>
                                                <td class="mb-player-score"><?php echo e(@$score[2]); ?></td>
                                            </tr>  
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                            <tr>
                                                <td colspan="7">No data found</td>                                            
                                            </tr>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- Upcoming Fixture -->

                        <!-- Upcoming Fixture -->
                        <div class="col-sm-12 col-xs-12 r-full-width media_buzz_players_table">
                            <h3><span><i class="red-color">Performance Buzz Last Week Winners</i></span><!-- <a class="view-all pull-right" href="<?php echo e(route('media.buzz.team')); ?>">view all<i class="fa fa-angle-double-right"></i></a> --></h3>
                            <div class="last-matches styel-3">
                                <!-- Table Style 3 -->
                                <div class="table-responsive padding-top-70">
                                    <table class="table table-bordered table-striped table-dark-header advance-v">
                                        <thead class="player_heading player_heading">
                                            <tr>
                                                <th class="date-v-d new-p-set">DATE</th>
                                                <th class="date-v-d">PB TOP<br>PLAYER</th>
                                                <th class="date-v-d new-p-set">SCORE</th>
                                                <th class="top-for">PB TOP<br>FORWARD</th>
                                                <th class="top-for new-p-set-2">
                                                    <span class="sc-set">SCORE</span></th>
                                                <th class="mide-v">PB TOP<br>MIDFIELDER</th>
                                                <th class="mide-v sec-d-using"><span class="sc-set">SCORE</span></th>
                                                <th class="defender-v">PB TOP<br>DEFENDER</th>
                                                <th class="defender-v sec-d-using"><span class="sc-set">SCORE</span></th>
                                            </tr>
                                        </thead>
                                        <tbody class="player_alldetails d-view-set">
                                            <?php if(!empty($PBWinners)): ?>
                                            <?php $__currentLoopData = $PBWinners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                        
                                            <tr>
                                                <td><?php echo e(date('d M',strtotime(@$history['date']))); ?><br/><?php echo e(date('Y',strtotime(@$history['date']))); ?></td>

                                                <!--TOP PLAYER DETAILS-->
                                                <td class="mb-player-details view-player" data-id="<?php echo e(@$history['topPlayerPlayerId']); ?>" data-teamid="<?php echo e(@$history['topPlayerTeamId']); ?>">
                                                    <div class="col-md-4 mb-player-image">
                                                        <?php if(@$history['topPlayerLocalImagePath'] != "" || @$history['topPlayerImagePath'] != ""): ?>
                                                        <img 
                                                            <?php if(@$history['topPlayerIsLocalImage'] == 1 && @$history['topPlayerLocalImagePath'] != ""): ?>
                                                            src="<?php echo e(asset('uploads/player-images/'.@$history['topPlayerLocalImagePath'] )); ?>"
                                                            <?php else: ?>
                                                            src="<?php echo e(@$history['topPlayerImagePath']); ?>"
                                                            <?php endif; ?>
                                                            >
                                                            <?php endif; ?>
                                                    </div>
                                                    <div class="player_heading_title">
                                                        <div class="col-md-8 remove_player_space">
                                                            <div class="mb-player-name">
                                                                <?php echo e(@$history['topPlayerFCommonName'] != "" ? @$history['topPlayerFCommonName'] : @$history['topPlayerName']); ?>

                                                            </div>
                                                            <div class="mb-player-team-name"><?php echo e(@$history['topPlayerTeamName']); ?></div>
                                                        </div> 
                                                    </div> 
                                                </td>

                                                <td class="mb-player-score"><?php echo e(@$history['topPlayerScore']); ?></td>

                                                <!--TOP FORWARD DETAILS-->
                                                <td class="view-player" data-id="<?php echo e(@$history['forwardPlayerPlayerId']); ?>" data-teamid="<?php echo e(@$history['forwardPlayerTeamId']); ?>">
                                                    <div class="col-md-4 mb-player-image">
                                                        <?php if(@$history['forwardLocalImagePath'] != "" || @$history['forwardImagePath'] != ""): ?>
                                                        <img 
                                                            <?php if(@$history['forwardIsLocalImage'] == 1 && @$history['forwardLocalImagePath'] != ""): ?>
                                                            src="<?php echo e(asset('uploads/player-images/'.@$history['forwardLocalImagePath'] )); ?>"
                                                            <?php else: ?>
                                                            src="<?php echo e(@$history['forwardImagePath']); ?>"
                                                            <?php endif; ?>
                                                            >
                                                            <?php endif; ?>
                                                    </div>
                                                    <div class="player_heading_title">
                                                        <div class="col-md-8 remove_player_space">
                                                            <div class="mb-player-name">
                                                                <?php echo e(@$history['forwardFCommonName'] != "" ? @$history['forwardFCommonName'] : @$history['forwardName']); ?>

                                                            </div>
                                                            <div class="mb-player-team-name"><?php echo e(@$history['forwardTeamName']); ?></div>
                                                        </div> 
                                                    </div> 
                                                </td>

                                                <td class="mb-player-score"><?php echo e(@$history['forwardScore']); ?></td>

                                                <!--TOP MIDFIELDER DETAILS-->
                                                <td class="view-player" data-id="<?php echo e(@$history['midfielderPlayerPlayerId']); ?>" data-teamid="<?php echo e(@$history['midfielderPlayerTeamId']); ?>">
                                                    <div class="col-md-4 mb-player-image">
                                                        <?php if(@$history['midfielderLocalImagePath'] != "" || @$history['midfielderImagePath'] != ""): ?>
                                                        <img 
                                                            <?php if(@$history['midfielderIsLocalImage'] == 1 && @$history['midfielderLocalImagePath'] != ""): ?>
                                                            src="<?php echo e(asset('uploads/player-images/'.@$history['midfielderLocalImagePath'] )); ?>"
                                                            <?php else: ?>
                                                            src="<?php echo e(@$history['midfielderImagePath']); ?>"
                                                            <?php endif; ?>
                                                            >
                                                            <?php endif; ?>
                                                    </div>
                                                    <div class="player_heading_title">
                                                        <div class="col-md-8 remove_player_space">
                                                            <div class="mb-player-name">
                                                                <?php echo e(@$history['midfielderFCommonName'] != "" ? @$history['midfielderFCommonName'] : @$history['midfielderName']); ?>

                                                            </div>
                                                            <div class="mb-player-team-name"><?php echo e(@$history['midfielderTeamName']); ?></div>
                                                        </div> 
                                                    </div> 
                                                </td>

                                                <td class="mb-player-score"><?php echo e(@$history['midfielderScore']); ?></td>

                                                <!--TOP DEFENDER DETAILS-->
                                                <td class="view-player" data-id="<?php echo e(@$history['defenderPlayerPlayerId']); ?>" data-teamid="<?php echo e(@$history['defenderPlayerTeamId']); ?>">
                                                    <div class="col-md-4 mb-player-image">
                                                        <?php if(@$history['defenderLocalImagePath'] != "" || @$history['defenderImagePath'] != ""): ?>
                                                        <img 
                                                            <?php if(@$history['defenderIsLocalImage'] == 1 && @$history['defenderLocalImagePath'] != ""): ?>
                                                            src="<?php echo e(asset('uploads/player-images/'.@$history['defenderLocalImagePath'] )); ?>"
                                                            <?php else: ?>
                                                            src="<?php echo e(@$history['defenderImagePath']); ?>"
                                                            <?php endif; ?>
                                                            >
                                                            <?php endif; ?>
                                                    </div>
                                                    <div class="player_heading_title">
                                                        <div class="col-md-8 remove_player_space">
                                                            <div class="mb-player-name">
                                                                <?php echo e(@$history['defenderFCommonName'] != "" ? @$history['defenderFCommonName'] : @$history['defenderName']); ?>

                                                            </div>
                                                            <div class="mb-player-team-name"><?php echo e(@$history['defenderTeamName']); ?></div>
                                                        </div> 
                                                    </div> 
                                                </td>

                                                <td class="mb-player-score"><?php echo e(@$history['defenderScore']); ?></td>
                                            </tr>  
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                            <tr>
                                                <td colspan="7">No data found</td>                                            
                                            </tr>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <!-- Upcoming Fixture -->  

                        <!-- Match Detail Content -->
                        <?php endif; ?>

                    </div>
                    <div class="container">
                        <div class="row theme-padding">
                            <div class="col-md-6">
                                <div class="about-video-caption">
                                    <h2><span class="red-color"><?php echo @$pages[8]['title']; ?></span>   </h2>
                                    <?php echo @$pages[8]['content']; ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="about-video-caption">
                                    <h2><span class="red-color"><?php echo @$pages[9]['title']; ?></span>   </h2>
                                    <?php echo @$pages[9]['content']; ?>                   
                                </div>

                            </div> 
                        </div>
                    </div>

                    </section>
                    <!-- Match Detail -->
<!-- Modal -->

<div class="modal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg new-modal-dialog-v" role="document">
    <div class="modal-content bg-set-t">
      <div class="modal-header new-edit-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row">
            <div class="col-sm-3">
                <div class="player-v">
                <img src="https://cdn.sportmonks.com/images/soccer/players/26/31002.png">
                </div>
                <div class="set-player-btn">
                    <div class="fd-btn-py">
                        Forword
                    </div>
                    <div class="avrage-btn">
                        <div class="av-pb">
                            Average<br>PB Score
                        </div>
                        <div class="sc-wo">
                         154
                        </div>
                    </div>
                    
                    <div class="game-btn-v">
                        <div class="game-text">
                         Game<br>Played
                        </div>
                        <div class="game-con">
                        20
                        </div>
                    </div>


                </div>
            </div>
       
            <div class="col-sm-9 up-c-set">
                <div class="player-v-text">
                <h2>HARRY KANE</h2>
                 <div class="harry-img-v">
                <img src="https://footyindexscout.co.uk/public/assets/front/images/brand-icons/img-1-6.png" alt="">
                </div>
                <div class="fiture-b">
                <h3>Tottenham Hotspur Premier league</h3>
                </div>
                <div class="next-f">
                <h3>Next Fixture: V Juventus (5 days)</h3>
                </div>
               </div>
               <div class="rank-box row"> 
                <div class="col-sm-3 p-frd-set">
                    <div class="frd-rank">
                        FRW<br>Rank
                        <span>3</span>
                    </div>
                </div>
                <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-4 wins-p">
                        <div class="overl-1">
                            <div class="sep-overrall">
                            Overall<br>Rank
                            </div>
                            <span>15</span>
                        </div>
                    </div>
                     <div class="col-sm-4 wins-p">
                        <div class="overl-1">
                            <div class="sep-overrall">
                            Overall<br>Rank
                            </div>
                            <span>15</span>
                        </div>
                    </div>
                    <div class="col-sm-4 wins-p">
                        <div class="overl-1">
                            <div class="sep-overrall">
                            Overall<br>Rank
                            </div>
                            <span>15</span>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row media-wind-v">
                        <div class="col-sm-3">
                            <div class="u-media-f">
                             media winds
                           </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="onest-p">
                             1st Place<br>
                             <span>15</span>
                          </div>
                        </div>
                        <div class="col-sm-3">
                           <div class="onest-p">
                             2st Place<br>
                             <span>5</span>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="onest-p">
                             3st Place<br>
                             <span>4</span>
                          </div>
                        </div>
                    </div>
                </div>
                </div>    
               </div>
            </div>
       </div>
      </div>
      <div class="row">
        <div class="last-f-s">
            <div class="lst-s">
                Last 5 Scores
            </div>
            <div class="top-s">
                Top 5 Scores
            </div>
        </div>
      </div>
      <div class="row">
        <div class="different-b">
            <div class="main-d-v-set">
                <div class="first-d-v">
                <span>216</span>
                </div>
                <p>6 Nov 17</p>
            </div>
             <div class="main-d-v-set">
                <div class="sec-d-v">
                <span>92</span>
                </div>
                <p>6 Nov 17</p>
             </div>
             <div class="main-d-v-set">
                <div class="thurd-d-v">
                 <span>-18</span>
                </div>
                <p>6 Nov 17</p>
            </div>
            <div class="main-d-v-set">
                <div class="four-d-v">
                <span>216</span>    
                </div>
                <p>6 Nov 17</p>
             </div>
             <div class="main-d-v-set">
                <div class="five-d-v">
                <span>216</span>
                </div>
                <p>6 Nov 17</p>
            </div>
        </div>
      </div>
      <div class="row search-f-box">
        <div class="col-sm-4">
            <div class="f-search-f">
                Search Fixtures
            </div>
        </div>
        <div class="col-sm-4">
            <div class="selected-f">
                Selected Fixture<br>Score
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ft-play">
            <img src="http://127.0.0.1:8000/assets/front/images/minus.png">    
            </div>
            <div class="ft-play-con">
            <span>Fit to play</span>    
            </div>
        </div>
      </div>

   </div>
  </div>
</div>


<!--09-01-2018 modal-end -->


                    <?php $__env->stopSection(); ?>

                    <?php $__env->startSection('page_js'); ?>
                    <?php echo $__env->make('front.pages.popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('front.pages.player-popup-script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>