<h2 class="trading-today">COMPLETE THE FORM BELOW AND START TRADING TODAY!</h2>
<div class="football-index">
    <form id="cashback-form">
        <h3>Sign up To Football Index and claim today!</h3>
        <div class="form-group">
            <label for="sel1">Your Email *</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Enter the address you would like us to pay your cashback">
            <label for="sel1">Where did you hear about us? *</label>
            <select class="form-control" id="source" name="source">
                <option value="">Please select</option>
                <option value="YouTube">YouTube</option>
                <option value="Twitter">Twitter</option>
                <option value="Google Search">Google Search</option>
                <option value="Facebook">Facebook</option>
                <option value="Instagram">Instagram</option>
            </select>

            <label for="sel1">Refer a Friend</label> 
            <input type="code" class="form-control" id="reference_code" name="reference_code" placeholder="Enter Refer a Friend code">
            <label class="checkbox-inline">
                <input type="checkbox" value="" name="terms"><p>I have read and accept the terms and conditions below<p>
                <p class="error errorToShow" id="errorToShow"></p>
            </label>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
<br><br>

