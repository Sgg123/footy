<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Article Categories</li>
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Article Categories
            <a href="<?php echo e(route('article-category.create')); ?>" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <div class="clearfix"></div>        
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>No. of Articles</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($articleCategories) <= 0): ?>
                        <tr>
                            <td colspan="6" class="text-center">Sorry no category found!</td>
                        </tr>
                        <?php else: ?>  
                        <?php $count = 1; ?>
                        <?php $__currentLoopData = $articleCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $articleCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $id = Helper::encrypt($articleCategory['id']); ?>
                        <tr>
                            <td><?php echo e($count); ?></td>
                            <td><?php echo e($articleCategory['title']); ?></td>
                            <td><?php echo e($articleCategory['getArticles']->count()); ?></td>
                            <td>
                                <a href="<?php echo e(route('article-category.edit',[$id])); ?>" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                <?php if($articleCategory['getArticles']->count() == 0): ?>
                                <a href="javascript:void(0)" data-url="<?php echo e(route('article-category.destroy',$id)); ?>" class="btn btn-sm btn-danger delete-list-record" ><i class="fa fa-fw fa-trash"></i>Delete</a>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php $count++;  ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?> 
                    </tbody> 
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <?php echo e($articleCategories->links()); ?>

                </div> 
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<?php echo $__env->make('alerts.delete-confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>