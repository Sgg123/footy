<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Spreadsheets</li>
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Spreadsheets
            <a href="<?php echo e(route('spreadsheets.create')); ?>" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        	<th>#</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>File Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php $__currentLoopData = $spreadsheets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spreadsheet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <tr>
                       		<td><?php echo e($spreadsheet->id); ?></td>
                       		<td><?php echo e($spreadsheet->title); ?></td>
                       		<td><?php echo e($spreadsheet->slug); ?></td>
                          <td><?php echo e($spreadsheet->file_name); ?></td>
                       		<td>
                       			<a class="btn btn-sm btn-success" href="<?php echo e(route('spreadsheets.edit', [$spreadsheet->id])); ?>"><i class="fa fa-fw fa-pencil"></i> Edit</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-danger delete-list-record" data-url="<?php echo e(route('spreadsheets.destroy', ['id' => $spreadsheet->id])); ?>"><i class="fa fa-fw fa-trash"></i>Delete</a>
                       		</td>
                       </tr>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody> 
                </table>
            </div>
            
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>

<?php echo $__env->make('alerts.delete-confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>