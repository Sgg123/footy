<?php $__env->startSection('page_css'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">            
            Sports Monk Api            
        </li>        
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Update Fixtures Data
        </div>
        <div class="card-body">

            <form method="POST" action="<?php echo e(route('sportmonk.fetch.api.data')); ?>">                
                <?php echo e(csrf_field()); ?>


                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="exampleInputName">Start Date <span class= "error">*</span></label>
                            <input type="text" id="start_date" name="start_date">
                            <label id="start_date-error" class="error" for="start_date"><?php echo e(@$errors->first('start_date')); ?></label>
                        </div>
                        <div class="col-md-6">
                            <label for="exampleInputName">End Date <span class= "error">*</span></label>
                            <input type="text" id="end_date" name="end_date">
                            <label id="end_date-error" class="error" for="end_date"><?php echo e(@$errors->first('end_date')); ?></label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <input type="submit" value="Fetch Data" class="btn btn-primary"/>
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
    $("#start_date").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function (date) {
            var dt2 = $('#end_date');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            dt2.datepicker('setDate', minDate);
            dt2.datepicker('option', 'minDate', minDate);
        }
    });
    $('#end_date').datepicker({
        dateFormat: "yy-mm-dd"
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>