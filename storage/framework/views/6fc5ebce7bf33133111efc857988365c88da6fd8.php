<!-- Products --><?php if(isset($updatedmatches)){ $updatedmatches = json_decode(json_encode($updatedmatches), True); }?>
<div class="products-holder gray-bg theme-padding">
    <?php if(isset($page) && $page == "homepage"): ?>
    <h1><span><i class="red-color">UPCOMING </i>FIXTURES</span></h1>
    <?php else: ?>
    <h3><span><i class="red-color">UPCOMING </i>FIXTURES</span></h3>
    <?php endif; ?>
    <div id="product-slider" class="product-slider nav-style-1">

        <?php if(count($upcomingMatches) > 0): ?>
        <?php $__currentLoopData = $upcomingMatches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $match): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <?php $triple_media_class = ""; ?>

        <?php if(@$match['title'] === "Triple Media"): ?>
        <?php $triple_media_class = "triple_media_class"; ?>
        <?php else: ?>
        <?php $triple_media_class = ""; ?>
        <?php endif; ?>                                                                                                                      

       
        <?php if(isset($updatedmatches)){ ?>
        <?php $__currentLoopData = $updatedmatches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $umatch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($match['date'] ==  date('D jS M', strtotime($umatch['date']))): ?>

            <?php if($umatch['type_of_day'] == 1): ?>
            <?php 
                $match['title'] = 'Single Performance';
                $match['color_codes'] = '#CF5A0A';
            ?>
            <?php endif; ?>
            <?php if($umatch['type_of_day'] == 2): ?>
            <?php 
                $match['title'] = 'Double Performance';
                $match['color_codes'] = '#8C909C';
            ?>
            <?php endif; ?>
            <?php if($umatch['type_of_day'] == 3): ?>
            <?php 
                $match['title'] = 'Triple Performance';
                $match['color_codes'] = '#DDAB27';
            ?>
            <?php endif; ?>
            <?php if($umatch['type_of_day'] == 4): ?>
            <?php 
                $match['title'] = 'Triple Media';
                $match['color_codes'] = '#439F05';
            ?>
            <?php endif; ?>
            <?php if($umatch['type_of_day'] == 5): ?>
            <?php 
                $match['title'] = 'Media Madness Top 5';
                $match['color_codes'] = '#19ADF3';
            ?>
            <?php endif; ?>
        <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php } ?>

         <!-- Product Column -->
        <?php @$startingAt = ""; ?>
        <?php if(@$match['title'] === 'Triple Media' || @$match['title'] === 'Media Madness Top 5' ): ?>
        <?php
        $route = route('page', [@$pages[6]['slug']]);
        $title = "View News"
        ?>
        <?php else: ?>
        <?php
        @$startingAt = Helper::encrypt(@$match['startingAt']);
        $route = route('pb.planner', @$startingAt);
        $title = "View Games"
        ?>
        <?php endif; ?>
        <div class="product-column" style="cursor: pointer;" onclick="location.href='<?php echo e(@$route); ?>'">

            <div class="r-full-width">
                <div class="next-matches">
                    <h4><?php echo e(@$match['date']); ?></h4>

                </div>
            </div>
            <div class="col-md-12 text-center media_box <?php echo e(@$triple_media_class); ?>" style="background-color: <?php echo e(@$match['color_codes']); ?>">

                <h3>
                    <?php echo e(@$match['title']); ?>

                </h3>
            </div>

            <div class="product-column-inner">


                <div class="row">
                    <div class="col-md-4 col-xs-4 position-title">
                        <?php echo e(@$match['title'] === 'Triple Media' || @$match['title'] === 'Media Madness Top 5'? '1st' : 'DEF'); ?>

                        <div class="def_mid"> <?php echo e(@$match['defender']); ?> 

                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 position-title">
                        <?php echo e(@$match['title'] === 'Triple Media' || @$match['title'] === 'Media Madness Top 5' ? '2nd' : 'MID'); ?>  
                        <div class="def_mid"><?php echo e(@$match['midfielder']); ?>

                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 position-title">
                        <?php echo e(@$match['title'] === 'Triple Media'  || @$match['title'] === 'Media Madness Top 5'? '3rd' : 'FWD'); ?>  
                        <div class="def_mid"><?php echo e(@$match['attacker']); ?></div>

                    </div>
                </div>

            </div> 
            <div class="btm">    
                <a class="btn btn-success btn-block" href="<?php echo e(@$route); ?>"><?php echo e(@$title); ?></a>
            </div>
        </div>
        <!-- Product Column -->
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

    </div>
</div>
<!-- Products -->