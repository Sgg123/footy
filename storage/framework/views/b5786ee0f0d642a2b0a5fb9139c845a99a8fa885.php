<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?php echo e(route('teams.players', ['id' => @$teamID])); ?>">
                Players
            </a>
        </li>
        <li class="breadcrumb-item active"><?php echo e(@$player ? 'Edit' : 'Add'); ?></li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> <?php echo e(@$player ? 'Edit' : 'Add'); ?> Player
        </div>
        <div class="card-body">
            <?php if(@$player): ?>
            <form method="POST" action="<?php echo e(route('teams.players.update', ['id' => Helper::encrypt(@$player->team_id), 'playerid' => Helper::encrypt(@$player->id)])); ?>" enctype="multipart/form-data">
                <?php echo e(method_field('PUT')); ?>

                <?php else: ?>
                <form method="POST" action="<?php echo e(route('teams.players.store', ['id' => @$teamID])); ?>" enctype="multipart/form-data">
                    <?php endif; ?>
                    <?php echo e(csrf_field()); ?>

                    <label><b>Team:</b> <?php echo e(@$teamName); ?> </label>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">First Name <span class= "error">*</span></label>
                                <input class="form-control" name="firstname" type="text" aria-describedby="nameHelp" placeholder="Enter First Name " value ="<?php echo e(old('firstname', @$player->firstname)); ?>" id="firstname">
                                <label id="firstname-error" class="error" for="firstname"><?php echo e(@$errors->first('firstname')); ?></label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputName">Middle Name <span class= "error">*</span></label>
                                <input class="form-control" name="middlename" type="text" aria-describedby="nameHelp" placeholder="Enter Middle Name " value ="<?php echo e(old('middlename', @$player->middlename)); ?>" id="middlename">
                                <label id="middlename-error" class="error" for="middlename"><?php echo e(@$errors->first('middlename')); ?></label>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">                       

                            <div class="col-md-6">
                                <label for="exampleInputName">Last Name <span class= "error">*</span></label>
                                <input class="form-control" name="lastname" type="text" aria-describedby="nameHelp" placeholder="Enter Last Name" value ="<?php echo e(old('lastname', @$player->lastname)); ?>" id="lastname">
                                <label id="lastname-error" class="error" for="lastname"><?php echo e(@$errors->first('lastname')); ?></label>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputName">Common Name <span class= "error">*</span></label>
                                <input class="form-control" name="common_name" type="text" aria-describedby="nameHelp" placeholder="Enter Common Name" value ="<?php echo e(old('common_name', @$player->common_name)); ?>" id="common_name">
                                <label id="common_name-error" class="error" for="common_name"><?php echo e(@$errors->first('common_name')); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">                       
                            <div class="col-md-4">
                                <label for="exampleInputName">Football Index Name<span class= "error">*</span></label>
                                <input class="form-control" name="football_index_common_name" type="text" aria-describedby="nameHelp" placeholder="Enter Football Index Name" value ="<?php echo e(old('football_index_common_name', @$player->football_index_common_name)); ?>" id="football_index_common_name">
                                <label id="football_index_common_name-error" class="error" for="football_index_common_name"><?php echo e(@$errors->first('football_index_common_name')); ?></label>
                            </div>
                            
                            <div class="col-md-4">
                                <label for="exampleInputName">Nationality <span class= "error">*</span></label>
                                <input class="form-control" name="nationality" type="text" aria-describedby="nameHelp" placeholder="Enter Nationality" value ="<?php echo e(old('nationality', @$player->nationality)); ?>" id="nationality">
                                <label id="nationality-error" class="error" for="nationality"><?php echo e(@$errors->first('nationality')); ?></label>
                            </div>

                            <div class="col-md-4">
                                <label for="exampleInputName">Birth Date <span class= "error">*</span></label>
                                <input class="form-control" name="birthdate" type="text" aria-describedby="nameHelp" placeholder="Enter Birth Date " value ="<?php echo e(old('birthdate', @$player->birthdate)); ?>" id="birthdate" readonly>
                                <label id="birthdate-error" class="error" for="birthdate"><?php echo e(@$errors->first('birthdate')); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">                       
                            <div class="col-md-4">
                                <label for="exampleInputName">Height <span class= "error">*</span></label>
                                <input class="form-control" name="height" type="text" aria-describedby="nameHelp" placeholder="Enter Height " value ="<?php echo e(old('height', @$player->height)); ?>" id="height">
                                <label id="height-error" class="error" for="height"><?php echo e(@$errors->first('height')); ?></label>
                            </div>

                            <div class="col-md-4">
                                <label for="exampleInputName">Weight <span class= "error">*</span></label>
                                <input class="form-control" name="weight" type="text" aria-describedby="nameHelp" placeholder="Enter Weight" value ="<?php echo e(old('weight', @$player->weight)); ?>" id="weight">
                                <label id="weight-error" class="error" for="weight"><?php echo e(@$errors->first('weight')); ?></label>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputName">Position <span class= "error">*</span></label>
                                <select name="position" class="form-control">
                                    <option value="">Select Position</option>
                                    <option value="Defender" <?php echo e(@$player->position == "Defender" ? 'selected' : ''); ?>>Defender</option>
                                    <option value="Forward" <?php echo e(@$player->position == "Forward" ? 'selected' : ''); ?>>Forward</option>
                                    <option value="Goalkeeper" <?php echo e(@$player->position == "Goalkeeper" ? 'selected' : ''); ?>>Goalkeeper</option>
                                    <option value="Midfielder" <?php echo e(@$player->position == "Midfielder" ? 'selected' : ''); ?>>Midfielder</option>
                                </select>
                                <label id="position-error" class="error" for="weight"><?php echo e(@$errors->first('position')); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">                       
                            <div class="col-md-4">
                                <label for="football_index_position_id">Football Index Position</label>
                                
                                <select name="football_index_position" id="football_index_position_id" class="form-control">
                                    <option value="">Select Position</option>
                                    <option value="Defender" <?php echo e(@$player->football_index_position == "Defender" ? 'selected' : ''); ?>>Defender</option>
                                    <option value="Forward" <?php echo e(@$player->football_index_position == "Forward" ? 'selected' : ''); ?>>Forward</option>
                                    <option value="Goalkeeper" <?php echo e(@$player->football_index_position == "Goalkeeper" ? 'selected' : ''); ?>>Goalkeeper</option>
                                    <option value="Midfielder" <?php echo e(@$player->football_index_position == "Midfielder" ? 'selected' : ''); ?>>Midfielder</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="exampleInputName">Image </label>  
                                <small class="text-muted">( upload image of size 150 * 150 for better result. )</small>                        
                                <input name="local_image_path" id="update-thumbnail-image" accept=".jpg,.jpeg,.png" type="file" class="form-control
                                       <?php if(file_exists('uploads/player-images/' . @$player->local_image_path) &&  @$player->local_image_path != '' &&  @$player->is_local_image == 1): ?>
                                       edit_image
                                       <?php else: ?>
                                       image
                                       <?php endif; ?>" />
                                <label id="local_image_path-error" class="error" for="local_image_path"><?php echo e(@$errors->first('local_image_path')); ?></label>
                            </div>
                            <div class="col-md-6">
                                <div class="add-preview-main">
                                    <img  name="thumbnail-preview" id="thumbnail-preview" class="article-image"
                                          <?php if(file_exists('uploads/player-images/' . @$player->local_image_path) &&  @$player->local_image_path != '' &&  @$player->is_local_image == 1): ?>
                                          src="<?php echo e(asset('uploads/player-images/'.@$player->local_image_path)); ?>"
                                          <?php else: ?>
                                          src="<?php echo e(@$player->image_path); ?>"
                                          <?php endif; ?>                                                 
                                          >
                                </div>
                            </div>

                        </div>
                    </div>

                    <input type="submit" value="<?php echo e(@$player ? 'Update' : 'Save'); ?>" class="btn btn-primary" />
                    <a class="btn btn-danger" href="<?php echo e(route('teams.players', ['id' => @$teamID])); ?>">Cancel</a>	
                </form>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
    $("#birthdate").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: '0',
        autoclose: true,
        changeYear: true,
        changeMonth: true,
        yearRange: "-80:+00"
    });
    $("#update-image").change(function () {
        readURLEdit(this);
    });

    function readURLEdit(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#preview").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    //Thumbnail Image Preview
    $("#update-thumbnail-image").change(function () {
        readURLEdit2(this);
    });

    function readURLEdit2(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#thumbnail-preview").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>