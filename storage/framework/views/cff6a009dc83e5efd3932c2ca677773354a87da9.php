<div class="modal" id="player_details_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg new-modal-dialog-v" role="document">
        <div class="modal-content bg-set-t">
            <div class="modal-header new-edit-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="playerdata">
                <div id="fixturedeta">
                </div>
            </div>

        </div>
    </div>
</div>
