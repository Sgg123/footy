<?php $__env->startSection('meta_desc'); ?>
<meta name="keywords" content="<?php echo e(@$page->meta_keywords); ?>" />
<meta name="description" content="<?php echo e(@$page->meta_description); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('assets/front/css/twitterfeed.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2><?php echo e(@$page->title); ?></h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Media Buzz</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('uploads/pages/'.@$page->image_path)); ?>">
</div>


<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                <?php echo $__env->make('front.pages.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Aside -->

                <!-- Match Result Contenet -->
                <div class="col-lg-9 col-sm-12 sidebarres">

                    <div class="detail-tabs">
                        <ul class="tab-nav" role="tablist">
                            <li class="active">
                                <a href="#news-feed" data-toggle="tab" aria-expanded="false">News</a>
                            </li>
                            <li class="">
                                <a href="#youtube-videos" data-toggle="tab" aria-expanded="false">Youtube</a>
                            </li>
                            <li class="">
                                <a href="#twitter-tweets" data-toggle="tab" aria-expanded="false">Twitter</a>
                            </li>                           
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="news-feed">
                                <div class="tabs-disc tabs-panal-widget">
                                    <div class="detail-tabs">
                                        <ul class="reviewer-list">
                                            <?php $__currentLoopData = @$googleTransferFeed->entry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <div class="comment-detail p-0">
                                                    <h5><a target="_blank" href="<?php echo e((string) @$entry->link[0]['href']); ?>"><?php echo @$entry->title; ?></a></h5>
                                                    <br>
                                                    <span><?php echo e(date('D, d M Y', strtotime(@$entry->published))); ?></span>
                                                    <!--<p><?php echo @$entry->content; ?></p>-->
                                                </div>
                                            </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                        <ul class="reviewer-list">
                                            <?php $__currentLoopData = @$transferRumours; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php
                                            $entryData = (array) $entry;

                                            ?>
                                            <li>
                                                <div class="comment-detail p-0">
                                                    <h5><a href="<?php echo e(@$entry->link); ?>" target="_blank"><?php echo e(@$entryData['title']); ?></a></h5><br>
                                                    <span><?php echo e(date('D, d M Y', strtotime(@$entryData['pubDate']))); ?></span>
                                                </div>
                                            </li>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="youtube-videos">
                                <div class="tabs-disc tabs-panal-widget">
                                    <div class="team-detail-content theme-padding-bottom">
                                        <?php echo $__env->make('front.pages.youtube', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="twitter-tweets">
                                <div class="tabs-disc tabs-panal-widget">
                                    <div class="team-detail-content">

                                        <h2><a href="https://twitter.com/FootyIndexScout" target="_blank">FOLLOW US ON TWITTER  <i class="fa fa-twitter"></i></a></h2>
                                        <div id="twitter-feed-container-footyindexscout">
                                            <div id="twitter-feed-footyindexscout"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<script src="<?php echo e(asset('assets/front/js/prettyPhoto.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('assets/front/js/twitterFetcher_min.js')); ?>"></script>

<script type="text/javascript">
    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
    animation_speed:'normal',
            theme:'dark_square',
            slideshow:3000,
            autoplay_slideshow:false,
            social_tools:false
    });
    jQuery("a[rel^='prettyPhoto']").prettyPhoto();</script>

<script>
    (function() {
    var config_footyindexscout = {
    "profile": {"screenName": 'footyindexscout'},
            "domId":'twitter-feed-footyindexscout',
            "maxTweets":3,
            "showUser": true,
            "showTime": false,
            "showRetweet": true,
            "showInteraction": false,
            "showImages": false,
            "linksInNewWindow": true,
    };
    function defer() {
    if (typeof twitterFetcher === 'object') {
    twitterFetcher.fetch(config_footyindexscout);
    } else {
    setTimeout(function() { defer(); }, 50);
    }
    }

    defer();
    })();
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>