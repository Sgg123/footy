<?php $__env->startSection('no_follow'); ?>
<meta name="robots" content="noindex, nofollow">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>


<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Performance Buzz Winner Player History</h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>>Performance Buzz Winner Player History</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg')); ?>">
</div>

<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">
                <?php if(\Auth::user()->isPremium()): ?>
                <!-- Search Form -->
                <div class="col-lg-9 col-sm-12">
                    <form name="performancebuzz_search_form" action="">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="player_name" placeholder="Player name" value="<?php echo e(@Request::get('player_name')); ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="from" id="start_date" placeholder="From" value="<?php echo e(@Request::get('from')); ?>" readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" id="end_date" name="to" placeholder="To" value="<?php echo e(@Request::get('to')); ?>" readonly>
                        </div>
                        <div class="form-group col-md-3 pull-right search-new-add">
                            <button type="submit" class="btn red-btn">Search</button>
                            <a href="<?php echo e(route('performance.buzz.player')); ?>" class="btn red-btn">Reset</a>
                        </div>
                    </form>
                </div>
                <!-- Search Form -->
                     <?php endif; ?>
                <!-- Match Result Contenet --> 
                <div class="col-lg-12 col-sm-12 p-d-v-set">

                    <!-- Piont Table -->
                    <div class="macth-fixture">

                        <div class="last-matches styel-3">
                            <!-- Table Style 3 -->
                            <div class="table-responsive padding-top-70">
                                <!--<img src="<?php echo e(asset('assets/front/images/PB-History-Table.jpg')); ?>" class="mb-player-history-img">-->
                                <table class="table table-bordered table-striped table-dark-header advance-v">
                                    <thead class="player_heading player_heading">
                                        <tr>
                                            <th class="date-v-d new-p-set">DATE</th>
                                            <th class="date-v-d table-th-set pg-width-set">PB TOP<br>PLAYER</th>
                                            <th class="date-v-d new-p-set" style="font-size: 11px !important;">SCORE</th>
                                            <th class="top-for pg-width-set">PB TOP<br>FORWARD</th>
                                            <th class="top-for new-p-set-2">
                                            <span class="sc-set">SCORE</span></th>
                                            <th class="mide-v pg-width-set">PB TOP<br>MIDFIELDER</th>
                                            <th class="mide-v sec-d-using"><span class="sc-set">SCORE</span></th>
                                            <th class="defender-v pg-width-set">PB TOP<br>DEFENDER</th>
                                            <th class="defender-v sec-d-using"><span class="sc-set">SCORE</span></th>
                                        </tr>
                                    </thead>
                                    <tbody class="player_alldetails d-view-set">
                                        <?php if(!empty($PBWinners)): ?>
                                        <?php $__currentLoopData = $PBWinners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                        
                                        <tr>
                                            <td><?php echo e(date('d M',strtotime(@$history['date']))); ?><br/><?php echo e(date('Y',strtotime(@$history['date']))); ?></td>

                                            <!--TOP PLAYER DETAILS-->
                                            <td class="view-player" data-id="<?php echo e(@$history['topPlayerPlayerId']); ?>" data-teamid="<?php echo e(@$history['topPlayerTeamId']); ?>">
                                                <div class="col-md-4 mb-player-image">
                                                    <?php if(@$history['topPlayerLocalImagePath'] != "" || @$history['topPlayerImagePath'] != ""): ?>
                                                    <img 
                                                        <?php if(@$history['topPlayerIsLocalImage'] == 1 && @$history['topPlayerLocalImagePath'] != ""): ?>
                                                        src="<?php echo e(asset('uploads/player-images/'.@$history['topPlayerLocalImagePath'] )); ?>"
                                                        <?php else: ?>
                                                        src="<?php echo e(@$history['topPlayerImagePath']); ?>"
                                                        <?php endif; ?>
                                                        >
                                                        <?php endif; ?>
                                                </div>
                                                <div class="player_heading_title">
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            <?php echo e(@$history['topPlayerFCommonName'] != "" ? @$history['topPlayerFCommonName'] : @$history['topPlayerName']); ?>

                                                        </div>
                                                        <div class="mb-player-team-name"><?php echo e(@$history['topPlayerTeamName']); ?></div>
                                                    </div> 
                                                </div> 
                                            </td>

                                            <td class="mb-player-score"><?php echo e(@$history['topPlayerScore']); ?></td>

                                            <!--TOP FORWARD DETAILS-->
                                            <td class="view-player" data-id="<?php echo e(@$history['forwardPlayerPlayerId']); ?>" data-teamid="<?php echo e(@$history['forwardPlayerTeamId']); ?>">
                                                <div class="col-md-4 mb-player-image">
                                                    <?php if(@$history['forwardLocalImagePath'] != "" || @$history['forwardImagePath'] != ""): ?>
                                                    <img 
                                                        <?php if(@$history['forwardIsLocalImage'] == 1 && @$history['forwardLocalImagePath'] != ""): ?>
                                                        src="<?php echo e(asset('uploads/player-images/'.@$history['forwardLocalImagePath'] )); ?>"
                                                        <?php else: ?>
                                                        src="<?php echo e(@$history['forwardImagePath']); ?>"
                                                        <?php endif; ?>
                                                        >
                                                        <?php endif; ?>
                                                </div>
                                                <div class="player_heading_title">
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            <?php echo e(@$history['forwardFCommonName'] != "" ? @$history['forwardFCommonName'] : @$history['forwardName']); ?>

                                                        </div>
                                                        <div class="mb-player-team-name"><?php echo e(@$history['forwardTeamName']); ?></div>
                                                    </div> 
                                                </div> 
                                            </td>

                                            <td class="mb-player-score"><?php echo e(@$history['forwardScore']); ?></td>

                                            <!--TOP MIDFIELDER DETAILS-->
                                            <td class="view-player" data-id="<?php echo e(@$history['midfielderPlayerPlayerId']); ?>" data-teamid="<?php echo e(@$history['midfielderPlayerTeamId']); ?>">
                                                <div class="col-md-4 mb-player-image">
                                                    <?php if(@$history['midfielderLocalImagePath'] != "" || @$history['midfielderImagePath'] != ""): ?>
                                                    <img 
                                                        <?php if(@$history['midfielderIsLocalImage'] == 1 && @$history['midfielderLocalImagePath'] != ""): ?>
                                                        src="<?php echo e(asset('uploads/player-images/'.@$history['midfielderLocalImagePath'] )); ?>"
                                                        <?php else: ?>
                                                        src="<?php echo e(@$history['midfielderImagePath']); ?>"
                                                        <?php endif; ?>
                                                        >
                                                        <?php endif; ?>
                                                </div>
                                                <div class="player_heading_title">
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            <?php echo e(@$history['midfielderFCommonName'] != "" ? @$history['midfielderFCommonName'] : @$history['midfielderName']); ?>

                                                        </div>
                                                        <div class="mb-player-team-name"><?php echo e(@$history['midfielderTeamName']); ?></div>
                                                    </div> 
                                                </div> 
                                            </td>

                                            <td class="mb-player-score"><?php echo e(@$history['midfielderScore']); ?></td>

                                            <!--TOP DEFENDER DETAILS-->
                                            <td class="view-player" data-id="<?php echo e(@$history['defenderPlayerPlayerId']); ?>" data-teamid="<?php echo e(@$history['defenderPlayerTeamId']); ?>">
                                                <div class="col-md-4 mb-player-image">
                                                    <?php if(@$history['defenderLocalImagePath'] != "" || @$history['defenderImagePath'] != ""): ?>
                                                    <img 
                                                        <?php if(@$history['defenderIsLocalImage'] == 1 && @$history['defenderLocalImagePath'] != ""): ?>
                                                        src="<?php echo e(asset('uploads/player-images/'.@$history['defenderLocalImagePath'] )); ?>"
                                                        <?php else: ?>
                                                        src="<?php echo e(@$history['defenderImagePath']); ?>"
                                                        <?php endif; ?>
                                                        >
                                                        <?php endif; ?>
                                                </div>
                                                <div class="player_heading_title">
                                                    <div class="col-md-8 remove_player_space">
                                                        <div class="mb-player-name">
                                                            <?php echo e(@$history['defenderFCommonName'] != "" ? @$history['defenderFCommonName'] : @$history['defenderName']); ?>

                                                        </div>
                                                        <div class="mb-player-team-name"><?php echo e(@$history['defenderTeamName']); ?></div>
                                                    </div> 
                                                </div> 
                                            </td>

                                            <td class="mb-player-score"><?php echo e(@$history['defenderScore']); ?></td>
                                        </tr>  
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                        <tr>
                                            <td colspan="7">No data found</td>                                            
                                        </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <div class="col-xs-12">
                                    <div class="mb-player-pagination">
                                     
                <?php if(@Request::get('player_name') != '' || @Request::get('from') != '' || @Request::get('to') != ''): ?>
                <?php echo $links->appends(['player_name' => @Request::get('player_name'), 'to' => @Request::get('to'),'from' => @Request::get('from')])->links(); ?>

                <?php else: ?>
                <?php echo $links->links(); ?>

                <?php endif; ?>

                                    </div>                               
                                </div>
                                <?php if(!\Auth::user()->isPremium()): ?>
                                <div class="col-xs-12 text-center">
                                <br>
                                <table class="MsoTableLightShadingAccent1" style="border-collapse: collapse; border: medium none;display: inline-block;" cellspacing="0" cellpadding="0" border="1" align="center">
                                    <tbody>
                                        <tr style="mso-yfti-irow:12;height:45.5pt">
                                          <td style="width:155.95pt;border:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                                          background:#fff;mso-background-themecolor:accent1;mso-background-themetint:
                                          63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="208">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                          margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                          mso-yfti-cnfc:68" align="center"><b><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
                                          color:#365F91;mso-themecolor:accent1;mso-themeshade:191">100% Full Media and Performance Buzz Winners History</span></b></p>
                                          </td>
                                          <td style="width:111.7pt;border-left:none;
                                          border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                          mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                          mso-border-alt:solid windowtext .5pt;background:#fff;mso-background-themecolor:
                                          accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="149">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                          margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                          mso-yfti-cnfc:64" align="center"><b style="mso-bidi-font-weight:normal"><span style="font-size:20.0pt;mso-bidi-font-size:11.0pt;color:#365F91;mso-themecolor:
                                          accent1;mso-themeshade:191">PREMIUM</span></b></p>
                                          </td>
                                          <td style="width:200.3pt;border-left:none;
                                          border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                          mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                          mso-border-alt:solid windowtext .5pt;background:#fff;mso-background-themecolor:
                                          accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="267">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;margin-bottom:.0001pt;
                                          mso-add-space:auto;line-height:normal;mso-yfti-cnfc:64"><span style="color:#365F91;mso-themecolor:accent1;mso-themeshade:191">View the full and complete winners of both Media and Performance Buzz.</span></p>
                                          </td>
                                         </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="text-center">
                                    <a href="<?php echo e(url('/upgrade-account')); ?>" class="text-uppercase btn btn-primary">Click here to upgrade</a>
                                </div><br>
                        <div class="text-center" style="font-weight: bold;font-size: 13pt;">
                         PREMIUM MEMBERSHIP ONLY £5 A MONTH. THE FIRST 30 DAYS ARE FREE!
                        </div>
                                <br>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- Piont Table -->

                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.pages.popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php $__env->startSection('page_js'); ?>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>
<?php echo $__env->make('front.pages.player-popup-script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>