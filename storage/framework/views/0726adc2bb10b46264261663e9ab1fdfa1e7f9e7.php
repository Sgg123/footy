<div class="col-lg-3 col-sm-3 col-xs-12 using-ex-d">

    <!-- Aside Widget -->
    <div class="aside-widget media-widget">
        <?php 
            $data = Helper::getRandomPlayerData(); 
            $playerId = @$data['player_details']->id;
            $team_ids = @$data['player_details']->team_id;
        ?>

       

        <div class="media-section">
         <div class="col-lg-7 col-xs-7 left-img">
                <img class="players-img view-player" data-id="<?php echo e($playerId); ?>" data-teamid="<?php echo e($team_ids); ?>"
                     <?php if(@$data['player_details']['is_local_image'] == 1 && file_exists(public_path('uploads/player-images/'.@$data['player_details']['local_image_path']))): ?>
                     src="<?php echo e(asset('uploads/player-images/'. @$data['player_details']['local_image_path'])); ?>"
                     <?php else: ?>
                     src ="<?php echo e(asset('assets/images/no-image.jpeg')); ?>" 
                     <?php endif; ?>
                     alt="">

            </div>
            <?php
            $colorCode = "";
            $title ="";
            ?>
            <?php if(@$data['player_details']['football_index_position'] == "Attacker" || @$data['player_details']['football_index_position'] == "Forward"): ?>

            <?php
            $colorCode = "#d52cbb";
            $title ="FWD";
            ?>

            <?php elseif(@$data['player_details']['football_index_position'] == "Defender"): ?>

            <?php
            $colorCode = "#19AEF4";
            $title ="DEF";
            ?>

            <?php elseif(@$data['player_details']['football_index_position'] == "Midfielder"): ?>

            <?php
            $colorCode = "#F89300";
            $title ="MID";
            ?>

            <?php elseif(@$data['player_details']['football_index_position'] == "Goalkeeper"): ?>

            <?php
            $colorCode = "#19AEF4";
            $title ="GK";
            ?>

            <?php endif; ?>
            <div class="col-lg-5 col-xs-5 right-img">
                <img class="teams-img" 
                     <?php if(@$data['player_details']['is_local_image'] == 1 && file_exists(public_path('uploads/team-images/'.@$data['player_details']['team_local_logo']))): ?>
                     src="<?php echo e(asset('uploads/team-images/'. @$data['player_details']['team_local_logo'])); ?>"
                     <?php else: ?>
                     src ="<?php echo e(asset('assets/images/no-image.jpeg')); ?>" 
                     <?php endif; ?>
                     alt="">
                     <span style="background:<?php echo e($colorCode); ?>"><?php echo e($title); ?> </span>  
            </div>          
       
        </div>

        <div class="player-title col-sm-12">
            <h2><?php echo e(@$data['player_details']['football_index_common_name']); ?></h2>            
        </div>
        <div class="use-sep-v col-sm-12">
            <?php
            $colorCode = "";
            $fontcolor ="";
            ?>
            <?php if(@$data['player_details']->avgscore <= 30): ?>
            <?php
            $colorCode = "#FF0000";
            $fontcolor = "#FFFFFF";
            ?>
            <?php elseif(@$data['player_details']->avgscore >= 31 && @$data['player_details']->avgscore <= 70): ?>
            <?php
            $colorCode = "#FF5733";
                $fontcolor = "#FFFFFF";
            ?>
            <?php elseif(@$data['player_details']->avgscore >= 71 && @$data['player_details']->avgscore <= 110): ?>
            <?php
            $colorCode = "#eacd0e";
            $fontcolor = "#000";
            ?>
            <?php elseif(@$data['player_details']->avgscore >= 111 && @$data['player_details']->avgscore <= 150): ?>
            <?php
            $colorCode = "#FFFF00";
            $fontcolor = "#000  ";
            ?>
            <?php elseif(@$data['player_details']->avgscore >= 151 && @$data['player_details']->avgscore <= 180): ?>
            <?php
            $colorCode = "#aad77d";
            $fontcolor = "#FFFFFF";
            ?>
            <?php elseif(@$data['player_details']->avgscore >= 180): ?>
            <?php
            $colorCode = "#39a02b";
            $fontcolor = "#FFFFFF";
            ?>
            <?php endif; ?>
            <div class="col-lg-3 col-xs-3 player-no" style="color:<?php echo e($fontcolor); ?>">
                <span style="background:<?php echo e($colorCode); ?>"><?php echo e(number_format($data['player_details']->avgscore ,0.5)); ?></span>   
            </div>  
            <div class="col-lg-4 col-xs-4 player-rank">        
                <span>Rank <?php echo e($data['playerdetail']); ?><br><?php echo e($data['playersector']); ?></span>
            </div>
            <div class="col-lg-4 col-xs-4 player-overrank">            
                <span>Rank <?php echo e($data['overallrankdetail']); ?><br>Overall</span> 
            </div>           
        </div>
        <div class="detail-of-player">
        <?php if(@$data['fixture_details'] != ""): ?>
            <span class="main-text">Next Fixture:<br></span>
            <span class="main-text"><?php echo e(@$data['fixture_details']['oppTeamName']); ?><br></span>
            <?php
            $date1 = date_create(date('Y-m-d'));
            $date2 = date_create(date('Y-m-d',strtotime(@$data['fixture_details']['starting_at'])));

            $diff = date_diff($date1,$date2);
            ?>
            <span class="main-text"><?php echo e(date('M d',strtotime(@$data['fixture_details']['starting_at']))); ?></span> ( 
            <?php if(@$diff->format("%a") == 0): ?>
            Today            
            <?php else: ?>
            <?php echo e($diff->format("%a") == 1 ? $diff->format("%a")  .' Day' : $diff->format("%a")  .' Days'); ?>

            <?php endif; ?>
            )
          <?php else: ?>
          <span class="main-text">Next Fixture:No Data<br></span>
        <?php endif; ?>
         </div>
    </div>
    <!-- Aside Widget -->

    <!-- Last Match -->
    <?php if(!empty(@$data['standings'])): ?>
    <div class="col-lg-12 col-md-12 col-sm-7 col-xs-6 r-full-width standings-table">
        <h3 class="m-b-set-v"><span><?php echo e(@$data['league_name']); ?></span></h3>
        <div class="last-matches styel-1">

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Team</th>
                        <th>PL</th>
                        <th>GD</th>
                        <th>P</th>
                        <!--<th>D</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty(@$data['standings'])): ?>

                    <?php for($i = 0; $i <= 9; $i++): ?>
                    <?php if(@$data['standings'][$i] != null): ?>
                    <tr>
                        <td><?php echo e(@$data['standings'][$i]->position); ?></td>
                        <td><?php echo e(@$data['standings'][$i]->team_name); ?></td>
                        <td><?php echo e(@$data['standings'][$i]->overall->games_played); ?></td>
                        <td><?php echo e(@$data['standings'][$i]->total->goal_difference); ?></td>
                        <td><?php echo e(@$data['standings'][$i]->points); ?></td>                    
                        <!--<td><?php echo e(@$standing->overall->draw); ?></td>-->                    
                    </tr> 
                    <?php endif; ?>
                    <?php endfor; ?>

                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php endif; ?>
    <!-- Last Match -->


</div>
<?php $__env->startSection('sidebar_js'); ?>
<?php echo $__env->make('front.pages.popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('front.pages.player-popup-script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>