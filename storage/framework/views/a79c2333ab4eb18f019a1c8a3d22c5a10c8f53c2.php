<div class="row" id="view_all_players">
  <div class="col-sm-6 detail-player">
    <table class="table table-bordered firstplayer-list">
      <tbody>
        <?php if(@$all_players): ?>
        <?php $__currentLoopData = @$all_players; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $player): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
          <td class="players-image-logo"><img class="player_image view-player" data-id="<?php echo e(@$player->id); ?>" data-teamid="<?php echo e(@$player->team_id); ?>"
            <?php if(@$player->is_local_image == 0): ?>
            src="<?php echo e(@$player->image_path); ?>"
            <?php elseif(@$player->is_local_image == 1 && file_exists(public_path('uploads/player-images/'.@$player->local_image_path))): ?>
            src="<?php echo e(asset('uploads/player-images/'.@$player->local_image_path)); ?>"
            <?php else: ?>
            src ="<?php echo e(asset('assets/images/no-image.jpeg')); ?>" 
            <?php endif; ?>
            >
          </td>
          <td class="view-player" data-id="<?php echo e(@$player->id); ?>" data-teamid="<?php echo e(@$player->team_id); ?>"><?php echo e(@$player->football_index_common_name != "" ? @$player->football_index_common_name : @$player->common_name); ?> - <?php echo e($player->football_index_position); ?>

            <br/>
            <?php
            $colorCode = "";
            $fontcolor = "";
            $playerScore = @number_format(@$player->total_score); 
            ?>
            <?php if(@$playerScore <= 30): ?>
            <?php
            $colorCode = "#FF0000";
            $fontcolor = "#FFFFFF";
            ?>
            <?php elseif(@$playerScore >= 31 && @$playerScore <= 70): ?>
            <?php
            $colorCode = "#FF5733";
            $fontcolor = "#FFFFFF";
            ?>
            <?php elseif(@$playerScore >= 71 && @$playerScore <= 110): ?>
            <?php
            $colorCode = "#eacd0e";
            $fontcolor = "#000";
            ?>
            <?php elseif(@$playerScore >= 111 && @$playerScore <= 150): ?>
            <?php

            $colorCode = "#FFFF00";
            $fontcolor = "#000";
            ?>
            <?php elseif(@$playerScore >= 151 && @$playerScore <= 180): ?>
            <?php
            $colorCode = "#aad77d";
            $fontcolor = "#FFFFFF";
            ?>
            <?php elseif(@$playerScore >= 181): ?>
            <?php
            $colorCode = "#39a02b";
            $fontcolor = "#FFFFFF";
            ?>
            <?php endif; ?>
            <div style="background:<?php echo e($colorCode); ?>; color:<?php echo e($fontcolor); ?>;">
              <span class="pb_player_score"><?php echo e(@number_format($player->total_score,0.5)); ?></span>
            </div>
          </td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
      </tbody> 
    </table>
  </div>
</div>