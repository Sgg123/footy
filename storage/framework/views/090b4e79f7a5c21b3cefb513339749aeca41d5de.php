<?php $__env->startSection('no_follow'); ?>
<meta name="robots" content="noindex, nofollow">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>


<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Performance Buzz Winner Player History</h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Performance Buzz Winner Player History</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg')); ?>">
</div>

<!-- Main Content -->
<main class="main-content"> 

    <!-- Match Result -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Aside -->
                <?php echo $__env->make('front.pages.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Aside -->
                <?php if(\Auth::user()->isPremium()): ?>
                <!-- Search Form -->
                <div class="col-lg-9 col-sm-12 sidebarres">
                    <form name="performancebuzz_search_form" action="">
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="team_name" placeholder="Team name" value="<?php echo e(@Request::get('team_name')); ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" name="from" id="start_date" placeholder="From" value="<?php echo e(@Request::get('from')); ?>" readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control" id="end_date" name="to" placeholder="To" value="<?php echo e(@Request::get('to')); ?>" readonly>
                        </div>
                        <div class="form-group col-md-3 pull-right search-new-add">
                            <button type="submit" class="btn red-btn">Search</button>
                            <a href="<?php echo e(route('performance.buzz.team')); ?>" class="btn red-btn">Reset</a>
                        </div>
                    </form>
                </div>
                <!-- Search Form -->
                <?php endif; ?>

                <!-- Match Result Contenet --> 
                <div class="col-lg-9 col-sm-12 sidebarres">

                    <!-- Piont Table -->
                    <div class="macth-fixture">

                        <div class="last-matches styel-3">
                            <!-- Table Style 3 -->
                            <div class="table-responsive padding-top-70">
                                <!--<img src="<?php echo e(asset('assets/front/images/PB-History-Table.jpg')); ?>" class="mb-player-history-img">-->
                                <table class="table table-bordered table-striped table-dark-header mb-team-table">
                                    <thead class="player_heading">
                                        <tr>
                                            <th>DATE</th>
                                            <th >PB TOP PLAYER</th>
                                            <th >PB TOP FORWARD</th>
                                            <th >PB TOP MIDFIELDER</th>
                                            <th >PB TOP DEFENDER</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($PBWinners)): ?>
                                        <?php $__currentLoopData = $PBWinners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                        
                                        <tr>
                                            <td><?php echo e(date('d M Y',strtotime(@$history['date']))); ?></td>

                                            <!--TOP PLAYER DETAILS-->
                                            <td><?php echo e(@$history['topPlayerTeamName']); ?></td>
                                            <!--TOP FORWARD DETAILS-->
                                            <td><?php echo e(@$history['forwardTeamName']); ?></td>
                                            <!--TOP MIDFIELDER DETAILS-->
                                            <td><?php echo e(@$history['midfielderTeamName']); ?></td>
                                            <!--TOP DEFENDER DETAILS-->
                                            <td><?php echo e(@$history['defenderTeamName']); ?></td>
                                        </tr>  
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                        <tr>
                                            <td colspan="5">No data found</td>                                            
                                        </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <div class="col-xs-12">
                                    <div class="mb-player-pagination">
                <?php if(@Request::get('team_name') != '' || @Request::get('from') != '' || @Request::get('to') != ''): ?>
                <?php echo $links->appends(['team_name' => @Request::get('team_name'), 'to' => @Request::get('to'),'from' => @Request::get('from')])->links(); ?>

                <?php else: ?>
                <?php echo $links; ?>

                <?php endif; ?>  
                                    </div>                               
                                </div>
                                <?php if(!\Auth::user()->isPremium()): ?>
                                <div class="col-xs-12 text-center">
                                <br>
                                <table class="MsoTableLightShadingAccent1" style="border-collapse: collapse; border: medium none;display: inline-block;" cellspacing="0" cellpadding="0" border="1" align="center">
                                    <tbody>
                                        <tr style="mso-yfti-irow:12;height:45.5pt">
                                          <td style="width:155.95pt;border:solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
                                          background:#fff;mso-background-themecolor:accent1;mso-background-themetint:
                                          63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="208">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                          margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                          mso-yfti-cnfc:68" align="center"><b><span style="font-size:12.0pt;mso-bidi-font-size:11.0pt;
                                          color:#365F91;mso-themecolor:accent1;mso-themeshade:191">100% Full Media and Performance Buzz Winners History</span></b></p>
                                          </td>
                                          <td style="width:111.7pt;border-left:none;
                                          border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                          mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                          mso-border-alt:solid windowtext .5pt;background:#fff;mso-background-themecolor:
                                          accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="149">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;
                                          margin-bottom:.0001pt;mso-add-space:auto;text-align:center;line-height:normal;
                                          mso-yfti-cnfc:64" align="center"><b style="mso-bidi-font-weight:normal"><span style="font-size:20.0pt;mso-bidi-font-size:11.0pt;color:#365F91;mso-themecolor:
                                          accent1;mso-themeshade:191">PREMIUM</span></b></p>
                                          </td>
                                          <td style="width:200.3pt;border-left:none;
                                          border-bottom:solid windowtext 1.0pt;border-top:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                                          mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
                                          mso-border-alt:solid windowtext .5pt;background:#fff;mso-background-themecolor:
                                          accent1;mso-background-themetint:63;padding:0cm 5.4pt 0cm 5.4pt;height:45.5pt" width="267">
                                          <p class="MsoNormalCxSpMiddle" style="margin-bottom:0cm;margin-bottom:.0001pt;
                                          mso-add-space:auto;line-height:normal;mso-yfti-cnfc:64"><span style="color:#365F91;mso-themecolor:accent1;mso-themeshade:191">View the full and complete winners of both Media and Performance Buzz.</span></p>
                                          </td>
                                         </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="text-center">
                                    <a href="<?php echo e(url('/upgrade-account')); ?>" class="text-uppercase btn btn-primary">Click here to upgrade</a>
                                </div><br>
                        <div class="text-center" style="font-weight: bold;font-size: 13pt;">
                         PREMIUM MEMBERSHIP ONLY £5 A MONTH. THE FIRST 30 DAYS ARE FREE!
                        </div>
                                <br>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- Piont Table -->

                </div>
                <!-- Match Result Contenet -->

            </div>
        </div>
    </div>
    <!-- Match Result -->

</main>
<!-- Main Content -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_js'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">
    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            onClose: function (selectedDate) {
//                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>