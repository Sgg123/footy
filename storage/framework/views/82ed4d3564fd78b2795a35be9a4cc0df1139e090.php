<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> 
<url>
    <loc>https://www.footyindexscout.co.uk/</loc>
</url>
<?php $__currentLoopData = $data['page']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<url>
    <loc>https://www.footyindexscout.co.uk/page/<?php echo e($d->slug); ?></loc>
</url>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__currentLoopData = $data['article']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<url>
    <loc>https://www.footyindexscout.co.uk/article/<?php echo e($d->slug); ?></loc>
</url>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<url>
    <loc>https://www.footyindexscout.co.uk/media-buzz/player</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/media-buzz/team</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/performance-buzz/player</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/performance-buzz/team</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/performance-buzz-statistics</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/performance-buzz-stats-history</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/database</loc>
</url>
<url>
    <loc>https://www.footyindexscout.co.uk/articles</loc>
</url>
</urlset>
