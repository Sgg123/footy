<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Articles</li>
    </ol>
    <?php echo $__env->make('alerts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Articles      
            <a href="<?php echo e(route('articles.create')); ?>" class="btn btn-sm btn-primary pull-right">
                <i class="fa fa-fw fa-plus"></i>
                Add New</a>
        </div>
        <!--        <div class="card-header">
                    <form method="POST" action="<?php echo e(route('articles.import_articles')); ?>" enctype="multipart/form-data" class="pull-right">
                        <?php echo e(csrf_field()); ?>

                        <input type="file" name="import_file">     
                        <label id="import_file-error" class="error" for="import_file"><?php echo e(@$errors->first('import_file')); ?></label>
                        <input type="submit" value="Import Articles" class="btn btn-sm btn-primary">
                    </form>
                </div>-->
        <div class="clearfix"></div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Content Image</th>
                            <th>Thumbnail Image</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($articles) <= 0): ?>
                        <tr>
                            <td colspan="6" class="text-center">Sorry no articles!</td>
                        </tr>
                        <?php else: ?>  
                        <?php $count = 1; ?>
                        <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $id = Helper::encrypt($article['id']); ?>
                        <tr>
                            <td><?php echo e($count); ?></td>
                            <td><?php echo e($article['title']); ?></td>
                            <td><?php echo e($article['getArticlesCategory']['title']); ?></td>
                            <td>
                                <img
                                    <?php if($article['image_path'] != '' && file_exists(public_path('uploads/articles/'.$article['image_path']))): ?>
                                    src="<?php echo e(asset('uploads/articles/'.$article['image_path'])); ?>"
                                    <?php else: ?>
                                    src="<?php echo e(asset('assets/images/no-image.jpeg')); ?>"
                                    <?php endif; ?>
                                    class="image-icon">
                            </td>
                            <td>
                                <img
                                    <?php if($article['thumbnail_image'] != '' && file_exists(public_path('uploads/articles/'.$article['thumbnail_image']))): ?>
                                    src="<?php echo e(asset('uploads/articles/'.$article['thumbnail_image'])); ?>"
                                    <?php else: ?>
                                    src="<?php echo e(asset('assets/images/no-image.jpeg')); ?>"
                                    <?php endif; ?>
                                    class="image-icon">
                            </td>
                            <td><?php echo e($article['date']); ?></td>
                            <td nowrap="">
                                <a href="<?php echo e(route('articles.edit',[$id])); ?>" class="btn btn-sm btn-success"><i class="fa fa-fw fa-pencil"></i>Edit</a>
                                <a href="javascript:void(0)" data-url="<?php echo e(route('articles.destroy',$id)); ?>" class="btn btn-sm btn-danger delete-list-record"><i class="fa fa-fw fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                        <?php $count++;  ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?> 
                    </tbody> 
                </table>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <?php echo e($articles->links()); ?>

                </div> 
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<?php echo $__env->make('alerts.delete-confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>