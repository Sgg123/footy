<?php $__env->startSection('page_css'); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo e(url('backoffice-fis')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?php echo e(route('performance_buzz_files_list')); ?>">
                Performance Buzz Data Files
            </a>
        </li>
        <li class="breadcrumb-item active">
            Add
        </li>
    </ol>
    <!-- Example DataTables Card-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Add Performance Buzz Data File
        </div>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-2">Columns</dt>
                <dd class="col-sm-10">Player Name, Score, Position (Optional), Team (Optional)</dd>

                <dt class="col-sm-2">Player Name</dt>
                <dd class="col-sm-10">Should contain valid "Football Index Common Name" of player or data will not be recorded in system</dd>

                <dt class="col-sm-2">Score</dt>
                <dd class="col-sm-10">Should be a valid numerical score.</dd>

                <dt class="col-sm-2">Position (Optional)</dt>
                <dd class="col-sm-10">Should be a proper position name ("Midfielder","Forward","Defender","Goalkeeper") only (No abbrevations). It will take current Players position if not entered.</dd>

                <dt class="col-sm-2 text-truncate">Team (Optional)</dt>
                <dd class="col-sm-10">Should be a proper team name in case of historical records. It will take current Players team if not entered</dd>

                <dt class="col-sm-3 text-truncate">SAMPLE PB DATA FILE</dt>
                <dd class="col-sm-9"><a href="<?php echo e(url('assets/sample_pb_files.csv')); ?>">Download File</a></dd>
            </dl>
            <form method="POST" action="<?php echo e(route('performance_buzz_files_post')); ?>" enctype="multipart/form-data">
  
                    <?php echo e(csrf_field()); ?>                   

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="mb_date">Date <span class= "error">*</span></label>
                                <input type="text" class="form-control" required="" name="date" id="mb_date" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="file_name">PB Data File <span class= "error">*</span></label>
                                <input type="file" class="form-control" required="" name="file_name" value="" id="file_name">
                            </div>
                        </div>
                    </div>
                    
                    <button class="btn btn-primary" type="submit">Add File</button>
                    <a class="btn btn-danger" href="<?php echo e(route('performance_buzz_files_list')); ?>">Cancel</a>  
                </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $("#mb_date").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
    });
    $('.select2').select2({dropdownCssClass: 'bigdrop'});
    function getdividends(obj){
        var day_type = $('#type_of_day').val();
        if(day_type==1){
            $('#top_performance_player').val(0.01);
            $('#top_positional_performance_player').val(0.02);
            $('#media_buzz_first').val(0.02);
            $('#media_buzz_second').val(0.00);
            $('#media_buzz_third').val(0.00);
        }
        if(day_type==2){
            $('#top_performance_player').val(0.02);
            $('#top_positional_performance_player').val(0.03);
            $('#media_buzz_first').val(0.02);
            $('#media_buzz_second').val(0.00);
            $('#media_buzz_third').val(0.00);
        }
        if(day_type==3){
            $('#top_performance_player').val(0.02);
            $('#top_positional_performance_player').val(0.05);
            $('#media_buzz_first').val(0.02);
            $('#media_buzz_second').val(0.00);
            $('#media_buzz_third').val(0.00);
        }
        if(day_type==4){
            $('#top_performance_player').val(0.00);
            $('#top_positional_performance_player').val(0.00);
            $('#media_buzz_first').val(0.03);
            $('#media_buzz_second').val(0.02);
            $('#media_buzz_third').val(0.01);
        }
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>