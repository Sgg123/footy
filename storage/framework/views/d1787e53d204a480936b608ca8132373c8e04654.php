<div class="col-sm-12 col-xs-12 r-full-width">
    <h3><span><i class="red-color">Latest Article</h3>
                <?php
                $article = Helper::getLatestArticle();
                ?>
                <div class="upcoming-fixture row">  
                    <p class="article-date"><strong>Posted on <?php echo e(date('d F Y',strtotime(@$article->date))); ?></strong></p>
                    <div class="col-lg-6 col-xs-12">

                        <div class="news-post-holder">

                            <!-- Widget -->
                            <div class="news-post-widget">
                                <a href="<?php echo e(route('article.detail',[@$article->slug])); ?>">
                                    <img
                                        <?php if(file_exists('uploads/articles/' . @$article->image_path) &&  @$article->image_path != ''): ?>
                                        src="<?php echo e(asset('uploads/articles/'.@$article->image_path)); ?>"
                                        <?php else: ?>
                                        src="<?php echo e(asset('assets/images/no-image.jpeg')); ?>"
                                        <?php endif; ?>  
                                        alt="">  
                                </a>
                            </div>
                            <!-- Widget -->                                                   

                        </div>
                    </div>

                    <div class="col-lg-6 col-xs-12">
                        <div class="news-post-holder">

                            <h2><a href="<?php echo e(route('article.detail',[@$article->slug])); ?>">
                                    <?php if(strlen(@$article->title) >= 59): ?> 
                                    <?php echo substr(@$article->title,0,58); ?>..
                                    <?php else: ?>
                                    <?php echo @$article->title; ?>

                                    <?php endif; ?>
                                </a></h2>
                            <div class="article-content">
                                <p>
                                    <?php
                                    $text = strip_tags(@$article->content);
                                    $this->orginal_content_count = str_word_count($text);
                                    ?>

                                    <?php if(strlen(@$text) >= 725): ?> 
                                    <?php echo substr(@$text,0,724); ?>....<p class="article_read_more"><strong><a href="<?php echo e(route('article.detail',[@$article->slug])); ?>">Read more</a></strong></p>
                                <?php else: ?>
                                <?php echo @$text; ?>

                                <?php endif; ?>
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
                </div>