<?php $__env->startSection('page_css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2><?php echo e(@$page->title); ?></h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Pages</li>
        </ul>
    </div>
</div>

<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('uploads/pages/'.@$page->image_path)); ?>">
</div>

<main class="main-content"> 
  <div class="theme-padding white-bg">
    <div class="container">
      <div class="row">
      <?php echo $__env->make('front.pages.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="col-lg-9 col-sm-8">
        <div class="team-detail-content theme-padding-bottom">
          <div class="panel panel-default">
            <div class="panel-heading">My Account</div>
            <div class="panel-body">
              <form class="form-horizontal" method="POST" action="<?php echo e(route('myaccount')); ?>">
                <?php echo e(csrf_field()); ?>


                <div class="form-group<?php echo e($errors->has('firstname') ? ' has-error' : ''); ?>">
                  <label for="name" class="col-md-4 control-label">First Name</label>

                  <div class="col-md-6">
                      <input id="firstname" type="text" class="form-control" name="firstname" value="<?php echo e(@$data->firstname); ?>" required autofocus>

                      <?php if($errors->has('firstname')): ?>
                      <span class="help-block">
                          <strong><?php echo e($errors->first('firstname')); ?></strong>
                      </span>
                      <?php endif; ?>
                  </div>
                </div>

                <div class="form-group<?php echo e($errors->has('lastname') ? ' has-error' : ''); ?>">
                  <label for="name" class="col-md-4 control-label">Last Name</label>

                  <div class="col-md-6">
                    <input id="lastname" type="text" class="form-control" name="lastname" value="<?php echo e(@$data->lastname); ?>" required autofocus>

                    <?php if($errors->has('lastname')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('lastname')); ?></strong>
                    </span>
                    <?php endif; ?>
                  </div>
                </div>

                <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                  <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="<?php echo e(@$data->email); ?>" required>

                    <?php if($errors->has('email')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('email')); ?></strong>
                    </span>
                    <?php endif; ?>
                  </div>
                </div>

                <div class="form-group<?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                  <label for="email" class="col-md-4 control-label">Phone</label>

                  <div class="col-md-6">
                    <input id="phone" type="text" class="form-control" name="phone" value="<?php echo e(@$data->phone); ?>" required>

                    <?php if($errors->has('phone')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('phone')); ?></strong>
                    </span>
                    <?php endif; ?>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                          Update
                      </button>
                  </div>
                </div>

              </form>           
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</main>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_js'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>