<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="<?php echo e(url('backoffice-fis')); ?>"><?php echo e(config('app.name', 'Laravel')); ?></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link <?php echo e(@$activeMenu == 'dashboard' ? 'active' : ''); ?>" href="<?php echo e(url('backoffice-fis')); ?>">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Dashboard</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Manage Articles">
                <a class="nav-link nav-link-collapse <?php echo e(@$activeMenu == 'manage-article' ? '' : 'collapsed'); ?>" data-toggle="collapse" href="#collapseExamplePages">
                    <i class="fa fa-fw fa-newspaper-o"></i>
                    <span class="nav-link-text">Manage Articles</span>
                </a>
                <ul class="sidenav-second-level collapse <?php echo e(@$activeMenu == 'manage-article' ? 'show' : ''); ?>" id="collapseExamplePages">
                    <li class="<?php echo e(@$activeSubMenu == 'article-category' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('article-category.index')); ?>"><i class="fa fa-fw fa-tags"></i> Categories</a>
                    </li>
                    <li class="<?php echo e(@$activeSubMenu == 'articles' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('articles.index')); ?>">
                            <i class="fa fa-fw fa-newspaper-o"></i>
                            Articles</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Manage Articles">
                <a class="nav-link nav-link-collapse <?php echo e(@$activeMenu == 'media-buzz' ? '' : 'collapsed'); ?>" data-toggle="collapse" href="#collapseMediaBuzzPages" data-parent="#exampleAccordion" <?php echo e(@$activeMenu == 'media-buzz' ? 'aria-expanded="true"' : ''); ?>>
                    <i class="fa fa-fw fa-video-camera"></i>
                    <span class="nav-link-text">Media Buzz Winners</span>
                </a>
                <ul class="sidenav-second-level collapse <?php echo e(@$activeMenu == 'media-buzz' ? 'show' : ''); ?>" id="collapseMediaBuzzPages">
                    <li class="<?php echo e(@$activeSubMenu == 'media-buzz-player-history' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('media-buzz-winner-player.index')); ?>"><i class="fa fa-fw fa-history"></i> Player History</a>
                    </li>
                    <li class="<?php echo e(@$activeSubMenu == 'media-buzz-team-history' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('media-buzz-winner-team.index')); ?>">
                            <i class="fa fa-fw fa-history"></i>
                            Team History</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Performance Buzz">
                <a class="nav-link nav-link-collapse <?php echo e(@$activeMenu == 'performance-buzz' ? '' : 'collapsed'); ?>" data-toggle="collapse" href="#collapsePerformanceBuzzPages" data-parent="#exampleAccordion" <?php echo e(@$activeMenu == 'performance-buzz' ? 'aria-expanded="true"' : ''); ?>>
                    <i class="fa fa-fw fa-video-camera"></i>
                    <span class="nav-link-text">Performance Buzz</span>
                </a>
                <ul class="sidenav-second-level collapse <?php echo e(@$activeMenu == 'performance-buzz' ? 'show' : ''); ?>" id="collapsePerformanceBuzzPages">
                    <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-player-history' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('performance-buzz-winner-player.index')); ?>"><i class="fa fa-fw fa-history"></i> Player History</a>
                    </li>
                    <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-team-history' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('performance-buzz-winner-team.index')); ?>">
                            <i class="fa fa-fw fa-history"></i>
                            Team History</a>
                    </li>
                    <li class="<?php echo e(@$activeSubMenu == 'performance_buzz_files_list' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('performance_buzz_files_list')); ?>">
                            <i class="fa fa-fw fa-history"></i>
                            Add PB Data File</a>
                    </li>
                    <li class="<?php echo e(@$activeSubMenu == 'performance-buzz-datapoints' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('performance-buzz.index')); ?>">
                            <i class="fa fa-fw fa-history"></i>
                            Performance Buzz History</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item <?php echo e(@$activeMenu == 'cahsback-emails' ? 'active' : ''); ?>" data-toggle="tooltip" data-placement="right" title="Cashback Offer Emails">
                <a class="nav-link" href="<?php echo e(route('cashback-offer.index')); ?>">
                    <i class="fa fa-fw fa-envelope"></i>
                    <span class="nav-link-text">Cashback Offer Emails</span>
                </a>
            </li>
           <li class="nav-item <?php echo e(@$activeMenu == 'contactus-emails' ? 'active' : ''); ?>" data-toggle="tooltip" data-placement="right" title="Contact Emails">
                <a class="nav-link" href="<?php echo e(route('contactus-emails.index')); ?>">
                    <i class="fa fa-fw fa-envelope"></i>
                    <span class="nav-link-text">ContactUs Emails</span>
                </a>
            </li>
            <li class="nav-item <?php echo e(@$activeMenu == 'pages' ? 'active' : ''); ?>" data-toggle="tooltip" data-placement="right" title="Pages">
                <a class="nav-link" href="<?php echo e(route('pages.index')); ?>">
                    <i class="fa fa-fw fa-navicon"></i>
                    <span class="nav-link-text">Pages</span>
                </a>
            </li>
            <li class="nav-item <?php echo e(@$activeMenu == 'spreadsheets' ? 'active' : ''); ?>" data-toggle="tooltip" data-placement="right" title="Spreadsheets">
                <a class="nav-link" href="<?php echo e(route('spreadsheets.index')); ?>">
                    <i class="fa fa-fw fa-navicon"></i>
                    <span class="nav-link-text">Spreadsheets</span>
                </a>
            </li>
            <li class="nav-item <?php echo e(@$activeMenu == 'users' ? 'active' : ''); ?>" data-toggle="tooltip" data-placement="right" title="Users">
                <a class="nav-link" href="<?php echo e(route('users')); ?>">
                    <i class="fa fa-fw fa-users"></i>
                    <span class="nav-link-text">Users</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="SportMonks Data">
                <a class="nav-link nav-link-collapse <?php echo e(@$activeMenu == 'games' ? '' : 'collapsed'); ?>" data-toggle="collapse" href="#collapseSeasonsPages">
                    <i class="fa fa-fw fa-newspaper-o"></i>
                    <span class="nav-link-text">SportMonks Data</span>
                </a>
                <ul class="sidenav-second-level collapse <?php echo e(@$activeMenu == 'games' ? 'show' : ''); ?>" id="collapseSeasonsPages">
                    <li class="<?php echo e(@$activeSubMenu == 'seasons' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('seasons')); ?>"><i class="fa fa-fw fa-tags"></i> Seasons</a>
                    </li>            
                    <li class="<?php echo e(@$activeSubMenu == 'fixtures' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('fixtures')); ?>"><i class="fa fa-fw fa-tags"></i> Fixtures</a>
                    </li>            
                    <li class="<?php echo e(@$activeSubMenu == 'fetch-api-data' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('sportmonk.fetch.data')); ?>"><i class="fa fa-fw fa-tags"></i> Sports Monk Api</a>
                    </li>            
                    <li class="<?php echo e(@$activeSubMenu == 'teams' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('teams')); ?>"><i class="fa fa-fw fa-tags"></i> Teams</a>
                    </li>   
                    <li class="<?php echo e(@$activeSubMenu == 'day_and_dividends_list' ? 'active' : ''); ?>">
                        <a href="<?php echo e(route('day_and_dividends_list')); ?>"><i class="fa fa-fw fa-tags"></i> Days and Dividends</a>
                    </li>       
                </ul>
            </li>
            <li class="nav-item <?php echo e(@$activeMenu == 'settings' ? 'active' : ''); ?>" data-toggle="tooltip" data-placement="right" title="Settings">
                <a class="nav-link" href="<?php echo e(route('settings')); ?>">
                    <i class="fa fa-fw fa-cog"></i>
                    <span class="nav-link-text">Settings</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('change-password')); ?>">
                    <i class="fa fa-fw fa-cog"></i>Reset Password</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-fw fa-sign-out"></i>Logout</a>
            </li>
        </ul>
    </div>
</nav>