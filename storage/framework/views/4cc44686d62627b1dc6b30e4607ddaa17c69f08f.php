<?php $__env->startSection('content'); ?>

<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2><?php echo e(Request::route()->getName() == 'articles.category' ? @$categoryDetail->title : 'Articles'); ?></h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>

            <?php if(Request::route()->getName() == 'articles.category'): ?>
            <li><a href="<?php echo e(url()->route('articles')); ?>">Articles</a></li>
            <li><?php echo e(@$categoryDetail->title); ?></li>
            <?php else: ?>
            <li>Articles</li>
            <?php endif; ?>

        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->
<div class="overlay-dark theme-padding parallax-window" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?php echo e(asset('assets/front/images/inner-banners/Wembley_Warm_Up_1920x1280.jpg')); ?>">
</div>

<!-- Main Content -->
<main class="main-content">

    <!-- Blog -->
    <div class="theme-padding white-bg">
        <div class="container">
            <div class="row">

                <!-- Blog Content -->
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 r-full-width"> 
                    <?php if(@Request::has('keyword')): ?>
                    <h2>Search results for: <?php echo e(@Request::get('keyword')); ?></h2>
                    <?php endif; ?>
                    <!-- Blog Grid View -->
                    <div class="blog-grid-view">
                        <!-- <div class="row"> -->
                            <?php if(@$articles->count() > 0): ?>
                            <div id="articles_list">
                                <?php $__currentLoopData = @$articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <!-- Post Img -->
                                <div class="upcoming-fixture row articles-fixture"> 
                                    <p class="article-date"><strong>Posted on <?php echo e(date('d F Y',strtotime(@$article->date))); ?></strong></p>
                                    <div class="col-lg-6 col-xs-12">

                                        <div class="news-post-holder">                                            
                                            <!-- Widget -->
                                            <div class="news-post-widget">
                                                <a href="<?php echo e(route('article.detail',[@$article->slug])); ?>">
                                                    <img
                                                        <?php if(file_exists('uploads/articles/' . @$article->image_path) &&  @$article->image_path != ''): ?>
                                                        src="<?php echo e(asset('uploads/articles/'.@$article->image_path)); ?>"
                                                        <?php else: ?>
                                                        src="<?php echo e(asset('assets/images/no-image.jpeg')); ?>"
                                                        <?php endif; ?>  
                                                        alt="">  
                                                </a>
                                            </div>
                                            <!-- Widget -->                                                   

                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-xs-12">
                                        <div class="news-post-holder">

                                            <h2><a href="<?php echo e(route('article.detail',[@$article->slug])); ?>">
                                                    <?php if(strlen(@$article->title) >= 40): ?> 
                                                    <?php echo substr(@$article->title,0,39); ?>..
                                                    <?php else: ?>
                                                    <?php echo @$article->title; ?>

                                                    <?php endif; ?>
                                                </a></h2>
                                            <div class="article-content">
                                                <?php
                                                $text = strip_tags(@$article->content);
                                                $this->orginal_content_count = str_word_count($text);
                                                ?>

                                                <?php if(strlen(@$text) >= 360): ?> 
                                                <?php echo substr(@$text,0,359); ?>....<p class="article_read_more"><strong><a href="<?php echo e(route('article.detail',[@$article->slug])); ?>">Read More</a></strong></p>
                                                <?php else: ?>
                                                <?php echo @$text; ?>

                                                <?php endif; ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- Post Img -->
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                            <a class="btn red-btn load_all_articles_btn" href="<?php echo e(route('all.articles')); ?>">See More</a>
                            <?php else: ?>
                            <h4>Sorry no articles found..!</h4>
                            <?php endif; ?>

                        <!-- </div> -->
                    </div>
                    <!-- Blog Grid View -->

                </div>
                <!-- Blog Content -->

                <!-- Aside -->
                <?php echo $__env->make('front.articles.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Aside -->

            </div>
        </div>
    </div>
    <!-- Blog -->

</main>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_js'); ?>
<script type="text/javascript">

    $(document).on("click", ".articles_search", function () {
        $("#articles_search_form").submit();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>