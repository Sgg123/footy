

<?php $__env->startSection('content'); ?>

<!-- Page Heading & Breadcrumbs  -->
<div class="page-heading-breadcrumbs">
    <div class="container">
        <h2>Fixtures</h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Fixture</li>
        </ul>
    </div>
</div>
<!-- Page Heading & Breadcrumbs  -->

<!-- Main Content -->
<main class="main-content"> 
    <!-- Upcoming match slider -->
    <?php echo $__env->make('front.common.upcoming-match-slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Upcoming match slider -->

    <!-- Match -->
    <div class="theme-padding white-bg">
        <?php $updatedmatches = json_decode(json_encode($updatedmatches), True); ?>
        <div class="container">
            <div class="row">

                <div class="matches-shedule-holder">                
                    <!-- match Contenet -->
                    <div class="">
                        <?php if(@$fixturesCount != 0): ?>
                        <!-- Page Heading banner -->

                        <div class="container">
                            <div class="team_section_main">
                                <div class="col-sm-8 team-ad-set">
                                    <div class="row">
                                        <div class="col-sm-3">
                                              <?php 
                                        $position = ""; 
                                        $topPlayer = "";
                                        $title = "";
                                        ?>

                                        <?php if($totalMatches >= 1 && $totalMatches <= 4): ?>                                             
                                        <?php 
                                        $title = "Single Performance";
                                        $position = "2p"; 
                                        $topPlayer = "1p";
                                        $colorCode = "#CF5A0A";
                                        ?>
                                        <?php elseif($totalMatches >= 5 && $totalMatches <= 14): ?>

                                        <?php 
                                        $title = "Double Performance";
                                        $position = "3p"; 
                                        $topPlayer = "2p";
                                        $colorCode = "#8C909C";
                                        ?>

                                        <?php elseif($totalMatches >= 15): ?>

                                        <?php
                                        $title = "Triple Performance";
                                        $position = "5p"; 
                                        $topPlayer = "2p";
                                        $colorCode = "#DDAB27";
                                        ?>

                                        <?php else: ?>

                                        <?php
                                        $title = "Triple Media";
                                        $colorCode = "#439F05";                                        
                                        ?>
                                        <?php endif; ?>
                                        <?php $__currentLoopData = $updatedmatches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $umatch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(date('F d Y',strtotime($startDate) ) ==  date('F d Y', strtotime($umatch['date']))): ?>

                                                <?php if($umatch['type_of_day'] == 1): ?> 
                                                    <?php 
                                                        $title = "Single Performance";
                                                        $position = "2p"; 
                                                        $topPlayer = "1p";
                                                        $colorCode = "#CF5A0A";
                                                    ?>
                                                <?php endif; ?>
                                                <?php if($umatch['type_of_day'] == 2): ?>
                                                <?php 
                                                    $title = "Double Performance";
                                                    $position = "3p"; 
                                                    $topPlayer = "2p";
                                                    $colorCode = "#8C909C";
                                                ?>
                                                <?php endif; ?>
                                                <?php if($umatch['type_of_day'] == 3): ?>
                                                <?php
                                                    $title = "Triple Performance";
                                                    $position = "5p"; 
                                                    $topPlayer = "2p";
                                                    $colorCode = "#DDAB27";
                                                ?>
                                                <?php endif; ?>
                                                <?php if($umatch['type_of_day'] == 4): ?>
                                                 <?php
                                                    $title = "Triple Performance";
                                                    $position = "5p"; 
                                                    $topPlayer = "2p";
                                                    $colorCode = "#DDAB27";
                                                 ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <div class="triple_box_media" style="background-color: <?php echo e($colorCode); ?>">
                                            <?php echo e($title); ?> <?php echo e(date('F d Y',strtotime($startDate) )); ?>

                                        </div>        

                                        </div>
                                        <div class="col-sm-3">
                                            <div class="box_top_player">
                                            Top Player<br><span class="top-player"><?php echo e($topPlayer); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                             <div class="box_position">
                                            Positional<br><span class="positional"><?php echo e($position); ?> </span>            
                                            </div>
                                        </div>
                                       <div class="col-sm-3">
                                            <div class="box_fixtures">
                                            <?php echo e(@$fixturesCount); ?> <?php echo e(@$fixturesCount > 1 ? 'Fixtures' : 'Fixture'); ?><br><?php echo e($totalPlayers); ?> Players             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 sec-team-ad">
                                    <div class="row">
                                      <div class="col-sm-4 v-t-play">
                                        <div class="view_team" id="vteam" value="show">
                                            VIEW TEAM PLAYERS             
                                        </div>
                                      </div>
                                      <div class="col-sm-4 v-t-play">
                                         <div class="view_form" id="vfteam" value="show">
                                            VIEW FORM PLAYERS            
                                        </div>
                                      </div>
                                      <div class="col-sm-4 v-t-play">
                                        <div class="view_form pb_top" id="vallpl" value="show">
                                            VIEW All PLAYERS            
                                        </div>
                                      </div>  
                                    </div> 
                                </div>
                                <div class="row">
                                </div>
                            </div>   
                        </div>
                        <!-- Page Heading banner -->
                        <div class="col-lg-2 leagues-list">
                            <!-- Matches Dates Shedule -->
                            <div class="matches-dates-shedule">
                                <?php if(@$fixturesCount != 0): ?>
                                <table>
                                    <?php $__currentLoopData = $fixtures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fixture): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td data-league_id="<?php echo e($fixture->league_id); ?>" class="leagues"><?php echo e($fixture->league); ?></td>                                       
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </table>                           
                                <?php else: ?>
                                <h4>Sorry no Matches found..!</h4>
                                <?php endif; ?>                           
                            </div>
                            <!-- Matches Dates Shedule -->                            
                        </div>
                        <div class="col-lg-5 list-player" id="fixtures-data">
                            <?php echo $__env->make('front.fixtures.fixtures-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                        <div class="col-lg-5 col-lg-offset-1" id="data">
                            <div class="col-lg-12">
                                <?php echo $__env->make('front.fixtures.team-players-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                            <div class="col-lg-12">
                                <?php echo $__env->make('front.fixtures.all-players', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                        </div>
                        <div class="col-lg-5 col-lg-offset-1" id="data_all">
                            <div class="col-lg-12">
                                <?php echo $__env->make('front.fixtures.all-players', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                        </div>
                        <!-- match Contenet -->
                       
                        <?php endif; ?>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- Match -->

</main>
<?php if(\Auth::user()->isPremium()): ?>
<div class="modal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg new-modal-dialog-v" role="document">
        <div class="modal-content bg-set-t">
            <div class="modal-header new-edit-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="playerdata">
                <div id="fixturedeta">

  <div>

            </div>

        </div>
    </div>
</div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_js'); ?>
<script type="text/javascript">
    $(document).on("click", ".view-player", function(){
    $id = $(this).attr("data-id");
    $teamid = $(this).attr("data-teamid");
    $("#exampleModalCenter").modal();
    $.ajax({
    url: "<?php echo e(route('viewplayer')); ?>?id=" + $id,
            success: function (response) {
            $("#playerdata").html(response);
            $('#id2').hide();
            }
    });
    })
    $(document).on("click", "#idlast", function(){
    $('#id1').show();
    $(this).addClass('pb_top');
    $('#idtop').removeClass('pb_top');
    $('#id2').hide();
    });
    $(document).on("click", "#idtop", function(){
    $('#id2').show();
    $(this).addClass('pb_top');
    $('#idlast').removeClass('pb_top');
    /*$('#idtop').css('background-color', '#192851');
     $('#idlast').css('background-color', '#808080');*/
    $('#id1').hide();
    });
    $(document).on("click", "#vfteam", function(){
        $('#view_form_players').show();
        $(this).addClass('pb_top');
        $('#vteam').removeClass('pb_top');
        $('#vallpl').removeClass('pb_top');
        $('#view_team_players').hide();
        $("#data_all").hide();
    });
    $(document).on("click", "#vteam", function(){
        $('#view_team_players').show();
        $(this).addClass('pb_top');
        $('#vfteam').removeClass('pb_top');
        $('#vallpl').removeClass('pb_top');
        $('#view_form_players').hide();
        $("#data_all").hide();
    });
    $(document).on("click", "#vallpl", function(){
        $('#view_team_players').hide();
        $('#view_form_players').hide();
        $('#data_all').show();
        $('#vteam').removeClass('pb_top');
        $('#vfteam').removeClass('pb_top');
        $(this).addClass('pb_top');
    });
    $(document).ready(function(){ 
     $load = $('#vallpl'); 
        $(document).on('click', '.show-filter', function(){
            $("#div1").fadeToggle("slow");  
        });
        $("button").click(function(){  
        });
    });



</script>
<script type="text/javascript">
    $("#data").hide();
    // GET FIXTURES LIST
    function getFixtures($date, $league_id) {
    $.ajax({
    url: "<?php echo e(route('get_fixtures_list')); ?>?date=" + $date + "&league_id=" + $league_id,
            success: function (response) {
            $("#data").hide();
            $("#fixtures-data").html(response);
//                var localteam_id = $("#localteam_id").val();
//                var visitorteam_id = $("#visitorteam_id").val();               
//                getPlayers(localteam_id, visitorteam_id);
            }
    });
    }

    $(document).on('click', '.leagues', function () {
    var league_id = $(this).attr("data-league_id");
    var date = "<?php echo e($startDate); ?>";
    getFixtures(date, league_id);
    });
    $(function () {
    var league_id = <?php echo e(@$fixtures[0]['league_id']); ?>;
    var date = "<?php echo e($startDate); ?>";
    getFixtures(date, league_id);
    });
    $(document).on('click', '.get-players-list', function () {
    $localteam_id = $(this).attr("data-localteam_id");
    $visitorteam_id = $(this).attr("data-visitorteam_id");
    getPlayers($localteam_id, $visitorteam_id);
    });
    function getPlayers($localteam_id, $visitorteam_id) {
    $.ajax({
    url: "<?php echo e(route('get_team_players')); ?>?localteam_id=" + $localteam_id + "&visitorteam_id=" + $visitorteam_id,
            success: function (response) {
            $("#data_all").hide();
            $('#vteam').addClass('pb_top');
            $('#vfteam').removeClass('pb_top');
            $('#vallpl').removeClass('pb_top');
            $("#data").show();
            $("#data").html(response);
            }
    });
    }
    function getfinalplayerfixturedata(year){
        $.ajax({
            url: "<?php echo e(route('getfinalplayerfixturedata')); ?>?player_id=" + $id + "&teamid=" + $teamid + "&year=" + year,
            success: function (response) {
                $('#player_fixture_score').html(response);
            }
        });
    }

    function getfixturemonthforyear(year){
        $.ajax({
            url: "<?php echo e(route('getfixturemonthforyear')); ?>?player_id=" + $id + "&year=" + year,
            success: function (response) {
                var data = JSON.parse(response);
                var html = '<option>Select</option>';
                // console.log(response);
                var arrayLength = data.length;
                for (var i = 0; i < arrayLength; i++) {
                    html+= '<option value="'+data[i]+'">'+getMonthName(data[i]-1)+'</option>';
                }
                $('#season_month').html(html);
            }
        });
    }

    function getfixtureoppositeteam(month){
        var year = $('#season_year').val();
        $.ajax({
            url: "<?php echo e(route('getfixtureoppositeteam')); ?>?player_id="+$id+"&teamid=" + $teamid + "&year=" + year + "&month="+month,
            success: function (response) {
                var data = JSON.parse(response);
                var html = '<option>Select</option>';
                var arrayLength = data.length;
                for (var i = 0; i < arrayLength; i++) {
                    html+= '<option value="'+data[i].starting_at+'" data-id="'+data[i].teamid+'" data-date="'+data[i].starting_at+'">'+data[i].teamname+' ('+data[i].played_at+')</option>';
                }
                $('#season_fixture').html(html);
            }
        });
    }

    function getfinalplayerfixturescore(date){
        var year = $('#season_year').val(), month = $('#season_month').val();
        $.ajax({
            url: "<?php echo e(route('getfinalplayerfixturescore')); ?>?player_id="+$id+"&date="+date,
            success: function (response) {

                $('#player_fixture_score').html(response.score).css('background', response.color);
                $('#player_fixture_score').css('color', response.fontcolor);
            }
        });
    }
    getMonthName = function (v) {
        var n = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return n[v]
    }
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_css'); ?>
<style type="text/css">
    #data_all{
        background: #d6d7d9;
        font-weight: 700;
        width: 39.5%;
        padding: 0;
        margin-left: 50px;
    }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>